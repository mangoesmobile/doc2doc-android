/**
 * 
 */
package com.mangoesmobile.doc2doc.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.foound.widget.AmazingAdapter;
import com.mangoesmobile.doc2doc.R;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class DraftAdapter extends AmazingAdapter implements Filterable {
	Bundle data;
	ArrayList<String> threadId = new ArrayList<String>(); // keeps track of id of the threads which are
								// added to data
	ArrayList<String> originalData = new ArrayList<String>();
	Filter filter;
	Activity context;

	private Object lock = new Object(); // used for performing synchoronized
										// filtering

	public DraftAdapter(Activity context) {
		this.filter = new BoardFilter();
		this.context = context;
	}

	public void setData(Bundle bundle) {
		Log.d("draft-adapter", "Bundle size: " + bundle.size());
		threadId = new ArrayList<String>();
		data = new Bundle();
		for (int i = bundle.size() - 1; i >= 0; i--) {
			Bundle content = bundle.getBundle(String.valueOf(i));
			data.putBundle(content.getString("id"), content);
			threadId.add(content.getString("id"));
			
		}
//		updateCache();
		originalData = threadId;
		notifyDataSetChanged();
		notifyNoMorePages();
	}
	
	public void removeItem(String id) {
		data.remove(id);
		threadId.remove(id);
		notifyDataSetChanged();
	}
	
//	private void updateCache() {
//		BoardCacheManager manager = new BoardCacheManager(context);
//		manager.open();
//		manager.deleteAllContents();
//		for(int i = 0; i < threadId.size(); i++) {
//			Bundle content = data.getBundle(threadId.get(i));
//			manager.insertContent(content.getString("id"), content.getString("subject"), content.getString("contents"), content.getString("timestamp"), content.getString("user_id"), content.getString("username"), content.getString("total_num_of_replies"), content.getString("reply_contents"), content.getString("reply_author"), content.getString("reply_time"), content.containsKey("attached_files") ? content.getString("attached_files") : "", content.containsKey("tagged_keywords") ? content.getString("tagged_keywords") : "", content.containsKey("tagged_users") ? content.getString("tagged_users") : "");
//		}
//		manager.close();
//	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return threadId.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.getBundle(threadId.get(arg0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#onNextPageRequested(int)
	 */
	@Override
	protected void onNextPageRequested(int page) {
		// TODO Auto-generated method stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.foound.widget.AmazingAdapter#bindSectionHeader(android.view.View,
	 * int, boolean)
	 */
	@Override
	protected void bindSectionHeader(View view, int position,
			boolean displaySectionHeader) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getAmazingView(int,
	 * android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getAmazingView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		View row = convertView;
		if (row == null) {
			row = context.getLayoutInflater().inflate(R.layout.row_showthread,
					null);
			holder = new ViewHolder();
			holder.imgUserImage = (ImageView) row
					.findViewById(R.id.img_showthread_user);
			holder.progUserImage = (ProgressBar) row
					.findViewById(R.id.prog_showthread_userimage);
			holder.lblSubject = (TextView) row
					.findViewById(R.id.lbl_showthread_userreply);
			holder.lblContents = (TextView) row
					.findViewById(R.id.lbl_showthread_replyauthor);
			holder.layoutButtons = (LinearLayout) row
					.findViewById(R.id.layout_showthread_replyattachment$location);
			holder.lblTimestamp = (TextView) row.findViewById(R.id.lbl_showthread_timestamp);
			holder.imgUserImage.setVisibility(View.GONE);
			holder.progUserImage.setVisibility(View.GONE);
			holder.layoutButtons.setVisibility(View.GONE);
			holder.lblTimestamp.setVisibility(View.VISIBLE);
			row.setTag(holder);
		} else {
			holder = (ViewHolder) row.getTag();
		}
		Bundle content = (Bundle) getItem(position);
		holder.lblSubject.setText(content.getString("subject"));
		holder.lblContents.setText(content.getString("contents"));
		holder.lblTimestamp.setText(content.getString("timestamp"));
		return row;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.foound.widget.AmazingAdapter#configurePinnedHeader(android.view.View,
	 * int, int)
	 */
	@Override
	public void configurePinnedHeader(View header, int position, int alpha) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getPositionForSection(int)
	 */
	@Override
	public int getPositionForSection(int section) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getSectionForPosition(int)
	 */
	@Override
	public int getSectionForPosition(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getSections()
	 */
	@Override
	public Object[] getSections() {
		// TODO Auto-generated method stub
		return null;
	}

	static class ViewHolder {
		public ProgressBar progUserImage;
		public ImageView imgUserImage;
		public TextView lblSubject;
		public TextView lblContents;
		public LinearLayout layoutButtons;
		public TextView lblTimestamp;
	}

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		if (filter == null) {
			filter = new BoardFilter();
		}
		return filter;
	}

	private class BoardFilter extends Filter {

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			// TODO Auto-generated method stub
			String query = constraint.toString().toLowerCase();
			FilterResults results = new FilterResults();
			ArrayList<String> temp;
			synchronized (lock) {
				temp = new ArrayList<String>(originalData);
			}
			if (query == null || query.length() == 0) {
				synchronized (lock) {
					results.values = originalData;
					results.count = originalData.size();
				}
			} else {
				ArrayList<String> filtered = new ArrayList<String>();
				for (int i = 0; i < temp.size(); i++) {
					String id = temp.get(i);
					Bundle content = data.getBundle(id);
					if (content.getString("subject").toLowerCase()
							.contains(query)
							|| content.getString("contents").toLowerCase()
									.contains(query)) {
						filtered.add(id);
					} else if (content.containsKey("tagged_keywords")) {
						if (content.getString("tagged_keywords").toLowerCase()
								.contains(query))
							filtered.add(id);
					} else if (content.containsKey("tagged_users")) {
						if (content.getString("tagged_users").toLowerCase()
								.contains(query))
							filtered.add(id);
					}
				}
				results.values = filtered;
				results.count = filtered.size();
			}
			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			// TODO Auto-generated method stub
			threadId = (ArrayList<String>) results.values;
			notifyDataSetChanged();
		}

	}

}

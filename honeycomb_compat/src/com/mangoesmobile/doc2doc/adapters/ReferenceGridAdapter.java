/**
 * 
 */
package com.mangoesmobile.doc2doc.adapters;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.contents.FileExplore;
import com.mangoesmobile.doc2doc.contents.OtherGuideline;
import com.mangoesmobile.doc2doc.contents.WebReferences;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class ReferenceGridAdapter extends BaseAdapter {

	GoogleAnalyticsTracker tracker;
	
	FragmentActivity context;
	int tag;
	Bundle data;
	LayoutInflater inflater;

	public ReferenceGridAdapter(FragmentActivity context, int tag) {
		tracker = GoogleAnalyticsTracker.getInstance();
		this.context = context;
		this.tag = tag;
		data = new Bundle();
		inflater = context.getLayoutInflater();
	}

	public void setData(Bundle data) {
		this.data = data;
		notifyDataSetChanged();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Bundle getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.getBundle(String.valueOf(arg0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		View row = arg1;
		if (row == null) {
			row = inflater.inflate(
					R.layout.reference_item, null);
			holder = new ViewHolder();
			holder.btnItem = (ImageButton) row.findViewById(R.id.btn_item);
			row.setTag(holder);
		} else {
			holder = (ViewHolder) row.getTag();
		}
		Bundle content = getItem(arg0);
		holder.btnItem.setImageResource(content.getInt("icon"));
		holder.btnItem.setOnClickListener(new ItemClickListener(arg0));
		return row;
	}

	static class ViewHolder {
		public ImageButton btnItem;
	}
	


	class ItemClickListener implements OnClickListener {

		int index;
		
		public ItemClickListener(int index) {
			// TODO Auto-generated constructor stub
			this.index = index;
		}

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			Bundle args = new Bundle();
			Fragment f;
			FragmentTransaction ft;
			switch (index) {
			case 0:
//				f = TreatmentGuideline.newInstance(tag, null);
//				ft = context.getSupportFragmentManager().beginTransaction();
//				ft.hide(D2DTabHost.history.get(tag).peek());
//				ft.add(tag, f);
//				ft.commit();
//				D2DTabHost.history.get(tag).push(f);
//				intent = new Intent(context,
//						TreatmentGuideline.class);
//				parent.startChildActivity("treatment_guideline", intent);
				Intent intent = new Intent(context, FileExplore.class);
				intent.putExtra("file_browser", false);
				context.startActivity(intent);
				tracker.trackEvent("Click", "Button", "Show Botswana Treatment Guideline", 0);
				break;
			case 1:
				args.putString("url", "http://m.medlineplus.gov/");
				f = WebReferences.newInstance(tag, args);
				ft = context.getSupportFragmentManager().beginTransaction();
				ft.hide(D2DTabHost.history.get(tag).peek());
				ft.add(tag, f);
				ft.commit();
				D2DTabHost.history.get(tag).push(f);
//				intent = new Intent(context, WebReferences.class);
//				intent.putExtra("url", "http://m.medlineplus.gov/");
//				parent.startChildActivity("web_reference", intent);
				tracker.trackEvent("Click", "Button", "Medline", 0);
				break;
			case 2:
				args.putString("url", "http://www.ncbi.nlm.nih.gov/m/pubmed/");
				f = WebReferences.newInstance(tag, args);
				ft = context.getSupportFragmentManager().beginTransaction();
				ft.hide(D2DTabHost.history.get(tag).peek());
				ft.add(tag, f);
				ft.commit();
				D2DTabHost.history.get(tag).push(f);
//				intent = new Intent(context, WebReferences.class);
//				intent.putExtra("url", "http://www.ncbi.nlm.nih.gov/m/pubmed/");
//				parent.startChildActivity("web_reference", intent);
				tracker.trackEvent("Click", "Button", "Pubmed", 0);
				break;
			case 3:
				args.putString("url", "http://dailymed.nlm.nih.gov/dailymed/mobile/index.cfm");
				f = WebReferences.newInstance(tag, args);
				ft = context.getSupportFragmentManager().beginTransaction();
				ft.hide(D2DTabHost.history.get(tag).peek());
				ft.add(tag, f);
				ft.commit();
				D2DTabHost.history.get(tag).push(f);
//				intent = new Intent(context, WebReferences.class);
//				intent.putExtra("url", "http://dailymed.nlm.nih.gov/dailymed/mobile/index.cfm");
//				parent.startChildActivity("web_reference", intent);
				tracker.trackEvent("Click", "Button", "Dailymed", 0);
				break;
			case 4:
				args.putString("url", "http://m.aidsinfo.nih.gov/");
				f = WebReferences.newInstance(tag, args);
				ft = context.getSupportFragmentManager().beginTransaction();
				ft.hide(D2DTabHost.history.get(tag).peek());
				ft.add(tag, f);
				ft.commit();
				D2DTabHost.history.get(tag).push(f);
//				intent = new Intent(context, WebReferences.class);
//				intent.putExtra("url", "http://m.aidsinfo.nih.gov/");
//				parent.startChildActivity("web_reference", intent);
				tracker.trackEvent("Click", "Button", "Aidsinfo", 0);
				break;
			case 5:
				f = OtherGuideline.newInstance(tag, null);
				ft = context.getSupportFragmentManager().beginTransaction();
				ft.hide(D2DTabHost.history.get(tag).peek());
				ft.add(tag, f);
				ft.commit();
				D2DTabHost.history.get(tag).push(f);
//				intent = new Intent(context,
//						OtherGuideline.class);
//				parent.startChildActivity("other_guideline", intent);
				tracker.trackEvent("Click", "Button", "Show Other Guideline", 0);
				break;				
			default:
				break;
			}
		}

	}

}

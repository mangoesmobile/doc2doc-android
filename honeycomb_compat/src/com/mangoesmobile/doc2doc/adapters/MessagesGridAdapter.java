/**
 * 
 */
package com.mangoesmobile.doc2doc.adapters;

import com.mangoesmobile.doc2doc.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class MessagesGridAdapter extends BaseAdapter {

	Activity context;
	Bundle data;
	LayoutInflater inflater;

	public MessagesGridAdapter(Activity context) {
		this.context = context;
		data = new Bundle();
		inflater = context.getLayoutInflater();
	}

	public void setData(Bundle data) {
		this.data = data;
		notifyDataSetChanged();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Bundle getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.getBundle(String.valueOf(arg0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		View row = arg1;
		if (row == null) {
			row = inflater.inflate(
					R.layout.messagecenter_item, null);
			holder = new ViewHolder();
			holder.imgIcon = (ImageView) row
					.findViewById(R.id.img_msgitem_icon);
			holder.lblTitle = (TextView) row
					.findViewById(R.id.lbl_msgitem_title);
			holder.lblNotification = (TextView) row
					.findViewById(R.id.lbl_msgitem_notification);
			row.setTag(holder);
		} else {
			holder = (ViewHolder) row.getTag();
		}
		Bundle content = getItem(arg0);
		holder.lblNotification.setBackgroundResource(content.getInt("badge"));
		holder.imgIcon.setImageResource(content.getInt("icon"));
		holder.lblTitle.setText(content.getString("title"));
		holder.lblNotification.setText(content.getString("notifications"));
		if(content.getString("notifications") != null && !content.getString("notifications").equals("0")) {
			holder.lblNotification.setVisibility(View.VISIBLE);
		} else {
			holder.lblNotification.setVisibility(View.INVISIBLE);
		}
		return row;
	}

	static class ViewHolder {
		public ImageView imgIcon;
		public TextView lblTitle;
		public TextView lblNotification;
	}

}

package com.mangoesmobile.doc2doc.adapters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import android.widget.Toast;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class MapOverlay extends ItemizedOverlay<OverlayItem> {

	GoogleAnalyticsTracker tracker;
	
	private ArrayList<OverlayItem> mOverlays = new ArrayList<OverlayItem>();
	private Activity mContext;

	public MapOverlay(Drawable defaultMarker, Activity context) {
		super(boundCenterBottom(defaultMarker));
		// TODO Auto-generated constructor stub
		tracker = GoogleAnalyticsTracker.getInstance();
		mContext = context;
		populate();
	}

	@Override
	protected OverlayItem createItem(int i) {
		// TODO Auto-generated method stub
		return mOverlays.get(i);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return mOverlays.size();
	}

	public void addOverlay(OverlayItem overlay) {
		mOverlays.add(overlay);
		populate();
	}

	public boolean removeOverlay(int index) {
		try {
			mOverlays.remove(index);
			populate();
			return true;
		} catch (IndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	@Override
	protected boolean onTap(int index) {
		OverlayItem item = mOverlays.get(index);
		GeoPoint point = item.getPoint();
		double lat = point.getLatitudeE6() / 1E6;
		double lng = point.getLongitudeE6() / 1E6;
		Log.d("location", "location: " + lat + " " + lng);
		Geocoder gc = new Geocoder(mContext);
		try {
			List<Address> addresses = gc.getFromLocation(lat, lng, 1);
			StringBuilder sb = new StringBuilder();

			if (addresses.size() > 0) {
				Address address = addresses.get(0);

				for (int i = 0; i < address.getMaxAddressLineIndex(); i++)
					sb.append(address.getAddressLine(i)).append("\n");
				Log.d("city", address.getAdminArea());
				sb.append(address.getCountryName());
			}
			Builder builder = new AlertDialog.Builder(mContext);
			builder.setTitle(item.getSnippet());
			builder.setMessage(sb.toString());
			AlertDialog alert = builder.create();
			alert.setCanceledOnTouchOutside(true);
			alert.show();
			tracker.trackEvent("Click", "Pin", "Show User Address", 0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Toast.makeText(mContext, "Address could not be retrieved", Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
		return true;
	}

}

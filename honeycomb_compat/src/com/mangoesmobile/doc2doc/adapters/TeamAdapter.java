/**
 * 
 */
package com.mangoesmobile.doc2doc.adapters;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.foound.widget.AmazingAdapter;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.cache.TeamCacheManager;
import com.mangoesmobile.doc2doc.contents.NewMessage;
import com.mangoesmobile.doc2doc.contents.UserLocation;
import com.mangoesmobile.doc2doc.contents.Vcard;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.ImageLoader;
import com.mangoesmobile.doc2doc.util.PingDoctor;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class TeamAdapter extends AmazingAdapter implements Filterable {

	FragmentActivity context;
	int tag;
	final String[] orgs = { "BUP", "UB Residents", "UB Faculty",
			"UB Library", "mangoes" };
	Bundle data;
	Bundle original;
	Bundle flat;
	ArrayList<Integer> sections;
	LayoutInflater inflater;
	ImageLoader imageLoader;

	// int firstOne = 0;
	// boolean isScrolling;

	Filter filter;
	private Object lock = new Object(); // used for performing synchoronized
										// filtering
	GoogleAnalyticsTracker tracker;

	public TeamAdapter(FragmentActivity context, int tag) {
		this.context = context;
		this.tag = tag;
		this.data = new Bundle();
		this.original = new Bundle();
		this.flat = new Bundle();
		inflater = context.getLayoutInflater();
		imageLoader = new ImageLoader(context);
		tracker = GoogleAnalyticsTracker.getInstance();
	}

	public void setData(Bundle bundle) {
		Log.d("team-adapter", "Bundle size: " + bundle.size());

		data = new Bundle();
		for (int i = 0; i < orgs.length; i++) {
			data.putBundle(String.valueOf(i), new Bundle());
		}

		for (int i = 0; i < bundle.size(); i++) {
			Bundle content = bundle.getBundle(String.valueOf(i));
			for (int j = 0; j < orgs.length; j++) {
				if (content.getString("organization").equalsIgnoreCase(orgs[j])) {
					Bundle put = data.getBundle(String.valueOf(j));
					put.putBundle(String.valueOf(put.size()), content);
					break;
				}
			}
		}

		flattenData();
		updateCache();
		original.putAll(data);
		notifyDataSetChanged();
		notifyNoMorePages();

		// data = new Bundle();
		// userId = new ArrayList<String>();
		// for (int i = 0; i < bundle.size(); i++) {
		// Bundle content = bundle.getBundle(String.valueOf(i));
		// data.putBundle(content.getString("user_id"), content);
		// userId.add(content.getString("user_id"));
		// }
		// updateCache();
		// originalData = userId;
		// notifyDataSetChanged();
		// notifyNoMorePages();
	}

	private void updateCache() {
		// TeamCacheManager manager = new TeamCacheManager(context);
		// manager.open();
		// manager.deleteAllContents();
		// for (int i = 0; i < userId.size(); i++) {
		// Bundle content = data.getBundle(userId.get(i));
		// manager.insertContent(content.getString("user_id"),
		// content.getString("first_name"),
		// content.getString("last_name"), content.getString("title"),
		// content.getString("email"), content.getString("phone"),
		// content.getString("profile_pic_file_path"),
		// content.getString("organization"),
		// content.getString("specialization"),
		// content.getString("username"),
		// content.getString("availability_status"),
		// content.getString("latitude"),
		// content.getString("longitude"));
		// }
		// manager.close();
		TeamCacheManager manager = new TeamCacheManager(context);
		manager.open();
		manager.deleteAllContents();
		for (int i = 0; i < flat.size(); i++) {
			Bundle content = flat.getBundle(String.valueOf(i));
			manager.insertContent(content.getString("user_id"),
					content.getString("first_name"),
					content.getString("last_name"), content.getString("title"),
					content.getString("email"), content.getString("phone"),
					content.getString("profile_pic_file_path"),
					content.getString("organization"),
					content.getString("specialization"),
					content.getString("username"),
					content.getString("availability_status"),
					content.getString("latitude"),
					content.getString("longitude"),
					content.getString("base_location"));
		}
		manager.close();
	}

	private void flattenData() {
		flat = new Bundle();
		sections = new ArrayList<Integer>();
		int c = 0;
		for (int i = 0; i < data.size(); i++) {
			sections.add(c);
			// flat.putBundle(String.valueOf(c++), null);
			Bundle group = data.getBundle(String.valueOf(i));
			for (int j = 0; j < group.size(); j++) {
				flat.putBundle(String.valueOf(c++),
						group.getBundle(String.valueOf(j)));
			}
		}
	}

	public Bundle getData() {
		return flat;
	}

	// public ArrayList<String> getIds() {
	// return userId;
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return flat.size();
		// return userId.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Bundle getItem(int arg0) {
		// TODO Auto-generated method stub
		return flat.getBundle(String.valueOf(arg0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#onNextPageRequested(int)
	 */
	@Override
	protected void onNextPageRequested(int page) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.foound.widget.AmazingAdapter#bindSectionHeader(android.view.View,
	 * int, boolean)
	 */
	@Override
	protected void bindSectionHeader(View view, int position,
			boolean displaySectionHeader) {
		// TODO Auto-generated method stub
		if (displaySectionHeader) {
			view.findViewById(R.id.pinned_header).setVisibility(View.VISIBLE);
			TextView lSectionTitle = (TextView) view
					.findViewById(R.id.pinned_header);
			lSectionTitle
					.setText(getSections()[getSectionForPosition(position)]);
		} else {
			view.findViewById(R.id.pinned_header).setVisibility(View.GONE);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getAmazingView(int,
	 * android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getAmazingView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.row_teamlist, null);
			holder = new ViewHolder();
			holder.progUserImage = (ProgressBar) convertView
					.findViewById(R.id.prog_teamlist_userimage);
			holder.imgTeamPhoto = (ImageView) convertView
					.findViewById(R.id.img_teamlist_user);
			holder.imgTeamStatus = (ImageView) convertView
					.findViewById(R.id.img_teamlist_userstatus);
			holder.lblName = (TextView) convertView
					.findViewById(R.id.lbl_teamlist_name);
			holder.lblSpecialization = (TextView) convertView
					.findViewById(R.id.lbl_teamlist_specialization);
			holder.lblLocation = (TextView) convertView
					.findViewById(R.id.lbl_teamlist_location);
			holder.lblUserStatus = (TextView) convertView
					.findViewById(R.id.lbl_teamlist_userstatus);
			holder.btnMessage = (Button) convertView
					.findViewById(R.id.btn_teamlist_message);
			holder.btnPing = (Button) convertView
					.findViewById(R.id.btn_teamlist_ping);
			holder.btnLocation = (Button) convertView
					.findViewById(R.id.btn_teamlist_location);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		Bundle content = (Bundle) getItem(position);

		// Global.loadPhoto(context,
		// Global.url + content.getString("profile_pic_file_path"),
		// holder.imgTeamPhoto, holder.progUserImage);

		int status;
		String user_status = content.getString("availability_status");
		if (user_status.equals("online")) {
			status = R.drawable.bg_photo_online;
			holder.lblUserStatus.setTextColor(Color.rgb(0, 102, 153));
		} else if (content.getString("availability_status").equals("on call")) {
			status = R.drawable.bg_photo_oncall;
			holder.lblUserStatus.setTextColor(Color.rgb(101, 176, 73));
		} else {
			status = R.drawable.bg_photo_offline;
			holder.lblUserStatus.setTextColor(Color.DKGRAY);
		}
		holder.imgTeamStatus.setImageResource(status);
		holder.lblName.setText(content.getString("first_name") + " "
				+ content.getString("last_name"));
		holder.lblUserStatus.setText(user_status.equals("null") ? ""
				: user_status.toUpperCase());
		holder.lblSpecialization.setText(content.getString("specialization")
				.equals("null") ? "" : content.getString("specialization"));
		holder.lblLocation.setText(content.getString("organization").equals(
				"null") ? "" : content.getString("organization"));
		holder.btnMessage.setOnClickListener(new MessageClickListener(content
				.getString("user_id"), content.getString("first_name") + " "
				+ content.getString("last_name")));
		holder.btnPing.setOnClickListener(new PingClickListener(content
				.getString("user_id"), content.getString("first_name")
				.substring(0, 1) + ". " + content.getString("last_name")));
		holder.btnLocation.setOnClickListener(new LocationClickListener(content
				.getString("user_id"), content.getString("first_name") + " "
				+ content.getString("last_name"),
				content.getString("latitude"), content.getString("longitude")));
		holder.imgTeamPhoto.setTag(Global.url
				+ content.getString("profile_pic_file_path"));
		imageLoader.DisplayImage(
				Global.url + content.getString("profile_pic_file_path"),
				holder.imgTeamPhoto, holder.progUserImage);
		convertView.setOnClickListener(new OnItemClick(position));

		return convertView;
	}

	// @Override
	// public void onScroll(AbsListView view, int firstVisibleItem, int
	// visibleItemCount, int totalItemCount) {
	// super.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
	// firstOne = firstVisibleItem;
	// isScrolling = (firstVisibleItem != 0);
	// }
	//
	// @Override
	// public void onScrollStateChanged(AbsListView view, int scrollState) {
	// if (scrollState != 0) {
	// Log.d("scroll-state", "stopped");
	// return;
	// }
	// int number = firstOne;
	// for (int i = 0; i < view.getChildCount(); i++) {
	// final Object rowItem = view.getChildAt(i).getTag();
	// if (rowItem != null && rowItem instanceof ViewHolder) {
	// final ViewHolder holder = (ViewHolder) rowItem;
	// Bundle content = getItem(number);
	// Global.loadPhoto(context, Global.url +
	// content.getString("profile_pic_file_path"),
	// holder.imgTeamPhoto, holder.progUserImage);
	// }
	// number++;
	// }
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.foound.widget.AmazingAdapter#configurePinnedHeader(android.view.View,
	 * int, int)
	 */
	@Override
	public void configurePinnedHeader(View header, int position, int alpha) {
		// TODO Auto-generated method stub
		TextView lSectionHeader = (TextView) header;
		lSectionHeader.setText(getSections()[getSectionForPosition(position)]);
		// lSectionHeader.setBackgroundColor(alpha << 24 | (0x233659));
		lSectionHeader.setBackgroundResource(R.drawable.header);
		lSectionHeader.setTextColor(alpha << 24 | (0xffffff));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getPositionForSection(int)
	 */
	@Override
	public int getPositionForSection(int section) {
		// TODO Auto-generated method stub
		if (section < 0) section = 0;
		if (section >= sections.size()) section = sections.size() - 1;
		return sections.get(section);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getSectionForPosition(int)
	 */
	@Override
	public int getSectionForPosition(int position) {
		// TODO Auto-generated method stub
		for (int i = 0; i < sections.size(); i++) {
			if (position < sections.get(i)) {
				return i - 1;
			}
		}
		return sections.size() - 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getSections()
	 */
	@Override
	public String[] getSections() {
		// TODO Auto-generated method stub
		return orgs;
	}

	static class ViewHolder {
		public ProgressBar progUserImage;
		public ImageView imgTeamPhoto;
		public ImageView imgTeamStatus;
		public TextView lblName;
		public TextView lblSpecialization;
		public TextView lblLocation;
		public TextView lblUserStatus;
		public Button btnMessage;
		public Button btnPing;
		public Button btnLocation;
	}

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		if (filter == null) {
			filter = new TeamFilter();
		}
		return filter;
	}

	private class MessageClickListener implements OnClickListener {

		String userId;
		String name;

		public MessageClickListener(String userId, String name) {
			this.userId = userId;
			this.name = name;
		}

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			Bundle args = new Bundle();
			args.putBoolean("has_recipient", true);
			args.putString("tagged_users", userId);
			args.putString("tagged_user_names", name);
			NewMessage nm = NewMessage.newInstance(tag, args);
			FragmentTransaction ft = context.getSupportFragmentManager()
					.beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, nm);
			ft.commit();
			D2DTabHost.history.get(tag).push(nm);
			// Intent intent = new Intent(context, NewMessage.class);
			// intent.putExtra("has_recipient", true);
			// intent.putExtra("id", userId);
			// intent.putExtra("name", name);
			// ActivityGroupTeam parent = (ActivityGroupTeam)
			// context.getParent();
			// parent.startChildActivity("new_message", intent);
			tracker.trackEvent("Click", "Button", "Create New Message", 0);
		}

	}

	private class PingClickListener implements OnClickListener {

		String userId;
		String name;

		public PingClickListener(String userId, String name) {
			this.userId = userId;
			this.name = name;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			PingDoctor pd = new PingDoctor(context, userId, name);
			pd.ping();
			tracker.trackEvent("Click", "Button", "Ping User", 0);
		}

	}

	private class LocationClickListener implements OnClickListener {

		String userId;
		String name;
		String lat;
		String lng;

		public LocationClickListener(String userId, String name, String lat,
				String lng) {
			this.userId = userId;
			this.name = name;
			this.lat = lat;
			this.lng = lng;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent intent = new Intent(context, UserLocation.class);
			intent.putExtra("update_individual", true);
			intent.putExtra("user_id", userId);
			intent.putExtra("name", name);
			intent.putExtra("latitude", lat);
			intent.putExtra("longitude", lng);
			Log.d("lat", "lat: " + lat);
			Log.d("lng", "lng: " + lng);
			context.startActivity(intent);
			tracker.trackEvent("Click", "Button", "Show Teammate's Location", 0);
		}

	}

	class OnItemClick implements OnClickListener {

		int index;

		public OnItemClick(int index) {
			// TODO Auto-generated constructor stub
			this.index = index;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			// Log.d("team", "clicked");
			Bundle args = getItem(index);
			Vcard vc = Vcard.newInstance(tag, args);
			FragmentTransaction ft = context.getSupportFragmentManager()
					.beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, vc);
			ft.commit();
			D2DTabHost.history.get(tag).push(vc);
			// Bundle data = getItem(index);
			// ActivityGroupTeam parent = (ActivityGroupTeam)
			// context.getParent();
			// Intent intent = new Intent(context, Vcard.class);
			// intent.putExtra("data", data);
			// parent.startChildActivity("vcard", intent);
			tracker.trackEvent("Click", "ListItem", "Show Vcard", 0);
		}

	}

	private class TeamFilter extends Filter {

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			// TODO Auto-generated method stub
			String query = constraint.toString().toLowerCase();
			FilterResults results = new FilterResults();
			Bundle temp;
			synchronized (lock) {
				temp = new Bundle();
				temp.putAll(original);
			}
			if (query == null || query.length() == 0) {
				synchronized (lock) {
					results.values = original;
					results.count = original.size();
				}
			} else {
				Bundle filtered = new Bundle();
				for (int i = 0; i < orgs.length; i++) {
					filtered.putBundle(String.valueOf(i), new Bundle());
				}

				for (int i = 0; i < temp.size(); i++) {
					Bundle group = temp.getBundle(String.valueOf(i));
					for (int j = 0; j < group.size(); j++) {
						Bundle content = group.getBundle(String.valueOf(j));
						if (content.getString("title").toLowerCase()
								.contains(query)
								|| content.getString("first_name")
										.toLowerCase().contains(query)
								|| content.getString("last_name").toLowerCase()
										.contains(query)
								|| content.getString("email").toLowerCase()
										.contains(query)
								|| content.getString("phone").toLowerCase()
										.contains(query)
								|| content.getString("organization")
										.toLowerCase().contains(query)
								|| content.getString("specialization")
										.toLowerCase().contains(query)
								|| content.getString("username").toLowerCase()
										.contains(query)
								|| content.getString("availability_status")
										.toLowerCase().contains(query)) {
							Bundle put = filtered.getBundle(String.valueOf(i));
							put.putBundle(String.valueOf(put.size()), content);
						}
					}
				}
				results.values = filtered;
				results.count = filtered.size();
			}
			return results;
		}

		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			// TODO Auto-generated method stub
			data = (Bundle) results.values;
			flattenData();
			notifyDataSetChanged();
		}

	}

}

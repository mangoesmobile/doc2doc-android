/**
 * 
 */
package com.mangoesmobile.doc2doc.adapters;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.foound.widget.AmazingAdapter;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.cache.PingCacheManager;
import com.mangoesmobile.doc2doc.contents.NewMessage;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.ImageLoader;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;
import com.mangoesmobile.doc2doc.util.PingDoctor;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class MyPingAdapter extends AmazingAdapter implements Filterable {

	String token;
	
	int tag;

	Bundle data;
	ArrayList<String> pingId; // keeps track of id of the threads which are
								// added to data
	ArrayList<String> originalData;
	Filter filter;
	FragmentActivity context;
	LayoutInflater inflater;
	ImageLoader imageLoader;

	private Object lock = new Object(); // used for performing synchoronized
										// filtering
	GoogleAnalyticsTracker tracker;

	public MyPingAdapter(FragmentActivity context, int tag, String token) {
		this.tag = tag;
		this.data = new Bundle();
		this.pingId = new ArrayList<String>();
		this.originalData = new ArrayList<String>();
		this.filter = new BoardFilter();
		this.context = context;
		this.token = token;
		inflater = context.getLayoutInflater();
		imageLoader = new ImageLoader(context);
		tracker = GoogleAnalyticsTracker.getInstance();	
	}

	public void setData(Bundle bundle) {
		Log.d("my_ping-adapter", "Bundle size: " + bundle.size());
		for (int i = bundle.size() - 1; i >= 0; i--) {
			Bundle content = bundle.getBundle(String.valueOf(i));
			// Log.d("board-adapter", "content id: " + content.getString("id"));
			Bundle old = data.getBundle(content.getString("id"));
			data.putBundle(content.getString("id"), content);
			// if the bundle was there before, remove the id from that position
			// and add it to the beginning of threadId
			if (old != null) {
				pingId.remove(content.getString("id"));
			}
			pingId.add(0, content.getString("id"));

		}
		updateCache();
		originalData = pingId;
		notifyDataSetChanged();
		notifyMayHaveMorePages();
	}

	private void updateCache() {
		PingCacheManager manager = new PingCacheManager(context);
		manager.open();
		manager.deleteAllContents();
		for (int i = 0; i < pingId.size(); i++) {
			Bundle content = data.getBundle(pingId.get(i));
			manager.insertContent(content.getString("id"),
					content.getString("sender_id"),
					content.getString("sender_name"),
					content.getString("sender_full_name"),
					content.getString("sender_profile_pic_file_path"),
					content.getString("recipient_id"),
					content.getString("recipient_name"),
					content.getString("recipient_full_name"),
					content.getString("recipient_profile_pic_file_path"),
					content.getString("status"), content.getString("timestamp"));
		}
		manager.close();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return pingId.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.getBundle(pingId.get(arg0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#onNextPageRequested(int)
	 */
	@Override
	protected void onNextPageRequested(int page) {
		// TODO Auto-generated method stub
		Log.d("my_ping-adapter-pages", "Got onNextPageRequested page = " + page);
		fetchThread(page);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.foound.widget.AmazingAdapter#bindSectionHeader(android.view.View,
	 * int, boolean)
	 */
	@Override
	protected void bindSectionHeader(View view, int position,
			boolean displaySectionHeader) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getAmazingView(int,
	 * android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getAmazingView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		View row = convertView;
		if (row == null) {
			row = inflater.inflate(R.layout.row_ping, null);
			holder = new ViewHolder();
			holder.progUserImage = (ProgressBar) row
					.findViewById(R.id.prog_ping_userimage);
			holder.imgThreadPhoto = (ImageView) row
					.findViewById(R.id.img_ping_userimage);
			holder.imgThreadStatus = (ImageView) row
					.findViewById(R.id.img_ping_status);
			holder.lblTitle = (TextView) row.findViewById(R.id.lbl_ping_title);
			holder.lblTimestamp = (TextView) row
					.findViewById(R.id.lbl_ping_timestamp);
			holder.btnPing = (Button) row.findViewById(R.id.btn_pingback);
			holder.btnMsg = (Button) row.findViewById(R.id.btn_ping_message);
			row.setTag(holder);
		} else {
			holder = (ViewHolder) row.getTag();
		}

		Bundle content = (Bundle) getItem(position);
		holder.lblTitle.setText(content.getString("sender_name")
				+ " pinged you");
		holder.lblTimestamp.setText("on "
				+ Global.formatDate(content.getString("timestamp")));
		holder.btnPing.setOnClickListener(new PingClickListener(content
				.getString("sender_id"), content.getString("sender_name")));
		holder.btnMsg.setOnClickListener(new MessageClickListener(content
				.getString("sender_id"), content.getString("sender_full_name")));
		if (content.getString("status").equals("new")) {
			holder.imgThreadStatus.setVisibility(View.VISIBLE);
			content.putString("status", "read");
		} else {
			holder.imgThreadStatus.setVisibility(View.GONE);
		}
		// Global.loadPhoto(context,
		// Global.url + content.getString("sender_profile_pic_file_path"),
		// holder.imgThreadPhoto, holder.progUserImage);
		imageLoader.DisplayImage(
				Global.url + content.getString("sender_profile_pic_file_path"),
				holder.imgThreadPhoto, holder.progUserImage);
		return row;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.foound.widget.AmazingAdapter#configurePinnedHeader(android.view.View,
	 * int, int)
	 */
	@Override
	public void configurePinnedHeader(View header, int position, int alpha) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getPositionForSection(int)
	 */
	@Override
	public int getPositionForSection(int section) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getSectionForPosition(int)
	 */
	@Override
	public int getSectionForPosition(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getSections()
	 */
	@Override
	public Object[] getSections() {
		// TODO Auto-generated method stub
		return null;
	}

	private void fetchThread(int page) {
		Doc2DocRequest dr = new Doc2DocRequest();
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", token);
		params.putString("page_no", String.valueOf(page));

		asyncTask.runTask("ping/read", params, new Doc2DocTaskListener() {

			@Override
			public void onMalformedURLException(MalformedURLException e) {
				// TODO Auto-generated method stub
				mHandler.sendMessage(Message.obtain(mHandler,
						Global.ERROR_RESPONSE));
				Log.d("my_ping-response", e.getMessage());
			}

			@Override
			public void onIOException(IOException e) {
				// TODO Auto-generated method stub
				mHandler.sendMessage(Message.obtain(mHandler,
						Global.CONNECTION_ERROR));
				Log.d("my_ping-response", e.getMessage());
			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e) {
				// TODO Auto-generated method stub
				mHandler.sendMessage(Message.obtain(mHandler,
						Global.ERROR_RESPONSE));
				Log.d("my_ping-response", e.getMessage());
			}

			@Override
			public void onComplete(String response) {
				Log.d("my_ping-response-json", response);
				if (!response.equals("FALSE")) {
					parseJson(response);
				} else {
					mHandler.sendMessage(Message.obtain(mHandler,
							Global.INVALID_SESSION));
				}
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				
			}
		});
	}

	private void parseJson(String response) {
		JSONArray arr = null;
		try {
			arr = new JSONArray(response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mHandler.sendMessage(Message
					.obtain(mHandler, Global.ERROR_RESPONSE));
			return;
		}
		if (arr.length() == 0) {
			mHandler.sendMessage(Message
					.obtain(mHandler, Global.EMPTY_RESPONSE));
			return;
		}
		for (int i = 0; i < arr.length(); i++) {
			Bundle content = new Bundle();
			JSONObject json;
			try {
				json = arr.getJSONObject(i);
				content.putString("id", json.getString("id"));
				// sender info
				JSONObject sender = json.getJSONObject("sender");
				content.putString("sender_id", sender.getString("user_id"));
				content.putString("sender_name", sender.getString("username"));
				content.putString("sender_full_name", sender.getString("full_name"));
				content.putString("sender_profile_pic_file_path",
						sender.getString("profile_pic_file_path"));
				// recipient info
				JSONObject recipient = json.getJSONObject("recipient");
				content.putString("recipient_id",
						recipient.getString("user_id"));
				content.putString("recipient_name",
						recipient.getString("username"));
				content.putString("recipient_full_name", recipient.getString("full_name"));
				content.putString("recipient_profile_pic_file_path",
						recipient.getString("profile_pic_file_path"));

				content.putString("status", json.getString("status"));
				content.putString("timestamp", json.getString("timestamp"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			data.putBundle(String.valueOf(i), content);
		}
		updateCache();
		mHandler.sendMessage(Message.obtain(mHandler, Global.GOT_RESPONSE));
	}

	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				// nextPage();
				// notifyDataSetChanged();
				// notifyMayHaveMorePages();
				notifyNoMorePages();
				break;
			case Global.EMPTY_RESPONSE:
				notifyNoMorePages();
				break;
			case Global.CONNECTION_ERROR:
				Toast.makeText(context, "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				notifyMayHaveMorePages();
				break;
			case Global.ERROR_RESPONSE:
				notifyMayHaveMorePages();
				break;
			case Global.INVALID_SESSION:
				Toast.makeText(context, "Invalid session! Please relogin",
						Toast.LENGTH_LONG).show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				context.sendBroadcast(i);
				break;
			}
		}
	};

	static class ViewHolder {
		public ProgressBar progUserImage;
		public ImageView imgThreadPhoto;
		public ImageView imgThreadStatus;
		public TextView lblTitle;
		public TextView lblTimestamp;
		public Button btnPing;
		public Button btnMsg;
	}

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		if (filter == null) {
			filter = new BoardFilter();
		}
		return filter;
	}

	private class PingClickListener implements OnClickListener {

		String userId;
		String name;

		public PingClickListener(String userId, String name) {
			this.userId = userId;
			this.name = name;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			PingDoctor pd = new PingDoctor(context, userId, name);
			pd.ping();
			tracker.trackEvent("Click", "Button", "Ping User", 0);
		}

	}

	private class MessageClickListener implements OnClickListener {

		String userId;
		String name;

		public MessageClickListener(String userId, String name) {
			this.userId = userId;
			this.name = name;
		}

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			Bundle args = new Bundle();
			args.putBoolean("has_recipient", true);
			args.putString("tagged_users", userId);
			args.putString("tagged_user_names", name);
			NewMessage nm = NewMessage.newInstance(tag, args);
			FragmentTransaction ft = context.getSupportFragmentManager()
					.beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, nm);
			ft.commit();
			D2DTabHost.history.get(tag).push(nm);
			// Intent intent = new Intent(context, NewMessage.class);
			// intent.putExtra("has_recipient", true);
			// intent.putExtra("id", userId);
			// intent.putExtra("name", name);
			// ActivityGroupTeam parent = (ActivityGroupTeam)
			// context.getParent();
			// parent.startChildActivity("new_message", intent);
			tracker.trackEvent("Click", "Button", "Create New Message", 0);
		}

	}

	private class BoardFilter extends Filter {

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			// TODO Auto-generated method stub
			String query = constraint.toString().toLowerCase();
			FilterResults results = new FilterResults();
			ArrayList<String> temp;
			synchronized (lock) {
				temp = new ArrayList<String>(originalData);
			}
			if (query == null || query.length() == 0) {
				synchronized (lock) {
					results.values = originalData;
					results.count = originalData.size();
				}
			} else {
				ArrayList<String> filtered = new ArrayList<String>();
				for (int i = 0; i < temp.size(); i++) {
					String id = temp.get(i);
					Bundle content = data.getBundle(id);
					if (content.getString("sender_id").toLowerCase()
							.contains(query)
							|| content.getString("recipient_id").toLowerCase()
									.contains(query)) {
						filtered.add(id);
					}
				}
				results.values = filtered;
				results.count = filtered.size();
			}
			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			// TODO Auto-generated method stub
			pingId = (ArrayList<String>) results.values;
			notifyDataSetChanged();
			if (constraint.length() > 0) {
				notifyNoMorePages();
			} else {
				notifyMayHaveMorePages();
			}
		}

	}

}

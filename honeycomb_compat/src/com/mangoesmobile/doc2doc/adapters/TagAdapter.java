/**
 * 
 */
package com.mangoesmobile.doc2doc.adapters;

import java.util.ArrayList;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.foound.widget.AmazingAdapter;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.cache.TeamCacheManager;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.ImageLoader;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class TagAdapter extends AmazingAdapter implements Filterable {

	FragmentActivity context;
	final String[] orgs = { "BUP", "UB Residents", "UB Faculty", "UB Library",
			"mangoes" };
	Bundle data;
	Bundle original;
	Bundle flat;
	ArrayList<Integer> sections;
	LayoutInflater inflater;
	ImageLoader imageLoader;

	Filter filter;
	private Object lock = new Object(); // used for performing synchoronized
										// filtering

	public TagAdapter(FragmentActivity context) {
		this.context = context;
		this.data = new Bundle();
		this.original = new Bundle();
		this.flat = new Bundle();
		inflater = context.getLayoutInflater();
		imageLoader = new ImageLoader(context);
	}

	public void setData(Bundle bundle) {
		Log.d("team-adapter", "Bundle size: " + bundle.size());

		data = new Bundle();
		for (int i = 0; i < orgs.length; i++) {
			data.putBundle(String.valueOf(i), new Bundle());
		}

		for (int i = 0; i < bundle.size(); i++) {
			Bundle content = bundle.getBundle(String.valueOf(i));
			for (int j = 0; j < orgs.length; j++) {
				if (content.getString("organization").equalsIgnoreCase(orgs[j])) {
					Bundle put = data.getBundle(String.valueOf(j));
					put.putBundle(String.valueOf(put.size()), content);
					break;
				}
			}
		}

		flattenData();
		updateCache();
		original.putAll(data);
		notifyDataSetChanged();
		notifyNoMorePages();
	}

	private void updateCache() {
		TeamCacheManager manager = new TeamCacheManager(context);
		manager.open();
		manager.deleteAllContents();
		for (int i = 0; i < flat.size(); i++) {
			Bundle content = flat.getBundle(String.valueOf(i));
			manager.insertContent(content.getString("user_id"),
					content.getString("first_name"),
					content.getString("last_name"), content.getString("title"),
					content.getString("email"), content.getString("phone"),
					content.getString("profile_pic_file_path"),
					content.getString("organization"),
					content.getString("specialization"),
					content.getString("username"),
					content.getString("availability_status"),
					content.getString("latitude"),
					content.getString("longitude"),
					content.getString("base_location"));
		}
		manager.close();
	}

	private void flattenData() {
		flat = new Bundle();
		sections = new ArrayList<Integer>();
		int c = 0;
		for (int i = 0; i < data.size(); i++) {
			sections.add(c);
			// flat.putBundle(String.valueOf(c++), null);
			Bundle group = data.getBundle(String.valueOf(i));
			for (int j = 0; j < group.size(); j++) {
				flat.putBundle(String.valueOf(c++),
						group.getBundle(String.valueOf(j)));
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return flat.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return flat.getBundle(String.valueOf(arg0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#onNextPageRequested(int)
	 */
	@Override
	protected void onNextPageRequested(int page) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.foound.widget.AmazingAdapter#bindSectionHeader(android.view.View,
	 * int, boolean)
	 */
	@Override
	protected void bindSectionHeader(View view, int position,
			boolean displaySectionHeader) {
		// TODO Auto-generated method stub
		if (displaySectionHeader) {
			view.findViewById(R.id.pinned_header).setVisibility(View.VISIBLE);
			TextView lSectionTitle = (TextView) view
					.findViewById(R.id.pinned_header);
			lSectionTitle
					.setText(getSections()[getSectionForPosition(position)]);
		} else {
			view.findViewById(R.id.pinned_header).setVisibility(View.GONE);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getAmazingView(int,
	 * android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getAmazingView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		View row = convertView;
		if (row == null) {
			row = inflater.inflate(R.layout.row_teamlist, null);
			row.setBackgroundResource(R.drawable.row_check);
			holder = new ViewHolder();
			holder.header = (TextView) row.findViewById(R.id.pinned_header);
			holder.progUserImage = (ProgressBar) row
					.findViewById(R.id.prog_teamlist_userimage);
			holder.imgTeamPhoto = (ImageView) row
					.findViewById(R.id.img_teamlist_user);
			holder.imgTeamStatus = (ImageView) row
					.findViewById(R.id.img_teamlist_userstatus);
			holder.lblName = (TextView) row
					.findViewById(R.id.lbl_teamlist_name);
			holder.lblSpecialization = (TextView) row
					.findViewById(R.id.lbl_teamlist_specialization);
			holder.lblLocation = (TextView) row
					.findViewById(R.id.lbl_teamlist_location);
			holder.lblUserStatus = (TextView) row
					.findViewById(R.id.lbl_teamlist_userstatus);
			holder.layoutMessaging = (LinearLayout) row
					.findViewById(R.id.layout_teamlist_messaging);
			holder.chkUserCheck = (CheckBox) row
					.findViewById(R.id.chk_teamlist_selected);
			holder.header.setVisibility(View.GONE);
			holder.lblUserStatus.setVisibility(View.GONE);
			holder.layoutMessaging.setVisibility(View.GONE);
			holder.chkUserCheck.setVisibility(View.VISIBLE);
			row.setTag(holder);
		} else {
			holder = (ViewHolder) row.getTag();
		}

		Bundle content = (Bundle) getItem(position);
		int status;
		String user_status = content.getString("availability_status");
		if (user_status.equals("online")) {
			status = R.drawable.bg_photo_online;
			holder.lblUserStatus.setTextColor(Color.rgb(0, 102, 153));
		} else if (user_status.equals("on call")) {
			status = R.drawable.bg_photo_oncall;
			holder.lblUserStatus.setTextColor(Color.rgb(101, 176, 73));
		} else {
			status = R.drawable.bg_photo_offline;
			holder.lblUserStatus.setTextColor(Color.DKGRAY);
		}
		holder.imgTeamStatus.setImageResource(status);
		holder.lblName.setText(content.getString("first_name") + " "
				+ content.getString("last_name"));
		holder.lblSpecialization.setText(content.getString("specialization")
				.equals("null") ? "" : content.getString("specialization"));
		holder.lblLocation.setText(content.getString("organization").equals(
				"null") ? "" : content.getString("organization"));
		holder.chkUserCheck.setChecked(content.getBoolean("checked"));
		// Global.loadPhoto(context, Global.url +
		// content.getString("profile_pic_file_path"), holder.imgTeamPhoto,
		// holder.progUserImage);
		imageLoader.DisplayImage(
				Global.url + content.getString("profile_pic_file_path"),
				holder.imgTeamPhoto, holder.progUserImage);
		row.setBackgroundResource(Global.getListItemBackground(content
				.getBoolean("checked")));
		return row;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.foound.widget.AmazingAdapter#configurePinnedHeader(android.view.View,
	 * int, int)
	 */
	@Override
	public void configurePinnedHeader(View header, int position, int alpha) {
		// TODO Auto-generated method stub
		TextView lSectionHeader = (TextView) header;
		lSectionHeader.setText(getSections()[getSectionForPosition(position)]);
		// lSectionHeader.setBackgroundColor(alpha << 24 | (0x233659));
		lSectionHeader.setBackgroundResource(R.drawable.header);
		lSectionHeader.setTextColor(alpha << 24 | (0xffffff));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getPositionForSection(int)
	 */
	@Override
	public int getPositionForSection(int section) {
		// TODO Auto-generated method stub
		if (section < 0)
			section = 0;
		if (section >= sections.size())
			section = sections.size() - 1;
		return sections.get(section);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getSectionForPosition(int)
	 */
	@Override
	public int getSectionForPosition(int position) {
		// TODO Auto-generated method stub
		for (int i = 0; i < sections.size(); i++) {
			if (position < sections.get(i)) {
				return i - 1;
			}
		}
		return sections.size() - 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getSections()
	 */
	@Override
	public String[] getSections() {
		// TODO Auto-generated method stub
		return orgs;
	}

	static class ViewHolder {
		public TextView header;
		public ProgressBar progUserImage;
		public ImageView imgTeamPhoto;
		public ImageView imgTeamStatus;
		public TextView lblName;
		public TextView lblSpecialization;
		public TextView lblLocation;
		public TextView lblUserStatus;
		public LinearLayout layoutMessaging;
		public CheckBox chkUserCheck;
	}

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		if (filter == null) {
			filter = new TagFilter();
		}
		return filter;
	}

	private class TagFilter extends Filter {

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			// TODO Auto-generated method stub
			String query = constraint.toString().toLowerCase();
			FilterResults results = new FilterResults();
			Bundle temp;
			synchronized (lock) {
				temp = new Bundle();
				temp.putAll(original);
			}
			if (query == null || query.length() == 0) {
				synchronized (lock) {
					results.values = original;
					results.count = original.size();
				}
			} else {
				Bundle filtered = new Bundle();
				for (int i = 0; i < orgs.length; i++) {
					filtered.putBundle(String.valueOf(i), new Bundle());
				}

				for (int i = 0; i < temp.size(); i++) {
					Bundle group = temp.getBundle(String.valueOf(i));
					for (int j = 0; j < group.size(); j++) {
						Bundle content = group.getBundle(String.valueOf(j));
						if (content.getString("title").toLowerCase()
								.contains(query)
								|| content.getString("first_name")
										.toLowerCase().contains(query)
								|| content.getString("last_name").toLowerCase()
										.contains(query)
								|| content.getString("username").toLowerCase()
										.contains(query)) {
							Bundle put = filtered.getBundle(String.valueOf(i));
							put.putBundle(String.valueOf(put.size()), content);
						}
					}
				}
				results.values = filtered;
				results.count = filtered.size();
			}
			return results;
		}

		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			// TODO Auto-generated method stub
			data = (Bundle) results.values;
			flattenData();
			notifyDataSetChanged();
		}

	}

}

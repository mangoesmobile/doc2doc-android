/**
 * 
 */
package com.mangoesmobile.doc2doc.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class TeamService extends Service {

	SharedPreferences prefs;
	String token = null;

	Messenger client;

	private Handler refreshHandler = new Handler();
	private Runnable runRefresh = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			fetchTeam();
			refreshHandler.postDelayed(this, 5 * 60000);
		}
	};

	class MessageHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.MSG_REGISTER_CLIENT:
				Log.d("TeamService", "client registered");
				client = msg.replyTo;
				break;
			case Global.REFRESH_TEAM:
				refreshHandler.removeCallbacks(runRefresh);
				refreshHandler.post(runRefresh);
				break;
			default:
				super.handleMessage(msg);
			}
		}
	};

	final Messenger mMessenger = new Messenger(new MessageHandler());

	private Bundle bundle = new Bundle();

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return mMessenger.getBinder();
	}

	@Override
	public void onCreate() {
		super.onCreate();

		prefs = getSharedPreferences("session", MODE_PRIVATE);
		token = prefs.getString("token", "");

		refreshHandler.post(runRefresh);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("TeamService", "service started");
		return START_STICKY; // run until explicitly stopped.
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d("TeamService", "service stopped");
		client = null;
		refreshHandler.removeCallbacks(runRefresh);
	}

	private void fetchTeam() {
		Log.d("Team", "fetching team");
		Doc2DocRequest dr = new Doc2DocRequest();
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", token);

		asyncTask.runTask("user/all_vcards", params, new Doc2DocTaskListener() {

			@Override
			public void onMalformedURLException(MalformedURLException e) {
				// TODO Auto-generated method stub
				teamHandler.sendMessage(Message.obtain(teamHandler,
						Global.ERROR_RESPONSE));
				Log.d("team-response", e.getMessage());
			}

			@Override
			public void onIOException(IOException e) {
				// TODO Auto-generated method stub
				teamHandler.sendMessage(Message.obtain(teamHandler,
						Global.CONNECTION_ERROR));
				Log.d("team-response", e.getMessage());
			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e) {
				// TODO Auto-generated method stub
				teamHandler.sendMessage(Message.obtain(teamHandler,
						Global.ERROR_RESPONSE));
				Log.d("team-response", e.getMessage());
			}

			@Override
			public void onComplete(String response) {
				Log.d("team-response", response);
				if (!response.equals("FALSE")) {
					parseJson(response);
				} else {
					teamHandler.sendMessage(Message.obtain(teamHandler,
							Global.INVALID_SESSION));
				}
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				
			}
		});
	}

	private void parseJson(String response) {
		JSONArray arr = null;
		try {
			arr = new JSONArray(response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			teamHandler.sendMessage(Message.obtain(teamHandler,
					Global.ERROR_RESPONSE));
			return;
		}
		if (arr.length() == 0) {
			teamHandler.sendMessage(Message.obtain(teamHandler,
					Global.EMPTY_RESPONSE));
			return;
		}
		for (int i = 0; i < arr.length(); i++) {
			Bundle content = new Bundle();
			JSONObject json;
			try {
				json = arr.getJSONObject(i);
				content.putString("user_id", json.getString("user_id"));
				content.putString("first_name", json.getString("first_name"));
				content.putString("last_name", json.getString("last_name"));
				content.putString("title", json.getString("title"));
				content.putString("email", json.getString("email"));
				content.putString("phone", json.getString("phone"));
				content.putString("profile_pic_file_path",
						json.getString("profile_pic_file_path"));
				content.putString("organization",
						json.getString("organization"));
				content.putString("specialization",
						json.getString("specialization"));
				content.putString("username", json.getString("username"));
				content.putString("availability_status",
						json.getString("availability_status"));
				JSONObject geolocation = json.getJSONObject("geolocation");
				content.putString("latitude", geolocation.getString("latitude"));
				content.putString("longitude",
						geolocation.getString("longitude"));
				content.putString("base_location",
						json.getString("base_location"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bundle.putBundle(String.valueOf(i), content);
		}
		teamHandler.sendMessage(Message
				.obtain(teamHandler, Global.GOT_RESPONSE));
	}

	private Handler teamHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				Log.d("TeamService", "sending data from service");
				Message response = Message.obtain();
				response.what = Global.GET_DATA;
				response.setData(bundle);
				try {
					client.send(response);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
				break;
			case Global.EMPTY_RESPONSE:
				break;
			case Global.CONNECTION_ERROR:
				break;
			case Global.ERROR_RESPONSE:
				break;
			case Global.INVALID_SESSION:
				Toast.makeText(TeamService.this,
						"Invalid session! Please relogin", Toast.LENGTH_LONG)
						.show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				sendBroadcast(i);
				break;
			}
		}
	};
}

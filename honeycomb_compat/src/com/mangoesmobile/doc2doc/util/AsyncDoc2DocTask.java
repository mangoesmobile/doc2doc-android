package com.mangoesmobile.doc2doc.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;


import android.os.Bundle;

public class AsyncDoc2DocTask {

	Doc2DocRequest dr;
	boolean requestCanceled = false;
	
	public AsyncDoc2DocTask(Doc2DocRequest dr) {
		this.dr = dr;
	}
	
	public void cancelRequest() {
		requestCanceled = true;
	}
	
	public void runTask(final String action, final Bundle params, final Doc2DocTaskListener listener) {
		new Thread() {
            @Override 
            public void run() {
                try {
                	String response = dr.request(action, params);
                    if (!requestCanceled) {
						listener.onComplete(response);
					} else {
						listener.onCancel();
					}
                } catch (FileNotFoundException e) {
                    if (!requestCanceled) {
						listener.onFileNotFoundException(e);
					} else {
						listener.onCancel();
					}
                } catch (MalformedURLException e) {
                    if (!requestCanceled) {
						listener.onMalformedURLException(e);
					} else {
						listener.onCancel();
					}
                } catch (IOException e) {
                    if (!requestCanceled) {
						listener.onIOException(e);
					} else {
						listener.onCancel();
					}
                }
            }
        }.start();
	}
	
	public interface Doc2DocTaskListener {
		
		/**
         * Called when a request completes with the given response.
         *
         * Executed by a background thread: do not update the UI in this method.
         */
		public void onComplete(String response);
		
		/**
         * Called when a request has a network or request error.
         *
         * Executed by a background thread: do not update the UI in this method.
         */
		public void onIOException(IOException e);

        /**
         * Called when a request fails because the requested resource is
         * invalid or does not exist.
         *
         * Executed by a background thread: do not update the UI in this method.
         */
        public void onFileNotFoundException(FileNotFoundException e);

        /**
         * Called if an invalid action is provided (which may result in a
         * malformed URL).
         *
         * Executed by a background thread: do not update the UI in this method.
         */
        public void onMalformedURLException(MalformedURLException e);
        
        /**
         * Called when a request is cancelled by the caller thread.
         *
         * Executed by a background thread: do not update the UI in this method.
         */
		public void onCancel();
	}
	
}

/**
 * 
 */
package com.mangoesmobile.doc2doc.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class PingDoctor {
	
	SharedPreferences prefs;
	String token = null;
	
	Activity context;
	String user;
	String name;
	
	public PingDoctor(Activity context, String user, String name) {
		this.context = context;
		this.user = user;
		this.name = name;

		prefs = this.context.getSharedPreferences("session", Context.MODE_PRIVATE);
		token = prefs.getString("token", "");
	}
	
	public void ping() {
		Log.d("PingDoctor", "pinging doctor");
		Doc2DocRequest dr = new Doc2DocRequest();
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", token);
		params.putString("recipient_id", user);

		Toast.makeText(context, "Pinging " + name + "...", Toast.LENGTH_LONG).show();
		
		asyncTask.runTask("ping/send", params,
				new Doc2DocTaskListener() {

					@Override
					public void onMalformedURLException(MalformedURLException e) {
						// TODO Auto-generated method stub
						pingHandler.sendMessage(Message.obtain(
								pingHandler, Global.ERROR_RESPONSE));
						Log.d("ping-response", e.getMessage());
					}

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						pingHandler.sendMessage(Message.obtain(
								pingHandler, Global.CONNECTION_ERROR));
						Log.d("ping-response", e.getMessage());
					}

					@Override
					public void onFileNotFoundException(FileNotFoundException e) {
						// TODO Auto-generated method stub
						pingHandler.sendMessage(Message.obtain(
								pingHandler, Global.ERROR_RESPONSE));
						Log.d("ping-response", e.getMessage());
					}

					@Override
					public void onComplete(String response) {
						Log.d("ping-response", response);
						if (response.equals("TRUE")) {
							pingHandler.sendMessage(Message.obtain(
									pingHandler, Global.GOT_RESPONSE));
						} else {
							pingHandler.sendMessage(Message.obtain(
									pingHandler, Global.INVALID_SESSION));
						}
					}

					@Override
					public void onCancel() {
						// TODO Auto-generated method stub
						
					}
				});
	}

	private Handler pingHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				Toast.makeText(context, "Successfully pinged", Toast.LENGTH_LONG).show();
				break;
			case Global.EMPTY_RESPONSE:
				break;
			case Global.CONNECTION_ERROR:
				Toast.makeText(context,
						"Connection error! Please retry", Toast.LENGTH_LONG)
						.show();
				break;
			case Global.ERROR_RESPONSE:
				break;
			case Global.INVALID_SESSION:
				Toast.makeText(context, "Invalid session! Please relogin", Toast.LENGTH_LONG).show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				context.sendBroadcast(i);
				break;
			}
		}
	};
}

/**
 * 
 */
package com.mangoesmobile.doc2doc.util;

import java.io.ByteArrayOutputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class ImageCompression {

	public byte[] compressImage(String file) {
		// Decode image size
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(file, o);

		// The new size we want to scale to
		int REQUIRED_WIDTH = 600;
		int REQUIRED_HEIGHT = 800;

		// Find the correct scale value. It should be the power of 2.
		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		
		if(height_tmp < width_tmp) {
			REQUIRED_WIDTH = 800;
			REQUIRED_HEIGHT = 600;
		}
		
		int scale = 1;
		while (true) {
			if (width_tmp < REQUIRED_WIDTH && height_tmp < REQUIRED_HEIGHT)
				break;
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		// Decode with inSampleSize
		o = new BitmapFactory.Options();
		o.inSampleSize = scale;
		Bitmap bitmap = BitmapFactory.decodeFile(file, o);

		// Converting to output stream with compression
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
		
		return stream.toByteArray();
	}
}

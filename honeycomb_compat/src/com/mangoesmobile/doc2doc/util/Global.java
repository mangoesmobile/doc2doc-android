/**
 * 
 */
package com.mangoesmobile.doc2doc.util;

import java.util.ArrayList;

import com.mangoesmobile.doc2doc.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class Global {

	public static final String UA = "UA-28559719-1";

	public static final String url = "http://50.18.120.168/";

	public static final String TAB_BOARD = "tab_board";
	public static final String TAB_MESSAGES = "tab_messages";
	public static final String TAB_TEAM = "tab_team";
	public static final String TAB_REFERENCE = "tab_reference";
	
	public static final int GOT_RESPONSE = 0;
	public static final int EMPTY_RESPONSE = 1;
	public static final int CONNECTION_ERROR = 2;
	public static final int ERROR_RESPONSE = 3;
	public static final int INVALID_SESSION = 4;
	public static final int MSG_REGISTER_CLIENT = 5;
	public static final int MSG_UNREGISTER_CLIENT = 6;
	public static final int REFRESH_BOARD = 7;
	public static final int REFRESH_MESSAGE = 8;
	public static final int REFRESH_TEAM = 9;
	public static final int GET_DATA = 10;
	public static final int CHANGE_STATUS = 11;
	public static final int CHANGE_PIC = 12;

	// public static final String REFRESH_ADAPTER = "REFRESH_ADAPTER";
	
	// keeps track of user's back button press
	public static boolean backPressed = false;

	public static String total = "0";
	public static String ping = "0";
	public static String thread = "0";
	public static String message = "0";

	public static Bundle userInfo = new Bundle();
	public static String userStatus = "";
	public static Drawable profilePic = null;

	public static Location currentBestLocation = null;

	public static void createAlert(final Activity a, String message,
			final String button) {
		Builder builder = new AlertDialog.Builder(a);
		builder.setTitle("Error");
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.setMessage(message);
		AlertDialog alert = builder.create();
		alert.setButton(button, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				return;
			}
		});
		alert.show();
	}

//	public static void loadPhoto(final Activity context, String url,
//			final ImageView view, final ProgressBar prog) {
//		ImageCache img = ImageCache.getInstance();
//		ImageCallback callback = new ImageCallback() {
//
//			@Override
//			public void onImageLoaded(final Drawable image, String url) {
//				// TODO Auto-generated method stub
//				context.runOnUiThread(new Runnable() {
//
//					@Override
//					public void run() {
//						// TODO Auto-generated method stub
//						view.setImageDrawable(image);
//						if (view.getId() == R.id.img_userimage
//								|| view.getId() == R.id.img_messages_userimage
//								|| view.getId() == R.id.img_team_userimage)
//							profilePic = image;
//						if (prog != null) {
//							prog.setVisibility(View.GONE);
//						}
//						view.setVisibility(View.VISIBLE);
//					}
//				});
//			}
//		};
//		img.loadAsync(url, callback, context);
//
//	}

	public static void showLoading(ProgressDialog loading, String message,
			boolean cancelable) {
		loading.setMessage(message);
		loading.setCancelable(cancelable);
		loading.show();
	}

	public static void hideLoading(ProgressDialog loading) {
		loading.hide();
	}

	public static int getListItemBackground(boolean isChecked) {
		if (isChecked) {
			return R.drawable.bg_thread_down;
		} else {
			return R.drawable.bg_thread;
		}
	}

	public static String formatDate(String timestamp) {
		String date = timestamp.substring(8, 10) + "/"
				+ timestamp.substring(5, 7) + " ";
		int hour = Integer.parseInt(timestamp.substring(11, 13));
		date += (hour > 12 ? hour - 12 : hour) + ":"
				+ timestamp.substring(14, 16) + (hour > 12 ? "pm" : "am");
		return date;
	}

	public static String formatValues(String values, String defaultValue) {
		if (values.length() > 0) {
			return values.replace(',', '-');
		}
		return defaultValue;
	}

	public static String getStringFromList(String ch,
			ArrayList<CharSequence> list) {
		if (list.size() == 0) {
			return "null";
		}
		String str = "";
		for (int i = 0; i < list.size(); i++) {
			str += list.get(i);
			if (i != list.size() - 1)
				str += ch;
		}
		return str;
	}

	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			// pre-condition
			return;
		}

		int totalHeight = 0;
		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(),
				MeasureSpec.AT_MOST);
		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += listItem.getMeasuredHeight();
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}
}

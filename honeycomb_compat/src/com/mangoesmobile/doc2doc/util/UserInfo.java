/**
 * 
 */
package com.mangoesmobile.doc2doc.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class UserInfo {
	
	public static final String GOT_USERINFO = "GOT_USERINFO";
	
	public static void fetchUserInfo(final Activity context, String token) {
		Log.d("userinfo", "fetching user info");
		Doc2DocRequest dr = new Doc2DocRequest();
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", token);

		asyncTask.runTask("user/my_vcard", params,
				new Doc2DocTaskListener() {

					@Override
					public void onMalformedURLException(MalformedURLException e) {
						// TODO Auto-generated method stub
						Log.d("userinfo-response", e.getMessage());
						Intent intent = new Intent();
						intent.setAction(GOT_USERINFO);
						intent.putExtra("response", Global.ERROR_RESPONSE);
						context.sendBroadcast(intent);
					}

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						Log.d("userinfo-response", e.getMessage());
						Intent intent = new Intent();
						intent.setAction(GOT_USERINFO);
						intent.putExtra("response", Global.CONNECTION_ERROR);
						context.sendBroadcast(intent);
					}

					@Override
					public void onFileNotFoundException(FileNotFoundException e) {
						// TODO Auto-generated method stub
						Log.d("userinfo-response", e.getMessage());
						Intent intent = new Intent();
						intent.setAction(GOT_USERINFO);
						intent.putExtra("response", Global.ERROR_RESPONSE);
						context.sendBroadcast(intent);
					}

					@Override
					public void onComplete(String response) {
						Log.d("userinfo-response", response);
						if (!response.equals("FALSE")) {
							parseJson(response, context);
						} else {
							Intent intent = new Intent();
							intent.setAction(GOT_USERINFO);
							intent.putExtra("response", Global.INVALID_SESSION);
							context.sendBroadcast(intent);
						}
					}

					@Override
					public void onCancel() {
						// TODO Auto-generated method stub
						
					}
				});
	}
	
	public static void changeStatus(final Activity context, String status, String token) {
		Log.d("userinfo", "changing status");
		Doc2DocRequest dr = new Doc2DocRequest();
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", token);
		params.putString("status_msg", status);

		asyncTask.runTask("user/set_user_availability_status", params,
				new Doc2DocTaskListener() {

					@Override
					public void onMalformedURLException(MalformedURLException e) {
						// TODO Auto-generated method stub
						Log.d("userinfo-response", e.getMessage());
						Intent intent = new Intent();
						intent.setAction(GOT_USERINFO);
						intent.putExtra("response", Global.ERROR_RESPONSE);
						context.sendBroadcast(intent);
					}

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						Log.d("userinfo-response", e.getMessage());
						Intent intent = new Intent();
						intent.setAction(GOT_USERINFO);
						intent.putExtra("response", Global.CONNECTION_ERROR);
						context.sendBroadcast(intent);
					}

					@Override
					public void onFileNotFoundException(FileNotFoundException e) {
						// TODO Auto-generated method stub
						Log.d("userinfo-response", e.getMessage());
						Intent intent = new Intent();
						intent.setAction(GOT_USERINFO);
						intent.putExtra("response", Global.ERROR_RESPONSE);
						context.sendBroadcast(intent);
					}

					@Override
					public void onComplete(String response) {
						Log.d("userinfo-response", response);
						if (response.equals("TRUE")) {
							Intent intent = new Intent();
							intent.setAction(GOT_USERINFO);
							intent.putExtra("response", Global.CHANGE_STATUS);
							intent.putExtra("userStatus", Global.userStatus.equals("online") ? "on call" : "online");
							context.sendBroadcast(intent);
						} else {
							Intent intent = new Intent();
							intent.setAction(GOT_USERINFO);
							intent.putExtra("response", Global.INVALID_SESSION);
							context.sendBroadcast(intent);
						}
					}

					@Override
					public void onCancel() {
						// TODO Auto-generated method stub
						
					}
				});
	}
	
	public static void parseJson(String response, Activity context) {
		try {
			JSONObject json = new JSONObject(response);
			Global.userInfo.putString("user_id", json.getString("user_id"));
			Global.userInfo.putString("first_name", json.getString("first_name"));
			Global.userInfo.putString("last_name", json.getString("last_name"));
			Global.userInfo.putString("title", json.getString("title"));
			Global.userInfo.putString("profile_pic_file_path", json.getString("profile_pic_file_path"));
			Global.userInfo.putString("username", json.getString("username"));
			Global.userInfo.putString("availability_status", json.getString("availability_status"));
			
			Intent intent = new Intent();
			intent.setAction(GOT_USERINFO);
			intent.putExtra("response", Global.GOT_RESPONSE);
			context.sendBroadcast(intent);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
}

package com.mangoesmobile.doc2doc.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class Doc2DocRequest {

	public static final int UPLOAD_START = 0;
	public static final int PUBLISH_PROGRESS = 1;
	public static final int UPLOAD_FINISH = 2;

	private final String doc2docUrl = "http://50.18.120.168/doc2doc/index.php/";
//	private final String doc2docUrl = "http://50.18.120.168/doc2doc-test/index.php/";
//	private final String doc2docUrl = "http://50.18.120.168/doc2doc-BETA/index.php/";

	Handler uploadHandler;

	public Doc2DocRequest(Handler handler) {
		uploadHandler = handler;
	}

	public Doc2DocRequest() {
		this(null);
	}

	public String request(String action, Bundle params)
			throws FileNotFoundException, MalformedURLException, IOException {
		String boundary = "====================";
		String endLine = "\r\n";

		OutputStream os;

		String url = doc2docUrl + action;
		Log.d("doc2doc-url", url);
		HttpURLConnection conn = (HttpURLConnection) new URL(url)
				.openConnection();
		conn.setRequestProperty("User-Agent", System.getProperties()
				.getProperty("http.agent") + " Doc2DocAndroidNative");
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "multipart/form-data;boundary="
				+ boundary);
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setRequestProperty("Connection", "Keep-Alive");

		if (params != null) {
			os = conn.getOutputStream();
			os.write(("--" + boundary + endLine).getBytes());
			os.write((encodePostBody(params, boundary)).getBytes());
			os.write((endLine + "--" + boundary + endLine).getBytes());
			for (int i = 1; i < 6; i++) {
				if (params.containsKey("file_" + i)) {
					File file = new File(params.getString("file_" + i));
					os.write(("Content-Disposition: form-data; name=\"file_"
							+ i + "\"; filename=\"" + file.getName() + "\"" + endLine)
							.getBytes());
					os.write(("Content-Type: content/unknown" + endLine + endLine)
							.getBytes());
					// send file name to the front end
					Message msg = Message.obtain();
					msg.what = Doc2DocRequest.UPLOAD_START;
					Bundle data = new Bundle();
					String filename = file.getName();
					data.putString("filename", filename);
					msg.setData(data);
					msg.arg1 = i;
					uploadHandler.sendMessage(msg);
					String ext = filename.substring(filename.lastIndexOf(".") + 1);
					InputStream is;
					Integer totalSize;
					if (ext.equalsIgnoreCase("jpg") || ext.equalsIgnoreCase("bmp") || ext.equalsIgnoreCase("gif") || ext.equalsIgnoreCase("png") || ext.equalsIgnoreCase("tif")) {
						ImageCompression ic = new ImageCompression();
						byte[] img = ic.compressImage(file.getAbsolutePath());
						is = new ByteArrayInputStream(img);
						totalSize = img.length;
					} else {
						is = new FileInputStream(file);
						totalSize = (int) file.length();					
					}
					int uploadedSize = 0;
					byte[] buffer = new byte[1024];
					int bytes_read = 0;
					int counter = -1;
					while ((bytes_read = is.read(buffer)) != -1) {
						os.write(buffer, 0, bytes_read);
						uploadedSize += bytes_read;
						int percentage = (int) ((double) uploadedSize
								/ totalSize * 100);
						Log.d("percentage", percentage + "");
						if (percentage > counter) {
							// Message msg1 = Message.obtain();
							// msg1.what = Doc2DocRequest.PUBLISH_PROGRESS;
							// msg1.arg1 = i;
							// msg1.arg2 = percentage;
							// uploadHandler.sendMessage(msg1);
							counter = percentage;
						}
					}
					is.close();
					os.write((endLine + "--" + boundary + endLine).getBytes());
					msg = Message.obtain();
					msg.what = Doc2DocRequest.UPLOAD_FINISH;
					msg.arg1 = i;
					uploadHandler.sendMessage(msg);
				} else {
					break;
				}
			}
			os.flush();
			os.close();
		}

		String response = read(conn.getInputStream());

		conn.disconnect();

		return response;
	}

	public static String read(InputStream in) throws IOException {
		StringBuilder sb = new StringBuilder();
		BufferedReader r = new BufferedReader(new InputStreamReader(in), 1000);
		for (String line = r.readLine(); line != null; line = r.readLine()) {
			sb.append(line);
		}
		in.close();
		return sb.toString();
	}

	private String encodePostBody(Bundle parameters, String boundary) {
		if (parameters == null)
			return "";
		StringBuilder sb = new StringBuilder();

		for (String key : parameters.keySet()) {
			if (key.startsWith("file_")) {
				continue;
			}

			sb.append("Content-Disposition: form-data; name=\"" + key
					+ "\"\r\n\r\n" + parameters.getString(key));
			sb.append("\r\n" + "--" + boundary + "\r\n");
		}

		return sb.toString();
	}
}

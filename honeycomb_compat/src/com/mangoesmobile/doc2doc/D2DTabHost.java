/**
 * 
 */
package com.mangoesmobile.doc2doc;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.EmptyStackException;
import java.util.Stack;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.ImageColumns;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.mangoesmobile.doc2doc.R;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.contents.NewMessage;
import com.mangoesmobile.doc2doc.services.Doc2DocService;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.UserInfo;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;
import com.mangoesmobile.doc2doc.util.Global;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class D2DTabHost extends FragmentActivity {

	SharedPreferences prefs;
	public static String token = null;
	
	public static SparseArray<Stack<Fragment>> history = new SparseArray<Stack<Fragment>>();
	View board;
	View messages;
	View team;
	View reference;

	int currTab;

	GoogleAnalyticsTracker tracker;

	// configure the notification
	Notification notification;
	NotificationManager notificationManager;

	public static final String REFRESH_BOARD = "REFRESH_BOARD";
	public static final String REFRESH_MESSAGES = "REFRESH_MESSAGES";
	public static final String REFRESH_TEAM = "REFRESH_TEAM";
	public static final String CLOSE_TAB = "CLOSE_TAB";
	public static final String INVALID_SESSION = "INVALID_SESSION";
	public static final String NEW_NOTIFICATION = "NEW_NOTIFICATION";
	private static final int CAMERA_PIC_REQUEST = 0;
	private static final int GALLERY_PIC_REQUEST = 1;

	private ProgressDialog loading;

	private Uri mCapturedImageURI;
	private String path;

	private BroadcastReceiver closeTabReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			// TODO Auto-generated method stub
			finish();
		}
	};

	private BroadcastReceiver invalidSessionReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			// TODO Auto-generated method stub
			interrupt();
		}
	};

	class D2DTabHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GET_DATA:
				Log.d("Messages", "got data from service");
				Bundle bundle = msg.getData();
				Global.total = bundle.getString("total");
				Global.ping = bundle.getString("ping");
				Global.thread = bundle.getString("thread");
				Global.message = bundle.getString("message");
				Intent intent = new Intent(NEW_NOTIFICATION);
				sendBroadcast(intent);
				showNotification();
				break;
			default:
				Log.d("Messages", "in default");
				super.handleMessage(msg);
				break;
			}
		}
	};

	final Messenger mMessenger = new Messenger(new D2DTabHandler());
	Messenger mService;

	private ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			mService = new Messenger(service);
			Message msg = Message.obtain();
			msg.what = Global.MSG_REGISTER_CLIENT;
			msg.replyTo = mMessenger;
			try {
				Log.d("D2DTab", "sending client to service");
				mService.send(msg);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected - process crashed.
			Log.d("D2DTab", "service disconnected");
			mService = null;
		}
	};

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub
		setContentView(R.layout.fragment_host);

		prefs = getSharedPreferences("session", MODE_PRIVATE);
		token = prefs.getString("token", "");
		
		history.put(R.id.tab_board, new Stack<Fragment>());
		history.put(R.id.tab_messages, new Stack<Fragment>());
		history.put(R.id.tab_team, new Stack<Fragment>());
		history.put(R.id.tab_reference, new Stack<Fragment>());

		board = findViewById(R.id.tab_board);
		messages = findViewById(R.id.tab_messages);
		team = findViewById(R.id.tab_team);
		reference = findViewById(R.id.tab_reference);

		tracker = GoogleAnalyticsTracker.getInstance();

		loading = new ProgressDialog(this);
		prepareNotification();

		startService(new Intent(D2DTabHost.this, Doc2DocService.class));

	}

	@Override
	public void onStart() {
		super.onStart();

		// listens for close tabs
		IntentFilter filter = new IntentFilter();
		filter.addAction(CLOSE_TAB);
		registerReceiver(closeTabReceiver, filter);

		// listens for close tabs
		IntentFilter invalidSessionFilter = new IntentFilter();
		invalidSessionFilter.addAction(INVALID_SESSION);
		registerReceiver(invalidSessionReceiver, invalidSessionFilter);
		
		// redirects to message center tab
		if (getIntent().getBooleanExtra("from_notification", false)) {
			TabFragment.btnMessages.performClick();
			TabFragment.tabHost.setCurrentTab(1);
		} else {
			TabFragment.tabHost.setCurrentTab(currTab);
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		bindService(new Intent(this, Doc2DocService.class), mConnection,
				Context.BIND_AUTO_CREATE);

		showNotification();
	}

	@Override
	public void onPause() {
		super.onPause();

		currTab = TabFragment.tabHost.getCurrentTab();

		if (mService != null) {
			try {
				Message msg = Message
						.obtain(null, Global.MSG_UNREGISTER_CLIENT);
				msg.replyTo = mMessenger;
				mService.send(msg);
			} catch (RemoteException e) {
				// There is nothing special we need to do if the service has
				// crashed.
			}
		}
		// Detach our existing connection.
		unbindService(mConnection);
	}

	@Override
	public void onBackPressed() {
		Global.backPressed = true;
		int current = getCurrentVisibleView();
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		try {
			ft.hide(history.get(current).pop());
			ft.show(history.get(current).peek());
		} catch (EmptyStackException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			super.onBackPressed();
		}
		ft.commit();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		loading.dismiss();

		unregisterReceiver(closeTabReceiver);
		unregisterReceiver(invalidSessionReceiver);

		tracker.stopSession();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.app_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_refresh) {
			refresh();
			tracker.trackEvent("Click", "OptionsMenu", "Refresh", 0);
			return true;
		} else if (item.getItemId() == R.id.menu_feedback) {
			sendFeedback();
			tracker.trackEvent("Click", "OptionsMenu", "Send Feedback", 0);
			return true;
		} else if (item.getItemId() == R.id.menu_changepp) {
			changePP();
			tracker.trackEvent("Click", "OptionsMenu", "Change Profile Pic", 0);
			return true;
		} else if (item.getItemId() == R.id.menu_logout) {
			Global.showLoading(loading, "Logging out...", false);
			logout();
			tracker.trackEvent("Click", "OptionsMenu", "Logout", 0);
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	private int getCurrentVisibleView() {
		if (board.isShown())
			return R.id.tab_board;
		else if (messages.isShown())
			return R.id.tab_messages;
		else if (team.isShown())
			return R.id.tab_team;
		else
			return R.id.tab_reference;
	}

	private void showNotification() {
		if (!Global.total.equals("0")
				&& TabFragment.tabHost.getCurrentTab() != 1) {
			TabFragment.lblNotifications.setText(Global.total);
			TabFragment.lblNotifications.setVisibility(View.VISIBLE);
		}
		Global.total = "0";
	}

	private void refresh() {
		Intent broadcast = new Intent();
		switch (TabFragment.tabHost.getCurrentTab()) {
		case 0:
			broadcast.setAction(REFRESH_BOARD);
			sendBroadcast(broadcast);
			Toast.makeText(D2DTabHost.this, "Refreshing Board...",
					Toast.LENGTH_LONG).show();
			break;
		case 1:
			broadcast.setAction(REFRESH_MESSAGES);
			sendBroadcast(broadcast);
			Message msg = Message.obtain();
			msg.what = Global.REFRESH_MESSAGE;
			try {
				mService.send(msg);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Toast.makeText(D2DTabHost.this, "Refreshing Messages...",
					Toast.LENGTH_LONG).show();
			break;
		case 2:
			broadcast.setAction(REFRESH_TEAM);
			sendBroadcast(broadcast);
			Toast.makeText(D2DTabHost.this, "Refreshing Team...",
					Toast.LENGTH_LONG).show();
			break;
		default:
			break;
		}
	}

	public void changePP() {
		showDialog(0);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog = new Dialog(this);
		switch (id) {
		case 0:
			dialog.show();
			break;
		case 1:
			dialog.show();
			break;
		default:
			dialog = null;
		}
		return dialog;
	}

	@Override
	protected void onPrepareDialog(int id, final Dialog dialog) {
		dialog.setContentView(R.layout.profilepic);
		dialog.setTitle("Change Profile Picture");
		switch (id) {
		case 0: {
			ImageView imgPhoto = (ImageView) dialog
					.findViewById(R.id.img_profilepic);
			if (Global.profilePic == null) {
				imgPhoto.setImageResource(R.drawable.default_photo);
			} else {
				imgPhoto.setImageDrawable(Global.profilePic);
			}
			Button btnChoose = (Button) dialog
					.findViewById(R.id.btn_choosephoto);
			btnChoose.setText("New Photo");
			btnChoose.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					chooseSource(dialog);
				}
			});
		}
			break;
		case 1: {
			ImageView imgPhoto = (ImageView) dialog
					.findViewById(R.id.img_profilepic);
			Drawable d = Drawable.createFromPath(path);
			imgPhoto.setImageDrawable(d);
			Button btnChoose = (Button) dialog
					.findViewById(R.id.btn_choosephoto);
			btnChoose.setText("Update Photo");
			btnChoose.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					updatePhoto();
				}
			});
		}
			break;
		}
		super.onPrepareDialog(id, dialog);
	}

	public void chooseSource(final Dialog photoDialog) {
		final CharSequence[] items = { "Camera", "Gallery" };
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Pick Source");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				photoDialog.dismiss();
				switch (item) {
				case (0):
					takePhoto();
					break;
				case (1):
					galleryPhoto();
					break;
				}
				dialog.dismiss();
			}
		});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	private void takePhoto() {
		String fileName = System.currentTimeMillis() + ".jpg";
		ContentValues values = new ContentValues();
		values.put(MediaColumns.TITLE, fileName);
		values.put(
				ImageColumns.ORIENTATION,
				getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ? 0
						: 90);
		mCapturedImageURI = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

		Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		cameraIntent.putExtra("crop", "true");
		cameraIntent.putExtra("aspectX", 1);
		cameraIntent.putExtra("aspectY", 1);
		cameraIntent.putExtra("outputX", 96);
		cameraIntent.putExtra("outputY", 96);
		cameraIntent.putExtra("return-data", "false");
		cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
		startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
	}

	private void galleryPhoto() {
		String fileName = System.currentTimeMillis() + ".jpg";
		ContentValues values = new ContentValues();
		values.put(MediaColumns.TITLE, fileName);
		values.put(
				ImageColumns.ORIENTATION,
				getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ? 0
						: 90);
		mCapturedImageURI = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
		galleryIntent.setType("image/*");
		galleryIntent.putExtra("crop", "true");
		galleryIntent.putExtra("aspectX", 1);
		galleryIntent.putExtra("aspectY", 1);
		galleryIntent.putExtra("outputX", 96);
		galleryIntent.putExtra("outputY", 96);
		galleryIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
		galleryIntent.putExtra("return-data", "false");
		startActivityForResult(
				Intent.createChooser(galleryIntent, "Select Picture"),
				GALLERY_PIC_REQUEST);
	}

	private void sendFeedback() {
		int curr = getCurrentVisibleView();
		Bundle args = new Bundle();
		args.putBoolean("has_recipient", true);
		args.putBoolean("feedback", true);
		args.putString("tagged_users", "1");
		args.putString("tagged_user_names", "To: Admiral Admin");
		args.putString("subject", "Feedback on Doc2Doc");
		NewMessage nm = NewMessage.newInstance(curr, args);
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.hide(history.get(curr).peek());
		ft.add(curr, nm);
		ft.commit();
		history.get(curr).push(nm);
		// Intent feedbackIntent = new Intent(Intent.ACTION_SEND);
		// feedbackIntent.setType("message/rfc822");
		// feedbackIntent.putExtra(Intent.EXTRA_EMAIL,
		// new String[] { "doc2doc@mangoesmobile.com" });
		// feedbackIntent.putExtra(Intent.EXTRA_SUBJECT,
		// "Feedback on Doc2Doc - "
		// + Global.userInfo.getString("username"));
		// startActivity(Intent.createChooser(feedbackIntent, "Send Feedback"));
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == CAMERA_PIC_REQUEST) {
				path = getPath(mCapturedImageURI);
				mCapturedImageURI = null;
				showDialog(1);
			} else if (requestCode == GALLERY_PIC_REQUEST) {
				path = getPath(mCapturedImageURI);
				mCapturedImageURI = null;
				showDialog(1);
			}
		}

	}

	// returns the real path from Uri (only applicable for gallery media)
	private String getPath(Uri uri) {
		String[] projection = { MediaColumns.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
		cursor.moveToFirst();
		String path = cursor.getString(column_index);
		cursor.close();
		return path;
	}

	private void updatePhoto() {
		Log.d("TabHost", "changing profile pic");
		// Global.showLoading(loading, "Changing profile pic...", false);
		Toast.makeText(D2DTabHost.this, "Changing profile pic...",
				Toast.LENGTH_SHORT).show();
		Doc2DocRequest dr = new Doc2DocRequest(uploadHandler);
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", token);
		params.putString("file_1", path);

		asyncTask.runTask("user/set_profile_pic", params,
				new Doc2DocTaskListener() {

					@Override
					public void onMalformedURLException(MalformedURLException e) {
						// TODO Auto-generated method stub
						profilePicHandler.sendMessage(Message.obtain(
								profilePicHandler, Global.ERROR_RESPONSE));
						Log.d("board-response", e.getMessage());
					}

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						profilePicHandler.sendMessage(Message.obtain(
								profilePicHandler, Global.CONNECTION_ERROR));
						Log.d("board-response", e.getMessage());
					}

					@Override
					public void onFileNotFoundException(FileNotFoundException e) {
						// TODO Auto-generated method stub
						profilePicHandler.sendMessage(Message.obtain(
								profilePicHandler, Global.ERROR_RESPONSE));
						Log.d("board-response", e.getMessage());
					}

					@Override
					public void onComplete(String response) {
						Log.d("board-response", response);
						if (response.equals("TRUE")) {
							profilePicHandler.sendMessage(Message.obtain(
									profilePicHandler, Global.GOT_RESPONSE));
						} else {
							profilePicHandler.sendMessage(Message.obtain(
									profilePicHandler, Global.INVALID_SESSION));
						}
					}

					@Override
					public void onCancel() {
						// TODO Auto-generated method stub
						
					}
				});
	}

	private Handler profilePicHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				// Global.hideLoading(loading);
				Toast.makeText(D2DTabHost.this,
						"Profile picture updated successfully",
						Toast.LENGTH_LONG).show();
				Global.profilePic = null;
				Intent intent = new Intent(UserInfo.GOT_USERINFO);
				intent.putExtra("response", Global.CHANGE_PIC);
				sendBroadcast(intent);
				break;
			case Global.EMPTY_RESPONSE:
				// Global.hideLoading(loading);
				break;
			case Global.CONNECTION_ERROR:
				// Global.hideLoading(loading);
				Toast.makeText(D2DTabHost.this,
						"Connection error! Please retry", Toast.LENGTH_LONG)
						.show();
				break;
			case Global.ERROR_RESPONSE:
				// Global.hideLoading(loading);
				break;
			case Global.INVALID_SESSION:
				// Global.hideLoading(loading);
				Toast.makeText(D2DTabHost.this,
						"Invalid session! Please relogin", Toast.LENGTH_LONG)
						.show();
				interrupt();
				break;
			}
		}
	};

	public void logout() {
		Doc2DocRequest dr = new Doc2DocRequest();
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", token);

		asyncTask.runTask("authenticate/logout", params,
				new Doc2DocTaskListener() {

					@Override
					public void onMalformedURLException(MalformedURLException e) {
						// TODO Auto-generated method stub
						logoutHandler.sendMessage(Message.obtain(logoutHandler,
								Global.ERROR_RESPONSE));
						e.printStackTrace();
					}

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						logoutHandler.sendMessage(Message.obtain(logoutHandler,
								Global.CONNECTION_ERROR));
						e.printStackTrace();
					}

					@Override
					public void onFileNotFoundException(FileNotFoundException e) {
						// TODO Auto-generated method stub
						logoutHandler.sendMessage(Message.obtain(logoutHandler,
								Global.ERROR_RESPONSE));
						e.printStackTrace();
					}

					@Override
					public void onComplete(String response) {
						if (response.equals("FALSE")) {
							logoutHandler.sendMessage(Message.obtain(
									logoutHandler, Global.INVALID_SESSION));
						} else {
							logoutHandler.sendMessage(Message.obtain(
									logoutHandler, Global.GOT_RESPONSE));
						}
						Log.d("logout-response", response);
					}

					@Override
					public void onCancel() {
						// TODO Auto-generated method stub
						
					}
				});
	}

	private Handler logoutHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				Global.hideLoading(loading);
				stopService(new Intent(D2DTabHost.this, Doc2DocService.class));
				finish();
				startActivity(new Intent(D2DTabHost.this, Login.class));
				token = "";
				Editor e = getSharedPreferences("session", MODE_PRIVATE).edit();
				e.putString("token", "");
				e.commit();
				Global.userInfo = new Bundle();
				Global.userStatus = "";
				Global.profilePic = null;
				tracker.stopSession();
				break;
			case Global.EMPTY_RESPONSE:
				Global.hideLoading(loading);
				break;
			case Global.INVALID_SESSION:
				Global.hideLoading(loading);
				Toast.makeText(D2DTabHost.this,
						"Invalid session! Please relogin", Toast.LENGTH_LONG)
						.show();
				interrupt();
				break;
			case Global.CONNECTION_ERROR:
				Global.hideLoading(loading);
				Toast.makeText(D2DTabHost.this,
						"Connection error! Please retry", Toast.LENGTH_LONG)
						.show();
				break;
			case Global.ERROR_RESPONSE:
				Global.hideLoading(loading);
				break;
			}
		}
	};

	private void interrupt() {
		stopService(new Intent(D2DTabHost.this, Doc2DocService.class));
		finish();
		startActivity(new Intent(D2DTabHost.this, Login.class));
		token = "";
		Editor e = getSharedPreferences("session", MODE_PRIVATE).edit();
		e.putString("token", "");
		e.commit();
		Global.userInfo = new Bundle();
		Global.userStatus = "";
		Global.profilePic = null;
		tracker.stopSession();
	}

	private void prepareNotification() {
		notification = new Notification(android.R.drawable.stat_sys_upload,
				"Uploading file", System.currentTimeMillis());
		notification.flags = notification.flags
				| Notification.FLAG_ONGOING_EVENT;
		notification.contentView = new RemoteViews(getApplicationContext()
				.getPackageName(), R.layout.update_notification);
		PendingIntent contentIntent = PendingIntent.getActivity(
				getApplicationContext(), 0, new Intent(),
				PendingIntent.FLAG_UPDATE_CURRENT);
		notification.contentIntent = contentIntent;
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	}

	private void updateProgress(int file) {
		notificationManager.notify(file, notification);
	}

	private Handler uploadHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Doc2DocRequest.UPLOAD_START:
				notification.contentView.setTextViewText(R.id.text,
						"Uploading " + msg.getData().getString("filename"));
				updateProgress(msg.arg1);
				break;
			case Doc2DocRequest.PUBLISH_PROGRESS:
				updateProgress(msg.arg1);
				break;
			case Doc2DocRequest.UPLOAD_FINISH:
				notificationManager.cancel(msg.arg1);
			default:
				break;
			}
		}
	};

}

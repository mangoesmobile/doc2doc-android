/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.adapters.MessagesGridAdapter;
import com.mangoesmobile.doc2doc.cache.DraftManager;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.ImageLoader;
import com.mangoesmobile.doc2doc.util.UserInfo;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class Messages extends Fragment implements OnClickListener,
		OnItemClickListener {

	private int tag;

	GoogleAnalyticsTracker tracker;

	public static final int MY_THREADS = 0;
	public static final int THREAD_DRAFTS = 1;
	public static final int INBOX = 2;
	public static final int SENT = 3;
	public static final int MESSAGE_DRAFTS = 4;
	public static final int PINGS = 5;

	private ImageView imgUser;
	private ProgressBar progUser;
	private ImageView imgUserStatus;
	private Button btnLocation;
	private TextView lblUsername;
	private TextView lblUserStatus;
	private Button btnPost;
	private GridView gridButtons;

	private Bundle data;
	private MessagesGridAdapter adapter;

	BroadcastReceiver userInfoReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			int response = intent
					.getIntExtra("response", Global.ERROR_RESPONSE);
			switch (response) {
			case Global.GOT_RESPONSE:
				updateUserInfo();
				break;
			case Global.CHANGE_STATUS:
				Global.userStatus = intent.getStringExtra("userStatus");
				Global.userInfo.putString("availability_status",
						Global.userStatus);
				updateUserInfo();
				break;
			case Global.CHANGE_PIC:
				fetchUserInfo();
				break;
			case Global.EMPTY_RESPONSE:
				break;
			case Global.CONNECTION_ERROR:
				Toast.makeText(getActivity(), "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				break;
			case Global.ERROR_RESPONSE:
				break;
			case Global.INVALID_SESSION:
				Toast.makeText(getActivity(),
						"Invalid session! Please relogin", Toast.LENGTH_LONG)
						.show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				getActivity().sendBroadcast(i);
				break;
			}
		}
	};

	BroadcastReceiver notificationreciver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			updateNotification();
		}
	};

	public Messages(int tag) {
		this.tag = tag;
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("Message Center");

		generateButtons();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.messages, container, false);
		instantiateLayout(v);
		return v;
	}

	@Override
	public void onStart() {
		super.onStart();

		// listens for update of user info
		IntentFilter userFilter = new IntentFilter();
		userFilter.addAction(UserInfo.GOT_USERINFO);
		getActivity().registerReceiver(userInfoReceiver, userFilter);

		// listens for update of user info
		IntentFilter notificationFilter = new IntentFilter();
		notificationFilter.addAction(D2DTabHost.NEW_NOTIFICATION);
		getActivity().registerReceiver(notificationreciver, notificationFilter);

		if (Global.userInfo.size() == 0) {
			fetchUserInfo();
		} else {
			updateUserInfo();
		}

		imgUser.setOnClickListener(this);
		btnLocation.setOnClickListener(this);
		btnPost.setOnClickListener(this);

		gridButtons.setAdapter(adapter);
		adapter.setData(data);
		gridButtons.setOnItemClickListener(this);
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (Global.backPressed) {
			Global.backPressed = false;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		getActivity().unregisterReceiver(userInfoReceiver);
		getActivity().unregisterReceiver(notificationreciver);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_messages_post) {
			NewThread nt = NewThread.newInstance(tag, null);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, nt);
			ft.commit();
			D2DTabHost.history.get(tag).push(nt);
			tracker.trackEvent("Click", "Button", "Create New Thread", 0);
		} else if (v.getId() == R.id.img_messages_userimage) {
			Toast.makeText(getActivity(), "Changing status...",
					Toast.LENGTH_LONG).show();
			UserInfo.changeStatus(getActivity(),
					Global.userStatus.equals("online") ? "on call" : "online", D2DTabHost.token);
			tracker.trackEvent("Click", "ImageView", "Change Status", 0);
		} else if (v.getId() == R.id.btn_messages_location) {
			Intent intent = new Intent(getActivity(), UserLocation.class);
			intent.putExtra("update_own", true);
			getActivity().startActivity(intent);
			tracker.trackEvent("Click", "Button", "Show Own Location", 0);
		}
	}

	private void instantiateLayout(View v) {
		imgUser = (ImageView) v.findViewById(R.id.img_messages_userimage);
		progUser = (ProgressBar) v.findViewById(R.id.prog_messages_userimage);
		imgUserStatus = (ImageView) v
				.findViewById(R.id.img_messages_userstatus);
		btnLocation = (Button) v.findViewById(R.id.btn_messages_location);
		lblUsername = (TextView) v.findViewById(R.id.lbl_messages_username);
		lblUserStatus = (TextView) v.findViewById(R.id.lbl_messages_userstatus);
		btnPost = (Button) v.findViewById(R.id.btn_messages_post);
		gridButtons = (GridView) v.findViewById(R.id.grid_messages);
		adapter = new MessagesGridAdapter(getActivity());
	}

	private void generateButtons() {
		final int[] icons = { R.drawable.btn_threads_click,
				R.drawable.btn_message_click, R.drawable.btn_ping_click,
				R.drawable.btn_drafts_click, R.drawable.btn_swinfen_click };
		final int[] badge = { R.drawable.notification_badge,
				R.drawable.notification_badge, R.drawable.notification_badge,
				R.drawable.notification_badge_purple,
				R.drawable.notification_badge };
		final String[] titles = { "Public", "Private", "Pings", "Drafts",
				"Swinfen" };
		final String[] notifications = { Global.thread, Global.message,
				Global.ping, String.valueOf(getTotalNoOfDrafts()), null };
		data = new Bundle();
		for (int i = 0; i < icons.length; i++) {
			Bundle content = new Bundle();
			content.putInt("icon", icons[i]);
			content.putInt("badge", badge[i]);
			content.putString("title", titles[i]);
			content.putString("notifications", notifications[i]);
			data.putBundle(String.valueOf(i), content);
		}
	}

	public long getTotalNoOfDrafts() {
		DraftManager boardManager = new DraftManager(getActivity(),
				"thread_draft");
		boardManager.open();
		long boardSize = boardManager.getCount();
		boardManager.close();

		DraftManager messageManager = new DraftManager(getActivity(),
				"message_draft");
		messageManager.open();
		long messageSize = messageManager.getCount();
		messageManager.close();

		return boardSize + messageSize;
	}

	private void updateNotification() {
		if (!Global.thread.equals("0")) {
			// TODO Auto-generated method stub
			Bundle content = data.getBundle("0");
			content.putString("notifications", Global.thread);
		}
		if (!Global.message.equals("0")) {
			Bundle content = data.getBundle("1");
			content.putString("notifications", Global.message);
		}
		if (!Global.ping.equals("0")) {
			Bundle content = data.getBundle("2");
			content.putString("notifications", Global.ping);
		}
		long totalDrafts = getTotalNoOfDrafts();
		if (totalDrafts > 0) {
			Bundle content = data.getBundle("3");
			content.putString("notifications", String.valueOf(totalDrafts));
		}
		adapter.setData(data);
	}

	private void fetchUserInfo() {
		UserInfo.fetchUserInfo(getActivity(), D2DTabHost.token);
	}

	private void updateUserInfo() {
		String userStatus = Global.userInfo.getString("availability_status");
		Log.d("user-status", userStatus);
		if (userStatus.equals("online")) {
			imgUserStatus.setImageResource(R.drawable.bg_photo_online);
			lblUserStatus.setTextColor(Color.rgb(0, 102, 153));
		} else if (userStatus.equals("on call")) {
			imgUserStatus.setImageResource(R.drawable.bg_photo_oncall);
			lblUserStatus.setTextColor(Color.rgb(101, 176, 73));
		}
		lblUserStatus.setText(userStatus.toUpperCase());
		lblUsername.setText(Global.userInfo.getString("first_name").substring(0, 1)
				+ ". " + Global.userInfo.getString("last_name"));
		if (Global.profilePic == null) {
			// Global.loadPhoto(Messages.this, Global.url +
			// Global.userInfo.getString("profile_pic_file_path"), imgUser,
			// progUser);
			ImageLoader il = new ImageLoader(getActivity());
			il.DisplayImage(
					Global.url
							+ Global.userInfo
									.getString("profile_pic_file_path"),
					imgUser, progUser);
		} else {
			imgUser.setImageDrawable(Global.profilePic);
			progUser.setVisibility(View.GONE);
		}
	}

	private void chooseMessageOption() {
		final CharSequence[] items = { "New Message", "Inbox", "Sent" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Messages");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				Bundle args = new Bundle();
				Fragment f;
				FragmentTransaction ft;
				switch (item) {
				case 0:
					args.putBoolean("has_recipient", false);
					f = NewMessage.newInstance(tag, args);
					ft = getFragmentManager().beginTransaction();
					ft.hide(D2DTabHost.history.get(tag).peek());
					ft.add(tag, f);
					ft.commit();
					D2DTabHost.history.get(tag).push(f);
					// intent = new Intent(Messages.this, NewMessage.class);
					// intent.putExtra("has_recipient", false);
					// parent.startChildActivity("new_message", intent);
					tracker.trackEvent("Click", "Button", "Create New Message",
							0);
					break;
				case 1:
					args.putInt("command", INBOX);
					args.putString("title", "Inbox");
					f = Notifications.newInstance(tag, args);
					ft = getFragmentManager().beginTransaction();
					ft.hide(D2DTabHost.history.get(tag).peek());
					ft.add(tag, f);
					ft.commit();
					D2DTabHost.history.get(tag).push(f);
					// intent = new Intent(Messages.this,
					// Notifications.class);
					// intent.putExtra("command", INBOX);
					// intent.putExtra("title", "Inbox");
					// parent.startChildActivity("inbox", intent);
					Bundle content = data.getBundle("1");
					content.putString("notifications", Global.message);
					adapter.setData(data);
					tracker.trackEvent("Click", "Button", "Show Inbox", 0);
					break;
				case 2:
					args.putInt("command", SENT);
					args.putString("title", "Sent");
					f = Notifications.newInstance(tag, args);
					ft = getFragmentManager().beginTransaction();
					ft.hide(D2DTabHost.history.get(tag).peek());
					ft.add(tag, f);
					ft.commit();
					D2DTabHost.history.get(tag).push(f);
					// intent = new Intent(Messages.this,
					// Notifications.class);
					// intent.putExtra("command", SENT);
					// intent.putExtra("title", "Sent");
					// parent.startChildActivity("sent", intent);
					tracker.trackEvent("Click", "Button", "Show Sentbox", 0);
					break;
				}
			}
		});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	private void chooseDraftOption() {
		DraftManager boardManager = new DraftManager(getActivity(),
				"thread_draft");
		boardManager.open();
		long boardSize = boardManager.getCount();
		boardManager.close();

		DraftManager messageManager = new DraftManager(getActivity(),
				"message_draft");
		messageManager.open();
		long messageSize = messageManager.getCount();
		messageManager.close();
		String board = "Public" + (boardSize > 0 ? " (" + boardSize + ")" : "");
		String message = "Private"
				+ (messageSize > 0 ? " (" + messageSize + ")" : "");
		final CharSequence[] items = { board, message };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Drafts");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				Bundle args = new Bundle();
				Fragment f;
				FragmentTransaction ft;
				switch (item) {
				case 0:
					args.putInt("command", THREAD_DRAFTS);
					args.putString("title", "Thread Drafts");
					f = Notifications.newInstance(tag, args);
					ft = getFragmentManager().beginTransaction();
					ft.hide(D2DTabHost.history.get(tag).peek());
					ft.add(tag, f);
					ft.commit();
					D2DTabHost.history.get(tag).push(f);
					// intent = new Intent(Messages.this, Notifications.class);
					// intent.putExtra("command", THREAD_DRAFTS);
					// intent.putExtra("title", "Thread Drafts");
					// parent.startChildActivity("thread_drafts", intent);
					tracker.trackEvent("Click", "Button", "Show Thread Drafts",
							0);
					break;
				case 1:
					args.putInt("command", MESSAGE_DRAFTS);
					args.putString("title", "Message Drafts");
					f = Notifications.newInstance(tag, args);
					ft = getFragmentManager().beginTransaction();
					ft.hide(D2DTabHost.history.get(tag).peek());
					ft.add(tag, f);
					ft.commit();
					D2DTabHost.history.get(tag).push(f);
					// intent = new Intent(Messages.this, Notifications.class);
					// intent.putExtra("command", MESSAGE_DRAFTS);
					// intent.putExtra("title", "Message Drafts");
					// parent.startChildActivity("message_drafts", intent);
					tracker.trackEvent("Click", "Button",
							"Show Message Drafts", 0);
					break;
				}
			}
		});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		Bundle args = new Bundle();
		Fragment f;
		FragmentTransaction ft;
		Bundle content;
		switch (arg2) {
		case 0:
			Global.thread = "0";
			args.putInt("command", MY_THREADS);
			args.putString("title", "My Threads");
			f = Notifications.newInstance(tag, args);
			ft = getFragmentManager().beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, f);
			ft.commit();
			D2DTabHost.history.get(tag).push(f);
			// intent = new Intent(Messages.this, Notifications.class);
			// intent.putExtra("command", MY_THREADS);
			// intent.putExtra("title", "My Threads");
			// parent.startChildActivity("my_threads", intent);
			content = data.getBundle("0");
			content.putString("notifications", Global.thread);
			adapter.setData(data);
			tracker.trackEvent("Click", "Button", "Show My Threads", 0);
			break;
		case 1:
			if (Global.message.equals("0")) {
				chooseMessageOption();
			} else {
				Global.message = "0";
				args.putInt("command", INBOX);
				args.putString("title", "Inbox");
				f = Notifications.newInstance(tag, args);
				ft = getFragmentManager().beginTransaction();
				ft.hide(D2DTabHost.history.get(tag).peek());
				ft.add(tag, f);
				ft.commit();
				D2DTabHost.history.get(tag).push(f);
				// intent = new Intent(Messages.this, Notifications.class);
				// intent.putExtra("command", INBOX);
				// intent.putExtra("title", "Inbox");
				// parent.startChildActivity("inbox", intent);
				content = data.getBundle("1");
				content.putString("notifications", Global.message);
				adapter.setData(data);
				tracker.trackEvent("Click", "Button", "Show Inbox", 0);
			}
			break;
		case 2:
			Global.ping = "0";
			args.putInt("command", PINGS);
			args.putString("title", "Pings");
			f = Notifications.newInstance(tag, args);
			ft = getFragmentManager().beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, f);
			ft.commit();
			D2DTabHost.history.get(tag).push(f);
			// intent = new Intent(Messages.this, Notifications.class);
			// intent.putExtra("command", PINGS);
			// intent.putExtra("title", "Pings");
			// parent.startChildActivity("my_threads", intent);
			content = data.getBundle("2");
			content.putString("notifications", Global.ping);
			adapter.setData(data);
			tracker.trackEvent("Click", "Button", "Show My Pings", 0);
			break;
		case 3:
			content = data.getBundle("3");
			content.putString("notifications", "0");
			adapter.setData(data);
			chooseDraftOption();
			break;
		case 4:
			Intent intent = new Intent();
			intent.setClassName("com.mangoesmobile.swinfen",
					"com.mangoesmobile.swinfen.SwinfenLauncher");
			intent.putExtra("from_doc2doc", true);
			try {
				getActivity().startActivity(intent);
				tracker.trackEvent("Click", "Button", "Launch Swinfen", 0);
			} catch (ActivityNotFoundException e) {
				// TODO Auto-generated catch block
				Toast.makeText(getActivity(), "Swinfen is not installed", Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			}
			break;
		}
	}

}

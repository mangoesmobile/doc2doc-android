/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import com.foound.widget.AmazingListView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.adapters.TeamAdapter;
import com.mangoesmobile.doc2doc.cache.TeamCacheManager;
import com.mangoesmobile.doc2doc.services.TeamService;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.ImageLoader;
import com.mangoesmobile.doc2doc.util.UserInfo;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class Team extends Fragment implements OnClickListener,
		OnCheckedChangeListener {

	private int tag;

	GoogleAnalyticsTracker tracker;

	Animation searchAnimEntrance;
	Animation searchAnimExit;

	private ImageView imgUser;
	private ProgressBar progUser;
	private ImageView imgUserStatus;
	private TextView lblUsername;
	private TextView lblUserStatus;
	private Button btnLocation;
	private Button btnPost;
	private ImageView imgTopBar;
	private EditText txtSearch;
	private CheckBox chkSearch;
	private AmazingListView listTeam;
	private TeamAdapter adapter;

	class TeamHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GET_DATA:
				Log.d("Board", "got data from service");
				adapter.setData(msg.getData());
				break;
			default:
				Log.d("Board", "in default");
				super.handleMessage(msg);
				break;
			}
		}
	};

	final Messenger mMessenger = new Messenger(new TeamHandler());
	Messenger mService;

	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			mService = new Messenger(service);
			Message msg = Message.obtain();
			msg.what = Global.MSG_REGISTER_CLIENT;
			msg.replyTo = mMessenger;
			try {
				Log.d("Board", "sending client to service");
				mService.send(msg);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected - process crashed.
			Log.d("Board", "service disconnected");
			mService = null;
		}
	};

	private BroadcastReceiver receiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			// TODO Auto-generated method stub
			refreshTeam();
		}
	};

	BroadcastReceiver userInfoReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			int response = intent
					.getIntExtra("response", Global.ERROR_RESPONSE);
			switch (response) {
			case Global.GOT_RESPONSE:
				updateUserInfo();
				break;
			case Global.CHANGE_STATUS:
				Global.userStatus = intent.getStringExtra("userStatus");
				Global.userInfo.putString("availability_status",
						Global.userStatus);
				updateUserInfo();
				break;
			case Global.CHANGE_PIC:
				fetchUserInfo();
				refreshTeam();
				break;
			case Global.EMPTY_RESPONSE:
				break;
			case Global.CONNECTION_ERROR:
				Toast.makeText(getActivity(), "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				break;
			case Global.ERROR_RESPONSE:
				break;
			case Global.INVALID_SESSION:
				Toast.makeText(getActivity(),
						"Invalid session! Please relogin", Toast.LENGTH_LONG)
						.show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				getActivity().sendBroadcast(i);
				break;
			}
		}
	};

	public Team(int tag) {
		this.tag = tag;
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("Team");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.team, container, false);
		instantiateLayout(v);
		return v;
	}

	@Override
	public void onStart() {
		super.onStart();

		getActivity().getApplicationContext().bindService(
				new Intent(getActivity(), TeamService.class), mConnection,
				Context.BIND_AUTO_CREATE);

		// listens for refresh from tab menu
		IntentFilter filter = new IntentFilter();
		filter.addAction(D2DTabHost.REFRESH_TEAM);
		getActivity().registerReceiver(receiver, filter);

		// listens for update of user info
		IntentFilter userFilter = new IntentFilter();
		userFilter.addAction(UserInfo.GOT_USERINFO);
		getActivity().registerReceiver(userInfoReceiver, userFilter);

		if (Global.userInfo.size() == 0) {
			fetchUserInfo();
		} else {
			updateUserInfo();
		}

		imgUser.setOnClickListener(this);
		chkSearch.setOnCheckedChangeListener(this);
		btnLocation.setOnClickListener(this);
		btnPost.setOnClickListener(this);
		txtSearch.addTextChangedListener(new TeamTextWatcher());

		listTeam.setLoadingView(getActivity().getLayoutInflater().inflate(
				R.layout.loading_view, null));
		listTeam.setPinnedHeaderView(getActivity().getLayoutInflater().inflate(
				R.layout.teamlist_header, listTeam, false));
		listTeam.setAdapter(adapter);
		fetchTeamFromCache();
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (Global.backPressed) {
			Global.backPressed = false;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		getActivity().getApplicationContext().unbindService(mConnection);
		getActivity().unregisterReceiver(receiver);
		getActivity().unregisterReceiver(userInfoReceiver);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_team_post) {
			NewThread nt = NewThread.newInstance(tag, null);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, nt);
			ft.commit();
			D2DTabHost.history.get(tag).push(nt);
			tracker.trackEvent("Click", "Button", "Create New Thread", 0);
		} else if (v.getId() == R.id.img_team_userimage) {
			Toast.makeText(getActivity(), "Changing status...",
					Toast.LENGTH_LONG).show();
			UserInfo.changeStatus(getActivity(),
					Global.userStatus.equals("online") ? "on call" : "online", D2DTabHost.token);
			tracker.trackEvent("Click", "ImageView", "Change Status", 0);
		} else if (v.getId() == R.id.btn_team_location) {
			Intent intent = new Intent(getActivity(), UserLocation.class);
			intent.putExtra("update_own", true);
			intent.putExtra("update_team", true);
			intent.putExtra("data", adapter.getData());
//			intent.putStringArrayListExtra("id", adapter.getIds());
			getActivity().startActivity(intent);
			tracker.trackEvent("Click", "Button", "Show Team Location", 0);
		}
	}

	private void instantiateLayout(View v) {
		searchAnimEntrance = AnimationUtils.loadAnimation(getActivity(),
				R.anim.searchbox_entrance);
		searchAnimExit = AnimationUtils.loadAnimation(getActivity(),
				R.anim.searchbox_exit);

		imgUser = (ImageView) v.findViewById(R.id.img_team_userimage);
		progUser = (ProgressBar) v.findViewById(R.id.prog_team_userimage);
		imgUserStatus = (ImageView) v.findViewById(R.id.img_team_userstatus);
		imgTopBar = (ImageView) v.findViewById(R.id.img_team_topbar);
		txtSearch = (EditText) v.findViewById(R.id.txt_team_search);
		chkSearch = (CheckBox) v.findViewById(R.id.chk_team_search);
		lblUsername = (TextView) v.findViewById(R.id.lbl_team_username);
		lblUserStatus = (TextView) v.findViewById(R.id.lbl_team_userstatus);
		btnLocation = (Button) v.findViewById(R.id.btn_team_location);
		btnPost = (Button) v.findViewById(R.id.btn_team_post);
		listTeam = (AmazingListView) v.findViewById(R.id.ls_team);
		adapter = new TeamAdapter(getActivity(), tag);
	}

	// sends message to service to refresh the board
	private void refreshTeam() {
		Message msg = Message.obtain();
		msg.what = Global.REFRESH_TEAM;
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void fetchTeamFromCache() {
		TeamCacheManager manager = new TeamCacheManager(getActivity());
		manager.open();
		Cursor cursor = manager.fetchAll();
		if (cursor.getCount() > 0) {
			Bundle data = new Bundle();
			cursor.moveToFirst();
			for (int i = 0; i < cursor.getCount(); i++) {
				Bundle content = new Bundle();
				content.putString("user_id", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.USER_ID)));
				content.putString("first_name", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.FIRST_NAME)));
				content.putString("last_name", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.LAST_NAME)));
				content.putString("title", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.TITLE)));
				content.putString("email", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.EMAIL)));
				content.putString("phone", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.PHONE)));
				content.putString("profile_pic_file_path", cursor
						.getString(cursor
								.getColumnIndex(TeamCacheManager.PROFILE_PIC)));
				content.putString("organization", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.ORGANIZATION)));
				content.putString("specialization", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.SPECIALIZATION)));
				content.putString("username", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.USERNAME)));
				content.putString(
						"availability_status",
						cursor.getString(cursor
								.getColumnIndex(TeamCacheManager.AVAILABILITY_STATUS)));
				content.putString("latitude", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.LATITUDE)));
				content.putString("longitude", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.LONGITUDE)));
				content.putString("base_location", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.BASE_LOCATION)));
				data.putBundle(String.valueOf(i), content);
				cursor.moveToNext();
			}
			adapter.setData(data);
		} else {
			adapter.notifyMayHaveMorePages();
		}
		cursor.close();
		manager.close();
	}

	private void fetchUserInfo() {
		UserInfo.fetchUserInfo(getActivity(), D2DTabHost.token);
	}

	private void updateUserInfo() {
		String userStatus = Global.userInfo.getString("availability_status");
		Log.d("user-status", userStatus);
		if (userStatus.equals("online")) {
			imgUserStatus.setImageResource(R.drawable.bg_photo_online);
			lblUserStatus.setTextColor(Color.rgb(0, 102, 153));
		} else if (userStatus.equals("on call")) {
			imgUserStatus.setImageResource(R.drawable.bg_photo_oncall);
			lblUserStatus.setTextColor(Color.rgb(101, 176, 73));
		}
		lblUserStatus.setText(userStatus.toUpperCase());
		lblUsername.setText(Global.userInfo.getString("first_name").substring(0, 1)
				+ ". " + Global.userInfo.getString("last_name"));
		if (Global.profilePic == null) {
			// Global.loadPhoto(
			// Team.this,
			// Global.url
			// + Global.userInfo
			// .getString("profile_pic_file_path"),
			// imgUser, progUser);
			ImageLoader il = new ImageLoader(getActivity());
			il.DisplayImage(
					Global.url
							+ Global.userInfo
									.getString("profile_pic_file_path"),
					imgUser, progUser);
		} else {
			imgUser.setImageDrawable(Global.profilePic);
			progUser.setVisibility(View.GONE);
		}
	}

	class TeamTextWatcher implements TextWatcher {

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			adapter.getFilter().filter(s);
			tracker.trackEvent("TextChange", "EditText", "Search Team", 0);
		}

	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		if (isChecked) {
			imgTopBar.setImageResource(R.drawable.bg_top_search);
			txtSearch.setVisibility(View.VISIBLE);
			txtSearch.startAnimation(searchAnimEntrance);
			txtSearch.requestFocus();
			((InputMethodManager) getActivity().getSystemService(
					Context.INPUT_METHOD_SERVICE)).showSoftInput(txtSearch,
					InputMethodManager.SHOW_FORCED);
		} else {
			txtSearch.startAnimation(searchAnimExit);
			txtSearch.setText("");
			searchAnimExit
					.setAnimationListener(new Animation.AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationRepeat(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationEnd(Animation animation) {
							// TODO Auto-generated method stub
							imgTopBar.setImageResource(R.drawable.bg_top);
							txtSearch.setVisibility(View.GONE);
						}
					});
			((InputMethodManager) getActivity().getSystemService(
					Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
					txtSearch.getWindowToken(), 0);
		}
	}
}

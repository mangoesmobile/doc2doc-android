/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.foound.widget.AmazingListView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.adapters.TagAdapter;
import com.mangoesmobile.doc2doc.cache.TeamCacheManager;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ListFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class NotifyTeam extends ListFragment implements OnClickListener {

	int tag;

	GoogleAnalyticsTracker tracker;

	public static final String PEOPLE_TAGGED = "PEOPLE_TAGGED";

	private ArrayList<CharSequence> tags = new ArrayList<CharSequence>();
	private ArrayList<CharSequence> names = new ArrayList<CharSequence>();
	private ArrayList<CharSequence> id = new ArrayList<CharSequence>();

	private boolean isChecked;
	EditText txtSearch;
	RelativeLayout relSelectAll;
	CheckBox chkSelectAll;
	AmazingListView listTags;
	Button btnDone;
	TagAdapter adapter;

	Bundle bundle = new Bundle();

	public static NotifyTeam newInstance(int tag, Bundle args) {
		NotifyTeam nt = new NotifyTeam(tag);
		if (args == null)
			args = new Bundle();
		nt.setArguments(args);
		return nt;
	}

	private NotifyTeam(int tag) {
		this.tag = tag;
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("Notify People");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.taglist, container, false);
		instantiateLayout(v);
		return v;
	}

	@Override
	public void onStart() {
		super.onStart();

		setListAdapter(adapter);
		listTags = (AmazingListView) getListView();
		listTags.setLoadingView(getActivity().getLayoutInflater().inflate(
				R.layout.loading_view, null));
		listTags.setPinnedHeaderView(getActivity().getLayoutInflater().inflate(
				R.layout.teamlist_header, listTags, false));
		listTags.setItemsCanFocus(false);
		txtSearch.addTextChangedListener(new TagTextWatcher());
		relSelectAll.setOnClickListener(this);
		btnDone.setOnClickListener(this);

		InputMethodManager imm = (InputMethodManager) getActivity()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(txtSearch.getWindowToken(), 0);

		fetchTeamFromCache();
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (Global.backPressed) {
			Global.backPressed = false;
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		CheckBox chkSelected = (CheckBox) v
				.findViewById(R.id.chk_teamlist_selected);
		Bundle content = (Bundle) adapter.getItem(position);
		content.putBoolean("checked", !chkSelected.isChecked());
		adapter.notifyDataSetChanged();
		tracker.trackEvent("Click", "ListItem", "Select Individual", 0);
	}

	public void onClick(View v) {
		if (v.getId() == R.id.rel_selectall) {
			isChecked = !isChecked;
			chkSelectAll.setChecked(isChecked);
			for (int i = 0; i < adapter.getCount(); i++) {
				Bundle content = (Bundle) adapter.getItem(i);
				content.putBoolean("checked", isChecked);
			}
			adapter.notifyDataSetChanged();
			tracker.trackEvent("Click", "Button", "Select All", 0);
		} else if (v.getId() == R.id.btn_taglist_done) {
			getTags();
			getActivity().onBackPressed();
		}
	}

	private void instantiateLayout(View v) {
		if (getArguments().getBoolean("has_tags", false)) {
			id = getArguments().getCharSequenceArrayList("tags");
		}
		isChecked = false;
		txtSearch = (EditText) v.findViewById(R.id.txt_taglist_search);
		relSelectAll = (RelativeLayout) v.findViewById(R.id.rel_selectall);
		chkSelectAll = (CheckBox) v.findViewById(R.id.chk_selectall);
		btnDone = (Button) v.findViewById(R.id.btn_taglist_done);
		adapter = new TagAdapter(getActivity());
	}

	private void fetchTeamFromCache() {
		adapter.notifyMayHaveMorePages();
		TeamCacheManager manager = new TeamCacheManager(getActivity());
		manager.open();
		Cursor cursor = manager.fetchAll();
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			for (int i = 0; i < cursor.getCount(); i++) {
				Bundle content = new Bundle();
				String user_id = cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.USER_ID));
				content.putString("user_id", user_id);
				content.putString("first_name", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.FIRST_NAME)));
				content.putString("last_name", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.LAST_NAME)));
				content.putString("title", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.TITLE)));
				content.putString("email", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.EMAIL)));
				content.putString("phone", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.PHONE)));
				content.putString("profile_pic_file_path", cursor
						.getString(cursor
								.getColumnIndex(TeamCacheManager.PROFILE_PIC)));
				content.putString("organization", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.ORGANIZATION)));
				content.putString("specialization", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.SPECIALIZATION)));
				content.putString("username", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.USERNAME)));
				content.putString(
						"availability_status",
						cursor.getString(cursor
								.getColumnIndex(TeamCacheManager.AVAILABILITY_STATUS)));
				content.putString("latitude", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.LATITUDE)));
				content.putString("longitude", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.LONGITUDE)));
				content.putString("base_location", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.BASE_LOCATION)));
				content.putBoolean("checked", id.contains(user_id));
				bundle.putBundle(String.valueOf(i), content);
				cursor.moveToNext();
			}
			adapter.setData(bundle);
		} else {
			fetchTeam();
		}
		cursor.close();
		manager.close();
	}

	private void fetchTeam() {
		Log.d("Team", "fetching team");
		Doc2DocRequest dr = new Doc2DocRequest();
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", D2DTabHost.token);

		asyncTask.runTask("user/all_vcards", params, new Doc2DocTaskListener() {

			@Override
			public void onMalformedURLException(MalformedURLException e) {
				// TODO Auto-generated method stub
				teamHandler.sendMessage(Message.obtain(teamHandler,
						Global.ERROR_RESPONSE));
				Log.d("team-response", e.getMessage());
			}

			@Override
			public void onIOException(IOException e) {
				// TODO Auto-generated method stub
				teamHandler.sendMessage(Message.obtain(teamHandler,
						Global.CONNECTION_ERROR));
				Log.d("team-response", e.getMessage());
			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e) {
				// TODO Auto-generated method stub
				teamHandler.sendMessage(Message.obtain(teamHandler,
						Global.ERROR_RESPONSE));
				Log.d("team-response", e.getMessage());
			}

			@Override
			public void onComplete(String response) {
				Log.d("team-response", response);
				if (!response.equals("FALSE")) {
					parseJson(response);
				} else {
					teamHandler.sendMessage(Message.obtain(teamHandler,
							Global.INVALID_SESSION));
				}
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub

			}
		});
	}

	private void parseJson(String response) {
		JSONArray arr = null;
		try {
			arr = new JSONArray(response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			teamHandler.sendMessage(Message.obtain(teamHandler,
					Global.ERROR_RESPONSE));
			return;
		}
		if (arr.length() == 0) {
			teamHandler.sendMessage(Message.obtain(teamHandler,
					Global.EMPTY_RESPONSE));
			return;
		}
		for (int i = 0; i < arr.length(); i++) {
			Bundle content = new Bundle();
			JSONObject json;
			try {
				json = arr.getJSONObject(i);
				String user_id = json.getString("user_id");
				content.putString("user_id", user_id);
				content.putString("first_name", json.getString("first_name"));
				content.putString("last_name", json.getString("last_name"));
				content.putString("title", json.getString("title"));
				content.putString("email", json.getString("email"));
				content.putString("phone", json.getString("phone"));
				content.putString("profile_pic_file_path",
						json.getString("profile_pic_file_path"));
				content.putString("organization",
						json.getString("organization"));
				content.putString("specialization",
						json.getString("specialization"));
				content.putString("username", json.getString("username"));
				content.putString("availability_status",
						json.getString("availability_status"));
				JSONObject geolocation = json.getJSONObject("geolocation");
				content.putString("latitude", geolocation.getString("latitude"));
				content.putString("longitude",
						geolocation.getString("longitude"));
				content.putString("base_location",
						json.getString("base_location"));
				content.putBoolean("checked", id.contains(user_id));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bundle.putBundle(String.valueOf(i), content);
		}
		teamHandler.sendMessage(Message
				.obtain(teamHandler, Global.GOT_RESPONSE));
	}

	private Handler teamHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				adapter.setData(bundle);
				break;
			case Global.EMPTY_RESPONSE:
				break;
			case Global.CONNECTION_ERROR:
				break;
			case Global.ERROR_RESPONSE:
				break;
			case Global.INVALID_SESSION:
				Toast.makeText(getActivity(),
						"Invalid session! Please relogin", Toast.LENGTH_LONG)
						.show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				getActivity().sendBroadcast(i);
				break;
			}
		}
	};

	private void getTags() {
		for (int i = 0; i < adapter.getCount(); i++) {
			Bundle content = (Bundle) adapter.getItem(i);
			if (content.getBoolean("checked")) {
				tags.add(content.getString("user_id"));
				names.add(content.getString("title") + " "
						+ content.getString("first_name") + " "
						+ content.getString("last_name"));
			}
		}

		Intent intent = new Intent();
		intent.setAction(PEOPLE_TAGGED);
		intent.putCharSequenceArrayListExtra("tags", tags);
		intent.putCharSequenceArrayListExtra("names", names);
		getActivity().sendBroadcast(intent);
	}

	class TagTextWatcher implements TextWatcher {

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			adapter.getFilter().filter(s);
			tracker.trackEvent("TextChange", "EditText", "Search Notifiy Team",
					0);
		}

	}

}

/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.cache.DraftManager;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class NewMessage extends Fragment implements OnClickListener {

	int tag;

	GoogleAnalyticsTracker tracker;

	// configure the notification
	Notification notification;
	NotificationManager notificationManager;

	boolean fromDraft;
	boolean feedback;
	boolean activityOpen = false;
	boolean isPosting = false;

	LinearLayout layoutRecipients;
	EditText txtSubject;
	EditText txtMessage;
	EditText txtTaggedKeywords;
	LinearLayout btnSelectRecipient;
	Button btnAttach;
	Button btnSend;
	Button btnSave;
	TextView lblNoOfFiles;

	private ArrayList<CharSequence> recipientId = new ArrayList<CharSequence>();
	private ArrayList<CharSequence> recipientName = new ArrayList<CharSequence>();
	protected ArrayList<CharSequence> attachedFiles = new ArrayList<CharSequence>();

	BroadcastReceiver fileReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			attachedFiles = intent.getCharSequenceArrayListExtra("files");
			if (attachedFiles.size() > 0) {
				lblNoOfFiles.setText("(" + attachedFiles.size() + ")");
			} else {
				lblNoOfFiles.setText("");
			}
		}
	};

	BroadcastReceiver tagReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			recipientId = intent.getCharSequenceArrayListExtra("tags");
			recipientName = intent.getCharSequenceArrayListExtra("names");
			addRecipients();
		}
	};

	public static NewMessage newInstance(int tag, Bundle args) {
		NewMessage nm = new NewMessage(tag);
		if (args == null)
			args = new Bundle();
		nm.setArguments(args);
		return nm;
	}

	private NewMessage(int tag) {
		this.tag = tag;
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("New Message");

		activityOpen = true;

		prepareNotification();
		fromDraft = getArguments().getBoolean("from_draft", false);
		feedback = getArguments().getBoolean("feedback", false);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.sendmessage, container, false);
		instantiateLayout(v);
		return v;
	}

	@Override
	public void onStart() {
		super.onStart();

		if(feedback) {
			btnSelectRecipient.setVisibility(View.GONE);
		}
		
		putValues();

		btnSelectRecipient.setOnClickListener(this);
		btnAttach.setOnClickListener(this);
		btnSend.setOnClickListener(this);
		btnSave.setOnClickListener(this);

		IntentFilter fileFilter = new IntentFilter();
		fileFilter.addAction(AttachFile.FILE_ATTACHED);
		getActivity().registerReceiver(fileReceiver, fileFilter);

		IntentFilter tagFilter = new IntentFilter();
		tagFilter.addAction(NotifyTeam.PEOPLE_TAGGED);
		getActivity().registerReceiver(tagReceiver, tagFilter);
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (Global.backPressed) {
			if (hidden) {
				getActivity().unregisterReceiver(fileReceiver);
				getActivity().unregisterReceiver(tagReceiver);
				if (!isPosting && !feedback) {
					if (txtSubject.getText().length() > 0
							|| txtMessage.getText().length() > 0
							|| attachedFiles.size() > 0) {
						saveToDraft();
						Toast.makeText(getActivity(), "Saved to drafts",
								Toast.LENGTH_SHORT).show();
						tracker.trackEvent("Click", "Button",
								"Save Message Draft", 0);
					}
				}
				activityOpen = false;
			}
			Global.backPressed = false;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		try {
			getActivity().unregisterReceiver(fileReceiver);
			getActivity().unregisterReceiver(tagReceiver);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// @Override
	// public void onBackPressed() {
	// super.onBackPressed();
	//
	// if (!isPosting) {
	// if (txtSubject.getText().length() > 0
	// || txtMessage.getText().length() > 0) {
	// saveToDraft();
	// Toast.makeText(NewMessage.this, "Saved to drafts",
	// Toast.LENGTH_SHORT).show();
	// tracker.trackEvent("Click", "Button", "Save Message Draft", 0);
	// }
	// }
	// }

	public void onClick(View v) {
		if (v.getId() == R.id.layout_sendmessage_recipient) {
			((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
			.hideSoftInputFromWindow(txtMessage.getWindowToken(), 0);
			Bundle args = new Bundle();
			if (recipientId.size() > 0) {
				args.putBoolean("has_tags", true);
				args.putCharSequenceArrayList("tags", recipientId);
			}
			NotifyTeam nt = NotifyTeam.newInstance(tag, args);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, nt);
			ft.commit();
			D2DTabHost.history.get(tag).push(nt);
			tracker.trackEvent("Click", "Button",
					"Launch Notify People Activity", 0);
		} else if (v.getId() == R.id.btn_sendmessage_attach) {
			((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
			.hideSoftInputFromWindow(txtMessage.getWindowToken(), 0);
			Bundle args = new Bundle();
			args.putString("subject", txtSubject.getText().toString());
			args.putString("username", "by you");
			if (attachedFiles.size() > 0) {
				args.putBoolean("has_attachments", true);
				args.putCharSequenceArrayList("attachments", attachedFiles);
			}
			AttachFile af = AttachFile.newInstance(tag, args);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, af);
			ft.commit();
			D2DTabHost.history.get(tag).push(af);
			tracker.trackEvent("Click", "Button",
					"Launch Attach File Activity", 0);
		} else if (v.getId() == R.id.btn_sendmessage_send) {
			((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
			.hideSoftInputFromWindow(txtMessage.getWindowToken(), 0);
			if (recipientId.size() > 0) {
				if (txtSubject.getText().length() > 0
						&& txtMessage.getText().length() > 0) {
					sendMessage(txtSubject.getText().toString(), txtMessage
							.getText().toString(), txtTaggedKeywords.getText()
							.toString());
					isPosting = true;
					tracker.trackEvent("Click", "Button", "Send Message", 0);
				} else {
					Global.createAlert(getActivity(),
							"Please enter subject and(or) message.", "OK");
				}
			} else {
				Global.createAlert(getActivity(),
						"Please select at least one recipient.", "OK");
			}
		} else if (v.getId() == R.id.btn_sendmessage_save) {
			((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
			.hideSoftInputFromWindow(txtMessage.getWindowToken(), 0);
			if (recipientId.size() > 0) {
				if (txtSubject.getText().length() > 0
						&& txtMessage.getText().length() > 0) {
					Toast.makeText(getActivity(), "Saved to drafts",
							Toast.LENGTH_SHORT).show();
					if (activityOpen) {
						getActivity().onBackPressed();
					}
					tracker.trackEvent("Click", "Button", "Save Message Draft",
							0);
				} else {
					Global.createAlert(getActivity(),
							"Please enter title and(or) details.", "OK");
				}
			} else {
				Global.createAlert(getActivity(),
						"Please select at least one recipient.", "OK");
			}
		}
	}

	private void instantiateLayout(View v) {
		layoutRecipients = (LinearLayout) v
				.findViewById(R.id.layout_sendmessage_recipientlist);
		btnSelectRecipient = (LinearLayout) v
				.findViewById(R.id.layout_sendmessage_recipient);
		txtSubject = (EditText) v.findViewById(R.id.txt_sendmessage_subject);
		txtMessage = (EditText) v.findViewById(R.id.txt_sendmessage_message);
		txtTaggedKeywords = (EditText) v
				.findViewById(R.id.txt_sendmessage_tags);
		btnAttach = (Button) v.findViewById(R.id.btn_sendmessage_attach);
		btnSend = (Button) v.findViewById(R.id.btn_sendmessage_send);
		btnSave = (Button) v.findViewById(R.id.btn_sendmessage_save);
		lblNoOfFiles = (TextView) v
				.findViewById(R.id.lbl_sendmessage_noofattachments);
	}

	private void addRecipients() {
		layoutRecipients.removeAllViews();
		for (int i = 0; i < recipientName.size(); i++) {
			View view = getActivity().getLayoutInflater().inflate(
					R.layout.row_text, null);
			TextView lblName = (TextView) view.findViewById(R.id.lbl_text);
			view.findViewById(R.id.btn_text_remove).setVisibility(View.GONE);
			lblName.setText(recipientName.get(i));
			layoutRecipients.addView(view);
		}
	}

	private void putValues() {
		if (fromDraft) {
			txtSubject.setText(getArguments().getString("subject"));
			txtMessage.setText(getArguments().getString("contents"));
			txtTaggedKeywords.setText(getArguments().getString(
					"tagged_keywords"));
			String attachments = getArguments().getString("attached_files");
			if (attachments != null && !attachments.equals("null")) {
				String[] attachs = attachments.split(",");
				for (int i = 0; i < attachs.length; i++) {
					attachedFiles.add(attachs[i]);
				}
				if (attachedFiles.size() > 0) {
					lblNoOfFiles.setText("(" + attachedFiles.size() + ")");
				}
			}
			String tagged = getArguments().getString("tagged_users");
			if (tagged != null && !tagged.equals("null")) {
				String[] tags = tagged.split(",");
				for (int i = 0; i < tags.length; i++) {
					recipientId.add(tags[i]);
				}
			}
			String tagged_names = getArguments().getString("tagged_user_names");
			if (tagged_names != null && !tagged_names.equals("null")) {
				String[] tags = tagged_names.split(",");
				for (int i = 0; i < tags.length; i++) {
					recipientName.add(tags[i]);
				}
				addRecipients();
			}
		} else if (getArguments().getBoolean("has_recipient", false)) {
			recipientId.add(getArguments().getString("tagged_users"));
			recipientName.add(getArguments().getString("tagged_user_names"));
			addRecipients();
			String subject = getArguments().getString("subject");
			if (subject != null) {
				txtSubject.setText(subject);
			}
		}
	}

	private void saveToDraft() {
		DraftManager manager = new DraftManager(getActivity(), "message_draft");
		manager.open();
		if (fromDraft) {
			manager.deleteContent(getArguments().getString("id"));
		}
		manager.insertContent(txtSubject.getText().toString(), txtMessage
				.getText().toString(), Global.getStringFromList(",",
				attachedFiles), txtTaggedKeywords.getText().toString(), Global
				.getStringFromList(",", recipientId), Global.getStringFromList(
				",", recipientName), new Timestamp(System.currentTimeMillis())
				.toLocaleString());
		manager.close();
		getActivity().sendBroadcast(new Intent(D2DTabHost.REFRESH_MESSAGES));
	}

	private void sendMessage(String subject, String contents, String tags) {
		Log.d("NewMessage", "sending message");
		// Global.showLoading(loading, "Sending Message...", false);
		Toast.makeText(getActivity(), "Sending Message...", Toast.LENGTH_LONG)
				.show();
		btnSend.setEnabled(false);
		Doc2DocRequest dr = new Doc2DocRequest(uploadHandler);
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", D2DTabHost.token);
		params.putString("subject", subject);
		params.putString("contents", contents);
		params.putString("tag_user_ids",
				Global.getStringFromList("-", recipientId));
		params.putString("tag_keywords", Global.formatValues(tags, "null"));
		for (int i = 0; i < attachedFiles.size(); i++) {
			params.putString("file_" + (i + 1), attachedFiles.get(i).toString());
		}

		asyncTask.runTask("message/post_new", params,
				new Doc2DocTaskListener() {

					@Override
					public void onMalformedURLException(MalformedURLException e) {
						// TODO Auto-generated method stub
						messageHandler.sendMessage(Message.obtain(
								messageHandler, Global.ERROR_RESPONSE));
						Log.d("send-new-response", e.getMessage());
					}

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						messageHandler.sendMessage(Message.obtain(
								messageHandler, Global.CONNECTION_ERROR));
						Log.d("send-new-response", e.getMessage());
					}

					@Override
					public void onFileNotFoundException(FileNotFoundException e) {
						// TODO Auto-generated method stub
						messageHandler.sendMessage(Message.obtain(
								messageHandler, Global.ERROR_RESPONSE));
						Log.d("send-new-response", e.getMessage());
					}

					@Override
					public void onComplete(String response) {
						Log.d("send-new-response", response);
						if (response.equals("TRUE")) {
							messageHandler.sendMessage(Message.obtain(
									messageHandler, Global.GOT_RESPONSE));
						} else {
							messageHandler.sendMessage(Message.obtain(
									messageHandler, Global.INVALID_SESSION));
						}
					}

					@Override
					public void onCancel() {
						// TODO Auto-generated method stub
						
					}
				});
	}

	private Handler messageHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				btnSend.setEnabled(true);
				if (fromDraft) {
					DraftManager manager = new DraftManager(getActivity(),
							"message_draft");
					manager.open();
					manager.deleteContent(getArguments().getString("id"));
					manager.close();
				}
				getActivity().sendBroadcast(
						new Intent(D2DTabHost.REFRESH_MESSAGES));
				Toast.makeText(getActivity(), "Your message has been sent",
						Toast.LENGTH_LONG).show();
				if (activityOpen) {
					getActivity().onBackPressed();
				}
				break;
			case Global.EMPTY_RESPONSE:
				btnSend.setEnabled(true);
				// Global.hideLoading(loading);
				break;
			case Global.CONNECTION_ERROR:
				btnSend.setEnabled(true);
				// Global.hideLoading(loading);
				Toast.makeText(getActivity(), "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				break;
			case Global.ERROR_RESPONSE:
				btnSend.setEnabled(true);
				// Global.hideLoading(loading);
				break;
			case Global.INVALID_SESSION:
				btnSend.setEnabled(true);
				// Global.hideLoading(loading);
				Toast.makeText(getActivity(),
						"Invalid session! Please relogin", Toast.LENGTH_LONG)
						.show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				getActivity().sendBroadcast(i);
				break;
			}
		}
	};

	private void prepareNotification() {
		notification = new Notification(android.R.drawable.stat_sys_upload,
				"Uploading file", System.currentTimeMillis());
		notification.flags |= Notification.FLAG_ONGOING_EVENT;
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.contentView = new RemoteViews(getActivity()
				.getApplicationContext().getPackageName(),
				R.layout.update_notification);
		PendingIntent contentIntent = PendingIntent.getActivity(getActivity()
				.getApplicationContext(), 0, new Intent(),
				PendingIntent.FLAG_UPDATE_CURRENT);
		notification.contentIntent = contentIntent;
		notificationManager = (NotificationManager) getActivity()
				.getSystemService(Context.NOTIFICATION_SERVICE);
	}

	private void updateProgress(int file) {
		notificationManager.notify(file, notification);
	}

	private Handler uploadHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Doc2DocRequest.UPLOAD_START:
				notification.contentView.setTextViewText(
						R.id.text,
						"Uploading "
								+ (String) msg.getData().getString("filename"));
				updateProgress(msg.arg1);
				break;
			case Doc2DocRequest.PUBLISH_PROGRESS:
				updateProgress(msg.arg1);
				break;
			case Doc2DocRequest.UPLOAD_FINISH:
				notificationManager.cancel(msg.arg1);
			default:
				break;
			}
		}
	};

}

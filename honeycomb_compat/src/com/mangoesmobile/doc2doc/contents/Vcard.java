/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.ImageLoader;
import com.mangoesmobile.doc2doc.util.PingDoctor;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class Vcard extends Fragment implements OnClickListener {

	int tag;

	GoogleAnalyticsTracker tracker;

	ImageView imgUser;
	TextView lblName;
	TextView lblUserStatus;
	TextView lblTitle;
	// TextView lblSpeciality;
	TextView lblOrganization;
	// TextView lblLocation;
	// TextView lblEmail;
	// TextView lblPhone;
	Button btnMessage;
	Button btnPing;

	ImageLoader imageLoader;
	Bundle content;

	public static Vcard newInstance(int tag, Bundle args) {
		Vcard vc = new Vcard(tag);
		if (args == null)
			args = new Bundle();
		vc.setArguments(args);
		return vc;
	}

	private Vcard(int tag) {
		this.tag = tag;
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("Vcard");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.vcard, container, false);
		instantiateLayout(v);
		return v;
	}

	@Override
	public void onStart() {
		super.onStart();

		putValues();
		btnMessage.setOnClickListener(this);
		btnPing.setOnClickListener(this);
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (Global.backPressed) {
			Global.backPressed = false;
		}
	}

	private void instantiateLayout(View v) {
		imgUser = (ImageView) v.findViewById(R.id.img_vcard_userimage);
		lblName = (TextView) v.findViewById(R.id.lbl_vcard_name);
		lblUserStatus = (TextView) v.findViewById(R.id.lbl_vcard_userstatus);
		lblTitle = (TextView) v.findViewById(R.id.lbl_vcard_title);
		// lblSpeciality = (TextView) v.findViewById(R.id.lbl_vcard_speciality);
		lblOrganization = (TextView) v
				.findViewById(R.id.lbl_vcard_organization);
		// lblLocation = (TextView) v.findViewById(R.id.lbl_vcard_location);
		// lblEmail = (TextView) v.findViewById(R.id.lbl_vcard_email);
		// lblPhone = (TextView) v.findViewById(R.id.lbl_vcard_phone);
		btnMessage = (Button) v.findViewById(R.id.btn_vcard_sendmessage);
		btnPing = (Button) v.findViewById(R.id.btn_vcard_ping);

		imageLoader = new ImageLoader(getActivity());
		content = getArguments();
	}

	private void putValues() {
		imageLoader.DisplayImage(
				Global.url + content.getString("profile_pic_file_path"),
				imgUser, null);
		lblName.setText(content.getString("first_name") + " "
				+ content.getString("last_name"));
		String user_status = content.getString("availability_status");
		lblUserStatus.setText(user_status.equals("null") ? "" : user_status
				.toUpperCase());
		if(user_status.equals("on call")) {
			lblUserStatus.setTextColor(Color.rgb(101, 176, 73));
		}
		lblTitle.setText(content.getString("title"));
		// lblSpeciality.setText((content.getString("specialization").equals(
		// "null") ? "" : content.getString("specialization")));
		lblOrganization.setText(((content.getString("organization").equals(
				"null") ? "" : content.getString("organization") + ", "))
				+ content.getString("base_location"));
		// lblLocation.setText(content.getString("base_location"));
		// lblEmail.setText(content.getString("email"));
		// lblPhone.setText((content.getString("phone").equals("null") ? ""
		// : content.getString("phone")));
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.btn_vcard_sendmessage) {
			Bundle args = new Bundle();
			args.putBoolean("has_recipient", true);
			args.putString("tagged_users", content.getString("user_id"));
			args.putString("tagged_user_names", content.getString("first_name")
					+ " " + content.getString("last_name"));
			NewMessage nm = NewMessage.newInstance(tag, args);
			FragmentTransaction ft = getActivity().getSupportFragmentManager()
					.beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, nm);
			ft.commit();
			D2DTabHost.history.get(tag).push(nm);
			tracker.trackEvent("Click", "Button", "Create New Message", 0);
		} else if (v.getId() == R.id.btn_vcard_ping) {
			PingDoctor pd = new PingDoctor(getActivity(),
					content.getString("user_id"), content.getString(
							"first_name").substring(0, 1)
							+ ". " + content.getString("last_name"));
			pd.ping();
			tracker.trackEvent("Click", "Button", "Ping User", 0);
		}
	}

}

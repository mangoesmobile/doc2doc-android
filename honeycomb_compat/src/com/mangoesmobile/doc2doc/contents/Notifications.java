/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import com.foound.widget.AmazingListView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.adapters.DraftAdapter;
import com.mangoesmobile.doc2doc.adapters.MyMessageAdapter;
import com.mangoesmobile.doc2doc.adapters.MyThreadAdapter;
import com.mangoesmobile.doc2doc.cache.BoardMessageCacheManager;
import com.mangoesmobile.doc2doc.notification.*;
import com.mangoesmobile.doc2doc.util.Global;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class Notifications extends ListFragment {

	int tag;

	GoogleAnalyticsTracker tracker;

	public int command;

	AmazingListView list;
	TextView lblTitle;
	EditText txtSearch;

	ThreadNotificationManager threadManager;
	MessageNotificationManager inboxManager;
	MessageNotificationManager sentboxManager;
	PingNotificationManager pingManager;
	DraftNotificationManager threadDraftManager;
	DraftNotificationManager messageDraftManager;

	private BroadcastReceiver refreshReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			// TODO Auto-generated method stub
			refreshData();
		}
	};

	public static Notifications newInstance(int tag, Bundle args) {
		Notifications nt = new Notifications(tag);
		if (args == null)
			args = new Bundle();
		nt.setArguments(args);
		return nt;
	}

	private Notifications(int tag) {
		this.tag = tag;
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView(getArguments().getString("title"));

		command = getArguments().getInt("command", 0);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.messages_notification, container,
				false);
		instantiateLayout(v);
		return v;
	}

	@Override
	public void onStart() {
		super.onStart();

		// listens for refresh from tab menu
		IntentFilter filter = new IntentFilter();
		filter.addAction(D2DTabHost.REFRESH_MESSAGES);
		getActivity().registerReceiver(refreshReceiver, filter);

		lblTitle.setText(getArguments().getString("title"));
		list = (AmazingListView) getListView();
		list.setLoadingView(getActivity().getLayoutInflater().inflate(
				R.layout.loading_view, null));

		switch (command) {
		case Messages.MY_THREADS:
			threadManager = new ThreadNotificationManager(getActivity(), list,
					txtSearch, D2DTabHost.token);
			break;
		case Messages.INBOX:
			inboxManager = new MessageNotificationManager(getActivity(), list,
					txtSearch, "fetch_inbox", D2DTabHost.token);
			break;
		case Messages.SENT:
			sentboxManager = new MessageNotificationManager(getActivity(),
					list, txtSearch, "fetch_sentbox", D2DTabHost.token);
			break;
		case Messages.PINGS:
			pingManager = new PingNotificationManager(getActivity(), list,
					txtSearch, tag, D2DTabHost.token);
			break;
		case Messages.THREAD_DRAFTS:
			threadDraftManager = new DraftNotificationManager(getActivity(),
					list, txtSearch, "thread_draft");
			break;
		case Messages.MESSAGE_DRAFTS:
			messageDraftManager = new DraftNotificationManager(getActivity(),
					list, txtSearch, "message_draft");
			break;
		}

		fetchData();
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (Global.backPressed) {
			if(hidden){
				getActivity().unregisterReceiver(refreshReceiver);
			}
			Global.backPressed = false;
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		try {
			getActivity().unregisterReceiver(refreshReceiver);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// Intent intent;
		Bundle content;
		Bundle args = new Bundle();
		Fragment f;
		FragmentTransaction ft;
		switch (command) {
		case Messages.MY_THREADS:
			content = (Bundle) ((MyThreadAdapter) l.getAdapter())
					.getItem(position);
			f = BoardDetails.newInstance(tag, content);
			ft = getFragmentManager().beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, f);
			ft.commit();
			D2DTabHost.history.get(tag).push(f);
			// intent = new Intent(Notifications.this, BoardDetails.class);
			// intent.putExtra("content", content);
			// parent.startChildActivity("my_thread", intent);
			content.putString("status", "read");
			((MyThreadAdapter) l.getAdapter()).notifyDataSetChanged();
			BoardMessageCacheManager manager = new BoardMessageCacheManager(
					getActivity(), "my_threads");
			manager.open();
			manager.updateStatus("read", content.getString("id"));
			manager.close();
			tracker.trackEvent("Click", "ListItem", "Show Thread Details", 0);
			break;
		case Messages.THREAD_DRAFTS:
			content = (Bundle) ((DraftAdapter) l.getAdapter())
					.getItem(position);
			args.putBoolean("from_draft", true);
			args.putString("id", content.getString("id"));
			args.putString("subject", content.getString("subject"));
			args.putString("contents", content.getString("contents"));
			args.putString("tagged_keywords",
					content.getString("tagged_keywords"));
			args.putString("attached_files",
					content.getString("attached_files"));
			args.putString("tagged_users", content.getString("tagged_users"));
			f = NewThread.newInstance(tag, args);
			ft = getFragmentManager().beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, f);
			ft.commit();
			D2DTabHost.history.get(tag).push(f);
			// intent = new Intent(Notifications.this, NewThread.class);
			// intent.putExtra("from_draft", true);
			// intent.putExtra("id", content.getString("id"));
			// intent.putExtra("subject", content.getString("subject"));
			// intent.putExtra("contents", content.getString("contents"));
			// intent.putExtra("tagged_keywords",
			// content.getString("tagged_keywords"));
			// intent.putExtra("attached_files",
			// content.getString("attached_files"));
			// intent.putExtra("tagged_users",
			// content.getString("tagged_users"));
			// parent.startChildActivity("thread_draft", intent);
			tracker.trackEvent("Click", "ListItem", "Open Thread Draft", 0);
			break;
		case Messages.INBOX:
			content = (Bundle) ((MyMessageAdapter) l.getAdapter())
					.getItem(position);
			f = MessageDetails.newInstance(tag, content);
			ft = getFragmentManager().beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, f);
			ft.commit();
			D2DTabHost.history.get(tag).push(f);
			// intent = new Intent(Notifications.this, MessageDetails.class);
			// intent.putExtra("content", content);
			// parent.startChildActivity("inbox-message", intent);
			content.putString("status", "read");
			((MyMessageAdapter) l.getAdapter()).notifyDataSetChanged();
			BoardMessageCacheManager manager2 = new BoardMessageCacheManager(
					getActivity(), "inbox");
			manager2.open();
			manager2.updateStatus("read", content.getString("id"));
			manager2.close();
			tracker.trackEvent("Click", "ListItem",
					"Show Inbox Message Details", 0);
			break;
		case Messages.SENT:
			content = (Bundle) ((MyMessageAdapter) l.getAdapter())
					.getItem(position);
			f = MessageDetails.newInstance(tag, content);
			ft = getFragmentManager().beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, f);
			ft.commit();
			D2DTabHost.history.get(tag).push(f);
			// intent = new Intent(Notifications.this, MessageDetails.class);
			// intent.putExtra("content", content);
			// parent.startChildActivity("sentbox-message", intent);
			tracker.trackEvent("Click", "ListItem",
					"Show Sentbox Message Details", 0);
			break;
		case Messages.MESSAGE_DRAFTS:
			content = (Bundle) ((DraftAdapter) l.getAdapter())
					.getItem(position);
			args.putBoolean("from_draft", true);
			args.putString("id", content.getString("id"));
			args.putString("subject", content.getString("subject"));
			args.putString("contents", content.getString("contents"));
			args.putString("tagged_keywords",
					content.getString("tagged_keywords"));
			args.putString("attached_files",
					content.getString("attached_files"));
			args.putString("tagged_users", content.getString("tagged_users"));
			args.putString("tagged_user_names",
					content.getString("tagged_user_names"));
			f = NewMessage.newInstance(tag, args);
			ft = getFragmentManager().beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, f);
			ft.commit();
			D2DTabHost.history.get(tag).push(f);
			// intent = new Intent(Notifications.this, NewMessage.class);
			// intent.putExtra("from_draft", true);
			// intent.putExtra("id", content.getString("id"));
			// intent.putExtra("subject", content.getString("subject"));
			// intent.putExtra("contents", content.getString("contents"));
			// intent.putExtra("tagged_keywords",
			// content.getString("tagged_keywords"));
			// intent.putExtra("attached_files",
			// content.getString("attached_files"));
			// intent.putExtra("tagged_users",
			// content.getString("tagged_users"));
			// intent.putExtra("tagged_user_names",
			// content.getString("tagged_user_names"));
			// parent.startChildActivity("message_draft", intent);
			tracker.trackEvent("Click", "ListItem", "Open Message Draft", 0);
			break;
		case Messages.PINGS:
			break;
		}
	}

	private void instantiateLayout(View v) {
		lblTitle = (TextView) v.findViewById(R.id.lbl_messages_notification);
		txtSearch = (EditText) v.findViewById(R.id.txt_messages_search);
	}

	private void fetchData() {
		switch (command) {
		case Messages.MY_THREADS:
			threadManager.fetchThreadFromCache();
			break;
		case Messages.THREAD_DRAFTS:
			threadDraftManager.fetchData();
			break;
		case Messages.INBOX:
			inboxManager.fetchMessageFromCache();
			break;
		case Messages.SENT:
			sentboxManager.fetchMessageFromCache();
			break;
		case Messages.MESSAGE_DRAFTS:
			messageDraftManager.fetchData();
			break;
		case Messages.PINGS:
			pingManager.fetchPingFromCache();
			break;
		}
	}

	private void refreshData() {
		switch (command) {
		case Messages.MY_THREADS:
			threadManager.fetchThread();
			break;
		case Messages.THREAD_DRAFTS:
			threadDraftManager.fetchData();
			break;
		case Messages.INBOX:
			inboxManager.fetchMessages();
			break;
		case Messages.SENT:
			sentboxManager.fetchMessages();
			break;
		case Messages.MESSAGE_DRAFTS:
			messageDraftManager.fetchData();
			break;
		case Messages.PINGS:
			pingManager.fetchPing();
			break;
		}
	}
}

package com.mangoesmobile.doc2doc.contents;

import java.io.File;
import java.io.FilenameFilter;
import java.text.DecimalFormat;
import java.util.ArrayList;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.R;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class FileExplore extends ListActivity {

	GoogleAnalyticsTracker tracker;
	
	// Stores names of traversed directories
	ArrayList<String> str = new ArrayList<String>();

	// Check if the first level of the directory structure is the one showing
	private Boolean firstLvl = true;

	private static final String TAG = "F_PATH";

	private Item[] fileList;
	private File path = new File(Environment.getExternalStorageDirectory() + "");
	private String chosenFile;
//	private static final int DIALOG_LOAD_FILE = 1000;

	ArrayAdapter<Item> adapter;
	
	TextView lblTitle;	
	TextView lblLocation;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.file_explorer);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("File Explorer");
		
		lblLocation = (TextView) findViewById(R.id.path);		
		lblTitle = (TextView) findViewById(R.id.lbl_file_title);
		
		if(!getIntent().getBooleanExtra("file_browser", true)) {
			lblTitle.setText("Treatment Guidelines");
			path = new File(path + "/Bots Guidelines/");
		}
		
		loadFileList();

//		showDialog(DIALOG_LOAD_FILE);
		Log.d(TAG, path.getAbsolutePath());

	}

	private void loadFileList() {
		try {
			path.mkdirs();
		} catch (SecurityException e) {
			Log.e(TAG, "unable to write on the sd card ");
		}

		// Checks whether path exists
		if (path.exists()) {
			FilenameFilter filter = new FilenameFilter() {
				@Override
				public boolean accept(File dir, String filename) {
					File sel = new File(dir, filename);
					// Filters based on whether the file is hidden or not
					return (sel.isFile() || sel.isDirectory())
							&& !sel.isHidden();

				}
			};

			String[] fList = path.list(filter);
			fileList = new Item[fList.length];
			for (int i = 0; i < fList.length; i++) {
				fileList[i] = new Item(fList[i], R.drawable.file_icon, 0, false);

				// Convert into file path
				File sel = new File(path, fList[i]);

				// Set drawables and size
				if (sel.isDirectory()) {
					fileList[i].icon = R.drawable.directory_icon;
//					Log.d("DIRECTORY", fileList[i].file);
				} else {
					fileList[i].size = sel.length();
					fileList[i].isFile = true;
//					Log.d("FILE", fileList[i].file);
				}
			}

			if (!firstLvl) {
				Item temp[] = new Item[fileList.length + 1];
				for (int i = 0; i < fileList.length; i++) {
					temp[i + 1] = fileList[i];
				}
				temp[0] = new Item("Up", R.drawable.directory_up, 0, false);
				fileList = temp;
			}
		} else {
			Log.e(TAG, "path does not exist");
		}
		
		lblLocation.setText("Location: " + path.getAbsolutePath());

		adapter = new ArrayAdapter<Item>(this, R.layout.row_explorer, fileList) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				// creates view
				View row;
				if(convertView == null) {
					row = getLayoutInflater().inflate(R.layout.row_explorer, null);
				} else {
					row = convertView;
				}
				
				ImageView imgFile = (ImageView) row.findViewById(R.id.img_explorer_file);
				TextView lblFileName = (TextView) row.findViewById(R.id.lbl_explorer_filename);
				TextView lblSize = (TextView) row.findViewById(R.id.lbl_explorer_filesize);
				
				Item item = getItem(position);
				
				imgFile.setImageResource(item.icon);
				lblFileName.setText(item.file);
				if (item.isFile) {
					String size = "";
					DecimalFormat df = new DecimalFormat("#.00");
					if(item.size < 1048576)
						size = df.format((double)(item.size / 1024.0)) + " KB";
					else
						size = df.format((double)(item.size / 1048576.0)) + " MB";
					lblSize.setText(size);
				} else {
					lblSize.setText("");
				}
				return row;
			}
		};
		setListAdapter(adapter);
		
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		chosenFile = fileList[position].file;
		File sel = new File(path + "/" + chosenFile);
		if (sel.isDirectory()) {
			firstLvl = false;

			// Adds chosen directory to list
			str.add(chosenFile);
			fileList = null;
			path = new File(sel + "");

			loadFileList();
//
//			removeDialog(DIALOG_LOAD_FILE);
//			showDialog(DIALOG_LOAD_FILE);
			Log.d(TAG, path.getAbsolutePath());

		}

		// Checks if 'up' was clicked
		else if (chosenFile.equalsIgnoreCase("up") && !sel.exists()) {

			// present directory removed from list
			String s = str.remove(str.size() - 1);

			// path modified to exclude present directory
			path = new File(path.toString().substring(0,
					path.toString().lastIndexOf(s)));
			fileList = null;

			// if there are no more directories in the list, then
			// its the first level
			if (str.isEmpty()) {
				firstLvl = true;
			}
			loadFileList();
//
//			removeDialog(DIALOG_LOAD_FILE);
//			showDialog(DIALOG_LOAD_FILE);
			Log.d(TAG, path.getAbsolutePath());

		}
		// File picked
		else {
			// Perform action with file picked
			if(!getIntent().getBooleanExtra("file_browser", true)) {
				String filename = sel.getName();
				String ext = filename.substring(filename.lastIndexOf(".") + 1);
				Log.d("ext", ext);
				Intent intent = new Intent();
				intent.setAction(android.content.Intent.ACTION_VIEW);
				switch (getFileType(ext)) {
				case 0:
					Toast.makeText(this, "No app found to open " + ext + " file!", Toast.LENGTH_LONG).show();
					break;
				case 1:
					intent.setDataAndType(Uri.fromFile(sel), "image/*");
					startActivity(intent);
					tracker.trackEvent("Click", "ListItem", "See image " + sel.getName(), 0);
					break;
				case 2:
					intent.setDataAndType(Uri.fromFile(sel), "video/*");
					startActivity(intent);
					tracker.trackEvent("Click", "ListItem", "Watch Video " + sel.getName(), 0);
					break;
				case 3:
					intent.setDataAndType(Uri.fromFile(sel), "audio/*");
					startActivity(intent);
					tracker.trackEvent("Click", "ListItem", "Listen To Audio " + sel.getName(), 0);
					break;
				case 4:
					intent.setDataAndType(Uri.fromFile(sel), "application/pdf");
					startActivity(intent);
					tracker.trackEvent("Click", "ListItem", "Open PDF " + sel.getName(), 0);
					break;
				case 5:
					intent.setDataAndType(Uri.fromFile(sel), "text/plain");
					startActivity(intent);
					tracker.trackEvent("Click", "ListItem", "Open Document " + sel.getName(), 0);
				default:
					break;
				}
			} else {
				Log.d(TAG, sel.getAbsolutePath());
				Bundle bundle = new Bundle();
				bundle.putString("file", sel.getAbsolutePath());
				Intent returnIntent = new Intent();
				returnIntent.putExtras(bundle);
				setResult(RESULT_OK, returnIntent);
				finish();
			}
		}
	}
	
	private int getFileType(String ext) {
		if(ext.equalsIgnoreCase("jpg") || ext.equalsIgnoreCase("bmp") || ext.equalsIgnoreCase("gif") || ext.equalsIgnoreCase("png") || ext.equalsIgnoreCase("tif"))
			return 1;
		else if(ext.equalsIgnoreCase("mp4") || ext.equalsIgnoreCase("mpg") || ext.equalsIgnoreCase(".mpeg") || ext.equalsIgnoreCase("avi") || ext.equalsIgnoreCase("3gp") || ext.equalsIgnoreCase("3gpp"))
			return 2;
		else if(ext.equalsIgnoreCase("mp3") || ext.equalsIgnoreCase("aac") || ext.equalsIgnoreCase("wav") || ext.equalsIgnoreCase("amr"))
			return 3;
		else if(ext.equalsIgnoreCase("doc") || ext.equalsIgnoreCase("docx") || ext.equalsIgnoreCase("pdf"))
			return 4;
		else if(ext.equalsIgnoreCase("rtf") || ext.equalsIgnoreCase("txt"))
			return 5;
		return 0;
	}

	private class Item {
		public String file;
		public int icon;
		public long size;
		public boolean isFile;

		public Item(String file, Integer icon, long size, boolean isFile) {
			this.file = file;
			this.icon = icon;
			this.size = size;
			this.isFile = isFile;
		}

		@Override
		public String toString() {
			return file;
		}
		
	}

//	@Override
//	protected Dialog onCreateDialog(int id) {
//		Dialog dialog = null;
//		AlertDialog.Builder builder = new Builder(this);
//
//		if (fileList == null) {
//			Log.e(TAG, "No files loaded");
//			dialog = builder.create();
//			return dialog;
//		}
//
//		switch (id) {
//		case DIALOG_LOAD_FILE:
//			builder.setTitle("Choose your file");
//			builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					chosenFile = fileList[which].file;
//					File sel = new File(path + "/" + chosenFile);
//					if (sel.isDirectory()) {
//						firstLvl = false;
//
//						// Adds chosen directory to list
//						str.add(chosenFile);
//						fileList = null;
//						path = new File(sel + "");
//
//						loadFileList();
//
//						removeDialog(DIALOG_LOAD_FILE);
//						showDialog(DIALOG_LOAD_FILE);
//						Log.d(TAG, path.getAbsolutePath());
//
//					}
//
//					// Checks if 'up' was clicked
//					else if (chosenFile.equalsIgnoreCase("up") && !sel.exists()) {
//
//						// present directory removed from list
//						String s = str.remove(str.size() - 1);
//
//						// path modified to exclude present directory
//						path = new File(path.toString().substring(0,
//								path.toString().lastIndexOf(s)));
//						fileList = null;
//
//						// if there are no more directories in the list, then
//						// its the first level
//						if (str.isEmpty()) {
//							firstLvl = true;
//						}
//						loadFileList();
//
//						removeDialog(DIALOG_LOAD_FILE);
//						showDialog(DIALOG_LOAD_FILE);
//						Log.d(TAG, path.getAbsolutePath());
//
//					}
//					// File picked
//					else {
//						// Perform action with file picked
//					}
//
//				}
//			});
//			break;
//		}
//		dialog = builder.show();
//		return dialog;
//	}

}
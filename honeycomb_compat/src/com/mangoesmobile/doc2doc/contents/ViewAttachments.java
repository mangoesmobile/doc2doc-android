/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.util.Global;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RemoteViews;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class ViewAttachments extends ListFragment {

	int tag;
	
	GoogleAnalyticsTracker tracker;
	
	// configure the notification
	Notification notification;
	NotificationManager notificationManager;

	String[] files;

	private static final int MALFORMED_URL = 0;
	private static final int FILE_NOT_FOUND = 1;
	private static final int NETWORK_ERROR = 2;
	private static final int DOWNLOAD_COMPLETE = 3;

	public static ViewAttachments newInstance(int tag, Bundle args) {
		ViewAttachments va = new ViewAttachments(tag);
		if (args == null)
			args = new Bundle();
		va.setArguments(args);
		return va;
	}

	private ViewAttachments(int tag) {
		this.tag = tag;
	}
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("View Attachments");
		
		prepareNotification();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.viewattachment, container, false);
	}

	@Override
	public void onStart() {
		super.onStart();

		formatAttachments();
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (Global.backPressed) {
			Global.backPressed = false;
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		File path = new File(Environment.getExternalStorageDirectory(), "doc2doc");
		
		if(!path.exists())
			path.mkdir();

		File file = new File(path, (String) l.getAdapter().getItem(position));
		
		if (!file.exists()) {
			Toast.makeText(
					getActivity(),
					"Your download will start shortly. Check notification bar for progress.",
					Toast.LENGTH_LONG).show();
			notification.contentView.setTextViewText(R.id.lbl_download_filename,
					"Downloading " + (String) l.getAdapter().getItem(position));
			new DownloadFile().execute(position, file);
		} else {
			Toast.makeText(getActivity(),
					"File already exists!",
					Toast.LENGTH_LONG).show();
			openFile(file);
		}
		tracker.trackEvent("Click", "ListItem", "Download and Open File", 0);
	}

	class DownloadFile extends AsyncTask<Object, Integer, Integer> {

		@Override
		protected Integer doInBackground(Object... params) {
			// TODO Auto-generated method stub
			int position = (Integer) params[0];
			File file = (File) params[1];
			try {
				// set the download URL, a url that points to a file on the
				// internet
				// this is the file to be downloaded
				URL url = new URL(Global.url + files[position]);

				// create the new connection
				HttpURLConnection urlConnection = (HttpURLConnection) url
						.openConnection();

				// set up some things on the connection
				urlConnection.setRequestMethod("GET");
				urlConnection.setDoOutput(true);
				urlConnection.connect();

				FileOutputStream fileOutput = new FileOutputStream(file);
				InputStream inputStream = urlConnection.getInputStream();
				int totalSize = urlConnection.getContentLength();
				int downloadedSize = 0;
				byte[] buffer = new byte[1024];
				int bufferLength = 0; // used to store a temporary size of the
										// buffer
				int counter = -1;
				while ((bufferLength = inputStream.read(buffer)) > 0) {
					fileOutput.write(buffer, 0, bufferLength);
					downloadedSize += bufferLength;
					int percentage = (int) ((double) downloadedSize / totalSize * 100);
					Log.d("percentage", percentage + "");
					if (percentage > counter) {
						publishProgress(position, (int) percentage);
						counter = percentage;
					}
				}
				fileOutput.close();
				Message msg = Message.obtain();
				msg.what = DOWNLOAD_COMPLETE;
				Bundle data = new Bundle();
				data.putSerializable("file", file);
				msg.setData(data);
				downloadHandler.sendMessage(msg);
			} catch (MalformedURLException e) {
				e.printStackTrace();
				Message msg = Message.obtain();
				msg.what = MALFORMED_URL;
				downloadHandler.sendMessage(msg);
				file.delete();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				Message msg = Message.obtain();
				msg.what = FILE_NOT_FOUND;
				downloadHandler.sendMessage(msg);
				file.delete();
			} catch (IOException e) {
				e.printStackTrace();
				Message msg = Message.obtain();
				msg.what = NETWORK_ERROR;
				downloadHandler.sendMessage(msg);
				file.delete();
			}
			return position;
		}

		@Override
		protected void onProgressUpdate(Integer... progress) {
			updateProgress(progress[0], progress[1]);
		}

		@Override
		protected void onPostExecute(Integer result) {
			notificationManager.cancel(result);
		}

	}

	private Handler downloadHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MALFORMED_URL:
				Toast.makeText(getActivity(), "Invalid url!",
						Toast.LENGTH_LONG).show();
				break;
			case FILE_NOT_FOUND:
				Toast.makeText(getActivity(),
						"File not found on the server!", Toast.LENGTH_LONG)
						.show();
				break;
			case NETWORK_ERROR:
				Toast.makeText(getActivity(),
						"Connection error! Please retry.", Toast.LENGTH_LONG)
						.show();
				break;
			case DOWNLOAD_COMPLETE:
				Toast.makeText(getActivity(), "Download complete!",
						Toast.LENGTH_LONG).show();
				File file = (File) msg.getData().getSerializable("file");
				openFile(file);
				break;
			default:
				break;
			}
		}
	};
	
	private void openFile(File file) {
		String filename = file.getName();
		String ext = filename.substring(filename.lastIndexOf(".") + 1);
		Log.d("ext", ext);
		Intent intent = new Intent();
		intent.setAction(android.content.Intent.ACTION_VIEW);
		switch (getFileType(ext)) {
		case 0:
			Toast.makeText(getActivity(), "No app found to open " + ext + " file!", Toast.LENGTH_LONG).show();
			break;
		case 1:
			intent.setDataAndType(Uri.fromFile(file), "image/*");
			startActivity(intent);
			break;
		case 2:
			intent.setDataAndType(Uri.fromFile(file), "video/*");
			startActivity(intent);
			break;
		case 3:
			intent.setDataAndType(Uri.fromFile(file), "audio/*");
			startActivity(intent);
			break;
		case 4:
			intent.setDataAndType(Uri.fromFile(file), "application/pdf");
			startActivity(intent);
			break;
		case 5:
			intent.setDataAndType(Uri.fromFile(file), "text/plain");
			startActivity(intent);
		default:
			break;
		}
	}
	
	private int getFileType(String ext) {
		if(ext.equalsIgnoreCase("jpg") || ext.equalsIgnoreCase("bmp") || ext.equalsIgnoreCase("gif") || ext.equalsIgnoreCase("png") || ext.equalsIgnoreCase("tif"))
			return 1;
		else if(ext.equalsIgnoreCase("mp4") || ext.equalsIgnoreCase("mpg") || ext.equalsIgnoreCase(".mpeg") || ext.equalsIgnoreCase("avi") || ext.equalsIgnoreCase("3gp") || ext.equalsIgnoreCase("3gpp"))
			return 2;
		else if(ext.equalsIgnoreCase("mp3") || ext.equalsIgnoreCase("aac") || ext.equalsIgnoreCase("wav") || ext.equalsIgnoreCase("amr"))
			return 3;
		else if(ext.equalsIgnoreCase("doc") || ext.equalsIgnoreCase("docx") || ext.equalsIgnoreCase("pdf"))
			return 4;
		else if(ext.equalsIgnoreCase("rtf") || ext.equalsIgnoreCase("txt"))
			return 5;
		return 0;
	}

	private void formatAttachments() {
		String attachments = getArguments().getString("attached_files");
		files = attachments.split(",");
		List<String> names = new ArrayList<String>();
		for (int i = 0; i < files.length; i++) {
			names.add(files[i].substring(files[i].lastIndexOf("/") + 1));
		}
		setListAdapter(new ArrayAdapter<String>(getActivity(),
				R.layout.row_attachment, R.id.lbl_attachment_filename, names));
	}

	private void prepareNotification() {
		notification = new Notification(android.R.drawable.stat_sys_download,
				"Downloading file", System.currentTimeMillis());
		notification.flags |= Notification.FLAG_ONGOING_EVENT;
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.contentView = new RemoteViews(getActivity()
				.getPackageName(), R.layout.row_download);
		PendingIntent contentIntent = PendingIntent.getActivity(
				getActivity(), 0, new Intent(),
				PendingIntent.FLAG_UPDATE_CURRENT);
		notification.contentIntent = contentIntent;
		notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
	}

	private void updateProgress(int file, int percentage) {
		notification.contentView.setTextViewText(R.id.lbl_download_percentage,
				percentage + "%");
		notification.contentView.setProgressBar(R.id.prog_download_percentage,
				100, percentage, false);
		notificationManager.notify(file, notification);
	}

}

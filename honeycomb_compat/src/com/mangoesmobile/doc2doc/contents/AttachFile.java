/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import java.io.File;
import java.util.ArrayList;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.util.Global;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class AttachFile extends Fragment implements OnClickListener {

	int tag;

	GoogleAnalyticsTracker tracker;

	public static final int CAMERA_PIC = 0;
	public static final int GALLERY_PIC = 1;
	public static final int CAMERA_VID = 2;
	public static final int GALLERY_VID = 3;
	public static final int CHOOSE_SOUND = 4;
	public static final int OTHER_FILE = 5;

	ImageView imgUser;
	TextView lblTitle;
	TextView lblAuthor;
	Button btnPhoto;
	Button btnVideo;
	Button btnSound;
	Button btnFile;
	Button btnDone;
	ListView listFiles;
	FileAdapter adapter;

	private ArrayList<CharSequence> attachedFiles = new ArrayList<CharSequence>();
	public static final String FILE_ATTACHED = "FILE_ATTACHED";

	public static AttachFile newInstance(int tag, Bundle args) {
		AttachFile nt = new AttachFile(tag);
		if (args == null)
			args = new Bundle();
		nt.setArguments(args);
		return nt;
	}

	private AttachFile(int tag) {
		this.tag = tag;
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("Attach File");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.attach, container, false);
		instantiateLayout(v);
		return v;
	}

	@Override
	public void onStart() {
		super.onStart();

		btnPhoto.setOnClickListener(this);
		btnVideo.setOnClickListener(this);
		btnSound.setOnClickListener(this);
		btnFile.setOnClickListener(this);
		btnDone.setOnClickListener(this);
		listFiles.setAdapter(adapter);
		putValues();
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (Global.backPressed) {
			Global.backPressed = false;
		}
	}

	private void instantiateLayout(View v) {
		imgUser = (ImageView) v.findViewById(R.id.img_attach_user);
		lblTitle = (TextView) v.findViewById(R.id.lbl_attach_title);
		lblAuthor = (TextView) v.findViewById(R.id.lbl_attach_author);
		btnPhoto = (Button) v.findViewById(R.id.btn_attach_photo);
		btnVideo = (Button) v.findViewById(R.id.btn_attach_video);
		btnSound = (Button) v.findViewById(R.id.btn_attach_sound);
		btnFile = (Button) v.findViewById(R.id.btn_attach_file);
		btnDone = (Button) v.findViewById(R.id.btn_attach_done);
		listFiles = (ListView) v.findViewById(R.id.ls_attach_files);
		adapter = new FileAdapter(getActivity());
	}

	private void putValues() {
		lblTitle.setText(getArguments().getString("subject"));
		lblAuthor.setText(getArguments().getString("username"));
		if (getArguments().getBoolean("has_attachments", false)) {
			attachedFiles = getArguments().getCharSequenceArrayList(
					"attachments");
			adapter.setData(attachedFiles);
			Global.setListViewHeightBasedOnChildren(listFiles);
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_attach_photo) {
			choosePhotoSource();
			tracker.trackEvent("Click", "Button", "Attach Photo", 0);
		} else if (v.getId() == R.id.btn_attach_video) {
			chooseVideoSource();
			tracker.trackEvent("Click", "Button", "Attach Video", 0);
		} else if (v.getId() == R.id.btn_attach_sound) {
			chooseSound();
			tracker.trackEvent("Click", "Button", "Attach Sound", 0);
		} else if (v.getId() == R.id.btn_attach_file) {
			openFileExplorer();
			tracker.trackEvent("Click", "Button", "Attach Other Filetypes", 0);
		} else if (v.getId() == R.id.btn_attach_done) {
			Intent intent = new Intent();
			intent.setAction(FILE_ATTACHED);
			intent.putCharSequenceArrayListExtra("files", attachedFiles);
			getActivity().sendBroadcast(intent);

			getActivity().onBackPressed();
		}
	}

	private void choosePhotoSource() {
		final CharSequence[] items = { "Capture Photo",
				"Pick Photo From Gallery" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Pick Photo Source");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				switch (item) {
				case 0:
					takePhoto();
					break;
				case 1:
					galleryPhoto();
					break;
				}
			}
		});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	private void chooseVideoSource() {
		final CharSequence[] items = { "Record Video",
				"Pick Video From Gallery" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Pick Video Source");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				switch (item) {
				case 0:
					recordVideo();
					break;
				case 1:
					galleryVideo();
					break;
				}
			}
		});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	// starts the camera intent to capture image
	private void takePhoto() {
		// String fileName = System.currentTimeMillis() + ".jpg";
		// ContentValues values = new ContentValues();
		// values.put(MediaStore.Images.Media.TITLE, fileName);
		// values.put(
		// MediaStore.Images.Media.ORIENTATION,
		// getResources().getConfiguration().orientation ==
		// Configuration.ORIENTATION_LANDSCAPE ? 0
		// : 90);
		// mCapturedImageURI = getActivity().getContentResolver().insert(
		// MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

		Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		// cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
		startActivityForResult(cameraIntent, CAMERA_PIC);
	}

	// starts the gallery intent to pick image
	private void galleryPhoto() {
		Intent galleryIntent = new Intent();
		galleryIntent.setType("image/*");
		galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(
				Intent.createChooser(galleryIntent, "Select Picture"),
				GALLERY_PIC);
	}

	// starts the camera intent to capture image
	private void recordVideo() {
		Intent cameraIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
		startActivityForResult(cameraIntent, CAMERA_VID);
	}

	// starts the gallery intent to pick image
	private void galleryVideo() {
		Intent galleryIntent = new Intent();
		galleryIntent.setType("video/*");
		galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(galleryIntent, GALLERY_VID);
	}

	// starts the gallery intent to pick image
	private void chooseSound() {
		Intent galleryIntent = new Intent();
		galleryIntent.setType("audio/*");
		galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(
				Intent.createChooser(galleryIntent, "Pick Sound Source"),
				CHOOSE_SOUND);
	}

	private void openFileExplorer() {
		Intent intent = new Intent();
		intent.setClass(getActivity(),
				com.mangoesmobile.doc2doc.contents.FileExplore.class);
		startActivityForResult(intent, OTHER_FILE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		handleActivityResult(requestCode, resultCode, intent);
	}

	public void handleActivityResult(int requestCode, int resultCode,
			Intent intent) {
		Log.d("attach-file", "got attachment");
		if (resultCode == Activity.RESULT_OK) {
			String filePath = "";
			if (requestCode == CAMERA_PIC) {
				// filePath = getPath(mCapturedImageURI);
				// mCapturedImageURI = null;
				filePath = getCapturedImagePath();
			} else if (requestCode == GALLERY_PIC || requestCode == CAMERA_VID
					|| requestCode == GALLERY_VID
					|| requestCode == CHOOSE_SOUND) {
				filePath = getPath(intent.getData());
			} else if (requestCode == OTHER_FILE) {
				filePath = (String) intent.getExtras().get("file");
			}
			if (filePath != null) {
				attachedFiles.add(filePath);
				adapter.setData(attachedFiles);
				Global.setListViewHeightBasedOnChildren(listFiles);
			}
		}
	}
	
	// returns the real path from Uri (only applicable for gallery media)
	private String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = getActivity().getApplicationContext()
				.getContentResolver().query(uri, projection, null, null, null);
		int column_index;
		try {
			column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		cursor.moveToFirst();
		String path = cursor.getString(column_index);
		cursor.close();
		return path;
	}

	// returns the path of the recently captured image
	private String getCapturedImagePath() {
		// Create new Cursor to obtain the file Path for the large image
		String[] largeFileProjection = { MediaStore.Images.ImageColumns._ID,
				MediaStore.Images.ImageColumns.DATA };

		String largeFileSort = MediaStore.Images.ImageColumns._ID + " DESC";
		Cursor largeCursor = getActivity()
				.getApplicationContext()
				.getContentResolver()
				.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
						largeFileProjection, null, null, largeFileSort);
		String largeImagePath = null;

		try {
			if (largeCursor.moveToFirst()) {
				// This will actually give you the file path location of the
				// image.
				largeImagePath = largeCursor
						.getString(largeCursor
								.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DATA));
			}
		} finally {
			largeCursor.close();
		}

		return largeImagePath;
	}

	class FileAdapter extends BaseAdapter {

		ArrayList<CharSequence> data;
		Activity context;

		public FileAdapter(Activity context) {
			this.context = context;
			data = new ArrayList<CharSequence>();
		}

		public void setData(ArrayList<CharSequence> data) {
			this.data = data;
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return data.size();
		}

		@Override
		public CharSequence getItem(int arg0) {
			// TODO Auto-generated method stub
			return data.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(final int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			View row;
			if (arg1 == null) {
				row = getActivity().getLayoutInflater().inflate(
						R.layout.row_text, null);
			} else {
				row = arg1;
			}
			Button btnRemove = (Button) row.findViewById(R.id.btn_text_remove);
			TextView txtFileName = (TextView) row.findViewById(R.id.lbl_text);
			File file = new File(getItem(arg0).toString());
			txtFileName.setText(file.getName());
			btnRemove.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					data.remove(arg0);
					notifyDataSetChanged();
				}
			});
			return row;
		}

	}

}

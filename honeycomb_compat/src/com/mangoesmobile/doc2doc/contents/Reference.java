/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.adapters.ReferenceGridAdapter;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.ImageLoader;
import com.mangoesmobile.doc2doc.util.UserInfo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class Reference extends Fragment implements OnClickListener {

	private int tag;

	GoogleAnalyticsTracker tracker;

	private ImageView imgUser;
	private ProgressBar progUser;
	private ImageView imgUserStatus;
	private Button btnLocation;
	private TextView lblUsername;
	private TextView lblUserStatus;
	private Button btnPost;

	GridView gridItems;

	private Bundle data;
	private ReferenceGridAdapter adapter;

	BroadcastReceiver userInfoReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			int response = intent
					.getIntExtra("response", Global.ERROR_RESPONSE);
			switch (response) {
			case Global.GOT_RESPONSE:
				updateUserInfo();
				break;
			case Global.CHANGE_STATUS:
				Global.userStatus = intent.getStringExtra("userStatus");
				Global.userInfo.putString("availability_status",
						Global.userStatus);
				updateUserInfo();
				break;
			case Global.CHANGE_PIC:
				fetchUserInfo();
				break;
			case Global.EMPTY_RESPONSE:
				break;
			case Global.CONNECTION_ERROR:
				Toast.makeText(getActivity(), "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				break;
			case Global.ERROR_RESPONSE:
				break;
			case Global.INVALID_SESSION:
				Toast.makeText(getActivity(),
						"Invalid session! Please relogin", Toast.LENGTH_LONG)
						.show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				getActivity().sendBroadcast(i);
				break;
			}
		}
	};

	public Reference(int tag) {
		this.tag = tag;
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("Reference");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.references, container, false);
		instantiateLayout(v);
		return v;
	}

	@Override
	public void onStart() {
		super.onStart();

		// listens for update of user info
		IntentFilter userFilter = new IntentFilter();
		userFilter.addAction(UserInfo.GOT_USERINFO);
		getActivity().registerReceiver(userInfoReceiver, userFilter);
		
		if (Global.userInfo.size() == 0) {
			fetchUserInfo();
		} else {
			updateUserInfo();
		}

		imgUser.setOnClickListener(this);
		btnLocation.setOnClickListener(this);
		btnPost.setOnClickListener(this);

		generateItems();
		gridItems.setAdapter(adapter);
		adapter.setData(data);
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (Global.backPressed) {
			Global.backPressed = false;
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		getActivity().unregisterReceiver(userInfoReceiver);
	}

	private void instantiateLayout(View v) {
		imgUser = (ImageView) v.findViewById(R.id.img_reference_userimage);
		progUser = (ProgressBar) v.findViewById(R.id.prog_reference_userimage);
		imgUserStatus = (ImageView) v
				.findViewById(R.id.img_reference_userstatus);
		btnLocation = (Button) v.findViewById(R.id.btn_reference_location);
		lblUsername = (TextView) v.findViewById(R.id.lbl_reference_username);
		lblUserStatus = (TextView) v
				.findViewById(R.id.lbl_reference_userstatus);
		btnPost = (Button) v.findViewById(R.id.btn_reference_post);
		gridItems = (GridView) v.findViewById(R.id.grid_reference);
		adapter = new ReferenceGridAdapter(getActivity(), tag);
	}

	private void generateItems() {
		final int[] icons = { R.drawable.ic_treatmentguideline,
				R.drawable.ic_medline, R.drawable.ic_pubmed,
				R.drawable.ic_dailymed, R.drawable.ic_aidsinfo,
				R.drawable.ic_other };
		data = new Bundle();
		for (int i = 0; i < icons.length; i++) {
			Bundle content = new Bundle();
			content.putInt("icon", icons[i]);
			data.putBundle(String.valueOf(i), content);
		}
	}

	private void fetchUserInfo() {
		UserInfo.fetchUserInfo(getActivity(), D2DTabHost.token);
	}

	private void updateUserInfo() {
		String userStatus = Global.userInfo.getString("availability_status");
		Log.d("user-status", userStatus);
		if (userStatus.equals("online")) {
			imgUserStatus.setImageResource(R.drawable.bg_photo_online);
			lblUserStatus.setTextColor(Color.rgb(0, 102, 153));
		} else if (userStatus.equals("on call")) {
			imgUserStatus.setImageResource(R.drawable.bg_photo_oncall);
			lblUserStatus.setTextColor(Color.rgb(101, 176, 73));
		}
		lblUserStatus.setText(userStatus.toUpperCase());
		lblUsername.setText(Global.userInfo.getString("first_name").substring(0, 1)
				+ ". " + Global.userInfo.getString("last_name"));
		if (Global.profilePic == null) {
			// Global.loadPhoto(
			// Board.this,
			// Global.url
			// + Global.userInfo
			// .getString("profile_pic_file_path"),
			// imgUser, progUser);
			ImageLoader il = new ImageLoader(getActivity());
			il.DisplayImage(
					Global.url
							+ Global.userInfo
									.getString("profile_pic_file_path"),
					imgUser, progUser);
		} else {
			imgUser.setImageDrawable(Global.profilePic);
			progUser.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.btn_reference_post) {
			NewThread nt = NewThread.newInstance(tag, null);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, nt);
			ft.commit();
			D2DTabHost.history.get(tag).push(nt);
			tracker.trackEvent("Click", "Button", "Create New Thread", 0);
		} else if (v.getId() == R.id.img_reference_userimage) {
			Toast.makeText(getActivity(), "Changing status...",
					Toast.LENGTH_LONG).show();
			UserInfo.changeStatus(getActivity(),
					Global.userStatus.equals("online") ? "on call" : "online", D2DTabHost.token);
			tracker.trackEvent("Click", "ImageView", "Change Status", 0);
		} else if (v.getId() == R.id.btn_reference_location) {
			Intent intent = new Intent(getActivity(), UserLocation.class);
			intent.putExtra("update_own", true);
			getActivity().startActivity(intent);
			tracker.trackEvent("Click", "Button", "Show Own Location", 0);
		}
	}

}

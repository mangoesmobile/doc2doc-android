/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.ImageLoader;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class PostReply extends Fragment implements OnClickListener {

	int tag;

	GoogleAnalyticsTracker tracker;

	// configure the notification
	Notification notification;
	NotificationManager notificationManager;

	boolean activityOpen = false;

	ImageView imgUser;
	TextView lblSubject;
	TextView lblAuthor;
	EditText txtReply;
	Button btnAttach;
	Button btnNotify;
	Button btnReply;
	TextView lblNoOfFiles;
	TextView lblNoOfTags;

	ArrayList<CharSequence> attachedFiles = new ArrayList<CharSequence>();
	ArrayList<CharSequence> taggedPeople = new ArrayList<CharSequence>();

	BroadcastReceiver fileReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			attachedFiles = intent.getCharSequenceArrayListExtra("files");
			if (attachedFiles.size() > 0) {
				lblNoOfFiles.setText("(" + attachedFiles.size() + ")");
			} else {
				lblNoOfFiles.setText("");
			}
		}
	};

	BroadcastReceiver tagReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			taggedPeople = intent.getCharSequenceArrayListExtra("tags");
			if (taggedPeople.size() > 0) {
				lblNoOfTags.setText("(" + taggedPeople.size() + ")");
			} else {
				lblNoOfTags.setText("");
			}
		}
	};

	public static PostReply newInstance(int tag, Bundle args) {
		PostReply nt = new PostReply(tag);
		if (args == null)
			args = new Bundle();
		nt.setArguments(args);
		return nt;
	}

	private PostReply(int tag) {
		this.tag = tag;
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub

		activityOpen = true;

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("Reply To "
				+ getArguments().getString("reply_to"));

		prepareNotification();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.replythread, container, false);
		instantiateLayout(v);
		return v;
	}

	@Override
	public void onStart() {
		super.onStart();

		if (getArguments().getString("reply_to").equalsIgnoreCase("Message")) {
			btnNotify.setVisibility(View.INVISIBLE);
		}

		putValues();

		btnAttach.setOnClickListener(this);
		btnNotify.setOnClickListener(this);
		btnReply.setOnClickListener(this);
		
		IntentFilter fileFilter = new IntentFilter();
		fileFilter.addAction(AttachFile.FILE_ATTACHED);
		getActivity().registerReceiver(fileReceiver, fileFilter);

		IntentFilter tagFilter = new IntentFilter();
		tagFilter.addAction(NotifyTeam.PEOPLE_TAGGED);
		getActivity().registerReceiver(tagReceiver, tagFilter);
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (Global.backPressed) {
			if(hidden){
				getActivity().unregisterReceiver(fileReceiver);
				getActivity().unregisterReceiver(tagReceiver);
				activityOpen = false;
			}
			Global.backPressed = false;
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		try {
			getActivity().unregisterReceiver(fileReceiver);
			getActivity().unregisterReceiver(tagReceiver);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onClick(View v) {
		if (v.getId() == R.id.btn_replythread_attach) {
			((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
			.hideSoftInputFromWindow(txtReply.getWindowToken(), 0);
			Bundle args = new Bundle();
			args.putString("subject", getArguments().getString("subject"));
			args.putString("username", getArguments().getString("username")
					+ " on " + getArguments().getString("timestamp"));
			if (attachedFiles.size() > 0) {
				args.putBoolean("has_attachments", true);
				args.putCharSequenceArrayList("attachments", attachedFiles);
			}
			AttachFile af = AttachFile.newInstance(tag, args);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, af);
			ft.commit();
			D2DTabHost.history.get(tag).push(af);
			tracker.trackEvent("Click", "Button",
					"Launch Attach File Activity", 0);
		} else if (v.getId() == R.id.btn_replythread_notify) {
			((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
			.hideSoftInputFromWindow(txtReply.getWindowToken(), 0);
			Bundle args = new Bundle();
			if (taggedPeople.size() > 0) {
				args.putBoolean("has_tags", true);
				args.putCharSequenceArrayList("tags", taggedPeople);
			}
			NotifyTeam nt = NotifyTeam.newInstance(tag, args);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, nt);
			ft.commit();
			D2DTabHost.history.get(tag).push(nt);
			tracker.trackEvent("Click", "Button",
					"Launch Notify People Activity", 0);
		} else if (v.getId() == R.id.btn_replythread_reply) {
			((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
			.hideSoftInputFromWindow(txtReply.getWindowToken(), 0);
			if (txtReply.getText().length() > 0) {
				postReply(txtReply.getText().toString(), getArguments()
						.getString("reply_to").toLowerCase());
				tracker.trackEvent("Click", "Button", "Post "
						+ getArguments().getString("reply_to") + " Reply", 0);
			} else {
				Global.createAlert(getActivity(),
						"Please write something to reply.", "OK");
			}
		}
	}

	private void instantiateLayout(View v) {
		imgUser = (ImageView) v.findViewById(R.id.img_replythread_user);
		lblSubject = (TextView) v.findViewById(R.id.lbl_replythread_title);
		lblAuthor = (TextView) v.findViewById(R.id.lbl_replythread_author);
		txtReply = (EditText) v.findViewById(R.id.txt_replythread_reply);
		btnAttach = (Button) v.findViewById(R.id.btn_replythread_attach);
		btnNotify = (Button) v.findViewById(R.id.btn_replythread_notify);
		btnReply = (Button) v.findViewById(R.id.btn_replythread_reply);
		lblNoOfFiles = (TextView) v
				.findViewById(R.id.lbl_replythread_noofattachment);
		lblNoOfTags = (TextView) v
				.findViewById(R.id.lbl_replythread_noofpeople);
	}

	private void putValues() {
		// Global.loadPhoto(PostReply.this, Global.url
		// + getIntent().getStringExtra("profile_pic"), imgUser, null);
		ImageLoader il = new ImageLoader(getActivity());
		il.DisplayImage(Global.url + getArguments().getString("profile_pic"),
				imgUser, null);
		lblSubject.setText(getArguments().getString("subject"));
		lblAuthor.setText("by " + getArguments().getString("username") + " on "
				+ getArguments().getString("timestamp"));
	}

	private void postReply(String reply, String type) {
		Log.d("ReplyBoardThread", "replying to a thread");
		// Global.showLoading(loading, "Posting reply...", false);
		Toast.makeText(getActivity(), "Posting reply...", Toast.LENGTH_LONG)
				.show();
		btnReply.setEnabled(false);
		Doc2DocRequest dr = new Doc2DocRequest(uploadHandler);
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", D2DTabHost.token);
		params.putString(type + "_id", getArguments().getString("id"));
		params.putString("contents", reply);
		params.putString("tag_user_ids",
				Global.getStringFromList("-", taggedPeople));
		for (int i = 0; i < attachedFiles.size(); i++) {
			params.putString("file_" + (i + 1), attachedFiles.get(i).toString());
		}

		asyncTask.runTask(type + "/post_reply", params,
				new Doc2DocTaskListener() {

					@Override
					public void onMalformedURLException(MalformedURLException e) {
						// TODO Auto-generated method stub
						replyHandler.sendMessage(Message.obtain(replyHandler,
								Global.ERROR_RESPONSE));
						Log.d("board-reply-response", e.getMessage());
					}

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						replyHandler.sendMessage(Message.obtain(replyHandler,
								Global.CONNECTION_ERROR));
						Log.d("board-reply-response", e.getMessage());
					}

					@Override
					public void onFileNotFoundException(FileNotFoundException e) {
						// TODO Auto-generated method stub
						replyHandler.sendMessage(Message.obtain(replyHandler,
								Global.ERROR_RESPONSE));
						Log.d("board-reply-response", e.getMessage());
					}

					@Override
					public void onComplete(String response) {
						Log.d("board-reply-response", response);
						if (response.equals("TRUE")) {
							replyHandler.sendMessage(Message.obtain(
									replyHandler, Global.GOT_RESPONSE));
						} else {
							replyHandler.sendMessage(Message.obtain(
									replyHandler, Global.INVALID_SESSION));
						}
					}

					@Override
					public void onCancel() {
						// TODO Auto-generated method stub
						
					}
				});
	}

	private Handler replyHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				btnReply.setEnabled(true);
				Toast.makeText(getActivity(), "Your reply has been submitted",
						Toast.LENGTH_LONG).show();
				if (activityOpen) {
					getActivity().onBackPressed();
				}
				if (getArguments().getString("reply_to").equalsIgnoreCase(
						"thread")) {
					getActivity().sendBroadcast(
							new Intent(D2DTabHost.REFRESH_BOARD));
				}
				getActivity().sendBroadcast(
						new Intent(D2DTabHost.REFRESH_MESSAGES));
				break;
			case Global.EMPTY_RESPONSE:
				btnReply.setEnabled(true);
				// Global.hideLoading(loading);
				break;
			case Global.CONNECTION_ERROR:
				btnReply.setEnabled(true);
				// Global.hideLoading(loading);
				Toast.makeText(getActivity(), "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				break;
			case Global.ERROR_RESPONSE:
				btnReply.setEnabled(true);
				// Global.hideLoading(loading);
				break;
			case Global.INVALID_SESSION:
				btnReply.setEnabled(true);
				// Global.hideLoading(loading);
				Toast.makeText(getActivity(),
						"Invalid session! Please relogin", Toast.LENGTH_LONG)
						.show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				getActivity().sendBroadcast(i);
				break;
			}
		}
	};

	private void prepareNotification() {
		notification = new Notification(android.R.drawable.stat_sys_upload,
				"Uploading file", System.currentTimeMillis());
		notification.flags = notification.flags
				| Notification.FLAG_ONGOING_EVENT;
		notification.contentView = new RemoteViews(getActivity()
				.getApplicationContext().getPackageName(),
				R.layout.update_notification);
		PendingIntent contentIntent = PendingIntent.getActivity(getActivity()
				.getApplicationContext(), 0, new Intent(),
				PendingIntent.FLAG_UPDATE_CURRENT);
		notification.contentIntent = contentIntent;
		notificationManager = (NotificationManager) getActivity()
				.getSystemService(Context.NOTIFICATION_SERVICE);
	}

	private void updateProgress(int file) {
		notificationManager.notify(file, notification);
	}

	private Handler uploadHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Doc2DocRequest.UPLOAD_START:
				notification.contentView.setTextViewText(
						R.id.text,
						"Uploading "
								+ (String) msg.getData().getString("filename"));
				updateProgress(msg.arg1);
				break;
			case Doc2DocRequest.PUBLISH_PROGRESS:
				updateProgress(msg.arg1);
				break;
			case Doc2DocRequest.UPLOAD_FINISH:
				notificationManager.cancel(msg.arg1);
			default:
				break;
			}
		}
	};

}

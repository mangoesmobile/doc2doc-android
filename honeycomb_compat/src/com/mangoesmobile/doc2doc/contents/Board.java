/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import com.foound.widget.AmazingListView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.adapters.BoardAdapter;
import com.mangoesmobile.doc2doc.cache.BoardMessageCacheManager;
import com.mangoesmobile.doc2doc.services.BoardService;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.ImageLoader;
import com.mangoesmobile.doc2doc.util.UserInfo;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class Board extends Fragment implements OnCheckedChangeListener,
		OnClickListener, OnItemClickListener {
	
	private int tag;

	GoogleAnalyticsTracker tracker;

	Animation searchAnimEntrance;
	Animation searchAnimExit;

	private ImageView imgUser;
	private ProgressBar progUser;
	private ImageView imgUserStatus;
	private TextView lblUsername;
	private TextView lblUserStatus;
	private ImageView imgTopBar;
	private EditText txtSearch;
	private CheckBox chkSearch;
	private Button btnPost;
	private Button btnLocation;
	private AmazingListView listBoard;
	BoardAdapter adapter;

	class BoardHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GET_DATA:
				Log.d("Board", "got data from service");
				adapter.setData(msg.getData());
				break;
			case Global.EMPTY_RESPONSE:
				adapter.notifyNoMorePages();
			default:
				Log.d("Board", "in default");
				super.handleMessage(msg);
				break;
			}
		}
	};

	final Messenger mMessenger = new Messenger(new BoardHandler());
	Messenger mService;

	public ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			mService = new Messenger(service);
			Message msg = Message.obtain();
			msg.what = Global.MSG_REGISTER_CLIENT;
			msg.replyTo = mMessenger;
			try {
				Log.d("Board", "sending client to service");
				mService.send(msg);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected - process crashed.
			Log.d("Board", "service disconnected");
			mService = null;
		}
	};

	private BroadcastReceiver refreshReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			// TODO Auto-generated method stub
			refreshBoard();
		}
	};

	BroadcastReceiver userInfoReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			int response = intent
					.getIntExtra("response", Global.ERROR_RESPONSE);
			switch (response) {
			case Global.GOT_RESPONSE:
				updateUserInfo();
				break;
			case Global.CHANGE_STATUS:
				Global.userStatus = intent.getStringExtra("userStatus");
				Global.userInfo.putString("availability_status",
						Global.userStatus);
				updateUserInfo();
				break;
			case Global.CHANGE_PIC:
				fetchUserInfo();
				refreshBoard();
				break;
			case Global.EMPTY_RESPONSE:
				break;
			case Global.CONNECTION_ERROR:
				Toast.makeText(getActivity(), "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				break;
			case Global.ERROR_RESPONSE:
				break;
			case Global.INVALID_SESSION:
				Toast.makeText(getActivity(),
						"Invalid session! Please relogin", Toast.LENGTH_LONG)
						.show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				getActivity().sendBroadcast(i);
				break;
			}
		}
	};

	public Board(int tag) {
		this.tag = tag;
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("Board");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.board, container, false);
		instantiateLayout(v);
		return v;
	}

	private void instantiateLayout(View v) {
		searchAnimEntrance = AnimationUtils.loadAnimation(getActivity(),
				R.anim.searchbox_entrance);
		searchAnimExit = AnimationUtils.loadAnimation(getActivity(),
				R.anim.searchbox_exit);

		imgUser = (ImageView) v.findViewById(R.id.img_userimage);
		progUser = (ProgressBar) v.findViewById(R.id.prog_userimage);
		imgUserStatus = (ImageView) v.findViewById(R.id.img_userstatus);
		lblUsername = (TextView) v.findViewById(R.id.lbl_username);
		lblUserStatus = (TextView) v.findViewById(R.id.lbl_userstatus);
		imgTopBar = (ImageView) v.findViewById(R.id.img_topbar);
		txtSearch = (EditText) v.findViewById(R.id.txt_search);
		chkSearch = (CheckBox) v.findViewById(R.id.chk_search);
		listBoard = (AmazingListView) v.findViewById(R.id.ls_board);
		btnPost = (Button) v.findViewById(R.id.btn_post);
		btnLocation = (Button) v.findViewById(R.id.btn_mainthread_location);
		adapter = new BoardAdapter(getActivity(), D2DTabHost.token);
	}

	@Override
	public void onStart() {
		super.onStart();

		getActivity().getApplicationContext().bindService(
				new Intent(getActivity(), BoardService.class), mConnection,
				Context.BIND_AUTO_CREATE);

		// listens for refresh from tab menu
		IntentFilter filter = new IntentFilter();
		filter.addAction(D2DTabHost.REFRESH_BOARD);
		getActivity().registerReceiver(refreshReceiver, filter);

		// listens for update of user info
		IntentFilter userFilter = new IntentFilter();
		userFilter.addAction(UserInfo.GOT_USERINFO);
		getActivity().registerReceiver(userInfoReceiver, userFilter);

		fetchUserInfo();

		imgUser.setOnClickListener(this);
		chkSearch.setOnCheckedChangeListener(this);
		btnPost.setOnClickListener(this);
		btnLocation.setOnClickListener(this);
		txtSearch.addTextChangedListener(new BoardTextWatcher());
		listBoard.setOnItemClickListener(this);
		listBoard.setLoadingView(getActivity().getLayoutInflater().inflate(
				R.layout.loading_view, null));
		listBoard.setAdapter(adapter);
		fetchThreadFromCache();
	}

	// @Override
	// public void onResume() {
	// super.onResume();
	//
	// if (Global.userInfo.size() > 0) {
	// updateUserInfo();
	// }
	// }

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (Global.backPressed) {
			Global.backPressed = false;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		getActivity().getApplicationContext().unbindService(mConnection);
		getActivity().unregisterReceiver(refreshReceiver);
		getActivity().unregisterReceiver(userInfoReceiver);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_post) {
			NewThread nt = NewThread.newInstance(tag, null);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, nt);
			ft.commit();
			D2DTabHost.history.get(tag).push(nt);
			tracker.trackEvent("Click", "Button", "Create New Thread", 0);
		} else if (v.getId() == R.id.img_userimage) {
			Toast.makeText(getActivity(), "Changing status...",
					Toast.LENGTH_LONG).show();
			UserInfo.changeStatus(getActivity(),
					Global.userStatus.equals("online") ? "on call" : "online", D2DTabHost.token);
			tracker.trackEvent("Click", "ImageView", "Change Status", 0);
		} else if (v.getId() == R.id.btn_mainthread_location) {
			Intent intent = new Intent(getActivity(), UserLocation.class);
			intent.putExtra("update_own", true);
			getActivity().startActivity(intent);
			tracker.trackEvent("Click", "Button", "Show Own Location", 0);
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		if (isChecked) {
			imgTopBar.setImageResource(R.drawable.bg_top_search);
			txtSearch.setVisibility(View.VISIBLE);
			txtSearch.startAnimation(searchAnimEntrance);
			txtSearch.requestFocus();
			((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
					.showSoftInput(txtSearch, InputMethodManager.SHOW_FORCED);
		} else {
			txtSearch.startAnimation(searchAnimExit);
			txtSearch.setText("");
			searchAnimExit
					.setAnimationListener(new Animation.AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationRepeat(Animation animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationEnd(Animation animation) {
							// TODO Auto-generated method stub
							imgTopBar.setImageResource(R.drawable.bg_top);
							txtSearch.setVisibility(View.GONE);
						}
					});
			((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
			.hideSoftInputFromWindow(txtSearch.getWindowToken(), 0);
		}
	}

	private void fetchThreadFromCache() {
		BoardMessageCacheManager manager = new BoardMessageCacheManager(
				getActivity(), "board");
		manager.open();
		Cursor cursor = manager.fetchAll();
//		getActivity().startManagingCursor(cursor);
		if (cursor.getCount() > 0) {
			Bundle data = new Bundle();
			cursor.moveToFirst();
			for (int i = 0; i < cursor.getCount(); i++) {
				Bundle content = new Bundle();
				content.putString("id", cursor.getString(cursor
						.getColumnIndex(BoardMessageCacheManager.ID)));
				content.putString("subject", cursor.getString(cursor
						.getColumnIndex(BoardMessageCacheManager.SUBJECT)));
				content.putString("contents", cursor.getString(cursor
						.getColumnIndex(BoardMessageCacheManager.CONTENTS)));
				content.putString("timestamp", cursor.getString(cursor
						.getColumnIndex(BoardMessageCacheManager.TIMESTAMP)));
				content.putString("status", cursor.getString(cursor
						.getColumnIndex(BoardMessageCacheManager.STATUS)));
				content.putString("user_id", cursor.getString(cursor
						.getColumnIndex(BoardMessageCacheManager.USER_ID)));
				content.putString("username", cursor.getString(cursor
						.getColumnIndex(BoardMessageCacheManager.USERNAME)));
				content.putString("profile_pic", cursor.getString(cursor
						.getColumnIndex(BoardMessageCacheManager.PROFILE_PIC)));
				String total_num_of_replies = cursor
						.getString(cursor
								.getColumnIndex(BoardMessageCacheManager.TOTAL_NUM_OF_REPLIES));
				content.putString("total_num_of_replies", total_num_of_replies);
				if (Integer.valueOf(total_num_of_replies) > 0) {
					content.putString(
							"reply_contents",
							cursor.getString(cursor
									.getColumnIndex(BoardMessageCacheManager.REPLY_CONTENTS)));
					content.putString(
							"reply_author",
							cursor.getString(cursor
									.getColumnIndex(BoardMessageCacheManager.REPLY_AUTHOR)));
					content.putString(
							"reply_time",
							cursor.getString(cursor
									.getColumnIndex(BoardMessageCacheManager.REPLY_TIME)));
				}
				String attachments = cursor
						.getString(cursor
								.getColumnIndex(BoardMessageCacheManager.ATTACHED_FILES));
				if (attachments.length() > 0)
					content.putString("attached_files", attachments);
				String keywords = cursor
						.getString(cursor
								.getColumnIndex(BoardMessageCacheManager.TAGGED_KEYWORDS));
				if (keywords.length() > 0)
					content.putString("tagged_keywords", keywords);
				String tags = cursor.getString(cursor
						.getColumnIndex(BoardMessageCacheManager.TAGGED_USERS));
				if (tags.length() > 0)
					content.putString("tagged_users", tags);
				data.putBundle(String.valueOf(i), content);
				cursor.moveToNext();
			}
			adapter.setData(data);
			adapter.setInitialPage(data.size() / 10); // it indicates that cache
														// has got this much
														// page of data
			adapter.resetPage();
		} else {
			adapter.notifyMayHaveMorePages();
		}
		cursor.close();
		manager.close();
	}

	private void fetchUserInfo() {
		UserInfo.fetchUserInfo(getActivity(), D2DTabHost.token);
	}

	private void updateUserInfo() {
		String userStatus = Global.userInfo.getString("availability_status");
		Log.d("user-status", userStatus);
		if (userStatus.equals("online")) {
			imgUserStatus.setImageResource(R.drawable.bg_photo_online);
			lblUserStatus.setTextColor(Color.rgb(0, 102, 153));
		} else if (userStatus.equals("on call")) {
			imgUserStatus.setImageResource(R.drawable.bg_photo_oncall);
			lblUserStatus.setTextColor(Color.rgb(101, 176, 73));
		}
		lblUserStatus.setText(userStatus.toUpperCase());
		lblUsername.setText(Global.userInfo.getString("first_name").substring(0, 1)
				+ ". " + Global.userInfo.getString("last_name"));
		if (Global.profilePic == null) {
			// Global.loadPhoto(
			// Board.this,
			// Global.url
			// + Global.userInfo
			// .getString("profile_pic_file_path"),
			// imgUser, progUser);
			ImageLoader il = new ImageLoader(getActivity());
			il.DisplayImage(
					Global.url
							+ Global.userInfo
									.getString("profile_pic_file_path"),
					imgUser, progUser);
		} else {
			imgUser.setImageDrawable(Global.profilePic);
			progUser.setVisibility(View.GONE);
		}
	}

	// sends message to service to refresh the board
	private void refreshBoard() {
		Message msg = Message.obtain();
		msg.what = Global.REFRESH_BOARD;
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// opens board details
	private void showBoardDetails(Bundle content) {
		// Intent intent = new Intent(Board.this, BoardDetails.class);
		// intent.putExtra("content", content);
		// ActivityGroupBoard parent = (ActivityGroupBoard) getParent();
		// parent.startChildActivity("board_details", intent);
		BoardDetails bd = BoardDetails.newInstance(tag, content);
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.hide(D2DTabHost.history.get(tag).peek());
		ft.add(tag, bd);
		ft.commit();
		D2DTabHost.history.get(tag).push(bd);
		content.putString("status", "read");
		adapter.notifyDataSetChanged();
		BoardMessageCacheManager manager = new BoardMessageCacheManager(
				getActivity(), "board");
		manager.open();
		manager.updateStatus("read", content.getString("id"));
		manager.close();
	}

	class BoardTextWatcher implements TextWatcher {

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			adapter.getFilter().filter(s);
			tracker.trackEvent("TextChange", "EditText", "Search Board", 0);
		}

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		Bundle content = (Bundle) adapter.getItem(arg2);
		showBoardDetails(content);
		tracker.trackEvent("Click", "ListItem", "Show Thread Details", 0);
	}
}

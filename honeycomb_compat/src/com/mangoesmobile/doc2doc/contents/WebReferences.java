/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.util.Global;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class WebReferences extends Fragment implements OnClickListener {

	int tag;
	
	GoogleAnalyticsTracker tracker;
	
	private ProgressDialog loading;
	//
	// SeekBar seekRate;
	// Button btnLogout;
	// TextView lblRate;
	WebView webview;
	ImageButton btnHome;
	ImageButton btnSelectText;
	Button btnBack;
	Button btnForward;

	public static WebReferences newInstance(int tag, Bundle args) {
		WebReferences wr = new WebReferences(tag);
		if (args == null)
			args = new Bundle();
		wr.setArguments(args);
		return wr;
	}

	private WebReferences(int tag) {
		this.tag = tag;
	}
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("References");
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.reference, container, false);
		instantiateLayout(v);
		return v;
	}
	
	private void instantiateLayout(View v) {
		loading = new ProgressDialog(getActivity());
		loading.setMessage("Loading, please wait...");

		webview = (WebView) v.findViewById(R.id.web_reference);
		btnHome = (ImageButton) v.findViewById(R.id.btn_reference_home);
		btnSelectText = (ImageButton) v.findViewById(R.id.btn_reference_selectandcopy);
		btnBack = (Button) v.findViewById(R.id.btn_ref_back);
		btnForward = (Button) v.findViewById(R.id.btn_ref_forward);
		
		webview.requestFocus(View.FOCUS_DOWN);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.getSettings().setBuiltInZoomControls(true);
		webview.loadUrl(getArguments().getString("url"));

		webview.setWebViewClient(new ReferenceWebViewClient());
	}

	private void emulateShiftHeld(WebView view) {
		try {
			KeyEvent shiftPressEvent = new KeyEvent(0, 0, KeyEvent.ACTION_DOWN,
					KeyEvent.KEYCODE_SHIFT_LEFT, 0, 0);
			shiftPressEvent.dispatch(view);
			Toast.makeText(getActivity(), "Select text now", Toast.LENGTH_SHORT).show();
		} catch (Exception e) {
			Log.e("dd", "Exception in emulateShiftHeld()", e);
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		
		btnHome.setOnClickListener(this);		
		btnSelectText.setOnClickListener(this);		
		btnBack.setOnClickListener(this);
		btnForward.setOnClickListener(this);
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (Global.backPressed) {
			Global.backPressed = false;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		loading.dismiss();
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==R.id.btn_reference_home) {
			getActivity().onBackPressed();
		} else if(v.getId()==R.id.btn_reference_selectandcopy) {
			emulateShiftHeld(webview);
			tracker.trackEvent("Click", "Button", "Copy Text From Reference Page", 0);
		} else if(v.getId()==R.id.btn_ref_back) {
			webview.goBack();
		} else if(v.getId()==R.id.btn_ref_forward) {
			webview.goForward();
		}
	}

	private class ReferenceWebViewClient extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			loading.show();
			btnBack.setClickable(false);
			btnForward.setClickable(false);
			btnBack.setBackgroundResource(R.drawable.btn_backward_down);
			btnForward.setBackgroundResource(R.drawable.btn_forward_down);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			loading.hide();
			if(view.canGoBack()) {
				btnBack.setClickable(true);
				btnBack.setBackgroundResource(R.drawable.btn_backward_click);
			}
			if(view.canGoForward()) {
				btnForward.setClickable(true);
				btnForward.setBackgroundResource(R.drawable.btn_forward_click);
			}
		}
	}
}

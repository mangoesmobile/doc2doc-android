/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.Login;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.cache.DraftManager;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;
import com.mangoesmobile.doc2doc.util.ImageLoader;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class NewThread extends Fragment implements OnClickListener {

	SharedPreferences prefs;
	String token;
	
	int tag;

	GoogleAnalyticsTracker tracker;

	boolean activityOpen = false;
	boolean isPosting = false;

	// configure the notification
	Notification notification;
	NotificationManager notificationManager;

	ImageView imgUser;
	EditText txtTitle;
	EditText txtDetails;
	EditText txtTaggedKeywords;
	Button btnAttach;
	TextView lblNoOfFiles;
	Button btnNotify;
	TextView lblNoOfTags;
	Button btnPost;
	Button btnSave;

	boolean fromDraft = false;
	boolean offline = false;

	private ArrayList<CharSequence> attachedFiles = new ArrayList<CharSequence>();
	private ArrayList<CharSequence> taggedPeople = new ArrayList<CharSequence>();

	BroadcastReceiver fileReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			attachedFiles = intent.getCharSequenceArrayListExtra("files");
			if (attachedFiles.size() > 0) {
				lblNoOfFiles.setText("(" + attachedFiles.size() + ")");
			} else {
				lblNoOfFiles.setText("");
			}
		}
	};

	BroadcastReceiver tagReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			taggedPeople = intent.getCharSequenceArrayListExtra("tags");
			if (taggedPeople.size() > 0) {
				lblNoOfTags.setText("(" + taggedPeople.size() + ")");
			} else {
				lblNoOfTags.setText("");
			}
		}
	};

	public static NewThread newInstance(int tag, Bundle args) {
		NewThread nt = new NewThread(tag);
		if (args == null)
			args = new Bundle();
		nt.setArguments(args);
		return nt;
	}

	private NewThread(int tag) {
		this.tag = tag;
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub

		prefs = getActivity().getSharedPreferences("session", Context.MODE_PRIVATE);
		token = prefs.getString("token", "");
		
		fromDraft = getArguments().getBoolean("from_draft", false);
		offline = getArguments().getBoolean("offline", false);

		if (!offline && token.equals("")) {
			getActivity().finish();
			startActivity(new Intent(getActivity(), Login.class));
		}

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("Post New Thread");

		activityOpen = true;

		prepareNotification();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.postthread, container, false);
		instantiateLayout(v);
		return v;
	}

	@Override
	public void onStart() {
		super.onStart();

		if (Global.profilePic != null) {
			imgUser.setImageDrawable(Global.profilePic);
		}
		putValues();

		btnAttach.setOnClickListener(this);
		btnNotify.setOnClickListener(this);
		btnPost.setOnClickListener(this);
		btnSave.setOnClickListener(this);

		IntentFilter fileFilter = new IntentFilter();
		fileFilter.addAction(AttachFile.FILE_ATTACHED);
		getActivity().registerReceiver(fileReceiver, fileFilter);

		IntentFilter tagFilter = new IntentFilter();
		tagFilter.addAction(NotifyTeam.PEOPLE_TAGGED);
		getActivity().registerReceiver(tagReceiver, tagFilter);
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (Global.backPressed) {
			if (hidden) {
				try {
					getActivity().unregisterReceiver(fileReceiver);
					getActivity().unregisterReceiver(tagReceiver);
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (!isPosting) {
					if (txtTitle.getText().length() > 0
							|| txtDetails.getText().length() > 0
							|| attachedFiles.size() > 0) {
						saveToDraft();
						Toast.makeText(getActivity(), "Saved to drafts",
								Toast.LENGTH_SHORT).show();
						tracker.trackEvent("Click", "Button",
								"Save Thread Draft", 0);
					}
				}
				activityOpen = false;
			}
			Global.backPressed = false;
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		try {
			getActivity().unregisterReceiver(fileReceiver);
			getActivity().unregisterReceiver(tagReceiver);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// @Override
	// public void onBackPressed() {
	// super.onBackPressed();
	//
	// if (!isPosting) {
	// if (txtTitle.getText().length() > 0
	// || txtDetails.getText().length() > 0
	// || attachedFiles.size() > 0) {
	// saveToDraft();
	// Toast.makeText(getActivity(), "Saved to drafts",
	// Toast.LENGTH_SHORT).show();
	// tracker.trackEvent("Click", "Button", "Save Thread Draft", 0);
	// }
	// }
	// }

	private void instantiateLayout(View v) {
		imgUser = (ImageView) v.findViewById(R.id.img_postthread_user);
		txtTitle = (EditText) v.findViewById(R.id.txt_postthread_title);
		txtDetails = (EditText) v.findViewById(R.id.txt_postthread_details);
		txtTaggedKeywords = (EditText) v.findViewById(R.id.txt_postthread_tags);
		btnAttach = (Button) v.findViewById(R.id.btn_postthread_attach);
		lblNoOfFiles = (TextView) v
				.findViewById(R.id.lbl_postthread_noofattachment);
		btnNotify = (Button) v.findViewById(R.id.btn_postthread_notify);
		lblNoOfTags = (TextView) v.findViewById(R.id.lbl_postthread_noofpeople);
		btnPost = (Button) v.findViewById(R.id.btn_postthread_post);
		btnSave = (Button) v.findViewById(R.id.btn_postthread_save);
		if (offline) {
			btnPost.setVisibility(View.GONE);
			btnNotify.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_postthread_attach) {
			((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
			.hideSoftInputFromWindow(txtDetails.getWindowToken(), 0);
			Bundle args = new Bundle();
			args.putString("subject", txtTitle.getText().toString());
			args.putString("username", "by you");
			if (attachedFiles.size() > 0) {
				args.putBoolean("has_attachments", true);
				args.putCharSequenceArrayList("attachments", attachedFiles);
			}
			AttachFile af = AttachFile.newInstance(tag, args);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, af);
			ft.commit();
			D2DTabHost.history.get(tag).push(af);
			tracker.trackEvent("Click", "Button",
					"Launch Attach File Activity", 0);
		} else if (v.getId() == R.id.btn_postthread_notify) {
			((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
			.hideSoftInputFromWindow(txtDetails.getWindowToken(), 0);
			Bundle args = new Bundle();
			if (taggedPeople.size() > 0) {
				args.putBoolean("has_tags", true);
				args.putCharSequenceArrayList("tags", taggedPeople);
			}
			NotifyTeam nt = NotifyTeam.newInstance(tag, args);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.hide(D2DTabHost.history.get(tag).peek());
			ft.add(tag, nt);
			ft.commit();
			D2DTabHost.history.get(tag).push(nt);
			tracker.trackEvent("Click", "Button",
					"Launch Notify People Activity", 0);
		} else if (v.getId() == R.id.btn_postthread_post) {
			((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
			.hideSoftInputFromWindow(txtDetails.getWindowToken(), 0);
			if (txtTitle.getText().length() > 0
					&& txtDetails.getText().length() > 0) {
				postThread(txtTitle.getText().toString(), txtDetails.getText()
						.toString(), txtTaggedKeywords.getText().toString());
				isPosting = true;
				tracker.trackEvent("Click", "Button", "Post Thread", 0);
			} else {
				Global.createAlert(getActivity(),
						"Please enter title and(or) details.", "OK");
			}
		} else if (v.getId() == R.id.btn_postthread_save) {
			((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
			.hideSoftInputFromWindow(txtDetails.getWindowToken(), 0);
			if (txtTitle.getText().length() > 0
					&& txtDetails.getText().length() > 0) {
				if (activityOpen) {
					getActivity().onBackPressed();
				}
				tracker.trackEvent("Click", "Button", "Save Thread Draft", 0);
			} else {
				Global.createAlert(getActivity(),
						"Please enter title and(or) details.", "OK");
			}
		}
	}

	private void saveToDraft() {
		DraftManager manager = new DraftManager(getActivity(), "thread_draft");
		manager.open();
		if (fromDraft) {
			manager.deleteContent(getArguments().getString("id"));
		}
		manager.insertContent(txtTitle.getText().toString(), txtDetails
				.getText().toString(), Global.getStringFromList(",",
				attachedFiles), txtTaggedKeywords.getText().toString(), Global
				.getStringFromList(",", taggedPeople), "null", new Timestamp(
				System.currentTimeMillis()).toLocaleString());
		manager.close();
		getActivity().sendBroadcast(new Intent(D2DTabHost.REFRESH_MESSAGES));
	}

	private void putValues() {
		ImageLoader il = new ImageLoader(getActivity());
		il.DisplayImage(
				Global.url + Global.userInfo.getString("profile_pic_file_path"),
				imgUser, null);
		if (fromDraft) {
			txtTitle.setText(getArguments().getString("subject"));
			txtDetails.setText(getArguments().getString("contents"));
			txtTaggedKeywords.setText(getArguments().getString(
					"tagged_keywords"));
			String attachments = getArguments().getString("attached_files");
			if (attachments != null && !attachments.equals("null")) {
				String[] attachs = attachments.split(",");
				for (int i = 0; i < attachs.length; i++) {
					attachedFiles.add(attachs[i]);
				}
				if (attachedFiles.size() > 0) {
					lblNoOfFiles.setText("(" + attachedFiles.size() + ")");
				}
			}
			String tagged = getArguments().getString("tagged_users");
			if (tagged != null && !tagged.equals("null")) {
				String[] tags = tagged.split(",");
				for (int i = 0; i < tags.length; i++) {
					taggedPeople.add(tags[i]);
				}
				if (taggedPeople.size() > 0) {
					lblNoOfTags.setText("(" + taggedPeople.size() + ")");
				}
			}
		} else if (getActivity().getIntent().getAction() != null
				&& getActivity().getIntent().getAction()
						.equals(Intent.ACTION_SEND)) {
			if (getActivity().getIntent().getExtras()
					.containsKey(Intent.EXTRA_STREAM)) {
				Uri uri = (Uri) getActivity().getIntent().getExtras()
						.getParcelable(Intent.EXTRA_STREAM);
				Log.d("path", uri.toString());
				if (uri.toString().startsWith("file")) {
					attachedFiles.add(uri.toString());
				} else {
					attachedFiles.add(getPath(uri));
				}
				lblNoOfFiles.setText("(" + attachedFiles.size() + ")");
			}
		}
	}

	private String getPath(Uri uri) {
		String[] projection = { MediaColumns.DATA };
		Cursor cursor = getActivity().managedQuery(uri, projection, null, null,
				null);
		int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
		cursor.moveToFirst();
		String path = cursor.getString(column_index);
		cursor.close();
		return path;
	}

	private void postThread(String subject, String contents, String tags) {
		Log.d("PostNew", "posting a thread");
		// Global.showLoading(loading, "Posting thread...", false);
		Toast.makeText(getActivity(), "Posting thread...", Toast.LENGTH_LONG)
				.show();
		btnPost.setEnabled(false);
		Doc2DocRequest dr = new Doc2DocRequest(uploadHandler);
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", token);
		params.putString("subject", subject);
		params.putString("contents", contents);
		params.putString("tag_user_ids",
				Global.getStringFromList("-", taggedPeople));
		params.putString("tag_keywords", Global.formatValues(tags, "null"));
		for (int i = 0; i < attachedFiles.size(); i++) {
			Log.d("file_" + (i + 1), attachedFiles.get(i).toString());
			params.putString("file_" + (i + 1), attachedFiles.get(i).toString());
		}

		asyncTask.runTask("thread/post_new", params, new Doc2DocTaskListener() {

			@Override
			public void onMalformedURLException(MalformedURLException e) {
				// TODO Auto-generated method stub
				postHandler.sendMessage(Message.obtain(postHandler,
						Global.ERROR_RESPONSE));
				Log.d("post-new-response-m", e.getMessage());
			}

			@Override
			public void onIOException(IOException e) {
				// TODO Auto-generated method stub
				postHandler.sendMessage(Message.obtain(postHandler,
						Global.CONNECTION_ERROR));
				Log.d("post-new-response-i", e.getMessage());
			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e) {
				// TODO Auto-generated method stub
				postHandler.sendMessage(Message.obtain(postHandler,
						Global.ERROR_RESPONSE));
				Log.d("post-new-response-f", e.getMessage());
			}

			@Override
			public void onComplete(String response) {
				Log.d("post-new-response-c", response);
				if (response.equals("TRUE")) {
					postHandler.sendMessage(Message.obtain(postHandler,
							Global.GOT_RESPONSE));
				} else {
					postHandler.sendMessage(Message.obtain(postHandler,
							Global.INVALID_SESSION));
				}
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				
			}
		});
	}

	private Handler postHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				btnPost.setEnabled(true);
				// Board.UPDATE_BOARD = true;
				if (fromDraft) {
					DraftManager manager = new DraftManager(getActivity(),
							"thread_draft");
					manager.open();
					manager.deleteContent(getArguments().getString("id"));
					manager.close();
				}
				Toast.makeText(getActivity(), "Your thread has been posted",
						Toast.LENGTH_LONG).show();
				getActivity().sendBroadcast(
						new Intent(D2DTabHost.REFRESH_BOARD));
				// sendBroadcast(new Intent(D2DTabHost.REFRESH_MESSAGES));
				if (activityOpen) {
					getActivity().onBackPressed();
				}
				break;
			case Global.EMPTY_RESPONSE:
				btnPost.setEnabled(true);
				// Global.hideLoading(loading);
				break;
			case Global.CONNECTION_ERROR:
				btnPost.setEnabled(true);
				// Global.hideLoading(loading);
				Toast.makeText(getActivity(), "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				break;
			case Global.ERROR_RESPONSE:
				btnPost.setEnabled(true);
				// Global.hideLoading(loading);
				break;
			case Global.INVALID_SESSION:
				btnPost.setEnabled(true);
				// Global.hideLoading(loading);
				Toast.makeText(getActivity(),
						"Invalid session! Please relogin", Toast.LENGTH_LONG)
						.show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				getActivity().sendBroadcast(i);
				break;
			}
		}
	};

	private void prepareNotification() {
		notification = new Notification(android.R.drawable.stat_sys_upload,
				"Uploading file", System.currentTimeMillis());
		notification.flags |= Notification.FLAG_ONGOING_EVENT;
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.contentView = new RemoteViews(getActivity()
				.getApplicationContext().getPackageName(),
				R.layout.update_notification);
		PendingIntent contentIntent = PendingIntent.getActivity(getActivity()
				.getApplicationContext(), 0, new Intent(),
				PendingIntent.FLAG_UPDATE_CURRENT);
		notification.contentIntent = contentIntent;
		notificationManager = (NotificationManager) getActivity()
				.getSystemService(Context.NOTIFICATION_SERVICE);
	}

	private void updateProgress(int file) {
		notificationManager.notify(file, notification);
	}

	private Handler uploadHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Doc2DocRequest.UPLOAD_START:
				notification.contentView.setTextViewText(R.id.text,
						"Uploading " + msg.getData().getString("filename"));
				updateProgress(msg.arg1);
				break;
			case Doc2DocRequest.PUBLISH_PROGRESS:
				updateProgress(msg.arg1);
				break;
			case Doc2DocRequest.UPLOAD_FINISH:
				notificationManager.cancel(msg.arg1);
			default:
				break;
			}
		}
	};

}

/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.util.Global;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class OtherGuideline extends Fragment {
	
	int tag;
	
	GoogleAnalyticsTracker tracker;
	
	Button btnMD;
	Button btnEP;
	Button btnMDCalc;
	Button btnMDConsult;
	Button btnCT;
	Button btnTN;
	Button btn5MC;
	
	public static OtherGuideline newInstance(int tag, Bundle args) {
		OtherGuideline wr = new OtherGuideline(tag);
		if (args == null)
			args = new Bundle();
		wr.setArguments(args);
		return wr;
	}

	private OtherGuideline(int tag) {
		this.tag = tag;
	}
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);	
	    // TODO Auto-generated method stub
	    
	    tracker = GoogleAnalyticsTracker.getInstance();
	    tracker.trackPageView("Other Guideline");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.other_guidelines, container, false);
		instantiateLayout(v);
		return v;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		btnMD.setOnClickListener(new ButtonClickListener(0));		
		btnEP.setOnClickListener(new ButtonClickListener(1));		
		btnMDCalc.setOnClickListener(new ButtonClickListener(2));		
		btnMDConsult.setOnClickListener(new ButtonClickListener(3));		
		btnCT.setOnClickListener(new ButtonClickListener(4));		
		btnTN.setOnClickListener(new ButtonClickListener(5));		
		btn5MC.setOnClickListener(new ButtonClickListener(6));		
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (Global.backPressed) {
			Global.backPressed = false;
		}
	}
	
	private void instantiateLayout(View v) {
		btnMD = (Button) v.findViewById(R.id.btn_medical_dictionary);
		btnEP = (Button) v.findViewById(R.id.btn_epocrates);
		btnMDCalc = (Button) v.findViewById(R.id.btn_mdcalc);
		btnMDConsult = (Button) v.findViewById(R.id.btn_mdconsult);
		btnCT = (Button) v.findViewById(R.id.btn_clinical_trails);
		btnTN = (Button) v.findViewById(R.id.btn_toxnet);
		btn5MC = (Button) v.findViewById(R.id.btn_5min_consult);
	}
	
	class ButtonClickListener implements OnClickListener {

		int index;
		
		public ButtonClickListener(int index) {
			this.index = index;
		}
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Bundle args = new Bundle();
			Fragment f;
			FragmentTransaction ft;
			switch (index) {
			case 0:
				args.putString("url", "http://www.merriam-webster.com/mobilemedlineplus/");
				f = WebReferences.newInstance(tag, args);
				ft = getFragmentManager().beginTransaction();
				ft.hide(D2DTabHost.history.get(tag).peek());
				ft.add(tag, f);
				ft.commit();
				D2DTabHost.history.get(tag).push(f);
//				intent = new Intent(OtherGuideline.this, WebReferences.class);
//				intent.putExtra("url", "http://www.merriam-webster.com/mobilemedlineplus/");
//				parent.startChildActivity("web_reference", intent);
				tracker.trackEvent("Click", "Button", "Medical Dictionary", 0);
				break;
			case 1:
				args.putString("url", "https://online.epocrates.com/noFrame/");
				f = WebReferences.newInstance(tag, args);
				ft = getFragmentManager().beginTransaction();
				ft.hide(D2DTabHost.history.get(tag).peek());
				ft.add(tag, f);
				ft.commit();
				D2DTabHost.history.get(tag).push(f);
//				intent = new Intent(OtherGuideline.this, WebReferences.class);
//				intent.putExtra("url", "https://online.epocrates.com/noFrame/");
//				parent.startChildActivity("web_reference", intent);
				tracker.trackEvent("Click", "Button", "Epocrates", 0);
				break;
			case 2:
				args.putString("url", "http://www.mdcalc.com/");
				f = WebReferences.newInstance(tag, args);
				ft = getFragmentManager().beginTransaction();
				ft.hide(D2DTabHost.history.get(tag).peek());
				ft.add(tag, f);
				ft.commit();
				D2DTabHost.history.get(tag).push(f);
//				intent = new Intent(OtherGuideline.this, WebReferences.class);
//				intent.putExtra("url", "http://www.mdcalc.com/");
//				parent.startChildActivity("web_reference", intent);
				tracker.trackEvent("Click", "Button", "MDCalc", 0);
				break;
			case 3:
				args.putString("url", "http://m.mdconsult.com/login/login.do?URI=http://mobile.mdconsult.com/home/home.do");
				f = WebReferences.newInstance(tag, args);
				ft = getFragmentManager().beginTransaction();
				ft.hide(D2DTabHost.history.get(tag).peek());
				ft.add(tag, f);
				ft.commit();
				D2DTabHost.history.get(tag).push(f);
//				intent = new Intent(OtherGuideline.this, WebReferences.class);
//				intent.putExtra("url", "http://m.mdconsult.com/login/login.do?URI=http://mobile.mdconsult.com/home/home.do");
//				parent.startChildActivity("web_reference", intent);
				tracker.trackEvent("Click", "Button", "MDConsult", 0);
				break;
			case 4:
				args.putString("url", "http://pubmedhh.nlm.nih.gov/nlmd/clinicaltrials/ctsearch.html");
				f = WebReferences.newInstance(tag, args);
				ft = getFragmentManager().beginTransaction();
				ft.hide(D2DTabHost.history.get(tag).peek());
				ft.add(tag, f);
				ft.commit();
				D2DTabHost.history.get(tag).push(f);
//				intent = new Intent(OtherGuideline.this, WebReferences.class);
//				intent.putExtra("url", "http://pubmedhh.nlm.nih.gov/nlmd/clinicaltrials/ctsearch.html");
//				parent.startChildActivity("web_reference", intent);
				tracker.trackEvent("Click", "Button", "Clinical Trials", 0);
				break;
			case 5:
				args.putString("url", "http://toxnet.nlm.nih.gov/pda/");
				f = WebReferences.newInstance(tag, args);
				ft = getFragmentManager().beginTransaction();
				ft.hide(D2DTabHost.history.get(tag).peek());
				ft.add(tag, f);
				ft.commit();
				D2DTabHost.history.get(tag).push(f);
//				intent = new Intent(OtherGuideline.this, WebReferences.class);
//				intent.putExtra("url", "http://toxnet.nlm.nih.gov/pda/");
//				parent.startChildActivity("web_reference", intent);
				tracker.trackEvent("Click", "Button", "ToxNet", 0);
				break;
			case 6:
				args.putString("url", "http://m.5minuteconsult.com/login");
				f = WebReferences.newInstance(tag, args);
				ft = getFragmentManager().beginTransaction();
				ft.hide(D2DTabHost.history.get(tag).peek());
				ft.add(tag, f);
				ft.commit();
				D2DTabHost.history.get(tag).push(f);
//				intent = new Intent(OtherGuideline.this, WebReferences.class);
//				intent.putExtra("url", "http://m.5minuteconsult.com/login");
//				parent.startChildActivity("web_reference", intent);
				tracker.trackEvent("Click", "Button", "5 Minute Consult", 0);
				break;

			default:
				break;
			}
		}
		
	}

}

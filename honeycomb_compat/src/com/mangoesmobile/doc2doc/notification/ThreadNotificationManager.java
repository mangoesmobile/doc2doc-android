/**
 * 
 */
package com.mangoesmobile.doc2doc.notification;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.foound.widget.AmazingListView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.adapters.MyThreadAdapter;
import com.mangoesmobile.doc2doc.cache.BoardMessageCacheManager;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class ThreadNotificationManager {
	
	GoogleAnalyticsTracker tracker;
	
	Activity context;
	AmazingListView list;
	TextView search;
	String token;
	MyThreadAdapter adapter;
	Bundle data;
	
	public ThreadNotificationManager (Activity context, AmazingListView list, TextView search, String token) {
		this.context = context;
		this.list = list;
		this.search = search;
		this.token = token;
		adapter = new MyThreadAdapter(context, "my_threads", token);
		list.setAdapter(adapter);
		adapter.notifyMayHaveMorePages();
		data = new Bundle();
		this.search.addTextChangedListener(new ThreadTextWatcher());
		tracker = GoogleAnalyticsTracker.getInstance();
	}
	
	public void fetchThreadFromCache() {
		BoardMessageCacheManager manager = new BoardMessageCacheManager(context, "my_threads");
		manager.open();
		Cursor cursor = manager.fetchAll();
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			for (int i = 0; i < cursor.getCount(); i++) {
				Bundle content = new Bundle();
				content.putString("id", cursor.getString(cursor
						.getColumnIndex(BoardMessageCacheManager.ID)));
				content.putString("subject", cursor.getString(cursor
						.getColumnIndex(BoardMessageCacheManager.SUBJECT)));
				content.putString("contents", cursor.getString(cursor
						.getColumnIndex(BoardMessageCacheManager.CONTENTS)));
				content.putString("timestamp", cursor.getString(cursor
						.getColumnIndex(BoardMessageCacheManager.TIMESTAMP)));
				content.putString("status", cursor.getString(cursor
						.getColumnIndex(BoardMessageCacheManager.STATUS)));
				content.putString("user_id", cursor.getString(cursor
						.getColumnIndex(BoardMessageCacheManager.USER_ID)));
				content.putString("username", cursor.getString(cursor
						.getColumnIndex(BoardMessageCacheManager.USERNAME)));
				content.putString("profile_pic", cursor.getString(cursor
						.getColumnIndex(BoardMessageCacheManager.PROFILE_PIC)));
				String total_num_of_replies = cursor
						.getString(cursor
								.getColumnIndex(BoardMessageCacheManager.TOTAL_NUM_OF_REPLIES));
				content.putString("total_num_of_replies", total_num_of_replies);
				if (Integer.valueOf(total_num_of_replies) > 0) {
					content.putString(
							"reply_contents",
							cursor.getString(cursor
									.getColumnIndex(BoardMessageCacheManager.REPLY_CONTENTS)));
					content.putString(
							"reply_author",
							cursor.getString(cursor
									.getColumnIndex(BoardMessageCacheManager.REPLY_AUTHOR)));
					content.putString(
							"reply_time",
							cursor.getString(cursor
									.getColumnIndex(BoardMessageCacheManager.REPLY_TIME)));
				}
				String attachments = cursor
						.getString(cursor
								.getColumnIndex(BoardMessageCacheManager.ATTACHED_FILES));
				if (attachments.length() > 0)
					content.putString("attached_files", attachments);
				String keywords = cursor
						.getString(cursor
								.getColumnIndex(BoardMessageCacheManager.TAGGED_KEYWORDS));
				if (keywords.length() > 0)
					content.putString("tagged_keywords", keywords);
				String tags = cursor.getString(cursor
						.getColumnIndex(BoardMessageCacheManager.TAGGED_USERS));
				if (tags.length() > 0)
					content.putString("tagged_users", tags);
				data.putBundle(String.valueOf(i), content);
				cursor.moveToNext();
			}
			adapter.setData(data);
			adapter.setInitialPage(data.size() / 10); // it indicates that cache
			// has got this much
			// page of data
			adapter.resetPage();
		}
		cursor.close();
		manager.close();
		fetchThread();
	}
	
	public void fetchThread() {
		Doc2DocRequest dr = new Doc2DocRequest();
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", token);
		params.putString("page_no", "0");

		asyncTask.runTask("thread/fetch_my_threads", params,
				new Doc2DocTaskListener() {

					@Override
					public void onMalformedURLException(MalformedURLException e) {
						// TODO Auto-generated method stub
						mHandler.sendMessage(Message.obtain(mHandler,
								Global.ERROR_RESPONSE));
						Log.d("thread_notification-response", e.getMessage());
					}

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						mHandler.sendMessage(Message.obtain(mHandler,
								Global.CONNECTION_ERROR));
						Log.d("thread_notification-response", e.getMessage());
					}

					@Override
					public void onFileNotFoundException(FileNotFoundException e) {
						// TODO Auto-generated method stub
						mHandler.sendMessage(Message.obtain(mHandler,
								Global.ERROR_RESPONSE));
						Log.d("thread_notification-response", e.getMessage());
					}

					@Override
					public void onComplete(String response) {
						Log.d("thread_notification-response-json", response);
						if (!response.equals("FALSE")) {
							parseJson(response);
						} else {
							mHandler.sendMessage(Message.obtain(mHandler,
									Global.INVALID_SESSION));
						}
					}

					@Override
					public void onCancel() {
						// TODO Auto-generated method stub
						
					}
				});
	}

	private void parseJson(String response) {
		JSONArray arr = null;
		try {
			arr = new JSONArray(response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mHandler.sendMessage(Message.obtain(mHandler,
					Global.ERROR_RESPONSE));
			return;
		}
		if (arr.length() == 0) {
			mHandler.sendMessage(Message.obtain(mHandler,
					Global.EMPTY_RESPONSE));
			return;
		}
		for (int i = 0; i < arr.length(); i++) {
			Bundle content = new Bundle();
			JSONObject json;
			try {
				json = arr.getJSONObject(i);
				content.putString("id", json.getString("id"));
				content.putString("subject", json.getString("subject"));
				content.putString("contents", json.getString("contents"));
				content.putString("timestamp", json.getString("timestamp"));
				content.putString("status", json.getString("read_status"));
				JSONObject author = json.getJSONObject("author");
				content.putString("user_id", author.getString("user_id"));
				content.putString("username", author.getString("username"));
				content.putString("profile_pic", author.getString("profile_pic_file_path"));
				content.putString("total_num_of_replies",
						json.getString("total_num_of_replies"));
				if (json.has("replies")) {
					JSONArray rep = json.getJSONArray("replies");
					JSONObject replies = rep.getJSONObject(0);
					content.putString("reply_contents",
							replies.getString("contents"));
					JSONObject reply_author = replies.getJSONObject("author");
					content.putString("reply_author",
							reply_author.getString("username"));
					content.putString("reply_time",
							replies.getString("timestamp"));
				}
				// adds attatchments if exist
				if (json.has("$attached_files")) {
					JSONArray attachments = json
							.getJSONArray("$attached_files");
					String file_urls = "";
					for (int j = 0; j < attachments.length(); j++) {
						file_urls += attachments.getJSONObject(j).getString(
								"file_path");
						if (j != attachments.length() - 1)
							file_urls += ",";
					}
					content.putString("attached_files", file_urls);
				}
				// adds tagged keywords if exists
				if (json.has("tagged_keywords")) {
					JSONArray keywords = json.getJSONArray("tagged_keywords");
					String tagged_keywords = "";
					for (int j = 0; j < keywords.length(); j++) {
						tagged_keywords += keywords.getString(j);
						if (j != keywords.length() - 1)
							tagged_keywords += ",";
					}
					content.putString("tagged_keywords", tagged_keywords);
				}
				if (json.has("tagged_users")) {
					JSONArray tags = json.getJSONArray("tagged_users");
					String tagged_users = "";
					for (int j = 0; j < tags.length(); j++) {
						tagged_users += tags.getJSONObject(j).getString(
								"username");
						if (j != tags.length() - 1)
							tagged_users += ",";
					}
					content.putString("tagged_users", tagged_users);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			data.putBundle(String.valueOf(i), content);
		}
		mHandler.sendMessage(Message.obtain(mHandler,
				Global.GOT_RESPONSE));
	}

	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				Log.d("thread-notification", "got response");
				adapter.setData(data);
				break;
			case Global.EMPTY_RESPONSE:
				adapter.notifyNoMorePages();
				break;
			case Global.CONNECTION_ERROR:
				Toast.makeText(context, "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				break;
			case Global.ERROR_RESPONSE:
				break;
			case Global.INVALID_SESSION:
				Toast.makeText(context, "Invalid session! Please relogin", Toast.LENGTH_LONG).show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				context.sendBroadcast(i);
				break;
			}
		}
	};

	class ThreadTextWatcher implements TextWatcher {

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			adapter.getFilter().filter(s);
			tracker.trackEvent("TextChange", "EditText", "Search Message Center", 0);
		}

	}
}

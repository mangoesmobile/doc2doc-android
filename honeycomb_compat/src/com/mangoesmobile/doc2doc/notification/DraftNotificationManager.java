/**
 * 
 */
package com.mangoesmobile.doc2doc.notification;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.TextView;

import com.foound.widget.AmazingListView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.adapters.DraftAdapter;
import com.mangoesmobile.doc2doc.cache.DraftManager;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class DraftNotificationManager {

	GoogleAnalyticsTracker tracker;

	Activity context;
	AmazingListView list;
	TextView search;
	String table;
	DraftAdapter adapter;
	Bundle data;

	public DraftNotificationManager(Activity context, AmazingListView list,
			TextView search, String table) {
		this.context = context;
		this.list = list;
		this.search = search;
		this.table = table;
		adapter = new DraftAdapter(context);
		list.setAdapter(adapter);
		list.setOnItemLongClickListener(new ListLongClickListener());
		adapter.notifyMayHaveMorePages();
		data = new Bundle();
		this.search.addTextChangedListener(new DraftTextWatcher());
		tracker = GoogleAnalyticsTracker.getInstance();
	}

	public void fetchData() {
		DraftManager manager = new DraftManager(context, table);
		manager.open();
		Cursor cursor = manager.fetchAll();
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			for (int i = 0; i < cursor.getCount(); i++) {
				Bundle content = new Bundle();
				content.putString("id", cursor.getString(cursor
						.getColumnIndex(DraftManager.ID)));
				content.putString("subject", cursor.getString(cursor
						.getColumnIndex(DraftManager.SUBJECT)));
				content.putString("contents", cursor.getString(cursor
						.getColumnIndex(DraftManager.CONTENTS)));

				String attachments = cursor.getString(cursor
						.getColumnIndex(DraftManager.ATTACHED_FILES));
				if (attachments.length() > 0 && !attachments.equals("null"))
					content.putString("attached_files", attachments);
				String keywords = cursor.getString(cursor
						.getColumnIndex(DraftManager.TAGGED_KEYWORDS));
				if (keywords.length() > 0 && !keywords.equals("null"))
					content.putString("tagged_keywords", keywords);
				String tags = cursor.getString(cursor
						.getColumnIndex(DraftManager.TAGGED_USERS));
				if (tags.length() > 0 && !tags.equals("null"))
					content.putString("tagged_users", tags);
				String tagged_names = cursor.getString(cursor
						.getColumnIndex(DraftManager.TAGGED_USER_NAMES));
				if (tagged_names.length() > 0 && !tagged_names.equals("null"))
					content.putString("tagged_user_names", tagged_names);
				content.putString("timestamp", cursor.getString(cursor
						.getColumnIndex(DraftManager.TIMESTAMP)));
				data.putBundle(String.valueOf(i), content);
				cursor.moveToNext();
			}
			adapter.setData(data);
		} else {
			adapter.notifyNoMorePages();
		}
		cursor.close();
		manager.close();
	}

	private void chooseOption(final String id) {
		final CharSequence[] items = { "Discard" };
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Options");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				switch (item) {
				case (0):
					AlertDialog alert = new AlertDialog.Builder(context)
							.create();
					alert.setTitle("Discard Draft");
					alert.setMessage("Are you sure you want to discard draft?");
					alert.setButton("Yes",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									DraftManager manager = new DraftManager(
											context, table);
									manager.open();
									manager.deleteContent(id);
									manager.close();
									adapter.removeItem(id);
									tracker.trackEvent("LongClick", "ListItem",
											"Delete Draft", 0);
								}
							});
					alert.setButton2("No",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
								}
							});
					alert.show();
					break;
				}
				dialog.dismiss();
			}
		});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	class ListLongClickListener implements OnItemLongClickListener {

		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int arg2, long arg3) {
			// TODO Auto-generated method stub
			Bundle content = (Bundle) adapter.getItem(arg2);
			chooseOption(content.getString("id"));
			return true;
		}

	}

	class DraftTextWatcher implements TextWatcher {

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			adapter.getFilter().filter(s);
			tracker.trackEvent("TextChange", "EditText",
					"Search Message Center", 0);
		}

	}
}

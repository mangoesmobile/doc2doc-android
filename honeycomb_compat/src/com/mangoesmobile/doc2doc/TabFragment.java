/**
 * 
 */
package com.mangoesmobile.doc2doc;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.contents.Board;
import com.mangoesmobile.doc2doc.contents.Messages;
import com.mangoesmobile.doc2doc.contents.Reference;
import com.mangoesmobile.doc2doc.contents.Team;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class TabFragment extends Fragment implements OnTabChangeListener,
		OnClickListener {
	
	GoogleAnalyticsTracker tracker;

	static TabHost tabHost;
	static TextView lblNotifications;
	static ImageView btnBoard;
	static ImageView btnMessages;
	static ImageView btnTeam;
	static ImageView btnReference;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub

		tracker = GoogleAnalyticsTracker.getInstance();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.tabs, container, false);
		instantiateLayout(v);
		return v;
	}

	@Override
	public void onStart() {
		super.onStart();

		openTab(Global.TAB_BOARD);
		tabHost.setCurrentTab(0);
		
		btnBoard.setOnClickListener(this);
		btnMessages.setOnClickListener(this);
		btnTeam.setOnClickListener(this);
		btnReference.setOnClickListener(this);
		tabHost.setOnTabChangedListener(this);
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (Global.backPressed) {
			Global.backPressed = false;
		}
	}

	private void instantiateLayout(View v) {
		tabHost = (TabHost) v.findViewById(android.R.id.tabhost);
		tabHost.setup();
		tabHost.addTab(newTab(Global.TAB_BOARD, "board", R.id.tab_board));
		tabHost.addTab(newTab(Global.TAB_MESSAGES, "messages", R.id.tab_messages));
		tabHost.addTab(newTab(Global.TAB_TEAM, "team", R.id.tab_team));
		tabHost.addTab(newTab(Global.TAB_REFERENCE, "reference", R.id.tab_reference));
		btnBoard = (ImageView) v.findViewById(R.id.rd_board);
		btnMessages = (ImageView) v.findViewById(R.id.rd_messages);
		btnTeam = (ImageView) v.findViewById(R.id.rd_team);
		btnReference = (ImageView) v.findViewById(R.id.rd_reference);
		lblNotifications = (TextView) v.findViewById(R.id.lbl_notifications);
	}

	@Override
	public void onTabChanged(String tabId) {
		// TODO Auto-generated method stub
		openTab(tabId);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.rd_board) {
			tabHost.setCurrentTab(0);
			btnBoard.setImageResource(R.drawable.btn_board_down);
			btnMessages.setImageResource(R.drawable.btn_messages);
			btnTeam.setImageResource(R.drawable.btn_team);
			btnReference.setImageResource(R.drawable.btn_reference);
			tracker.trackEvent("Select", "Tab", "Launch Board", 0);
		} else if (v.getId() == R.id.rd_messages) {
			tabHost.setCurrentTab(1);
			btnBoard.setImageResource(R.drawable.btn_board);
			btnMessages.setImageResource(R.drawable.btn_messages_down);
			btnTeam.setImageResource(R.drawable.btn_team);
			btnReference.setImageResource(R.drawable.btn_reference);
			lblNotifications.setVisibility(View.GONE);
			tracker.trackEvent("Select", "Tab", "Launch Messages", 0);
		} else if (v.getId() == R.id.rd_team) {
			tabHost.setCurrentTab(2);
			btnBoard.setImageResource(R.drawable.btn_board);
			btnMessages.setImageResource(R.drawable.btn_messages);
			btnTeam.setImageResource(R.drawable.btn_team_down);
			btnReference.setImageResource(R.drawable.btn_reference);
			tracker.trackEvent("Select", "Tab", "Launch Team", 0);
		} else if (v.getId() == R.id.rd_reference) {
			tabHost.setCurrentTab(3);
			btnBoard.setImageResource(R.drawable.btn_board);
			btnMessages.setImageResource(R.drawable.btn_messages);
			btnTeam.setImageResource(R.drawable.btn_team);
			btnReference.setImageResource(R.drawable.btn_reference_down);
			tracker.trackEvent("Select", "Tab", "Launch References", 0);
		}
	}

	private TabSpec newTab(String tag, String indicator, int tabContentId) {
		TabSpec tabSpec = tabHost.newTabSpec(tag);
		tabSpec.setIndicator(indicator);
		tabSpec.setContent(tabContentId);
		return tabSpec;
	}

	private void openTab(String tabId) {
		int id;
		Fragment f;
		if (tabId.equals(Global.TAB_BOARD)) {
			id = R.id.tab_board;
			f = new Board(id);
		} else if (tabId.equals(Global.TAB_MESSAGES)) {
			id = R.id.tab_messages;
			f = new Messages(id);
		} else if (tabId.equals(Global.TAB_TEAM)) {
			id = R.id.tab_team;
			f = new Team(id);
		} else {
			id = R.id.tab_reference;
			f = new Reference(id);
		}
		
		FragmentManager fm = getFragmentManager();
		if(fm.findFragmentByTag(tabId) == null) {
			fm.beginTransaction().add(id, f, tabId).commit();
			D2DTabHost.history.get(id).push(f);
		}
	}

}

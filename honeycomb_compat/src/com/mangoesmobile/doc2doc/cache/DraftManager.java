/**
 * 
 */
package com.mangoesmobile.doc2doc.cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class DraftManager {
	
	private String table;
	// database fields
	public static final String ID = "_id";
	public static final String SUBJECT = "subject";
	public static final String CONTENTS = "contents";
	public static final String ATTACHED_FILES = "attached_files";
	public static final String TAGGED_KEYWORDS = "tagged_keywords";
	public static final String TAGGED_USERS = "tagged_users";
	public static final String TAGGED_USER_NAMES = "tagged_user_names";
	public static final String TIMESTAMP = "timestamp";
	
	public static final String[] COLUMNS = new String[] {ID, SUBJECT, CONTENTS, ATTACHED_FILES, TAGGED_KEYWORDS, TAGGED_USERS, TAGGED_USER_NAMES, TIMESTAMP};
	
	private Context context;
	private SQLiteDatabase db;
	private CacheHelper helper;
	
	public DraftManager(Context context, String table) {
		this.context = context;
		this.table = table;
	}
	
	public DraftManager open() throws SQLException {
		helper = new CacheHelper(context);
		db = helper.getWritableDatabase();
		return this;
	}
	
	public void close() {
		helper.close();
	}
	
	public long insertContent(String subject, String contents, String attached_files, String tagged_keywords, String tagged_users, String tagged_user_names, String timestamp) {
		ContentValues values = createContentValues(subject, contents, attached_files, tagged_keywords, tagged_users, tagged_user_names, timestamp);
		return db.insert(table, null, values);
	}
	
	public int deleteAllContents() {
		return db.delete(table, null, null);
	}
	
	public boolean deleteContent(String id) {
		return db.delete(table, ID + "=?", new String[] {id}) > 0;
	}
	
	public Cursor fetchAll() {
		return db.query(table, COLUMNS, null, null, null, null, null);
	}
	
	public Cursor fetchRow(String id) throws SQLException {
		return db.query(table, COLUMNS, ID + "=?", new String[] {id}, null, null, null);
	}
	
	public long getCount() {
		return db.compileStatement("SELECT COUNT(*) FROM " + table).simpleQueryForLong();
	}
	
	private ContentValues createContentValues(String subject, String contents, String attached_files, String tagged_keywords, String tagged_users, String tagged_user_names, String timestamp) {
		ContentValues values = new ContentValues();
		values.put(SUBJECT, subject);
		values.put(CONTENTS, contents);
		values.put(ATTACHED_FILES, attached_files);
		values.put(TAGGED_KEYWORDS, tagged_keywords);
		values.put(TAGGED_USERS, tagged_users);
		values.put(TAGGED_USER_NAMES, tagged_user_names);
		values.put(TIMESTAMP, timestamp);
		return values;
	}

}

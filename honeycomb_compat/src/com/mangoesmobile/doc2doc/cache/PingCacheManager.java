/**
 * 
 */
package com.mangoesmobile.doc2doc.cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class PingCacheManager {
	private static final String TABLE = "ping";
	// database fields
	public static final String ID = "id";
	public static final String SENDER_ID = "sender_id";
	public static final String SENDER_NAME = "sender_name";
	public static final String SENDER_FULL_NAME = "sender_full_name";
	public static final String SENDER_PRO_PIC = "sender_profile_pic";
	public static final String RECIPIENT_ID = "recipient_id";
	public static final String RECIPIENT_NAME = "recipient_name";
	public static final String RECIPIENT_FULL_NAME = "recipient_full_name";
	public static final String RECIPIENT_PRO_PIC = "recipient_profile_pic";
	public static final String STATUS = "status";
	public static final String TIMESTAMP = "timestamp";
	
	public static final String[] COLUMNS = new String[] {ID, SENDER_ID, SENDER_NAME, SENDER_FULL_NAME, SENDER_PRO_PIC, RECIPIENT_ID, RECIPIENT_NAME, RECIPIENT_FULL_NAME, RECIPIENT_PRO_PIC, STATUS, TIMESTAMP};
	
	private Context context;
	private SQLiteDatabase db;
	private CacheHelper helper;
	
	public PingCacheManager(Context context) {
		this.context = context;
	}
	
	public PingCacheManager open() throws SQLException {
		helper = new CacheHelper(context);
		db = helper.getWritableDatabase();
		return this;
	}
	
	public void close() {
		helper.close();
	}
	
	public long insertContent(String id, String sender_id, String sender_name, String sender_full_name, String sender_pro_pic, String recipient_id, String recipient_name, String recipient_full_name, String recipient_pro_pic, String status, String timestamp) {
		ContentValues values = createContentValues(id, sender_id, sender_name, sender_full_name, sender_pro_pic, recipient_id, recipient_name, recipient_full_name, recipient_pro_pic, status, timestamp);
		return db.insert(TABLE, null, values);
	}
	
	public boolean updateStatus(String status, String id) {
		ContentValues values = new ContentValues();
		values.put(STATUS, status);
		return db.update(TABLE, values, ID + "=?", new String[] {id}) > 0;
	}
	
	public int deleteAllContents() {
		return db.delete(TABLE, null, null);
	}
	
	public boolean deleteContent(String id) {
		return db.delete(TABLE, ID + "=?", new String[] {id}) > 0;
	}
	
	public Cursor fetchAll() {
		return db.query(TABLE, COLUMNS, null, null, null, null, null);
	}
	
	public Cursor fetchRow(String id) throws SQLException {
		return db.query(TABLE, COLUMNS, ID + "=?", new String[] {id}, null, null, null);
	}
	
	private ContentValues createContentValues(String id, String sender_id, String sender_name, String sender_full_name, String sender_pro_pic, String recipient_id, String recipient_name, String recipient_full_name, String recipient_pro_pic, String status, String timestamp) {
		ContentValues values = new ContentValues();
		values.put(ID, id);
		values.put(SENDER_ID, sender_id);
		values.put(SENDER_NAME, sender_name);
		values.put(SENDER_FULL_NAME, sender_full_name);
		values.put(SENDER_PRO_PIC, sender_pro_pic);
		values.put(RECIPIENT_ID, recipient_id);
		values.put(RECIPIENT_NAME, recipient_name);
		values.put(RECIPIENT_FULL_NAME, recipient_full_name);
		values.put(RECIPIENT_PRO_PIC, recipient_pro_pic);
		values.put(STATUS, status);
		values.put(TIMESTAMP, timestamp);
		return values;
	}
}

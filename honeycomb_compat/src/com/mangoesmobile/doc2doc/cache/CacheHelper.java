/**
 * 
 */
package com.mangoesmobile.doc2doc.cache;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class CacheHelper extends SQLiteOpenHelper {
	
	private static final String DB = "doc2doc";
	private static final int VERSION = 2;
	
	// create table
	private static final String TABLE_BOARD = "create table board (_id integer primary key autoincrement, id text, subject text, contents text, timestamp text, status text, user_id text, username text, profile_pic text, total_num_of_replies text, reply_contents text, reply_author text, reply_time text, attached_files text, tagged_keywords text, tagged_users text)";
	private static final String TABLE_TEAM = "create table team (_id integer primary key autoincrement, user_id text, first_name text, last_name text, title text, email text, phone text, profile_pic text, organization text, specialization text, username text, availability_status text, latitude text, longitude text, base_location text)";
	private static final String TABLE_MY_THREADS = "create table my_threads (_id integer primary key autoincrement, id text, subject text, contents text, timestamp text, status text, user_id text, username text, profile_pic text, total_num_of_replies text, reply_contents text, reply_author text, reply_time text, attached_files text, tagged_keywords text, tagged_users text)";
	private static final String TABLE_MESSAGES_INBOX = "create table inbox (_id integer primary key autoincrement, id text, subject text, contents text, timestamp text, status text, user_id text, username text, profile_pic text, total_num_of_replies text, reply_contents text, reply_author text, reply_time text, attached_files text, tagged_keywords text, tagged_users text)";
	private static final String TABLE_MESSAGES_SENT = "create table sent (_id integer primary key autoincrement, id text, subject text, contents text, timestamp text, status text, user_id text, username text, profile_pic text, total_num_of_replies text, reply_contents text, reply_author text, reply_time text, attached_files text, tagged_keywords text, tagged_users text)";
	private static final String TABLE_PING = "create table ping (_id integer primary key autoincrement, id text, sender_id text, sender_name text, sender_full_name text, sender_profile_pic text, recipient_id text, recipient_name text, recipient_full_name text, recipient_profile_pic text, status text, timestamp text)";
	private static final String TABLE_THREAD_DRAFT = "create table thread_draft (_id integer primary key autoincrement, subject text, contents text, attached_files text, tagged_keywords text, tagged_users text, tagged_user_names text, timestamp text)";
	private static final String TABLE_MESSAGE_DRAFT = "create table message_draft (_id integer primary key autoincrement, subject text, contents text, attached_files text, tagged_keywords text, tagged_users text, tagged_user_names text, timestamp text)";
	// drop table
	private static final String DROP_TABLE_BOARD = "drop table if exists board";
	private static final String DROP_TABLE_TEAM = "drop table if exists team";
	private static final String DROP_TABLE_MY_THREADS = "drop table if exists my_threads";
	private static final String DROP_TABLE_MESSAGES_INBOX = "drop table if exists inbox";
	private static final String DROP_TABLE_MESSAGES_SENT = "drop table if exists sent";
	private static final String DROP_TABLE_PING = "drop table if exists ping";
	private static final String DROP_TABLE_THREAD_DRAFT = "drop table if exists thread_draft";
	private static final String DROP_TABLE_MESSAGE_DRAFT = "drop table if exists message_draft";
	
	public CacheHelper(Context context) {
		super(context, DB, null, VERSION);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase)
	 */
	@Override
	public void onCreate(SQLiteDatabase database) {
		// TODO Auto-generated method stub
		database.execSQL(TABLE_BOARD);
		database.execSQL(TABLE_TEAM);
		database.execSQL(TABLE_MY_THREADS);
		database.execSQL(TABLE_MESSAGES_INBOX);
		database.execSQL(TABLE_MESSAGES_SENT);
		database.execSQL(TABLE_PING);
		database.execSQL(TABLE_THREAD_DRAFT);
		database.execSQL(TABLE_MESSAGE_DRAFT);

	}

	/* (non-Javadoc)
	 * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
	 */
	@Override
	public void onUpgrade(SQLiteDatabase database, int arg1, int arg2) {
		// TODO Auto-generated method stub
		database.execSQL(DROP_TABLE_BOARD);
		database.execSQL(DROP_TABLE_TEAM);
		database.execSQL(DROP_TABLE_MY_THREADS);
		database.execSQL(DROP_TABLE_MESSAGES_INBOX);
		database.execSQL(DROP_TABLE_MESSAGES_SENT);
		database.execSQL(DROP_TABLE_PING);
		database.execSQL(DROP_TABLE_THREAD_DRAFT);
		database.execSQL(DROP_TABLE_MESSAGE_DRAFT);
		onCreate(database);
		
	}

}

/**
 * 
 */
package com.mangoesmobile.doc2doc;

import java.util.EmptyStackException;
import java.util.Stack;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.contents.NewThread;
import com.mangoesmobile.doc2doc.util.Global;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class OfflinePost extends FragmentActivity {

	GoogleAnalyticsTracker tracker;
	
	private int tag = R.id.frame_postnew;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);	
	    // TODO Auto-generated method stub
	    setContentView(R.layout.postnew);

	    tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("Offline Post Creation");
	    
		D2DTabHost.history.put(tag, new Stack<Fragment>());
		
	    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
	    Bundle args = new Bundle();
	    args.putBoolean("offline", getIntent().getBooleanExtra("offline", false));
	    NewThread nt = NewThread.newInstance(tag, args);
	    D2DTabHost.history.get(tag).push(nt);
	    ft.add(tag, nt);
	    ft.commit();
	}
	
	@Override
	public void onBackPressed() {
		Global.backPressed = true;
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		try {
			ft.hide(D2DTabHost.history.get(tag).pop());
			ft.show(D2DTabHost.history.get(tag).peek());
		} catch (EmptyStackException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			super.onBackPressed();
		}
		ft.commit();
	}

}

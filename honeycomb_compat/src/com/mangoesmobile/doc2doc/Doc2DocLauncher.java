package com.mangoesmobile.doc2doc;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.util.Global;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class Doc2DocLauncher extends Activity {

	protected boolean _active = true;
	protected int _splashTime = 3000; // time to display the splash screen in ms

	GoogleAnalyticsTracker tracker;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.startNewSession(Global.UA, 10, this);
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		SharedPreferences pref = getSharedPreferences("session", MODE_PRIVATE);
		
		if (pref.getString("token", "").equals("")) {
			// thread for displaying the SplashScreen
			Thread splashTread = new Thread() {
				@Override
				public void run() {
					try {
						int waited = 0;
						while (_active && (waited < _splashTime)) {
							sleep(100);
							if (_active) {
								waited += 100;
							}
						}
					} catch (InterruptedException e) {
						// do nothing
					} finally {
						finish();
						Intent login = new Intent(Doc2DocLauncher.this,
								Login.class);
						startActivity(login);
					}
				}
			};
			splashTread.start();
		} else {
			finish();
			Intent loggedIn = new Intent(Doc2DocLauncher.this,
					D2DTabHost.class);
			loggedIn.putExtra("from_notification", getIntent().getBooleanExtra("from_notification", false));
			startActivity(loggedIn);
		}
	}
}
package com.mangoesmobile.doc2doc.util;

public class ReplyNode {
	public String replyID;	
	public String timestamp;
	public String authorID;
	public String author;
	public String message;
	public String icon;
	public String image;
	public String photo;
	
	public ReplyNode(String rid, String t, String aid, String a, String m, String ic, String im, String p){
		replyID = rid;
		timestamp = t;
		authorID = aid;
		author = a;
		message = m;
		icon = ic;
		image = im;
		photo = p;
	}
}

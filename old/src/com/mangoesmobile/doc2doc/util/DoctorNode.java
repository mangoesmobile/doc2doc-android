package com.mangoesmobile.doc2doc.util;

public class DoctorNode {
	public String id;
	public String title;
	public String lastname;
	public String firstname;
	public String displayName;
	public String specialization;
	public String location;
	public String email;
	public String phone;
	public String status;
	public String photo;
	
	public DoctorNode(String i, String t, String l, String f, String d, String s, String lo, String e, String p, String st, String ph){
		id = i;
		title = t;
		lastname = l;
		firstname = f;
		displayName = d;
		specialization = s;
		location = lo;
		email = e;
		phone = p;
		status = st;
		photo = ph;
	}
}

package com.mangoesmobile.doc2doc.util;

import java.net.URI;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.xml.sax.InputSource;

import android.util.Log;

import com.mangoesmobile.doc2doc.global.Global;

public class ServerConnection {
	// private String url = Global.baseUrl;
	private MultipartEntity parameter;

	public ServerConnection(MultipartEntity param) {
		parameter = param;
	}

	public void requestXML(String act) {
		Log.i("connection", "Connecting to server");
		// HttpURLConnection connection = null;
		HttpResponse response = null;
		try {
			// Create connection
			String url = Global.baseUrl;
			// connection = (HttpURLConnection) url.openConnection();
			// connection.setConnectTimeout(30000);
			// connection.setRequestMethod("POST");
			// connection.setRequestProperty("Content-Type",
			// "application/x-www-form-urlencoded");
			//
			// connection.setRequestProperty("Content-Length",
			// "" + Integer.toString(parameter.getBytes().length));
			// connection.setRequestProperty("Content-Language", "en-US");
			//
			// connection.setUseCaches(false);
			// connection.setDoInput(true);
			// connection.setDoOutput(true);
			//
			// // Send request
			// DataOutputStream wr = new DataOutputStream(
			// connection.getOutputStream());
			// wr.writeBytes(parameter);
			// wr.flush();
			// wr.close();
			HttpPost request = new HttpPost();
			request.setURI(new URI(url));
//			request.setHeader("Content-Type", "text/xml");
			if (parameter != null)
				request.setEntity(parameter);

			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is
			// established.
			int timeoutConnection = 30000;
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
//			int timeoutSocket = 30000;
//			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

			HttpClient client = new DefaultHttpClient(httpParameters);
			response = client.execute(request);

			HttpEntity entity = response.getEntity();

			// Get Response
			InputSource source = new InputSource(entity.getContent());
			source.setEncoding(HTTP.UTF_8);
			// BufferedReader rd = new BufferedReader(new
			// InputStreamReader(stream));
			// String line;
			// StringBuffer response = new StringBuffer();
			// while ((line = rd.readLine()) != null) {
			// response.append(line);
			// response.append('\r');
			// }
			// rd.close();

			XMLParser parser = new XMLParser(source);
			parser.parseXML(act);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

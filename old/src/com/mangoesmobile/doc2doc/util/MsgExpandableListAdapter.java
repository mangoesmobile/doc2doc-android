package com.mangoesmobile.doc2doc.util;

import java.util.ArrayList;

import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.global.Global;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

public class MsgExpandableListAdapter extends BaseExpandableListAdapter {

	private Context context;
	LayoutInflater inflater;

	private String[] groupIndex = { "Public", "Private" };

	private ArrayList<ArrayList<String>> messageIndex;
	private ArrayList<String> publicIndex;
	private ArrayList<String> privateIndex;

	public MsgExpandableListAdapter(Context c) {
		context = c;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		fetchMessageIndex();
	}

	private void fetchMessageIndex() {
		messageIndex = new ArrayList<ArrayList<String>>();
		publicIndex = new ArrayList<String>();
		privateIndex = new ArrayList<String>();
		// adding public message index
		MsgCenterNode msg;
		for (int i = 0; i < Global.msgs[0][0].size(); i++) {
			msg = (MsgCenterNode) Global.msgs[0][0].get(i);
			publicIndex.add(msg.subject);
		}
		publicIndex.add("   Read");
		publicIndex.add("   Sent");
		publicIndex.add("   Drafts");

		// adding private message index
		for (int i = 0; i < Global.msgs[1][0].size(); i++) {
			msg = (MsgCenterNode) Global.msgs[1][0].get(i);
			if (msg.type.equals("ping")) {
				privateIndex.add(msg.authorName + " pinged you");
			} else {
				privateIndex.add(msg.subject);
			}
		}
		privateIndex.add("   Read");
		privateIndex.add("   Sent");

		// adding all public and private indexes to the parent
		messageIndex.add(publicIndex);
		messageIndex.add(privateIndex);
	}

	@Override
	public Object getChild(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return messageIndex.get(arg0).get(arg1);
	}

	@Override
	public long getChildId(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return arg1;
	}

	@Override
	public View getChildView(int arg0, int arg1, boolean arg2, View arg3,
			ViewGroup arg4) {
		// TODO Auto-generated method stub
		if (arg3 == null) {
			arg3 = inflater.inflate(R.layout.grouptitle, null);
		}		
		TextView lblChildTitle = (TextView) arg3
				.findViewById(R.id.lbl_grouptitle);
		
		// checking whether there is any new message or not
		int newMsgs = getChildrenCount(arg0);
		if(arg0==0){
			newMsgs -= 3;
		} else {
			newMsgs -= 2;
		}
		// making new messages bold
		if(arg1<newMsgs){
			lblChildTitle.setTypeface(null, Typeface.BOLD);
		} else {
			lblChildTitle.setTypeface(null, Typeface.NORMAL);
		}
		if (arg0 == 0 && arg1 == messageIndex.get(arg0).size() - 1) {
			lblChildTitle.setText(messageIndex.get(arg0).get(arg1) + " ("
					+ Global.msgs[0][3].size() + ")");
			lblChildTitle.setTextColor(Color.RED);
		} else {
			lblChildTitle.setText(messageIndex.get(arg0).get(arg1));
			lblChildTitle.setTextColor(Color.BLACK);
		}

		return arg3;
	}

	@Override
	public int getChildrenCount(int arg0) {
		// TODO Auto-generated method stub
		return messageIndex.get(arg0).size();
	}

	@Override
	public Object getGroup(int arg0) {
		// TODO Auto-generated method stub
		return groupIndex[arg0];
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return groupIndex.length;
	}

	@Override
	public long getGroupId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getGroupView(int arg0, boolean arg1, View arg2, ViewGroup arg3) {
		// TODO Auto-generated method stub
		if (arg2 == null) {
			arg2 = inflater.inflate(R.layout.grouptitle, null);
		}
		TextView lblGroupTitle = (TextView) arg2
				.findViewById(R.id.lbl_grouptitle);
		lblGroupTitle.setText(groupIndex[arg0] + " ("
				+ Global.msgs[arg0][0].size() + ")");

		return arg2;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return true;
	}
	
	@Override
	public void notifyDataSetChanged(){
		fetchMessageIndex();
		super.notifyDataSetChanged();
	}
}

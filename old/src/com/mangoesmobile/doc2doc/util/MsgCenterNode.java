package com.mangoesmobile.doc2doc.util;

public class MsgCenterNode {

	public String id;
	public String type;
	public String subject;
	public String timestamp;
	public String msgStatus;
	public String to;
	public String from;
	public String authorName;
	public String photo;
	public String replies;
	public String author;
	public String message;
	public String lastReplyAuthor;
	public String lastMessage;
	public String time;
	
	public MsgCenterNode(String i, String ty, String sub, String ts, String ms, String t,String f, String an, String p, String re, String au, String m, String la, String lm, String ti) {
		id = i;
		type = ty;
		subject = sub;
		timestamp = ts;
		msgStatus = ms;
		to = t;
		from = f;
		authorName = an;
		photo = p;
		replies = re;
		author = au;
		message = m;
		lastReplyAuthor = la;
		lastMessage = lm;
		time = ti;
	}
	
}

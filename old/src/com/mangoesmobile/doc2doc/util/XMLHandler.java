package com.mangoesmobile.doc2doc.util;

import java.util.LinkedList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.mangoesmobile.doc2doc.global.Global;

@SuppressWarnings("unused")
public class XMLHandler extends DefaultHandler {
	// xml data container
	public static LinkedList<Object> threadList;
	public static LinkedList<Object> detailsList;
	public static LinkedList<Object> msgCenterList;
	public static LinkedList<Object> teamList;
	public static LinkedList<Object> statusList;
	public static LinkedList<Object> logoutList;
	public static LinkedList<Object> loginInfo;
	public static LinkedList<Object> pingList;
	public static LinkedList<Object> photoChangeList;
	public static LinkedList<Object> personalMsgList;
	public static LinkedList<Object> detailMsgList;
	public static String str_notifications;
	public static String version;

	// action requested
	String action;

	private boolean ver = false;
	
	// for thread node
	// outer tags
	private boolean status = false;
	private boolean token = false;
	private boolean thread = false;
	// inner tags
	private boolean id = false; // common
	private boolean type = false;
	private boolean timestamp = false; // common
	private boolean authorID = false;
	private boolean author = false; // common
	private boolean title = false; // common
	private boolean details = false; // common
	private boolean replies = false;
	private boolean icon = false;
	private boolean image = false;
	private boolean lastAuthor = false;
	private boolean lastMessage = false;
	private boolean time = false;
	private boolean threadStatus = false;
	

	// for notifications
	private boolean notifications = false;
	// message center
	private boolean msgCtr = false;
	private boolean subject = false;
	private boolean msgStatus = false;
	private boolean to = false;
	private boolean from = false;
	private boolean authorName = false;
	private boolean toID = false;
	private boolean fromID = false;
	private boolean privateMsg = false;

	// 2nd level inner tags
	private boolean message = false;

	// for reply node
	private boolean reply = false;
	private boolean replyID = false;

	// for doclist node
	private boolean doctor = false;
	private boolean lastname = false;
	private boolean firstname = false;
	private boolean displayName = false;
	private boolean specialization = false;
	private boolean location = false;
	private boolean email = false;
	private boolean phone = false;
	private boolean photo = false;
	private boolean currentUser = false;
	private boolean pingStatus = false;
	
	// status
	private String str_status = "";

	// xml thread element container
	private String str_id = ""; // common
	private String str_type = "";
	private String str_timestamp = ""; // common
	private String str_authorID = "";
	private String str_author = ""; // common
	private String str_title = "";
	private String str_details = "";
	private String str_replies = "";
	private String str_icon = "";
	private String str_image = "";
	private String str_lastReplyAuthor = "";
	private String str_lastMessage = "";
	private String str_lastReplyTime = "";
	private String str_threadStatus = "";

	// xml msg center element container
	private String str_message = "";
	private String str_subject = "";
	private String str_msgStatus = "";
	private String str_to = "";
	private String str_from = "";
	private String str_authorName = "";
	private String str_fromID = "";
	private String str_toID = "";

	// for getDetails action
	private String str_replyID = "";
	private String str_replyTimestamp = "";
	private String str_replyAuthorID = "";
	private String str_replyAuthor = "";
	private String str_replyIcon = "";
	private String str_replyImage = "";
	private String str_replyPhoto = "";

	// for getUsers action
	private String str_lastname = "";
	private String str_firstname = "";
	private String str_displayName = "";
	private String str_specialization = "";
	private String str_location = "";
	private String str_email = "";
	private String str_phone = "";
	private String str_docstatus = "";
	private String str_photo = "";
	private String str_pingStatus = "";

	public XMLHandler(String act) {
		action = act;
	}

	@Override
	public void startDocument() throws SAXException {
		if (action.equals("login") || action.equals("getThreads")) {
			threadList = new LinkedList<Object>();
			loginInfo = new LinkedList<Object>();
		} else if (action.equals("getDetails") || action.equals("postThread")
				|| action.equals("reply")) {
			detailsList = new LinkedList<Object>();
		} else if (action.equals("getMsgCtr")) {
			msgCenterList = new LinkedList<Object>();
		} else if (action.equals("getUsers")) {
			teamList = new LinkedList<Object>();
		} else if (action.equals("statusChange")) {
			statusList = new LinkedList<Object>();
		} else if (action.equalsIgnoreCase("logout")) {
			logoutList = new LinkedList<Object>();
		} else if (action.equalsIgnoreCase("ping")) {
			pingList = new LinkedList<Object>();
		} else if (action.equalsIgnoreCase("photoChange")) {
			photoChangeList = new LinkedList<Object>();
		} else if (action.equalsIgnoreCase("postMsg")
				|| action.equalsIgnoreCase("replyMsg")) {
			personalMsgList = new LinkedList<Object>();
		} else if (action.equalsIgnoreCase("getMsgCtr")) {
			msgCenterList = new LinkedList<Object>();
		} else if (action.equalsIgnoreCase("getMsgDetails")) {
			detailMsgList = new LinkedList<Object>();
		}
	}

	@Override
	public void endDocument() throws SAXException {
		if (str_status.equalsIgnoreCase("failure")) {
			if (action.equals("login") || (action.equals("getThreads"))) {
				threadList.add(str_id);
				threadList.add(str_message);
			} else if (action.equals("getDetails")
					|| action.equals("postThread") || action.equals("reply")) {
				detailsList.add(str_id);
				detailsList.add(str_message);
			} else if (action.equals("getUsers")) {
				teamList.add(str_id);
				teamList.add(str_message);
			} else if (action.equals("statusChange")) {
				statusList.add(str_id);
				statusList.add(str_message);
			} else if (action.equals("logout")) {
				logoutList.add(str_id);
				logoutList.add(str_message);
			} else if (action.equals("ping")) {
				pingList.add(str_id);
				pingList.add(str_message);
			} else if (action.equals("photoChange")) {
				photoChangeList.add(str_id);
				photoChangeList.add(str_message);
			} else if (action.equals("postMsg") || action.equals("replyMsg")) {
				personalMsgList.add(str_id);
				personalMsgList.add(str_message);
			} else if (action.equalsIgnoreCase("getMsgCtr")) {
				msgCenterList.add(str_id);
				msgCenterList.add(str_message);
			}else if (action.equalsIgnoreCase("getMsgDetails")) {
				detailMsgList.add(str_id);
				detailMsgList.add(str_message);
			}
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		if (localName.equalsIgnoreCase("ver")) {
			ver = true;
		} else if (localName.equalsIgnoreCase("status")) {
			status = true;
		} else if (localName.equalsIgnoreCase("token")) {
			token = true;
		} else if (localName.equalsIgnoreCase("thread")) {
			thread = true;
		} else if (localName.equalsIgnoreCase("id")) {
			id = true;
		} else if (localName.equalsIgnoreCase("type")) {
			type = true;
		} else if (localName.equalsIgnoreCase("timestamp")) {
			timestamp = true;
		} else if (localName.equalsIgnoreCase("authorID")) {
			authorID = true;
		} else if (localName.equalsIgnoreCase("author")) {
			author = true;
		} else if (localName.equalsIgnoreCase("title")) {
			title = true;
		} else if (localName.equalsIgnoreCase("details")) {
			details = true;
		} else if (localName.equalsIgnoreCase("replies")) {
			replies = true;
		} else if (localName.equalsIgnoreCase("icon")) {
			icon = true;
		} else if (localName.equalsIgnoreCase("image")) {
			image = true;
		} else if (localName.equalsIgnoreCase("threadStatus")) {
			threadStatus = true;
		} else if (localName.equalsIgnoreCase("notifications")) {
			notifications = true;
		} else if (localName.equalsIgnoreCase("message")) {
			message = true;
		} else if (localName.equalsIgnoreCase("reply")) {
			reply = true;
		} else if (localName.equalsIgnoreCase("replyid")) {
			replyID = true;
		} else if (localName.equalsIgnoreCase("doctor")) {
			doctor = true;
		} else if (localName.equalsIgnoreCase("lastname")) {
			lastname = true;
		} else if (localName.equalsIgnoreCase("firstname")) {
			firstname = true;
		} else if (localName.equalsIgnoreCase("displayName")) {
			displayName = true;
		} else if (localName.equalsIgnoreCase("specialization")) {
			specialization = true;
		} else if (localName.equalsIgnoreCase("location")) {
			location = true;
		} else if (localName.equalsIgnoreCase("email")) {
			email = true;
		} else if (localName.equalsIgnoreCase("phone")) {
			phone = true;
		} else if (localName.equalsIgnoreCase("photo")) {
			photo = true;
		} else if (localName.equalsIgnoreCase("lastAuthor")) {
			lastAuthor = true;
		} else if (localName.equalsIgnoreCase("lastMessage")) {
			lastMessage = true;
		} else if (localName.equalsIgnoreCase("time")) {
			time = true;
		} else if (localName.equalsIgnoreCase("currentUser")) {
			currentUser = true;
		} else if (localName.equalsIgnoreCase("pingStatus")) {
			pingStatus = true;
		} else if (localName.equalsIgnoreCase("msgCtr")) {
			msgCtr = true;
		} else if (localName.equalsIgnoreCase("subject")) {
			subject = true;
		} else if (localName.equalsIgnoreCase("msgStatus")) {
			msgStatus = true;
		} else if (localName.equalsIgnoreCase("to")) {
			to = true;
		} else if (localName.equalsIgnoreCase("from")) {
			from = true;
		} else if (localName.equalsIgnoreCase("authorName")) {
			authorName = true;
		} else if (localName.equalsIgnoreCase("toID")) {
			toID = true;
		} else if (localName.equalsIgnoreCase("fromID")) {
			fromID = true;
		} else if (localName.equalsIgnoreCase("privateMsg")) {
			privateMsg = true;
		}
	}

	@Override
	public void endElement(String namespaceURI, String localName, String qName)
			throws SAXException {
		if (localName.equalsIgnoreCase("ver")) {
			ver = false;
		} else if (localName.equalsIgnoreCase("status")) {
			status = false;
		} else if (localName.equalsIgnoreCase("token")) {
			token = false;
		} else if (localName.equalsIgnoreCase("thread")) {
			if (action.equals("login") || action.equals("getThreads")) {
				threadList.add(new ThreadNode(str_id, str_type, str_timestamp,
						str_authorID, str_author, str_title, str_details,
						str_replies, str_icon, str_image, str_lastReplyAuthor,
						str_lastMessage, str_lastReplyTime, str_threadStatus,
						str_photo));
				str_id = "";
				str_type = "";
				str_timestamp = "";
				str_authorID = "";
				str_author = "";
				str_title = "";
				str_details = "";
				str_replies = "";
				str_icon = "";
				str_image = "";
				str_lastReplyAuthor = "";
				str_lastMessage = "";
				str_lastReplyTime = "";
				str_threadStatus = "";
				str_photo = "";
			} else if (action.equals("getDetails")
					|| action.equals("postThread") || action.equals("reply")) {
				detailsList.add(new ThreadNode(str_id, str_type, str_timestamp,
						str_authorID, str_author, str_title, str_details,
						str_replies, str_icon, str_image, str_lastReplyAuthor,
						str_lastMessage, str_lastReplyTime, str_threadStatus,
						str_photo));
				str_id = "";
				str_type = "";
				str_timestamp = "";
				str_authorID = "";
				str_author = "";
				str_title = "";
				str_details = "";
				str_replies = "";
				str_icon = "";
				str_image = "";
				str_lastReplyAuthor = "";
				str_lastMessage = "";
				str_lastReplyTime = "";
				str_threadStatus = "";
				str_photo = "";
			}
			thread = false;
		} else if (localName.equalsIgnoreCase("id")) {
			id = false;
		} else if (localName.equalsIgnoreCase("type")) {
			type = false;
		} else if (localName.equalsIgnoreCase("timestamp")) {
			timestamp = false;
		} else if (localName.equalsIgnoreCase("authorID")) {
			authorID = false;
		} else if (localName.equalsIgnoreCase("author")) {
			author = false;
		} else if (localName.equalsIgnoreCase("title")) {
			title = false;
		} else if (localName.equalsIgnoreCase("details")) {
			details = false;
		} else if (localName.equalsIgnoreCase("replies")) {
			replies = false;
		} else if (localName.equalsIgnoreCase("icon")) {
			icon = false;
		} else if (localName.equalsIgnoreCase("image")) {
			image = false;
		} else if (localName.equalsIgnoreCase("threadStatus")) {
			threadStatus = false;
		} else if (localName.equalsIgnoreCase("notifications")) {
			notifications = false;
		} else if (localName.equalsIgnoreCase("message")) {
			message = false;
		} else if (localName.equalsIgnoreCase("reply")) {
			ReplyNode rn = new ReplyNode(str_replyID, str_replyTimestamp,
					str_replyAuthorID, str_replyAuthor, str_message,
					str_replyIcon, str_replyImage, str_replyPhoto);			
			if (action.equals("getDetails")) {
				detailsList.add(rn);
			} else {
				detailMsgList.add(rn);
			}
			reply = false;
			str_replyID = "";
			str_replyTimestamp = "";
			str_replyAuthorID = "";
			str_replyAuthor = "";
			str_message = "";
			str_replyIcon = "";
			str_replyImage = "";
			str_replyPhoto = "";
		} else if (localName.equalsIgnoreCase("replyid")) {
			replyID = false;
		} else if (localName.equalsIgnoreCase("doctor")) {
			teamList.add(new DoctorNode(str_id, str_title, str_lastname,
					str_firstname, str_displayName, str_specialization, str_location, str_email, str_phone,
					str_docstatus, str_photo));
			doctor = false;
			str_id = "";
			str_title = "";
			str_lastname = "";
			str_firstname = "";
			str_specialization = "";
			str_location = "";
			str_email = "";
			str_phone = "";
			str_docstatus = "";
			str_photo = "";
		} else if (localName.equalsIgnoreCase("lastname")) {
			lastname = false;
		} else if (localName.equalsIgnoreCase("firstname")) {
			firstname = false;
		} else if (localName.equalsIgnoreCase("displayName")) {
			displayName = false;
		} else if (localName.equalsIgnoreCase("specialization")) {
			specialization = false;
		} else if (localName.equalsIgnoreCase("location")) {
			location = false;
		} else if (localName.equalsIgnoreCase("email")) {
			email = false;
		} else if (localName.equalsIgnoreCase("phone")) {
			phone = false;
		} else if (localName.equalsIgnoreCase("photo")) {
			photo = false;
		} else if (localName.equalsIgnoreCase("lastAuthor")) {
			lastAuthor = false;
		} else if (localName.equalsIgnoreCase("lastMessage")) {
			lastMessage = false;
		} else if (localName.equalsIgnoreCase("time")) {
			time = false;
		} else if (localName.equalsIgnoreCase("currentUser")) {
			loginInfo.add(new LoginInfoNode(str_id, str_title, str_lastname,
					str_firstname, str_displayName, str_docstatus, str_photo));
			currentUser = false;
			str_id = "";
			str_lastname = "";
			str_firstname = "";
			str_docstatus = "";
			str_photo = "";
		} else if (localName.equalsIgnoreCase("pingStatus")) {
			pingList.add(str_pingStatus);
			pingStatus = false;
			str_pingStatus = "";
		} else if (localName.equalsIgnoreCase("msgCtr")) {
			msgCenterList.add(new MsgCenterNode(str_id, str_type, str_subject,
					str_timestamp, str_msgStatus, str_to, str_from,
					str_authorName, str_photo, str_replies, str_author,
					str_message, str_lastReplyAuthor, str_lastMessage, str_lastReplyTime));
			msgCtr = false;
			str_id = "";
			str_type = "";
			str_subject = "";
			str_timestamp = "";
			str_msgStatus = "";
			str_to = "";
			str_from = "";
			str_authorName = "";
			str_photo = "";
			str_replies = "";
			str_author = "";
			str_message = "";
			str_lastReplyAuthor = "";
			str_lastMessage = "";
			str_lastReplyTime = "";
		} else if (localName.equalsIgnoreCase("subject")) {
			subject = false;
		} else if (localName.equalsIgnoreCase("msgStatus")) {
			msgStatus = false;
		} else if (localName.equalsIgnoreCase("to")) {
			to = false;
		} else if (localName.equalsIgnoreCase("from")) {
			from = false;
		} else if (localName.equalsIgnoreCase("authorName")) {
			authorName = false;
		} else if (localName.equalsIgnoreCase("toID")) {
			toID = false;
		} else if (localName.equalsIgnoreCase("fromID")) {
			fromID = false;
		} else if (localName.equalsIgnoreCase("privateMsg")) {
			detailMsgList.add(new MessageNode(str_id, str_timestamp, str_fromID, str_from, str_photo, str_toID, str_to, str_subject, str_details, str_msgStatus, str_replies, str_author, str_message, str_lastReplyTime));
			privateMsg = false;
			str_id = "";
			str_timestamp = "";
			str_fromID = "";
			str_from = "";
			str_photo = "";
			str_toID = "";
			str_to = "";
			str_subject = "";
			str_details = "";
			str_msgStatus = "";
			str_replies = "";
			str_author = "";
			str_message = "";
			str_lastReplyTime = "";
		}
	}

	@Override
	public void characters(char ch[], int start, int length) {
		/*
		 * Would be called on the following structure:
		 * <element>characters</element>
		 */
		String str = new String(ch, start, length);
		if(ver) {
			version = str;
		} else if (status) {
			if (doctor || currentUser) {
				str_docstatus = str;
			} else {
				str_status = str;
				if (action.equalsIgnoreCase("login")
						|| (action.equalsIgnoreCase("getThreads"))) {
					threadList.add(str_status);
				} else if (action.equalsIgnoreCase("getDetails")
						|| action.equalsIgnoreCase("postThread")
						|| action.equals("reply")) {
					detailsList.add(str_status);
				} else if (action.equalsIgnoreCase("getUsers")) {
					teamList.add(str_status);
				} else if (action.equalsIgnoreCase("statusChange")) {
					statusList.add(str_status);
				} else if (action.equalsIgnoreCase("logout")) {
					logoutList.add(str_status);
				} else if (action.equalsIgnoreCase("photoChange")) {
					photoChangeList.add(str_status);
				} else if (action.equalsIgnoreCase("postMsg")
						|| action.equalsIgnoreCase("replyMsg")) {
					personalMsgList.add(str_status);
				} else if (action.equalsIgnoreCase("getMsgCtr")) {
					msgCenterList.add(str_status);
				} else if (action.equalsIgnoreCase("getMsgDetails")) {
					detailMsgList.add(str_status);
				}
			}
		} else if (token) {
			Global.token = str;
		} else if (id) {
			str_id = str;
		} else if (type) {
			str_type = str;
		} else if (timestamp) {
			if (reply) {
				str_replyTimestamp = str;
			} else {
				str_timestamp = str;
			}
		} else if (authorID) {
			if (reply) {
				str_replyAuthorID = str;
			} else {
				str_authorID = str;
			}
		} else if (author) {
			if (reply) {
				str_replyAuthor = str;
			} else {
				str_author = str;
			}
		} else if (title) {
			str_title = str;
		} else if (details) {
			str_details = str;
		} else if (replies) {
			str_replies = str;
		} else if (icon) {
			if (reply) {
				str_replyIcon = str;
			} else {
				str_icon = str;
			}
		} else if (image) {
			if (reply) {
				str_replyImage = str;
			} else {
				str_image = str;
			}
		} else if (threadStatus) {
			str_threadStatus = str;
		} else if (message) {
			str_message = str;
		} else if (replyID) {
			str_replyID = str;
		} else if (lastname) {
			str_lastname = str;
		} else if (firstname) {
			str_firstname = str;
		} else if(displayName) {
			str_displayName = str;
		} else if (specialization) {
			str_specialization = str;
		} else if (location) {
			str_location = str;
		} else if (email) {
			str_email = str;
		} else if (phone) {
			str_phone = str;
		} else if (photo) {
			if (reply) {
				str_replyPhoto = str;
			} else {
				str_photo = str;
				if (action.equalsIgnoreCase("photoChange")) {
					photoChangeList.add(str_photo);
					str_photo = "";
				}
			}
		} else if (lastAuthor) {
			str_lastReplyAuthor = str;
		} else if (lastMessage) {
			str_lastMessage = str;
		} else if (time) {
			str_lastReplyTime = str;
		} else if (notifications) {
			str_notifications = str;
		} else if (pingStatus) {
			str_pingStatus = str;
		} else if (subject) {
			str_subject = str;
		} else if (msgStatus) {
			str_msgStatus = str;
		} else if (to) {
			str_to = str;
		} else if (from) {
			str_from = str;
		} else if (authorName) {
			str_authorName = str;
		} else if (toID) {
			str_toID = str;
		} else if (fromID) {
			str_fromID = str;
		}
	}
}

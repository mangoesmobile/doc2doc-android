package com.mangoesmobile.doc2doc.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbaseHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "doc2doc";

	private static final int DATABASE_VERSION = 2;
	
	private static final String CREATE_TABLE_DRAFT = "create table draft (_id integer primary key autoincrement, title text, details text, attach text, notify text);";
	private static final String CREATE_TABLE_TEAM = "create table team (_id integer primary key autoincrement, user_id text, title text, lastname text, firstname text, display_name text, specialization text, location text, email text, phone text, status text, photo text);";
	private static final String CREATE_TABLE_MESSAGE = "create table messages (_id integer primary key autoincrement, message_id text, type text, subject text, timestamp text, msg_status text, message_to text, message_from text, author_name text, photo text, replies text, author text, message text, last_reply_author text, last_message, time text);";

	public DbaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Method is called during creation of the database
	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(CREATE_TABLE_DRAFT);
		database.execSQL(CREATE_TABLE_TEAM);
		database.execSQL(CREATE_TABLE_MESSAGE);
	}

	// Method is called during an upgrade of the database, e.g. if you increase
	// the database version
	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		Log.w(DbaseHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		database.execSQL("DROP TABLE IF EXISTS draft");
		database.execSQL("DROP TABLE IF EXISTS team");
		database.execSQL("DROP TABLE IF EXISTS message");
		onCreate(database);
	}
}
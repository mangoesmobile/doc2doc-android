package com.mangoesmobile.doc2doc.util;

public class MessageNode {
	  public String id;
	  public String timestamp;
	  public String fromID;
	  public String from;
	  public String photo;
	  public String toID;
	  public String to;
	  public String subject;
	  public String details;
	  public String messageStatus;
	  public String replies;
	  public String author;
	  public String message;
	  public String time;
	  
	  public MessageNode(String i, String ts, String fi, String f, String p, String ti, String t, String s, String d, String ms, String re, String au, String m, String tm) {
		  id = i;
		  timestamp = ts;
		  fromID = fi;
		  from = f;
		  photo = p;
		  toID = ti;
		  to = t;
		  subject = s;
		  details = d;
		  messageStatus = ms;
		  replies = re;
		  author = au;
		  message = m;
		  time = tm;
	  }
}

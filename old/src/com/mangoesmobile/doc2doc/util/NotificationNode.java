package com.mangoesmobile.doc2doc.util;

public class NotificationNode {
	
	public String id;
	public String message;
	public String author;
	public String timestamp;
	public String sourceThread;
	
	public NotificationNode(String i, String m, String a, String t, String s){
		id = i;
		message = m;
		author = a;
		timestamp = t;
		sourceThread = s;
	}

}

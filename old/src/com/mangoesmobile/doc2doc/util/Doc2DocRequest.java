package com.mangoesmobile.doc2doc.util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.mangoesmobile.doc2doc.global.Global;


import android.os.Bundle;
import android.util.Log;

public class Doc2DocRequest {

	private final String doc2docUrl = "http://192.168.1.101/doc2doc/";

	public String request(String action, Bundle params)
			throws FileNotFoundException, MalformedURLException, IOException {
		String boundary = "====================";
		String endLine = "\r\n";

		OutputStream os;

		String url = doc2docUrl + action;
		Log.d("doc2doc-url", url);
		HttpURLConnection conn = (HttpURLConnection) new URL(url)
				.openConnection();
		conn.setRequestProperty("User-Agent", System.getProperties()
				.getProperty("http.agent") + " Doc2DocAndroidNative");
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "multipart/form-data;boundary="
				+ boundary);
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setRequestProperty("Connection", "Keep-Alive");
		// in case of any action except login
		if (!action.equalsIgnoreCase("authenticate/login")) {
			conn.setRequestProperty("Cookie", Global.cookie);
		}

		if (params != null) {
			os = new BufferedOutputStream(conn.getOutputStream());
			os.write(("--" + boundary + endLine).getBytes());
			os.write((encodePostBody(params, boundary)).getBytes());
			os.write((endLine + "--" + boundary + endLine).getBytes());
			if (params.containsKey("upfile")) {
				File file = new File(params.getString("upfile"));
				os.write(("Content-Disposition: form-data; name=\"upfile\"; filename=\""
						+ file.getName() + "\"" + endLine).getBytes());
				os.write(("Content-Type: content/unknown" + endLine + endLine)
						.getBytes());
				FileInputStream is = new FileInputStream(file);
				byte[] buffer = new byte[4096];
				int bytes_read;
				while ((bytes_read = is.read(buffer)) != -1) {
					os.write(buffer, 0, bytes_read);
				}
				os.write((endLine + "--" + boundary + endLine).getBytes());
				is.close();
			}
			os.flush();
			os.close();
		}

		// stores the cookie during login
		if (action.equalsIgnoreCase("authenticate/login")) {
			String headerName = null;
			for (int i = 1; (headerName = conn.getHeaderFieldKey(i)) != null; i++) {
				if (headerName.equalsIgnoreCase("Set-Cookie")) {
					Global.cookie = conn.getHeaderField(i);
					Log.d("cookie", Global.cookie);
					break;
				}
			}
		}

		String response = read(conn.getInputStream());

		conn.disconnect();

		return response;
	}
	
	public static String read(InputStream in) throws IOException {
		StringBuilder sb = new StringBuilder();
		BufferedReader r = new BufferedReader(new InputStreamReader(in), 1000);
		for (String line = r.readLine(); line != null; line = r.readLine()) {
			sb.append(line);
		}
		in.close();
		return sb.toString();
	}

	private String encodePostBody(Bundle parameters, String boundary) {
		if (parameters == null)
			return "";
		StringBuilder sb = new StringBuilder();

		for (String key : parameters.keySet()) {
			if (key.equalsIgnoreCase("upfile")) {
				continue;
			}

			sb.append("Content-Disposition: form-data; name=\"" + key
					+ "\"\r\n\r\n" + parameters.getString(key));
			sb.append("\r\n" + "--" + boundary + "\r\n");
		}

		return sb.toString();
	}
}

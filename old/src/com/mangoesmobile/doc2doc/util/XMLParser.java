package com.mangoesmobile.doc2doc.util;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

public class XMLParser {
	
	InputSource xml;
		
	public XMLParser(InputSource x){
		xml = x;
	}

	public void parseXML(String act) {
		try {

			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			// send xml to handler
			XMLReader myReader = saxParser.getXMLReader();
			myReader.setContentHandler(new XMLHandler(act));
			myReader.parse(xml);
//			saxParser.parse(xml, new XMLHandler(act));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

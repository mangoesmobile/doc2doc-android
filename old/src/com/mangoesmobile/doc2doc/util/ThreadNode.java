package com.mangoesmobile.doc2doc.util;

public class ThreadNode {	
	public String id;
	public String type;
	public String timestamp;
	public String authorID;
	public String author;
	public String title;
	public String details;
	public String replies;
	public String icon;
	public String image;
	public String lastReplyAuthor;
	public String lastReply;
	public String lastReplyTime;
	public String threadStatus;
	public String photo;
	
	public ThreadNode(String i, String ty, String tim, String aid, String a, String tle, String det, String rep, String ic, String im, String ra, String rm, String rt, String ts, String ph){
		id = i;
		type = ty;
		timestamp = tim;
		authorID = aid;
		author = a;
		title = tle;
		details = det;
		replies = rep;
		icon = ic;
		image = im;
		lastReplyAuthor = ra;
		lastReply = rm;
		lastReplyTime = rt;
		threadStatus = ts;
		photo = ph;
	}	
}

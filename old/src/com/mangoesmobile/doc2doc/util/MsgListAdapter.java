package com.mangoesmobile.doc2doc.util;

import java.util.LinkedList;

import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.global.Global;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MsgListAdapter extends BaseAdapter {

	Context context;
	LayoutInflater inflater;

	int group, child;

	LinkedList<MsgCenterNode> messages;

	LoginInfoNode loginInfo;
	
	@SuppressWarnings("unchecked")
	public MsgListAdapter(Context c, int gr, int ch) {
		context = c;
		group = gr;
		child = ch;
		messages = Global.msgs[group][child];
		loginInfo = (LoginInfoNode) XMLHandler.loginInfo.get(0);
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return messages.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return messages.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		MsgCenterNode msg = (MsgCenterNode) getItem(arg0);
		if (arg1 == null) {
			arg1 = inflater.inflate(R.layout.row_messagecenter, null);
			System.out.println("true");
		}
		ImageView imgAuthor = (ImageView) arg1
				.findViewById(R.id.img_messagecenter_author);
		TextView lblTopic = (TextView) arg1
				.findViewById(R.id.lbl_messagecenter_topic);
		TextView lblTitle = (TextView) arg1
				.findViewById(R.id.lbl_messagecenter_title);
		TextView lblTimestamp = (TextView) arg1
				.findViewById(R.id.lbl_messagecenter_timestamp);
		TextView lblResponse = (TextView) arg1.findViewById(R.id.lbl_messagecenter_response);
		TextView lblLatestReply = (TextView) arg1.findViewById(R.id.lbl_messagecenter_latestreply);
		TextView lblReplyAuthor = (TextView) arg1.findViewById(R.id.lbl_messagecenter_replyauthor);
		TextView lblReplyTime = (TextView) arg1.findViewById(R.id.lbl_messagecenter_replytime);
		if (!msg.photo.equals("")) {
			String url = Global.baseUrl + "?act=image&ur="
					+ Global.username + "&tkn=" + Global.token + "&im="
					+ msg.photo;
			Global.loadPhoto(context, url, imgAuthor);
		} else {
			imgAuthor.setImageResource(R.drawable.user);
		}
		System.out.println(msg.timestamp);
		lblTimestamp.setText(Global.formatDate(msg.timestamp));
		if (msg.type.equalsIgnoreCase("ping")) {
			if (group == 1 && child == 2) {
				lblTopic.setText("You sent a ping");
			} else {
				lblTopic.setText(msg.authorName + " pinged you");
			}
			lblResponse.setVisibility(View.GONE);
			lblTitle.setVisibility(View.GONE);
			lblLatestReply.setVisibility(View.GONE);
			lblReplyAuthor.setVisibility(View.GONE);
			lblReplyTime.setVisibility(View.GONE);
		} else if (msg.type.equalsIgnoreCase("message")) {
			if (group == 1 && child == 2) {
				lblTopic.setText(msg.subject);
				lblTitle.setText(msg.message);
			} else {
				if (msg.from.equalsIgnoreCase(loginInfo.id)) {
					lblTopic.setText(msg.lastReplyAuthor + " replied to your message");
					lblTitle.setText(msg.subject);
				} else {
					lblTopic.setText(msg.authorName + " sent you a message");
					lblTitle.setText(msg.subject);
				}
			}
			lblResponse.setVisibility(View.GONE);
			lblTitle.setVisibility(View.VISIBLE);
			lblLatestReply.setVisibility(View.GONE);
			lblReplyAuthor.setVisibility(View.GONE);
			lblReplyTime.setVisibility(View.GONE);
		} else if (msg.type.equalsIgnoreCase("notification")) {
			if (group == 0 && child == 2) {
				lblTopic.setText(msg.subject);
				lblTitle.setText(msg.message);
			} else {
				if (msg.from.equalsIgnoreCase(loginInfo.id)) {
					lblTopic.setText(msg.lastReplyAuthor + " commented on your thread");
					lblTitle.setText(msg.subject);
				} else {
					lblTopic.setText(msg.authorName + " sent you a notification");
					lblTitle.setText(msg.subject);
				}
			}
			if (msg.author.length() > 0) {
				lblResponse.setText("Re: \"");
				lblLatestReply.setText(msg.lastMessage);
				lblReplyAuthor.setText("\" by " + msg.lastReplyAuthor);
				lblReplyTime.setText(", " + msg.time + " ago");
			} else {
				lblResponse.setText("No one responded yet");
				lblReplyAuthor.setText("");
				lblLatestReply.setText("");
				lblReplyTime.setText("");
			}
			lblTitle.setVisibility(View.VISIBLE);
			lblLatestReply.setVisibility(View.VISIBLE);
			lblReplyAuthor.setVisibility(View.VISIBLE);
			lblReplyTime.setVisibility(View.VISIBLE);
		} else {                  // for outbox
			lblTopic.setText(msg.subject);
			lblTitle.setText(msg.message);
			lblResponse.setVisibility(View.GONE);
			lblTitle.setVisibility(View.VISIBLE);
			lblLatestReply.setVisibility(View.GONE);
			lblReplyAuthor.setVisibility(View.GONE);
			lblReplyTime.setVisibility(View.GONE);
		}
		return arg1;
	}

}

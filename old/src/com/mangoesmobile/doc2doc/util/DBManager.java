package com.mangoesmobile.doc2doc.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DBManager {
//	public static final String KEY_ID = "_id";
//	public static final String KEY_TITLE = "title";
//	public static final String KEY_DETAILS = "details";
//	public static final String KEY_ATTACH = "attach";
//	public static final String KEY_NOTIFY = "notify";
//	private static final String DATABASE_TABLE = "draft";
	private Context context;
	private String dbTable;
	private String[] fields;
	private SQLiteDatabase database;
	private DbaseHelper dbHelper;

	public DBManager(Context c, String t, String[] f) {
		context = c;
		dbTable = t;
		fields = f;
	}

	public void open() throws SQLException {
		dbHelper = new DbaseHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	/**
	 * Create a new table If the table is successfully created return the new
	 * rowId for that note, otherwise return a -1 to indicate failure.
	 */
	public long insertToDB(String[] key, String[] value) {
		ContentValues initialValues = createContentValues(key, value);

		return database.insert(dbTable, null, initialValues);
	}

	/**
	 * Update the table
	 */
	public boolean updateDB(long rowId, String[] key, String[] value) {
		ContentValues updateValues = createContentValues(key, value);

		return database.update(dbTable, updateValues, "_id="
				+ rowId, null) > 0;
	}

	/**
	 * Deletes table
	 */
	public boolean deleteFromDB(long rowId) {
		return database.delete(dbTable, "_id=" + rowId, null) > 0;
	}
	
	/**
	 * Deletes all the rows from table
	 */
	public boolean clearDB() {
		return database.delete(dbTable, null, null) > 0;
	}

	/**
	 * Return a Cursor over the list of all table in the database
	 * 
	 * @return Cursor over all notes
	 */
	public Cursor fetchAllFromDB() {
		return database.query(dbTable, fields, null, null, null, null,
				null);
	}

	/**
	 * Return a Cursor positioned at the defined table
	 */
	public Cursor fetchFromDB(long rowId) throws SQLException {
		Cursor mCursor = database.query(true, dbTable, fields,
				"_id=" + rowId, null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	private ContentValues createContentValues(String[] key, String[] value) {
		ContentValues values = new ContentValues();
		for (int i = 0; i < key.length; i++) {
			values.put(key[i], value[i]);
		}
		return values;
	}
}

package com.mangoesmobile.doc2doc.util;

public class LoginInfoNode {
	public String id;
	public String title;
	public String lastname;
	public String firstname;
	public String displayName;
	public String status;
	public String photo;
	
	public LoginInfoNode(String i, String t, String l, String f, String d, String st, String ph){
		id = i;
		title = t;
		lastname = l;
		firstname = f;
		displayName = d;
		status = st;
		photo = ph;
	}
}
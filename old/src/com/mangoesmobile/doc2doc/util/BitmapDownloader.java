package com.mangoesmobile.doc2doc.util;

import java.io.BufferedInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.net.URI;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;

import com.mangoesmobile.doc2doc.global.Global;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

public class BitmapDownloader extends AsyncTask<MultipartEntity, Void, Bitmap> {
	// private String url;
	private final SoftReference<ImageView> imageViewReference;
	private int scale;

	public BitmapDownloader(ImageView imageView, int s) {
		imageViewReference = new SoftReference<ImageView>(imageView);
		scale = s;
	}

	@Override
	// Actual download method, run in the task thread
	protected Bitmap doInBackground(MultipartEntity... params) {
		// params comes from the execute() call: params[0] is the url.
		return downloadBitmap(params[0]);
	}

	@Override
	// Once the image is downloaded, associates it to the imageView
	protected void onPostExecute(Bitmap bitmap) {
		if (isCancelled()) {
			bitmap = null;
		}

		if (imageViewReference != null) {
			ImageView imageView = imageViewReference.get();
			if (imageView != null) {
				imageView.setImageBitmap(bitmap);
			}
		}
	}

	private Bitmap downloadBitmap(MultipartEntity parameter) {
		HttpResponse response = null;
		try {
			// Create connection
			String url = Global.baseUrl;
			HttpPost request = new HttpPost();
			request.setURI(new URI(url));
			if (parameter != null)
				request.setEntity(parameter);
			HttpClient client = new DefaultHttpClient();
			response = client.execute(request);

			HttpEntity entity = response.getEntity();

			// Get Response
			InputStream stream = entity.getContent();

			BufferedInputStream bis = new BufferedInputStream(stream, 32 * 1024);

			ByteArrayBuffer baf = new ByteArrayBuffer(50);
			int current = 0;
			while ((current = bis.read()) != -1) {
				baf.append((byte) current);
			}
			byte[] imageData = baf.toByteArray();
			System.out.println(imageData.length);

			// decode image size
			BitmapFactory.Options optionsSize = new BitmapFactory.Options();
			optionsSize.inJustDecodeBounds = true;
			BitmapFactory.decodeByteArray(imageData, 0, imageData.length,
					optionsSize);

			// Find the correct scale value. It should be the power of 2.
			int reqWidth = 0;
			int reqHeight = 0;
			int width_tmp = optionsSize.outWidth, height_tmp = optionsSize.outHeight;
			if (width_tmp <= height_tmp) {  // for portrait image
				reqWidth = 60 * scale;
				reqHeight = 80 * scale;
			} else {						// for landscape image
				reqWidth = 60 * scale;
				reqHeight = 45 * scale;
			}
			System.out.println(width_tmp + " " + height_tmp);
			int scale = 1;
			while (width_tmp/2 > reqWidth || height_tmp/2 > reqHeight) {
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}

			// decode with inSampleSize
			BitmapFactory.Options optionsScale = new BitmapFactory.Options();
			optionsScale.inSampleSize = scale;
			Bitmap bmImg = BitmapFactory.decodeByteArray(imageData, 0,
					imageData.length, optionsScale);

			System.out.println(bmImg.getWidth() + " " + bmImg.getHeight());
			return bmImg;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	static class FlushedInputStream extends FilterInputStream {
		public FlushedInputStream(InputStream inputStream) {
			super(inputStream);
		}

		@Override
		public long skip(long n) throws IOException {
			long totalBytesSkipped = 0L;
			while (totalBytesSkipped < n) {
				long bytesSkipped = in.skip(n - totalBytesSkipped);
				if (bytesSkipped == 0L) {
					int byt = read();
					if (byt < 0) {
						break; // we reached EOF
					} else {
						bytesSkipped = 1; // we read one byte
					}
				}
				totalBytesSkipped += bytesSkipped;
			}
			return totalBytesSkipped;
		}
	}
}

package com.mangoesmobile.doc2doc.tabcontents;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.global.Global;

public class ViewAttachment extends Activity{

	GoogleAnalyticsTracker tracker;
	
	private ProgressDialog webProgress;

	WebView webview;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.viewattachment);
		
		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.start(Global.UA, 20, this);
		tracker.trackPageView("/ViewAttachment");

		webProgress = new ProgressDialog(getParent());
		webProgress.setMessage("Loading, please wait..");
		
		String imageId = getIntent().getStringExtra("imageId");
		
		String url = Global.baseUrl + "?act=image&ur=" + Global.username + "&tkn=" + Global.token + "&im=" + imageId;

		webview = (WebView) findViewById(R.id.web_viewattachment);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.loadUrl(url);

		webview.setWebViewClient(new ReferenceWebViewClient());
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {
			webview.goBack();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		webProgress.dismiss();
	}

	@Override
	public void onBackPressed() {
		if (getParent() instanceof ActivityGroupMain) {
			ActivityGroupMain parentActivity = (ActivityGroupMain) getParent();
		parentActivity.onBackPressed();
		} else {
			ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
			parentActivity.onBackPressed();
		}
	}

	private class ReferenceWebViewClient extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			webProgress.show();
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			webProgress.hide();
		}
	}
}

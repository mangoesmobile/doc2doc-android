package com.mangoesmobile.doc2doc.tabcontents;

import java.util.LinkedList;
import java.util.Timer;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.global.Global;
import com.mangoesmobile.doc2doc.util.DBManager;
import com.mangoesmobile.doc2doc.util.DoctorNode;
import com.mangoesmobile.doc2doc.util.LoginInfoNode;
import com.mangoesmobile.doc2doc.util.ServerConnection;
import com.mangoesmobile.doc2doc.util.XMLHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class Team extends Activity {

	GoogleAnalyticsTracker tracker;

	private static final int CAMERA_PIC_REQUEST = 226;
	private static final int GALLERY_PIC_REQUEST = 425;
	Timer timer;
	boolean paused = false;

	TableLayout tblDocList;

	ImageView imgUserImage;
	ImageView imgUserStatus;
	TextView lblUsername;
	TextView lblUserStatus;

	EditText txtSearch;

	String messageTo;

	Handler refreshHandler;
	Runnable runRefresh = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			if (!paused) {
				System.out.println("refreshing team");
				refreshDoclist();
			} else {
				System.out.println("thread is not active");
			}
			refreshHandler.postDelayed(this, 30000);
		}
	};

	boolean finish = false;
	Thread refreshListener = new Thread() {
		@Override
		public void run() {
			try {
				while (!finish) {
					sleep(1000);
					if (Global.refreshWhat == 2) {
						Global.refreshWhat = -1;
						refreshHandler.removeCallbacks(runRefresh);
						refreshHandler.post(runRefresh);
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	};

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.team);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.start(Global.UA, 20, this);
		tracker.trackPageView("/Team");

		Global.team = this;

		refreshHandler = new Handler();
		refreshHandler.postDelayed(runRefresh, 30000);

		refreshListener.setPriority(Thread.MIN_PRIORITY);
		refreshListener.start();

		tblDocList = (TableLayout) findViewById(R.id.tbl_doclist);

		txtSearch = (EditText) findViewById(R.id.txt_team_search);

		imgUserImage = (ImageView) findViewById(R.id.img_team_userimage);
		imgUserStatus = (ImageView) findViewById(R.id.img_team_userstatus);
		lblUsername = (TextView) findViewById(R.id.lbl_team_username);
		lblUserStatus = (TextView) findViewById(R.id.lbl_team_userstatus);

		updateUserInfo();

		DBManager manager = new DBManager(getParent(), Global.TABLE_TEAM,
				Global.FIELDS_TEAM_WITH_ID);
		manager.open();
		Cursor cache = manager.fetchAllFromDB();
		if (cache.getCount() == 0) {
			Toast toast = Toast.makeText(getParent(), "Loading Team...",
					Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();

			MultipartEntity parameters = new MultipartEntity();

			try {
				parameters.addPart("act", new StringBody("getUsers"));
				parameters.addPart("ur", new StringBody(Global.username));
				parameters.addPart("tkn", new StringBody(Global.token));
			} catch (Exception e) {
				e.printStackTrace();
			}

			new DocListTask().execute(parameters);
			
			cache.close();
		} else {
			LinkedList<Object> team = new LinkedList<Object>();
			cache.moveToFirst();
			for (int i = 0; i < cache.getCount(); i++) {
				DoctorNode docinfo = new DoctorNode(cache.getString(cache
						.getColumnIndex("user_id")), cache.getString(cache
						.getColumnIndex("title")), cache.getString(cache
						.getColumnIndex("lastname")), cache.getString(cache
						.getColumnIndex("firstname")), cache.getString(cache
						.getColumnIndex("display_name")), cache.getString(cache
						.getColumnIndex("specialization")),
						cache.getString(cache.getColumnIndex("location")),
						cache.getString(cache.getColumnIndex("email")),
						cache.getString(cache.getColumnIndex("phone")),
						cache.getString(cache.getColumnIndex("status")),
						cache.getString(cache.getColumnIndex("photo")));

				team.add(docinfo);
				cache.moveToNext();
			}
			cache.close();
			createDocList(team, 0);
			refreshDoclist();
		}
		manager.close();
	}

	private void updateUserInfo() {
		LoginInfoNode loginInfo = (LoginInfoNode) XMLHandler.loginInfo.get(0);
		Global.name = loginInfo.title + " " + loginInfo.lastname;
		Global.status = loginInfo.status;
		lblUsername.setText(loginInfo.displayName);
		lblUserStatus.setText(loginInfo.status);
		System.out.println("u r " + loginInfo.status);
		if (loginInfo.status.equalsIgnoreCase("ONLINE")) {
			imgUserStatus.setImageResource(R.drawable.bg_photo_online);
			lblUserStatus.setTextColor(Color.rgb(0, 102, 153));
		} else {
			imgUserStatus.setImageResource(R.drawable.bg_photo_oncall);
			lblUserStatus.setTextColor(Color.rgb(101, 176, 73));
		}
		if (Global.imageDrawable != null) {
			imgUserImage.setImageDrawable(Global.imageDrawable);
		} else {
			imgUserImage.setImageResource(R.drawable.default_photo);
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		txtSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				for (int i = 0; i < tblDocList.getChildCount(); i++) {
					TableRow row = (TableRow) tblDocList.getChildAt(i);
					TextView name = (TextView) row
							.findViewById(R.id.lbl_teamlist_name);

					if (name.getText().toString().toLowerCase()
							.contains(arg0.toString().toLowerCase())) {
						row.setVisibility(View.VISIBLE);
					} else {
						row.setVisibility(View.GONE);
					}
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}
		});

		imgUserImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				tracker.trackEvent("click", "Button", "Change Status", 0);
				Toast toast = Toast.makeText(getParent(), "Changing status...",
						Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 80);
				toast.show();
				MultipartEntity parameters = new MultipartEntity();
				try {
					parameters.addPart("act", new StringBody("statusChange"));
					parameters.addPart("ur", new StringBody(Global.username));
					parameters.addPart("tkn", new StringBody(Global.token));
					parameters.addPart("stat", new StringBody(Global.stat));
				} catch (Exception e) {
					e.printStackTrace();
				}

				new ChangeStatusTask().execute(parameters);
			}
		});
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if (hasFocus) {
			if (Global.updateProfileInTeam) {
				updateUserInfo();
				Global.updateProfileInTeam = false;
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		paused = false;
		if (Global.updateProfileInTeam) {
			updateUserInfo();
			Global.updateProfileInTeam = false;
		}
	}

	public void refreshDoclist() {
		System.out.println("Refreshing doclist..");
		MultipartEntity parameters = new MultipartEntity();

		try {
			parameters.addPart("act", new StringBody("getUsers"));
			parameters.addPart("ur", new StringBody(Global.username));
			parameters.addPart("tkn", new StringBody(Global.token));
		} catch (Exception e) {
			e.printStackTrace();
		}

		new DocListTask().execute(parameters);
	}

	private void createDocList(LinkedList<Object> result, int startingIndex) {
		LinkedList<Object> doclist = result;
		for (int i = startingIndex; i < doclist.size(); i++) {
			final DoctorNode docinfo = (DoctorNode) doclist.get(i);
			TableRow row = (TableRow) LayoutInflater.from(this).inflate(
					R.layout.row_teamlist, null, false);
			row.setId(Integer.parseInt(docinfo.id));

			ImageView imgDocImage = (ImageView) row
					.findViewById(R.id.img_teamlist_user);
			ImageView imgStatus = (ImageView) row
					.findViewById(R.id.img_teamlist_userstatus);
			TextView lblName = (TextView) row
					.findViewById(R.id.lbl_teamlist_name);
			TextView lblSpecialization = (TextView) row
					.findViewById(R.id.lbl_teamlist_specialization);
			TextView lblLocation = (TextView) row
					.findViewById(R.id.lbl_teamlist_location);
			TextView lblStatus = (TextView) row
					.findViewById(R.id.lbl_teamlist_userstatus);

			ImageView btnPing = (ImageView) row
					.findViewById(R.id.img_teamlist_ping);
			ImageView btnSendMessage = (ImageView) row
					.findViewById(R.id.img_teamlist_message);

			if (!docinfo.photo.equals("")) {
				String url = Global.baseUrl + "?act=image&ur="
						+ Global.username + "&tkn=" + Global.token + "&im="
						+ docinfo.photo;
				Global.loadPhoto(this, url, imgDocImage);
			}

			int icon;
			if (docinfo.status.equalsIgnoreCase("ONLINE")) {
				icon = R.drawable.bg_photo_online;
				lblStatus.setTextColor(Color.rgb(0, 102, 153));
			} else if (docinfo.status.equalsIgnoreCase("ON CALL")) {
				icon = R.drawable.bg_photo_oncall;
				docinfo.status = "*" + docinfo.status + "*";
				lblStatus.setTextColor(Color.rgb(101, 176, 73));
			} else {
				icon = R.drawable.bg_photo_offline;
				lblStatus.setTextColor(Color.DKGRAY);
			}
			imgStatus.setImageResource(icon);
			lblStatus.setText(docinfo.status);

			lblName.setText(docinfo.firstname + " " + docinfo.lastname);
			lblSpecialization.setText(docinfo.specialization);
			lblLocation.setText(docinfo.location);

			btnPing.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					tracker.trackEvent("click", "Button", "Ping", 0);
					MultipartEntity parameters = new MultipartEntity();

					try {
						parameters.addPart("act", new StringBody("ping"));
						parameters.addPart("ur",
								new StringBody(Global.username));
						parameters.addPart("tkn", new StringBody(Global.token));
						parameters.addPart("to", new StringBody(docinfo.id));
					} catch (Exception e) {
						e.printStackTrace();
					}

					Toast toast = Toast.makeText(getParent(), "Pinging "
							+ docinfo.displayName + "...", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();

					new PingTask().execute(parameters);
				}
			});

			btnSendMessage.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					sendMessage(docinfo.id);
				}
			});

			tblDocList.addView(row);
		}
	}

	private void sendMessage(String id) {
		messageTo = id;
		showDialog(0);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog = new Dialog(this);
		switch (id) {
		case 0:
			dialog.show();
			break;
		default:
			dialog = null;
		}
		return dialog;
	}

	@Override
	protected void onPrepareDialog(int id, final Dialog dialog) {
		dialog.setContentView(R.layout.sendmessage);
		dialog.setTitle("          Send New Message          ");
		switch (id) {
		case 0:
			final TextView txtSubject = (TextView) dialog
					.findViewById(R.id.txt_sendmessage_subject);
			final TextView txtMessage = (TextView) dialog
					.findViewById(R.id.txt_sendmessage_message);
			Button btnSend = (Button) dialog
					.findViewById(R.id.btn_sendmessage_send);
			btnSend.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					tracker.trackEvent("click", "Button",
							"Send Private Message", 0);
					if (txtSubject.getText().length() > 0
							&& txtMessage.getText().length() > 0) {
						MultipartEntity parameters = new MultipartEntity();

						try {
							parameters
									.addPart("act", new StringBody("postMsg"));
							parameters.addPart("ur", new StringBody(
									Global.username));
							parameters.addPart("tkn", new StringBody(
									Global.token));
							parameters.addPart("to", new StringBody(messageTo));
							parameters.addPart("sub", new StringBody(txtSubject
									.getText().toString()));
							parameters.addPart("msg", new StringBody(txtMessage
									.getText().toString()));
						} catch (Exception e) {
							e.printStackTrace();
						}

						dialog.dismiss();
						Toast toast = Toast.makeText(getParent(),
								"Sending Message...", Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();

						new PostMsgTask().execute(parameters);
					}
				}
			});
			break;
		}
		super.onPrepareDialog(id, dialog);
	}

	public void chooseSource(final Dialog photoDialog) {
		final CharSequence[] items = { "Camera", "Gallery" };
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Pick Source");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				photoDialog.dismiss();
				switch (item) {
				case (0):
					takePhoto();
					break;
				case (1):
					galleryPhoto();
					break;
				}
				dialog.dismiss();
			}
		});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	private void takePhoto() {
		Intent cameraIntent = new Intent(
				android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
	}

	private void galleryPhoto() {
		Intent galleryIntent = new Intent();
		galleryIntent.setType("image/*");
		galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(
				Intent.createChooser(galleryIntent, "Select Picture"),
				GALLERY_PIC_REQUEST);
	}
	
	public void cacheData(LinkedList<Object> data) {
		System.out.println("caching data");
		DBManager manager = new DBManager(getParent(), Global.TABLE_TEAM,
				Global.FIELDS_TEAM_WITH_ID);
		manager.open();
		manager.clearDB();
		for (int i = 1; i < data.size(); i++) {
			DoctorNode docinfo = (DoctorNode) data.get(i);
			manager.insertToDB(Global.FIELDS_TEAM, new String[] { docinfo.id,
					docinfo.title, docinfo.lastname, docinfo.firstname,
					docinfo.displayName, docinfo.specialization,
					docinfo.location, docinfo.email, docinfo.phone,
					docinfo.status, docinfo.photo });
		}
		manager.close();
	}

	private class DocListTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {
		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.teamList = null;
			con.requestXML("getUsers");
			return XMLHandler.teamList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					if (tblDocList.getChildCount() > 0) {
						tblDocList.removeAllViews();
					}
					createDocList(result, 1);
					cacheData(result);
					System.out.println("doclist updated");
				} else {
					Global.createAlert(Global.team, result.get(2).toString(),
							"Login");
				}
			} catch (Exception e) {
				Toast.makeText(Global.team, "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
		}
	}

	private class PingTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {
		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.pingList = null;
			con.requestXML("ping");
			return XMLHandler.pingList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					Toast toast = Toast.makeText(getParent(), "Pinged!",
							Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					System.out.println("Successfully pinged");
				} else {
					Global.createAlert(Global.team, result.get(2).toString(),
							"Login");
				}
			} catch (Exception e) {
				Toast.makeText(Global.team, "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
		}
	}

	private class PostMsgTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {
		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.personalMsgList = null;
			con.requestXML("postMsg");
			return XMLHandler.personalMsgList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					Toast toast = Toast.makeText(getParent(), "Message sent",
							Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				} else {
					Global.createAlert(Global.team, result.get(2).toString(),
							"Login");
				}
			} catch (Exception e) {
				Toast.makeText(Global.team, "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		paused = true;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		tracker.stop();
		finish = true;
		refreshHandler.removeCallbacks(runRefresh);
	}

	@Override
	public void onBackPressed() {

	}

	private class ChangeStatusTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {

		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.statusList = null;
			con.requestXML("statusChange");
			return XMLHandler.statusList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					int img;
					String statText = "";
					if (Global.stat.equals("2")) {
						img = R.drawable.bg_photo_oncall;
						((LoginInfoNode) XMLHandler.loginInfo.get(0)).status = "ON CALL";
						lblUserStatus.setTextColor(Color.rgb(101, 176, 73));
						statText = "Status: On Call";
						Global.stat = "1";
					} else {
						img = R.drawable.bg_photo_online;
						((LoginInfoNode) XMLHandler.loginInfo.get(0)).status = "ONLINE";
						lblUserStatus.setTextColor(Color.rgb(0, 102, 153));
						statText = "Status: Online";
						Global.stat = "2";
					}
					Global.updateProfile = true;
					Global.updateProfileInMessages = true;
					imgUserStatus.setImageResource(img);
					lblUserStatus.setText(((LoginInfoNode) XMLHandler.loginInfo
							.get(0)).status);
					Toast toast = Toast.makeText(getParent(), statText,
							Toast.LENGTH_LONG);
					toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL,
							0, 80);
					toast.show();
				} else {
					Global.createAlert(getParent(), result.get(2).toString(),
							"Login");
				}
			} catch (Exception e) {
				Toast.makeText(Global.team, "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
			}
		}
	}

}

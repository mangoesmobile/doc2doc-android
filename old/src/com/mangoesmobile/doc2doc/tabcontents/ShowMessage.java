package com.mangoesmobile.doc2doc.tabcontents;

import java.util.LinkedList;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.global.Global;
import com.mangoesmobile.doc2doc.util.MessageNode;
import com.mangoesmobile.doc2doc.util.ReplyNode;
import com.mangoesmobile.doc2doc.util.ServerConnection;
import com.mangoesmobile.doc2doc.util.XMLHandler;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TableRow;

public class ShowMessage extends Activity {

	GoogleAnalyticsTracker tracker;

	Activity showThread;

	TableLayout tblReplies;
	ImageView imgParent;
	TextView lblTitle;
	TextView lblDetails;
	TextView lblAuthor;
	// TextView lblTimestamp;

	ImageView btnAttachment;
	ImageView btnAuthorLocation;
	ImageView btnReply;
	TextView txtReply;

	// EditText txtReply;
	// Button btnReply;

	private String title;
	private String author;
	private String id;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.showmessage);
		
		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.start(Global.UA, 20, this);
		tracker.trackPageView("/ShowMessage");

		showThread = this;

		tblReplies = (TableLayout) findViewById(R.id.tbl_showmessage_reply);
		imgParent = (ImageView) findViewById(R.id.img_showmessage_parentuser);
		lblTitle = (TextView) findViewById(R.id.lbl_showmessage_title);
		lblDetails = (TextView) findViewById(R.id.lbl_showmessage_detail);
		lblAuthor = (TextView) findViewById(R.id.lbl_showmessage_author);
		// lblTimestamp = (TextView) findViewById(R.id.lbl_timeshowmessage);
		btnAttachment = (ImageView) findViewById(R.id.img_showmessage_attachment);
		btnAuthorLocation = (ImageView) findViewById(R.id.img_showmessage_location);
		btnReply = (ImageView) findViewById(R.id.img_showmessage_reply);
		txtReply = (TextView) findViewById(R.id.txt_showmessage_reply);

		LinkedList<Object> details = XMLHandler.detailMsgList;
		System.out.println(details.get(details.size() - 1));
		MessageNode msg = (MessageNode) details.get(details.size() - 1);
		title = msg.subject;
		System.out.println("this is the title " + title);
		author = msg.from;
		id = msg.id;
		// if (!thread.image.equals("")) {
		// imgAttachment.setImageResource(R.drawable.loading_animation);
		// final AnimationDrawable animator = (AnimationDrawable) imgAttachment
		// .getDrawable();
		// class Starter implements Runnable {
		// public void run() {
		// animator.start();
		// }
		// }
		// imgAttachment.post(new Starter());
		// } else {
		// imgAttachment.setImageResource(R.drawable.noimage);
		// }
		lblTitle.setText(title);
		lblDetails.setText(msg.details);
		lblAuthor.setText("by " + author + " on "
				+ Global.formatDate(msg.timestamp));
		// lblTimestamp.setText(thread.timestamp);

		if (!msg.photo.equals("")) {
			// MultipartEntity parameters = new MultipartEntity();
			//
			// try {
			// parameters.addPart("act", new StringBody("image"));
			// parameters.addPart("ur", new StringBody(Global.username));
			// parameters.addPart("tkn", new StringBody(Global.token));
			// parameters.addPart("im", new StringBody(thread.photo));
			// } catch (Exception e) {
			// e.printStackTrace();
			// }
			//
			// BitmapDownloader task = new BitmapDownloader(imgParent, 1);
			// task.execute(parameters);
			String url = Global.baseUrl + "?act=image&ur="
					+ Global.username + "&tkn=" + Global.token + "&im="
					+ msg.photo;
			Global.loadPhoto(this, url, imgParent);
		}
		
		System.out.println("I am after loading photo");

		createTableReplies();

		// txtReply = (EditText) findViewById(R.id.txt_reply);
		// btnReply = (Button) findViewById(R.id.btn_reply);
	}

	private void createTableReplies() {
		LinkedList<Object> details = XMLHandler.detailMsgList;
		for (int i = 1; i < details.size() - 1; i++) {
			final ReplyNode replies = (ReplyNode) details.get(i);
			TableRow row = (TableRow) LayoutInflater.from(this).inflate(
					R.layout.row_showthread, null, false);
			row.setId(i);

			ImageView imgUser = (ImageView) row
					.findViewById(R.id.img_showthread_user);
			TextView lblReply = (TextView) row
					.findViewById(R.id.lbl_showthread_userreply);
			TextView lblAuthor = (TextView) row
					.findViewById(R.id.lbl_showthread_replyauthor);

			System.out.println("this is the photo id: " + replies.photo);
			if (!replies.photo.equals("")) {
				String url = Global.baseUrl + "?act=image&ur="
						+ Global.username + "&tkn=" + Global.token + "&im="
						+ replies.photo;
				Global.loadPhoto(this, url, imgUser);
			}

			lblReply.setText(replies.message);
			lblAuthor.setText("by " + replies.author + " on "
					+ Global.formatDate(replies.timestamp));

			ImageView btnReplyAttachment = (ImageView) row
					.findViewById(R.id.img_showthread_replyattachment);
			ImageView btnReplyUserLocation = (ImageView) row
					.findViewById(R.id.img_showthread_replylocation);

			if (replies.icon.equals("")) {
				btnReplyAttachment.setVisibility(View.GONE);
			}
			btnReplyUserLocation.setVisibility(View.GONE);

			// btnReplyAttachment.setOnClickListener(new View.OnClickListener()
			// {
			//
			// @Override
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// if (!replies.icon.equals("")) {
			// TODO Auto-generated method stub
			// viewAttachment(replies.icon);
			// }
			// }
			// });

			btnReplyUserLocation.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					System.out.println("User location clicked");
				}
			});

			tblReplies.addView(row);
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		// btnAttachment.setOnClickListener(new View.OnClickListener() {
		//
		// @Override
		// public void onClick(View arg0) {
		// ThreadNode thread = (ThreadNode)
		// XMLHandler.detailsList.get(XMLHandler.detailsList.size() - 1);
		// if (!thread.image.equals("")) {
		// // TODO Auto-generated method stub
		// viewAttachment(thread.image);
		// }
		// }
		// });
		//
		// btnAuthorLocation.setOnClickListener(new View.OnClickListener() {
		//
		// @Override
		// public void onClick(View arg0) {
		// // TODO Auto-generated method stub
		// System.out.println("location clicked");
		// }
		// });

		btnReply.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (txtReply.getText().length() > 0) {
					MultipartEntity parameters = new MultipartEntity();

					try {
						parameters.addPart("act", new StringBody("replyMsg"));
						parameters.addPart("ur",
								new StringBody(Global.username));
						parameters.addPart("tkn", new StringBody(Global.token));
						parameters.addPart("id", new StringBody(id));
						parameters.addPart("msg", new StringBody(txtReply
								.getText().toString()));
					} catch (Exception e) {
						e.printStackTrace();
					}

					Toast toast = Toast.makeText(getParent(),
							"Sending Message...", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();

					new ReplyMessageTask().execute(parameters);
				}
			}
		});
	}

	@SuppressWarnings("unused")
	private void viewAttachment(String imageId) {
		Intent viewAttachment = new Intent(getParent(), ViewAttachment.class);
		viewAttachment.putExtra("imageId", imageId);
		// Replace the view of this ActivityGroup
		// TabMain.group.replaceView(replyThread, "ReplyThread");
		if (getParent() instanceof ActivityGroupMain) {
			ActivityGroupMain parentActivity = (ActivityGroupMain) getParent();
			parentActivity.startChildActivity("ViewAttachment", viewAttachment);
		} else {
			ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
			parentActivity.startChildActivity("ViewAttachment", viewAttachment);
		}
	}

	// private void postReply() {
	// if (txtReply.getText().length() > 0) {
	// replyProgress.setMessage("Posting reply..");
	// replyProgress.setCancelable(false);
	// replyProgress.show();
	// MultipartEntity parameters = new MultipartEntity();
	//
	// try {
	// parameters.addPart("act", new StringBody("reply"));
	// parameters.addPart("ur", new StringBody(Global.username));
	// parameters.addPart("tkn", new StringBody(Global.token));
	// parameters.addPart("id", new StringBody(id));
	// parameters.addPart("msg", new StringBody(txtReply.getText()
	// .toString()));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// new PostReplyTask().execute(parameters);
	// }
	// }

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		System.gc();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
		parentActivity.onBackPressed();
	}

	private class ReplyMessageTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {
		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.personalMsgList = null;
			con.requestXML("replyMsg");
			return XMLHandler.personalMsgList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					Toast toast = Toast.makeText(getParent(), "Message sent",
							Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					txtReply.setText("");
					MultipartEntity parameters = new MultipartEntity();

					try {
						parameters.addPart("act", new StringBody(
								"getMsgDetails"));
						parameters.addPart("ur",
								new StringBody(Global.username));
						parameters.addPart("tkn", new StringBody(Global.token));
						parameters.addPart("id", new StringBody(id));
					} catch (Exception e) {
						e.printStackTrace();
					}

					new ShowMessageTask().execute(parameters);
				} else {
					Global.createAlert(Global.team, result.get(2).toString(),
							"Login");
				}
			} catch (Exception e) {
				Global.createAlert(
						Global.team,
						"Could not connect to the remote server. Please check your connection Settings.",
						"OK");
				e.printStackTrace();
			}
		}
	}

	private class ShowMessageTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {

		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.detailMsgList = null;
			con.requestXML("getMsgDetails");
			return XMLHandler.detailMsgList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					tblReplies.removeAllViews();
					createTableReplies();
				} else {
					Global.createAlert(getParent(), result.get(2).toString(),
							"Login");
				}
			} catch (Exception e) {
				Global.createAlert(
						getParent(),
						"Could not connect to the remote server. Please check your connection Settings.",
						"OK");
			}
		}
	}

	// private class PostReplyTask extends
	// AsyncTask<MultipartEntity, Void, LinkedList<Object>> {
	//
	// protected LinkedList<Object> doInBackground(MultipartEntity... param) {
	// ServerConnection con = new ServerConnection(param[0]);
	// con.requestXML("reply");
	// return XMLHandler.detailsList;
	// }
	//
	// protected void onPostExecute(LinkedList<Object> result) {
	// try {
	// if (result.get(0).equals("success")) {
	// replyProgress.hide();
	// tblReplies.removeAllViews();
	// createTableDetailThread();
	// txtReply.setText("");
	// Global.update = true;
	// } else {
	// replyProgress.dismiss();
	// Global.createAlert(showThread, result.get(2).toString(),
	// "Login");
	// }
	// } catch (Exception e) {
	// replyProgress.hide();
	// Global.createAlert(
	// showThread,
	// "Could not connect to the remote server. Please check your connection Settings.",
	// "OK");
	// }
	// }
	// }
}

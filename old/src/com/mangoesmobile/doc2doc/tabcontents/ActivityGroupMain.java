package com.mangoesmobile.doc2doc.tabcontents;

import com.mangoesmobile.doc2doc.TabMain;
import com.mangoesmobile.doc2doc.global.Global;

import android.content.Intent;
import android.os.Bundle;

public class ActivityGroupMain extends TabMain {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Global.main = this;
		
		startChildActivity("MainThread", new Intent(this, MainThread.class));
	}
}

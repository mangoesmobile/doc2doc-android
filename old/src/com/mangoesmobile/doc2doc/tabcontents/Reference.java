package com.mangoesmobile.doc2doc.tabcontents;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.global.Global;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.Toast;

public class Reference extends Activity {

	GoogleAnalyticsTracker tracker;

	private ProgressDialog webProgress;
	//
	// SeekBar seekRate;
	// Button btnLogout;
	// TextView lblRate;
	WebView webview;
	ImageButton btnHome;
	ImageButton btnSelectText;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reference);
		
		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.start(Global.UA, 20, this);
		tracker.trackPageView("/Reference");

		Global.reference = this;

		webProgress = new ProgressDialog(this);
		webProgress.setMessage("Loading, please wait...");

		webview = (WebView) findViewById(R.id.web_reference);
		btnHome = (ImageButton) findViewById(R.id.btn_reference_home);
		btnSelectText = (ImageButton) findViewById(R.id.btn_reference_selectandcopy);
		
		webview.getSettings().setJavaScriptEnabled(true);
		webview.getSettings().setBuiltInZoomControls(true);
		webview.loadUrl("file:///android_asset/html/references/menu.html");

		webview.setWebViewClient(new ReferenceWebViewClient());
	}

	private void emulateShiftHeld(WebView view) {
		try {
			KeyEvent shiftPressEvent = new KeyEvent(0, 0, KeyEvent.ACTION_DOWN,
					KeyEvent.KEYCODE_SHIFT_LEFT, 0, 0);
			shiftPressEvent.dispatch(view);
			Toast.makeText(this, "Select text now", Toast.LENGTH_SHORT).show();
		} catch (Exception e) {
			Log.e("dd", "Exception in emulateShiftHeld()", e);
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		
		btnHome.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				webview.loadUrl("file:///android_asset/html/references/menu.html");
			}
		});
		
		btnSelectText.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				tracker.trackEvent("click", "Button", "Copy Text From Reference", 0);
				emulateShiftHeld(webview);
			}
		});
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {
			webview.goBack();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		tracker.stop();
		webProgress.dismiss();
	}

	@Override
	public void onBackPressed() {

	}

	private class ReferenceWebViewClient extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			tracker.trackEvent("click", "Button", "View Reference", 0);
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			webProgress.show();
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			webProgress.hide();
		}
	}

}

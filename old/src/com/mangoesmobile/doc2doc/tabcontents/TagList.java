package com.mangoesmobile.doc2doc.tabcontents;

import java.util.ArrayList;
import java.util.LinkedList;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.global.Global;
import com.mangoesmobile.doc2doc.util.DBManager;
import com.mangoesmobile.doc2doc.util.DoctorNode;
import com.mangoesmobile.doc2doc.util.ServerConnection;
import com.mangoesmobile.doc2doc.util.XMLHandler;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class TagList extends Activity {

	GoogleAnalyticsTracker tracker;

	TableLayout tblDocList;

	RelativeLayout relSelectAll;
	CheckBox chkSetChecked;

	LinearLayout layoutDone;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.taglist);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.start(Global.UA, 20, this);
		tracker.trackPageView("/TagList");

		tblDocList = (TableLayout) findViewById(R.id.tbl_taglist);

		relSelectAll = (RelativeLayout) findViewById(R.id.rel_selectall);
		chkSetChecked = (CheckBox) findViewById(R.id.chk_selectall);

		layoutDone = (LinearLayout) findViewById(R.id.layout_back);		

		DBManager manager = new DBManager(getParent(), Global.TABLE_TEAM,
				Global.FIELDS_TEAM_WITH_ID);
		manager.open();
		Cursor cache = manager.fetchAllFromDB();
		if (cache.getCount() == 0) {
			Toast toast = Toast.makeText(getParent(), "Loading Team...",
					Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			
			MultipartEntity parameters = new MultipartEntity();
			try {
				parameters.addPart("act", new StringBody("getUsers"));
				parameters.addPart("ur", new StringBody(Global.username));
				parameters.addPart("tkn", new StringBody(Global.token));
			} catch (Exception e) {
				e.printStackTrace();
			}

			new TagListTask().execute(parameters);

			cache.close();
		} else {
			LinkedList<Object> team = new LinkedList<Object>();
			cache.moveToFirst();
			for (int i = 0; i < cache.getCount(); i++) {
				DoctorNode docinfo = new DoctorNode(cache.getString(cache
						.getColumnIndex("user_id")), cache.getString(cache
						.getColumnIndex("title")), cache.getString(cache
						.getColumnIndex("lastname")), cache.getString(cache
						.getColumnIndex("firstname")), cache.getString(cache
						.getColumnIndex("display_name")), cache.getString(cache
						.getColumnIndex("specialization")),
						cache.getString(cache.getColumnIndex("location")),
						cache.getString(cache.getColumnIndex("email")),
						cache.getString(cache.getColumnIndex("phone")),
						cache.getString(cache.getColumnIndex("status")),
						cache.getString(cache.getColumnIndex("photo")));

				team.add(docinfo);
				cache.moveToNext();
			}
			cache.close();
			createDocList(team, 0);
		}
		manager.close();

	}

	@Override
	public void onStart() {
		super.onStart();

		relSelectAll.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (chkSetChecked.isChecked()) {
					Global.taggedId = new ArrayList<String>();
					arg0.setBackgroundResource(R.drawable.bg_thread);
					chkSetChecked.setChecked(false);
					for (int i = 0; i < tblDocList.getChildCount(); i++) {
						TableRow row = (TableRow) tblDocList.getChildAt(i);
						CheckBox chkIsChecked = (CheckBox) row
								.findViewById(R.id.chk_teamlist_selected);
						chkIsChecked.setChecked(false);
						row.setBackgroundResource(R.drawable.bg_thread);
					}
				} else {
					tracker.trackEvent("click", "Button",
							"All Selected To Notify", 0);
					Global.taggedId = new ArrayList<String>();
					arg0.setBackgroundResource(R.drawable.bg_thread_down);
					chkSetChecked.setChecked(true);
					for (int i = 0; i < tblDocList.getChildCount(); i++) {
						TableRow row = (TableRow) tblDocList.getChildAt(i);
						CheckBox chkIsChecked = (CheckBox) row
								.findViewById(R.id.chk_teamlist_selected);
						chkIsChecked.setChecked(true);
						row.setBackgroundResource(R.drawable.bg_thread_down);
						Global.taggedId.add("" + row.getId());
					}
				}
			}
		});

		layoutDone.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (getParent() instanceof ActivityGroupMain) {
					ActivityGroupMain parentActivity = (ActivityGroupMain) getParent();
					parentActivity.onBackPressed();
				} else {
					ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
					parentActivity.onBackPressed();
				}
			}
		});
	}

	private void createDocList(LinkedList<Object> doclist, int startingIndex) {
		for (int i = startingIndex; i < doclist.size(); i++) {
			DoctorNode docinfo = (DoctorNode) doclist.get(i);
			final String id = docinfo.id;
			TableRow row = (TableRow) LayoutInflater.from(this).inflate(
					R.layout.row_teamlist, null, false);
			row.setId(Integer.parseInt(docinfo.id));

			ImageView imgUserImage = (ImageView) row
					.findViewById(R.id.img_teamlist_user);
			ImageView imgStatus = (ImageView) row
					.findViewById(R.id.img_teamlist_userstatus);
			TextView lblName = (TextView) row
					.findViewById(R.id.lbl_teamlist_name);
			TextView lblSpecialization = (TextView) row
					.findViewById(R.id.lbl_teamlist_specialization);
			TextView lblLocation = (TextView) row
					.findViewById(R.id.lbl_teamlist_location);
			TextView lblStatus = (TextView) row
					.findViewById(R.id.lbl_teamlist_userstatus);
			CheckBox chkSelected = (CheckBox) row
					.findViewById(R.id.chk_teamlist_selected);
			ImageView btnPing = (ImageView) row
					.findViewById(R.id.img_teamlist_ping);
			ImageView btnLocation = (ImageView) row
					.findViewById(R.id.img_teamlist_message);

			btnPing.setVisibility(View.GONE);
			btnLocation.setVisibility(View.GONE);
			chkSelected.setVisibility(View.VISIBLE);

			if (!docinfo.photo.equals("")) {
				String url = Global.baseUrl + "?act=image&ur="
						+ Global.username + "&tkn=" + Global.token + "&im="
						+ docinfo.photo;
				Global.loadPhoto(this, url, imgUserImage);
			}

			int icon;
			if (docinfo.status.equalsIgnoreCase("ONLINE")) {
				icon = R.drawable.bg_photo_online;
				lblStatus.setTextColor(Color.rgb(0, 102, 153));
			} else if (docinfo.status.equalsIgnoreCase("ON CALL")) {
				icon = R.drawable.bg_photo_oncall;
				docinfo.status = "*" + docinfo.status + "*";
				lblStatus.setTextColor(Color.rgb(101, 176, 73));
			} else {
				icon = R.drawable.bg_photo_offline;
				lblStatus.setTextColor(Color.DKGRAY);
			}
			imgStatus.setImageResource(icon);
			lblStatus.setText(docinfo.status);

			lblName.setText(docinfo.firstname + " " + docinfo.lastname);
			lblSpecialization.setText(docinfo.specialization);
			lblLocation.setText(docinfo.location);

			for (int p = 0; p < Global.taggedId.size(); p++) {
				if (Global.taggedId.get(p).equalsIgnoreCase(id)) {
					row.setBackgroundResource(R.drawable.bg_thread_down);
					chkSelected.setChecked(true);
				}
			}

			row.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					CheckBox chkRowSelected = (CheckBox) arg0
							.findViewById(R.id.chk_teamlist_selected);
					if (!chkRowSelected.isChecked()) {
						tracker.trackEvent("click", "TableRow",
								"Selected Individually To Notify", 36);
						arg0.setBackgroundResource(R.drawable.bg_thread_down);
						chkRowSelected.setChecked(true);
						tagDoc(id);
					} else {
						arg0.setBackgroundResource(R.drawable.bg_thread);
						chkRowSelected.setChecked(false);
						removeDoc(id);
					}
				}
			});

			tblDocList.addView(row);
		}
	}

	private void tagDoc(String id) {
		Global.taggedId.add(id);
	}

	private void removeDoc(String id) {
		for (int i = 0; i < Global.taggedId.size(); i++) {
			if (Global.taggedId.get(i).equals(id)) {
				Global.taggedId.remove(i);
				break;
			}
		}
	}

	public void cacheData(LinkedList<Object> data) {
		System.out.println("caching data");
		DBManager manager = new DBManager(getParent(), Global.TABLE_TEAM,
				Global.FIELDS_TEAM_WITH_ID);
		manager.open();
		manager.clearDB();
		for (int i = 1; i < data.size(); i++) {
			DoctorNode docinfo = (DoctorNode) data.get(i);
			manager.insertToDB(Global.FIELDS_TEAM, new String[] { docinfo.id,
					docinfo.title, docinfo.lastname, docinfo.firstname,
					docinfo.displayName, docinfo.specialization,
					docinfo.location, docinfo.email, docinfo.phone,
					docinfo.status, docinfo.photo });
		}
		manager.close();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		tracker.stop();
	}

	@Override
	public void onBackPressed() {
		if (getParent() instanceof ActivityGroupMain) {
			ActivityGroupMain parentActivity = (ActivityGroupMain) getParent();
			parentActivity.onBackPressed();
		} else {
			ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
			parentActivity.onBackPressed();
		}
	}

	private class TagListTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {
		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.teamList = null;
			con.requestXML("getUsers");
			return XMLHandler.teamList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					createDocList(result, 1);
					cacheData(result);
				} else {
					Global.createAlert(getParent(), result.get(2).toString(),
							"Login");
				}
			} catch (Exception e) {
				Toast.makeText(getParent(), "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
		}
	}
}

package com.mangoesmobile.doc2doc.tabcontents;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.global.Global;
import com.mangoesmobile.doc2doc.util.LoginInfoNode;
import com.mangoesmobile.doc2doc.util.DBManager;
import com.mangoesmobile.doc2doc.util.ServerConnection;
import com.mangoesmobile.doc2doc.util.XMLHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class PostThread extends Activity {

	GoogleAnalyticsTracker tracker;

	// private static final int CAMERA_PIC_REQUEST = 226;
	// private static final int SELECT_PICTURE = 425;
	// private static final int GET_TAGS = 824;

	private ProgressDialog postThreadProgress;

	Activity postThread;

	ImageView imgUser;

	EditText txtTitle;
	EditText txtDetails;

	TextView lblNoOfAttachment;
	TextView lblNoOfPeople;

	ImageView btnAttach;
	ImageView btnNotify;

	ImageView btnPost;
	ImageView btnSave;

	Bitmap bmp = null;

	LoginInfoNode loginInfo;

	long id = -1;
	int index = -1;

	String attach = "";
	String notify = "";
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.postthread);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.start(Global.UA, 20, this);
		tracker.trackPageView("/PostThread");

		imgUser = (ImageView) findViewById(R.id.img_postthread_user);

		txtTitle = (EditText) findViewById(R.id.txt_postthread_title);
		txtDetails = (EditText) findViewById(R.id.txt_postthread_details);

		lblNoOfAttachment = (TextView) findViewById(R.id.lbl_postthread_noofattachment);
		lblNoOfPeople = (TextView) findViewById(R.id.lbl_postthread_noofpeople);

		btnAttach = (ImageView) findViewById(R.id.img_postthread_attach);
		btnNotify = (ImageView) findViewById(R.id.img_postthread_notify);

		btnPost = (ImageView) findViewById(R.id.img_postthread_post);
		btnSave = (ImageView) findViewById(R.id.img_postthread_save);

		if (getParent() instanceof ActivityGroupMain
				|| getParent() instanceof ActivityGroupMessages) {
			loginInfo = (LoginInfoNode) XMLHandler.loginInfo.get(0);
			postThread = getParent();
		} else {
			setTitle("Doc2Doc - Offline Mode");
			loginInfo = new LoginInfoNode("", "", "", "", "anonymous",
					"OFFLINE", "");
			postThread = this;
			btnNotify.setVisibility(View.GONE);
			btnPost.setVisibility(View.GONE);
		}

		if (Global.imageDrawable != null) {
			imgUser.setImageDrawable(Global.imageDrawable);
		}

		index = getIntent().getIntExtra("index", -1);
		if (index != -1) {
			if (!getIntent().getStringExtra("id").equals("")) {
				id = Integer.parseInt(getIntent().getStringExtra("id"));
			}
			txtTitle.setText(getIntent().getStringExtra("title"));
			txtDetails.setText(getIntent().getStringExtra("details"));
			if (!getIntent().getStringExtra("attach").equals("")) {
				Global.imageUri = Uri.parse(getIntent()
						.getStringExtra("attach"));
			}
			if (!getIntent().getStringExtra("notify").equals("")) {
				String[] notify = getIntent().getStringExtra("notify").split(
						",");
				for (int i = 0; i < notify.length; i++) {
					Global.taggedId.add(notify[i]);
				}
			}
		}

	}

	@Override
	public void onStart() {
		super.onStart();

		btnAttach.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				tracker.trackEvent("click", "Button", "Open Attachment Page", 0);
				Intent attach = new Intent(postThread, Attach.class);
				attach.putExtra("title", txtTitle.getText().toString());
				attach.putExtra("author", "by you");
				if (getParent() instanceof ActivityGroupMain) {
					ActivityGroupMain parentActivity = (ActivityGroupMain) getParent();
					parentActivity.startChildActivity("Attach", attach);
				} else if (getParent() instanceof ActivityGroupMessages) {
					ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
					parentActivity.startChildActivity("Attach", attach);
				} else {
					startActivity(attach);
				}
			}
		});

		btnNotify.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tracker.trackEvent("click", "Button", "Open Notify Page", 0);
				Intent notify = new Intent(postThread, TagList.class);
				if (getParent() instanceof ActivityGroupMain) {
					ActivityGroupMain parentActivity = (ActivityGroupMain) getParent();
					parentActivity.startChildActivity("Notify", notify);
				} else if (getParent() instanceof ActivityGroupMessages) {
					ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
					parentActivity.startChildActivity("Notify", notify);
				} else {
					startActivity(notify);
				}
			}
		});

		btnPost.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tracker.trackEvent("click", "Button", "Send Post", 0);
				if (txtTitle.getText().length() > 0
						&& txtDetails.getText().length() > 0) {
					if (Global.imageUri != null) {
						attach = Global.imageUri.toString();
					}
					for (int i = 0; i < Global.taggedId.size(); i++) {
						notify += Global.taggedId.get(i);
						if (i != Global.taggedId.size() - 1) {
							notify += ",";
						}
					}
//					DBManager manager = new DBManager(postThread);
//					manager.open();
//					manager.addToOutbox(txtTitle.getText().toString(),
//							txtDetails.getText().toString(), attach, notify);
//					Cursor tempCursor = manager.fetchAllFromDB();
//					tempCursor.moveToLast();
//					tempId = tempCursor.getLong(tempCursor.getColumnIndex("_id"));
//					tempCursor.close();
//					manager.close();
					if (Messages.running) {
						Global.updateOutbox = true;
					}
				}
				try {
					sendPost();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		btnSave.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				tracker.trackEvent("click", "Button", "Save To Outbox", 0);
				if (txtTitle.getText().length() > 0
						&& txtDetails.getText().length() > 0) {
					String attach = "";
					if (Global.imageUri != null) {
						attach = Global.imageUri.toString();
					}
					String notify = "";
					for (int i = 0; i < Global.taggedId.size(); i++) {
						notify += Global.taggedId.get(i);
						if (i != Global.taggedId.size() - 1) {
							notify += ",";
						}
					}
					DBManager manager = new DBManager(postThread, Global.TABLE_DRAFT, Global.FIELDS_DRAFT_WITH_ID);
					manager.open();
					String[] key = Global.FIELDS_DRAFT;
					if (id == -1) {
						manager.insertToDB(key, new String[] {txtTitle.getText().toString(),
								txtDetails.getText().toString(), attach, notify});
					} else {
						manager.updateDB(id, key, new String[] {txtTitle.getText().toString(),
								txtDetails.getText().toString(), attach, notify});
					}
					manager.close();
					if (Messages.running) {
						Global.updateOutbox = true;
					}
					Toast toast = Toast.makeText(postThread, "Saved to Drafts",
							Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				} else {
					Global.createAlert(postThread,
							"Please enter title and(or) detail.", "OK");
				}
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		if (Global.taggedId.size() > 0) {
			lblNoOfPeople.setText("(" + Global.taggedId.size() + ")");
		} else {
			lblNoOfPeople.setText("");
		}
		if (Global.imageUri != null) {
			lblNoOfAttachment.setText("(1)");
		} else {
			lblNoOfAttachment.setText("");
		}
	}

	private void sendPost() throws FileNotFoundException, IOException {
		if (txtTitle.getText().length() > 0
				&& txtDetails.getText().length() > 0) {
			tracker.trackEvent("click", "Button", "Submit Post", 77);
			postThreadProgress = new ProgressDialog(getParent());
			postThreadProgress.setMessage("Posting Thread..");
			postThreadProgress.setCancelable(false);
			postThreadProgress.show();
			String notify = "";
			if (Global.taggedId != null) {
				for (int i = 0; i < Global.taggedId.size(); i++) {
					notify += Global.taggedId.get(i);
					if (i != Global.taggedId.size() - 1) {
						notify += ",";
					}
				}
			}
			MultipartEntity parameters = new MultipartEntity();
			byte[] bitmapdata = null;
			if (Global.imageUri != null) {
				bmp = MediaStore.Images.Media.getBitmap(getContentResolver(),
						Global.imageUri);
			}
			if (bmp != null) {
				System.out.println("bmp is not null");
				int bitmapWidth = bmp.getWidth(), bitmapHeight = bmp
						.getHeight();
				int dstWidth = 0, dstHeight = 0;
				if (bitmapWidth <= bitmapHeight) {
					dstWidth = 300;
					dstHeight = 400;
				} else {
					dstWidth = 400;
					dstHeight = 300;
				}
				while (bitmapWidth / 2 > dstWidth
						|| bitmapHeight / 2 > dstHeight) {
					bitmapWidth /= 2;
					bitmapHeight /= 2;
				}
				bmp = Bitmap.createScaledBitmap(bmp, bitmapWidth, bitmapHeight,
						true);
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bmp.compress(Bitmap.CompressFormat.JPEG, 50, stream);
				bitmapdata = stream.toByteArray();
			}
			try {

				parameters.addPart("act", new StringBody("postThread"));
				parameters.addPart("ur", new StringBody(Global.username));
				parameters.addPart("tkn", new StringBody(Global.token));
				parameters.addPart("typ", new StringBody("1"));
				parameters.addPart("tle", new StringBody(txtTitle.getText()
						.toString()));
				parameters.addPart("msg", new StringBody(txtDetails.getText()
						.toString()));
				if (!notify.equals("")) {
					parameters.addPart("nfy", new StringBody(notify));
				}
				if (bitmapdata != null) {
					parameters.addPart("img", new InputStreamBody(
							new ByteArrayInputStream(bitmapdata), "image"));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			new SendPostTask().execute(parameters);
		} else {
			Global.createAlert(postThread,
					"Please enter title and(or) detail.", "OK");
		}
	}

	private class SendPostTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {

		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.detailsList = null;
			con.requestXML("postThread");
			return XMLHandler.detailsList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					postThreadProgress.dismiss();
					Global.imageUri = null;
					Global.taggedId = new ArrayList<String>();
					if (id != -1) {
						DBManager manager = new DBManager(postThread, Global.TABLE_DRAFT, Global.FIELDS_DRAFT_WITH_ID);
						manager.open();
						manager.deleteFromDB(id);
						manager.close();
						Global.msgs[0][3].remove(index);
						Global.updateMessageCenter = true;
					}
					backToMain();
				} else {
					postThreadProgress.dismiss();
					Global.createAlert(postThread, result.get(2).toString(),
							"Login");
				}
				attach = "";
				notify = "";
			} catch (Exception e) {
				postThreadProgress.hide();
				DBManager manager = new DBManager(postThread, Global.TABLE_DRAFT, Global.FIELDS_DRAFT_WITH_ID);
				manager.open();
				manager.insertToDB(Global.FIELDS_DRAFT, new String[] {txtTitle.getText().toString(),
						txtDetails.getText().toString(), attach, notify});
				manager.close();
				attach = "";
				notify = "";
				Global.createAlert(
						postThread,
						"Could not connect to the remote server. Please check your connection Settings.",
						"OK");
				e.printStackTrace();
			}
		}
	}

	private void backToMain() {
		Global.update = true;
		onBackPressed();
		if (getParent() instanceof ActivityGroupMessages) {
			onBackPressed();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		System.out.println("postthread destroyed");
		Global.imageUri = null;
		Global.taggedId = new ArrayList<String>();
		id = -1;
		index = -1;
		tracker.stop();
	}

	@Override
	public void onBackPressed() {
		if (getParent() instanceof ActivityGroupMain) {
			ActivityGroupMain parentActivity = (ActivityGroupMain) getParent();
			parentActivity.onBackPressed();
		} else if (getParent() instanceof ActivityGroupMessages) {
			ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
			parentActivity.onBackPressed();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		System.gc();
	}
}

package com.mangoesmobile.doc2doc.tabcontents;

import com.mangoesmobile.doc2doc.TabMessages;
import com.mangoesmobile.doc2doc.global.Global;

import android.content.Intent;
import android.os.Bundle;

public class ActivityGroupMessages extends TabMessages {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Global.messages = this;
		
		startChildActivity("Messages", new Intent(this, Messages.class));
	}
}

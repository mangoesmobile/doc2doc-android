package com.mangoesmobile.doc2doc.tabcontents;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.global.Global;
import com.mangoesmobile.doc2doc.util.ServerConnection;
import com.mangoesmobile.doc2doc.util.XMLHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class ReplyThread extends Activity {

	GoogleAnalyticsTracker tracker;

	private ProgressDialog postThreadProgress;

	ImageView imgUser;
	TextView lblTitle;
	TextView lblAuthor;
	EditText txtReply;
	ImageView btnAttach;
	ImageView btnNotify;
	TextView lblNoOfAttachment;
	TextView lblNoOfPeople;
	ImageView btnReply;

	String title;
	String author;
	String id;

	Bitmap bmp = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.replythread);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.start(Global.UA, 20, this);
		tracker.trackPageView("/ReplyThread");

		imgUser = (ImageView) findViewById(R.id.img_replythread_user);
		lblTitle = (TextView) findViewById(R.id.lbl_replythread_title);
		lblAuthor = (TextView) findViewById(R.id.lbl_replythread_author);
		txtReply = (EditText) findViewById(R.id.txt_replythread_reply);
		btnAttach = (ImageView) findViewById(R.id.img_replythread_attach);
		btnNotify = (ImageView) findViewById(R.id.img_replythread_notify);
		lblNoOfAttachment = (TextView) findViewById(R.id.lbl_replythread_noofattachment);
		lblNoOfPeople = (TextView) findViewById(R.id.lbl_replythread_noofpeople);
		btnReply = (ImageView) findViewById(R.id.img_replythread_reply);

		title = "RE: " + getIntent().getStringExtra("title");
		author = getIntent().getStringExtra("author");

		lblTitle.setText(title);
		lblAuthor.setText(author);
		id = getIntent().getStringExtra("id");
	}

	@Override
	public void onStart() {
		super.onStart();

		btnAttach.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				tracker.trackEvent("click", "Button", "Open Attachment Page", 0);
				Intent attach = new Intent(getParent(), Attach.class);
				attach.putExtra("title", title);
				attach.putExtra("author", author);
				if (getParent() instanceof ActivityGroupMain) {
					ActivityGroupMain parentActivity = (ActivityGroupMain) getParent();
					parentActivity.startChildActivity("Attach", attach);
				} else {
					ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
					parentActivity.startChildActivity("Attach", attach);
				}
			}
		});

		btnNotify.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tracker.trackEvent("click", "Button", "Open Notify Page", 0);
				Intent notify = new Intent(getParent(), TagList.class);
				if (getParent() instanceof ActivityGroupMain) {
					ActivityGroupMain parentActivity = (ActivityGroupMain) getParent();
					parentActivity.startChildActivity("Notify", notify);
				} else {
					ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
					parentActivity.startChildActivity("Notify", notify);
				}
			}
		});

		btnReply.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tracker.trackEvent("click", "Button", "Reply", 737);
				try {
					reply();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		if (Global.taggedId.size() > 0) {
			lblNoOfPeople.setText("(" + Global.taggedId.size() + ")");
		} else {
			lblNoOfPeople.setText("");
		}
		if (Global.imageUri != null) {
			lblNoOfAttachment.setText("(1)");
		} else {
			lblNoOfAttachment.setText("");
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		tracker.stop();
		Global.imageUri = null;
	}

	@Override
	public void onBackPressed() {
		if (getParent() instanceof ActivityGroupMain) {
			ActivityGroupMain parentActivity = (ActivityGroupMain) getParent();
			parentActivity.onBackPressed();
		} else {
			ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
			parentActivity.onBackPressed();
		}
	}

	private void backToMain() {
		Global.update = true;
		onBackPressed();
	}

	private void reply() throws FileNotFoundException, IOException {
		if (txtReply.getText().length() > 0) {
			postThreadProgress = new ProgressDialog(getParent());
			postThreadProgress.setMessage("Posting Reply..");
			postThreadProgress.setCancelable(false);
			postThreadProgress.show();
			String notify = "";
			if (Global.taggedId != null) {
				for (int i = 0; i < Global.taggedId.size(); i++) {
					notify += Global.taggedId.get(i);
					if (i != Global.taggedId.size() - 1) {
						notify += ",";
					}
				}
			}
			MultipartEntity parameters = new MultipartEntity();
			byte[] bitmapdata = null;
			if (Global.imageUri != null) {
				bmp = MediaStore.Images.Media.getBitmap(
						this.getContentResolver(), Global.imageUri);
			}
			if (bmp != null) {
				System.out.println("bmp is not null");
				int bitmapWidth = bmp.getWidth(), bitmapHeight = bmp
						.getHeight();
				int dstWidth = 0, dstHeight = 0;
				if (bitmapWidth <= bitmapHeight) {
					dstWidth = 360;
					dstHeight = 480;
				} else {
					dstWidth = 480;
					dstHeight = 360;
				}
				while (bitmapWidth / 2 > dstWidth
						|| bitmapHeight / 2 > dstHeight) {
					bitmapWidth /= 2;
					bitmapHeight /= 2;
				}
				bmp = Bitmap.createScaledBitmap(bmp, bitmapWidth, bitmapHeight,
						true);
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bmp.compress(Bitmap.CompressFormat.JPEG, 75, stream);
				bitmapdata = stream.toByteArray();
			}
			try {
				parameters.addPart("act", new StringBody("reply"));
				parameters.addPart("ur", new StringBody(Global.username));
				parameters.addPart("tkn", new StringBody(Global.token));
				parameters.addPart("id", new StringBody(id));
				parameters.addPart("msg", new StringBody(txtReply.getText()
						.toString()));
				if (!notify.equals("")) {
					parameters.addPart("nfy", new StringBody(notify));
				}
				if (bitmapdata != null) {
					parameters.addPart("img", new InputStreamBody(
							new ByteArrayInputStream(bitmapdata), "image"));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			new SendPostTask().execute(parameters);
		} else {
			Global.createAlert(getParent(), "Please enter a reply message.",
					"OK");
		}
	}

	private class SendPostTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {

		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.detailsList = null;
			con.requestXML("reply");
			return XMLHandler.detailsList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					postThreadProgress.dismiss();
					backToMain();
					Global.imageUri = null;
					Global.taggedId = new ArrayList<String>();
				} else {
					postThreadProgress.dismiss();
					Global.createAlert(getParent(), result.get(2).toString(),
							"Login");
				}
			} catch (Exception e) {
				postThreadProgress.hide();
				Global.createAlert(
						getParent(),
						"Could not connect to the remote server. Please check your connection Settings.",
						"OK");
			}
		}
	}
}

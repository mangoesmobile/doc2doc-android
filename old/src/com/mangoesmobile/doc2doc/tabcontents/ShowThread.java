package com.mangoesmobile.doc2doc.tabcontents;

import java.util.LinkedList;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.global.Global;
import com.mangoesmobile.doc2doc.util.ReplyNode;
import com.mangoesmobile.doc2doc.util.ServerConnection;
import com.mangoesmobile.doc2doc.util.ThreadNode;
import com.mangoesmobile.doc2doc.util.XMLHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TableRow;

public class ShowThread extends Activity {

	GoogleAnalyticsTracker tracker;

	private ProgressDialog replyProgress;

	Activity showThread;

	TableLayout tblReplies;
	ImageView imgParent;
	TextView lblTitle;
	TextView lblDetails;
	TextView lblAuthor;
	// TextView lblTimestamp;

	ImageView btnAttachment;
	ImageView btnAuthorLocation;
	ImageView btnReply;

	// EditText txtReply;
	// Button btnReply;

	private String title;
	private String author;
	private String id;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.showthread);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.start(Global.UA, 20, this);
		tracker.trackPageView("/ShowThread");

		showThread = this;

		tblReplies = (TableLayout) findViewById(R.id.tbl_showthread_reply);
		imgParent = (ImageView) findViewById(R.id.img_showthread_parentuser);
		lblTitle = (TextView) findViewById(R.id.lbl_showthread_title);
		lblDetails = (TextView) findViewById(R.id.lbl_showthread_detail);
		lblAuthor = (TextView) findViewById(R.id.lbl_showthread_author);
		// lblTimestamp = (TextView) findViewById(R.id.lbl_timeshowthread);
		btnAttachment = (ImageView) findViewById(R.id.img_showthread_attachment);
		btnAuthorLocation = (ImageView) findViewById(R.id.img_showthread_location);
		btnReply = (ImageView) findViewById(R.id.img_showthread_reply);

		LinkedList<Object> details = XMLHandler.detailsList;
		ThreadNode thread = (ThreadNode) details.get(details.size() - 1);
		title = thread.title;
		System.out.println(title);
		author = thread.author;
		id = thread.id;
		lblTitle.setText(title);
		lblDetails.setText(thread.details);
		lblAuthor.setText("by " + author + " on "
				+ Global.formatDate(thread.timestamp));

		if (!thread.photo.equals("")) {			
			String url = Global.baseUrl + "?act=image&ur="
					+ Global.username + "&tkn=" + Global.token + "&im="
					+ thread.photo;
			Global.loadPhoto(this, url, imgParent);
		}

		if (thread.image.equals("")) {
			btnAttachment.setVisibility(View.GONE);
		}

		createTableDetailThread();

		replyProgress = new ProgressDialog(getParent());
	}

	private void createTableDetailThread() {
		LinkedList<Object> details = XMLHandler.detailsList;
		for (int i = 1; i < details.size() - 1; i++) {
			final ReplyNode replies = (ReplyNode) details.get(i);
			TableRow row = (TableRow) LayoutInflater.from(this).inflate(
					R.layout.row_showthread, null, false);
			row.setId(i);

			ImageView imgUser = (ImageView) row
					.findViewById(R.id.img_showthread_user);
			TextView lblReply = (TextView) row
					.findViewById(R.id.lbl_showthread_userreply);
			TextView lblAuthor = (TextView) row
					.findViewById(R.id.lbl_showthread_replyauthor);

			System.out.println("this is the photo id: " + replies.photo);
			if (!replies.photo.equals("")) {				
				String url = Global.baseUrl + "?act=image&ur="
						+ Global.username + "&tkn=" + Global.token + "&im="
						+ replies.photo;
				Global.loadPhoto(this, url, imgUser);
			}

			lblReply.setText(replies.message);
			lblAuthor.setText("by " + replies.author + " on "
					+ Global.formatDate(replies.timestamp));

			ImageView btnReplyAttachment = (ImageView) row
					.findViewById(R.id.img_showthread_replyattachment);
			ImageView btnReplyUserLocation = (ImageView) row
					.findViewById(R.id.img_showthread_replylocation);

			if (replies.icon.equals("")) {
				btnReplyAttachment.setVisibility(View.GONE);
			}

			btnReplyAttachment.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (!replies.icon.equals("")) {
						// TODO Auto-generated method stub
						tracker.trackEvent("click", "Button", "Attachment Of Reply Viewed", 0);
						viewAttachment(replies.icon);
					}
				}
			});

			btnReplyUserLocation.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					System.out.println("User location clicked");
				}
			});

			tblReplies.addView(row);
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		btnAttachment.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				ThreadNode thread = (ThreadNode) XMLHandler.detailsList
						.get(XMLHandler.detailsList.size() - 1);
				if (!thread.image.equals("")) {
					// TODO Auto-generated method stub
					tracker.trackEvent("click", "Button", "Attachment Of Thread Viewed", 0);
					viewAttachment(thread.image);
				}
			}
		});

		btnAuthorLocation.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				System.out.println("location clicked");
			}
		});

		btnReply.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent replyThread = new Intent(getParent(), ReplyThread.class);
				replyThread.putExtra("title", title);
				replyThread.putExtra("author", author);
				replyThread.putExtra("id", id);
				// Replace the view of this ActivityGroup
				// TabMain.group.replaceView(replyThread, "ReplyThread");
				if (getParent() instanceof ActivityGroupMain) {
					ActivityGroupMain parentActivity = (ActivityGroupMain) getParent();
					parentActivity.startChildActivity("ReplyThread",
							replyThread);
				} else {
					ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
					parentActivity.startChildActivity("ReplyThread",
							replyThread);
				}
			}
		});
	}

	private void viewAttachment(String imageId) {
		Intent viewAttachment = new Intent(getParent(), ViewAttachment.class);
		viewAttachment.putExtra("imageId", imageId);
		if (getParent() instanceof ActivityGroupMain) {
			ActivityGroupMain parentActivity = (ActivityGroupMain) getParent();
			parentActivity.startChildActivity("ViewAttachment", viewAttachment);
		} else {
			ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
			parentActivity.startChildActivity("ViewAttachment", viewAttachment);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (Global.update) {
			MultipartEntity parameters = new MultipartEntity();

			try {
				parameters.addPart("act", new StringBody("getDetails"));
				parameters.addPart("ur", new StringBody(Global.username));
				parameters.addPart("tkn", new StringBody(Global.token));
				parameters.addPart("id", new StringBody(id));
			} catch (Exception e) {
				e.printStackTrace();
			}
			new ShowThreadTask().execute(parameters);
		}
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		System.gc();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		tracker.stop();
		replyProgress.dismiss();
		replyProgress = null;
	}

	@Override
	public void onBackPressed() {
		if (getParent() instanceof ActivityGroupMain) {
			ActivityGroupMain parentActivity = (ActivityGroupMain) getParent();
			parentActivity.onBackPressed();
		} else {
			ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
			parentActivity.onBackPressed();
		}
	}

	private class ShowThreadTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {

		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.detailsList = null;
			con.requestXML("getDetails");
			return XMLHandler.detailsList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					tblReplies.removeAllViews();
					createTableDetailThread();
				} else {
					Global.createAlert(getParent(), result.get(2).toString(),
							"Login");
				}
			} catch (Exception e) {
				Global.createAlert(
						getParent(),
						"Could not connect to the remote server. Please check your connection Settings.",
						"OK");
			}
		}
	}
}

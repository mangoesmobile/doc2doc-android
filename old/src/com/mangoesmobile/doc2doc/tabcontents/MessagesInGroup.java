package com.mangoesmobile.doc2doc.tabcontents;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.global.Global;
import com.mangoesmobile.doc2doc.util.MsgCenterNode;
import com.mangoesmobile.doc2doc.util.MsgListAdapter;
import com.mangoesmobile.doc2doc.util.DBManager;
import com.mangoesmobile.doc2doc.util.ServerConnection;
import com.mangoesmobile.doc2doc.util.XMLHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MessagesInGroup extends Activity {

	GoogleAnalyticsTracker tracker;

	ProgressDialog dialog;

	ImageView imgUser;
	TextView lblTitle;
	LinearLayout layoutSendAll;
	ListView lstMessages;

	int group;
	int child;

	MsgListAdapter adapter;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.messagesingroup);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.start(Global.UA, 20, this);
		tracker.trackPageView("/OlderMessages");

		dialog = new ProgressDialog(getParent());

		imgUser = (ImageView) findViewById(R.id.img_messagesingroup_user);
		lblTitle = (TextView) findViewById(R.id.lbl_messagesingroup_title);
		layoutSendAll = (LinearLayout) findViewById(R.id.layout_messagesingroup_sendall);
		lstMessages = (ListView) findViewById(R.id.lst_messagesingroup);

		group = getIntent().getIntExtra("group", 0);
		child = getIntent().getIntExtra("child", 0);

		if (group == 0 && child == 3) {
			layoutSendAll.setVisibility(View.VISIBLE);
		}

		if (Global.imageDrawable != null) {
			imgUser.setImageDrawable(Global.imageDrawable);
		}
		lblTitle.setText("Message Center:\n" + Global.type[group][child]);

		adapter = new MsgListAdapter(this, group, child);
		lstMessages.setAdapter(adapter);
	}

	@Override
	public void onStart() {
		super.onStart();
		lstMessages
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						MsgCenterNode item = (MsgCenterNode) Global.msgs[group][child]
								.get(arg2);
						if (group == 0) {
							if (child < 3) {
								tracker.trackEvent("click", "Button",
										"Open Thread from Old Notifications", 0);
								dialog.setMessage("Getting Details...");
								dialog.show();
								MultipartEntity parameters = new MultipartEntity();
								try {
									parameters.addPart("act", new StringBody(
											"getDetails"));
									parameters.addPart("ur", new StringBody(
											Global.username));
									parameters.addPart("tkn", new StringBody(
											Global.token));
									parameters.addPart("id", new StringBody(
											item.id));
								} catch (Exception e) {
									e.printStackTrace();
								}
								new ShowThreadTask().execute(parameters);
							} else {
								tracker.trackEvent("click", "Button",
										"Open Post From Outbox", 0);
								Intent postThread = new Intent(getParent(),
										PostThread.class);
								postThread.putExtra("index", arg2);
								postThread.putExtra("id", item.id);
								postThread.putExtra("title", item.subject);
								postThread.putExtra("details", item.message);
								postThread.putExtra("attach", item.author);
								postThread.putExtra("notify", item.to);
								ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
								parentActivity.startChildActivity("PostThread",
										postThread);
							}
						} else {
							tracker.trackEvent("click", "Button",
									"Open Private Message From Old Messages", 0);
							if (item.type.equalsIgnoreCase("message")) {
								dialog.setMessage("Loading Message...");
								dialog.show();
								MultipartEntity parameters = new MultipartEntity();
								System.out.println(item.id);
								try {
									parameters.addPart("act", new StringBody(
											"getMsgDetails"));
									parameters.addPart("ur", new StringBody(
											Global.username));
									parameters.addPart("tkn", new StringBody(
											Global.token));
									parameters.addPart("id", new StringBody(
											item.id));
								} catch (Exception e) {
									e.printStackTrace();
								}

								new ShowMessageTask().execute(parameters);
							}
						}
					}
				});
		lstMessages
				.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View arg1, int arg2, long arg3) {
						// TODO Auto-generated method stub
						if (group == 0 && child == 3) {
							MsgCenterNode item = (MsgCenterNode) Global.msgs[group][child]
									.get(arg2);
							chooseOption(item.id, arg2);
							return true;
						} else {
							return false;
						}
					}
				});
		layoutSendAll.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (Global.msgs[group][child].size() > 0) {
					// TODO Auto-generated method stub
					tracker.trackEvent("click", "Button",
							"Send All From Draft", 0);
					new SendPostTask().execute();
					Toast toast = Toast.makeText(getParent(),
							"Sending post(s)...", Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			}
		});
	}

	public void chooseOption(final String id, final int index) {
		final CharSequence[] items = { "Discard" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getParent());
		builder.setTitle("Options");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				switch (item) {
				case (0):
					AlertDialog alert = new AlertDialog.Builder(getParent())
							.create();
					alert.setTitle("Discard Draft");
					alert.setMessage("Are you sure you want to discard draft?");
					alert.setButton("Yes",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									Global.msgs[group][child].remove(index);
									DBManager manager = new DBManager(getParent(), Global.TABLE_DRAFT, Global.FIELDS_DRAFT_WITH_ID);
									manager.open();
									manager.deleteFromDB(Integer
											.parseInt(id));
									manager.close();
									updateOutbox();
									Global.updateOutbox = true;
								}
							});
					alert.setButton2("No",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
								}
							});
					alert.show();
					break;
				}
				dialog.dismiss();
			}
		});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	private void updateOutbox() {
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		tracker.stop();
	}

	@Override
	public void onBackPressed() {
		ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
		parentActivity.onBackPressed();
	}

	private void openThread() {
		Intent showThread = new Intent(getParent(), ShowThread.class);
		// Replace the view of this ActivityGroup
		// TabMain.group.replaceView(showThread, "ShowThread");
		ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
		parentActivity.startChildActivity("ShowThread", showThread);
	}

	private void openMessage() {
		Intent showMessage = new Intent(getParent(), ShowMessage.class);
		// Replace the view of this ActivityGroup
		// TabMain.group.replaceView(showThread, "ShowThread");
		ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
		parentActivity.startChildActivity("ShowMessage", showMessage);
	}

	private class SendPostTask extends
			AsyncTask<Void, Void, LinkedList<LinkedList<Object>>> {

		protected LinkedList<LinkedList<Object>> doInBackground(Void... params) {
			LinkedList<LinkedList<Object>> list = new LinkedList<LinkedList<Object>>();
			for (int i = 0; i < Global.msgs[0][3].size(); i++) {
				MsgCenterNode node = (MsgCenterNode) Global.msgs[0][3].get(i);

				MultipartEntity parameters = new MultipartEntity();
				byte[] bitmapdata = null;
				Bitmap bmp = null;
				if (!node.author.equals("")) {
					Uri imageUri = Uri.parse(node.author);
					try {
						bmp = MediaStore.Images.Media.getBitmap(
								getContentResolver(), imageUri);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (bmp != null) {
					System.out.println("bmp is not null");
					int bitmapWidth = bmp.getWidth(), bitmapHeight = bmp
							.getHeight();
					int dstWidth = 0, dstHeight = 0;
					if (bitmapWidth <= bitmapHeight) {
						dstWidth = 360;
						dstHeight = 480;
					} else {
						dstWidth = 480;
						dstHeight = 360;
					}
					while (bitmapWidth / 2 > dstWidth
							|| bitmapHeight / 2 > dstHeight) {
						bitmapWidth /= 2;
						bitmapHeight /= 2;
					}
					bmp = Bitmap.createScaledBitmap(bmp, bitmapWidth,
							bitmapHeight, true);
					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					bmp.compress(Bitmap.CompressFormat.JPEG, 75, stream);
					bitmapdata = stream.toByteArray();
				}
				try {

					parameters.addPart("act", new StringBody("postThread"));
					parameters.addPart("ur", new StringBody(Global.username));
					parameters.addPart("tkn", new StringBody(Global.token));
					parameters.addPart("typ", new StringBody("1"));
					parameters.addPart("tle", new StringBody(node.subject));
					parameters.addPart("msg", new StringBody(node.message));
					if (!node.to.equals("")) {
						parameters.addPart("nfy", new StringBody(node.to));
					}
					if (bitmapdata != null) {
						parameters.addPart("img", new InputStreamBody(
								new ByteArrayInputStream(bitmapdata), "image"));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				ServerConnection con = new ServerConnection(parameters);
				XMLHandler.detailsList = null;
				con.requestXML("postThread");
				list.add(XMLHandler.detailsList);
				System.out.println("added " + i);
			}
			return list;
		}

		protected void onPostExecute(LinkedList<LinkedList<Object>> result) {
			ArrayList<Integer> index = new ArrayList<Integer>();
			boolean failure = false;
			int count = 0;
			for (int i = 0; i < result.size(); i++) {
				LinkedList<Object> postResult = result.get(i);
				if (postResult != null) {
					System.out.println(postResult.get(0) + " at " + i);
					if (postResult.get(0).equals("success")) {
						index.add(Integer
								.parseInt(((MsgCenterNode) Global.msgs[0][3]
										.get(i)).id));
						Global.msgs[0][3].remove(i);
					} else {
						Global.createAlert(getParent(), postResult.get(2)
								.toString(), "Login");
						break;
					}
				} else {
					System.out.println("null at " + i);
					failure = true;
					count++;
				}
			}
			if (failure) {
				Toast toast = Toast
						.makeText(
								getParent(),
								count
										+ " post(s) could not be sent due to error in connection",
								Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			} else {
				Toast toast = Toast.makeText(getParent(),
						"All post(s) successfully sent", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
			for (int i = 0; i < index.size(); i++) {
				DBManager manager = new DBManager(getParent(), Global.TABLE_DRAFT, Global.FIELDS_DRAFT_WITH_ID);
				manager.open();
				manager.deleteFromDB(index.get(i));
				manager.close();
				System.out.println("removed " + index.get(i));
			}
			updateOutbox();
			Global.updateMessageCenter = true;
			Global.update = true;
		}
	}

	private class ShowThreadTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {

		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.detailsList = null;
			con.requestXML("getDetails");
			return XMLHandler.detailsList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					dialog.hide();
					openThread();
				} else {
					dialog.dismiss();
					Global.createAlert(getParent(), result.get(2).toString(),
							"Login");
				}
			} catch (Exception e) {
				dialog.hide();
				Toast.makeText(getParent(), "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
			}
		}
	}

	private class ShowMessageTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {

		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.detailMsgList = null;
			con.requestXML("getMsgDetails");
			return XMLHandler.detailMsgList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					dialog.hide();
					openMessage();
				} else {
					dialog.dismiss();
					Global.createAlert(getParent(), result.get(2).toString(),
							"Login");
				}
			} catch (Exception e) {
				dialog.hide();
				Toast.makeText(getParent(), "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
		}
	}

}

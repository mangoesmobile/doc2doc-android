package com.mangoesmobile.doc2doc.tabcontents;

import java.util.LinkedList;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.global.Global;
import com.mangoesmobile.doc2doc.util.LoginInfoNode;
import com.mangoesmobile.doc2doc.util.MsgCenterNode;
import com.mangoesmobile.doc2doc.util.MsgExpandableListAdapter;
import com.mangoesmobile.doc2doc.util.DBManager;
import com.mangoesmobile.doc2doc.util.ServerConnection;
import com.mangoesmobile.doc2doc.util.XMLHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Messages extends Activity {

	GoogleAnalyticsTracker tracker;

	private ProgressDialog messagesProgress;

	public static boolean running = false;

	LoginInfoNode loginInfo;

	ImageView imgUserImage;
	ImageView imgUserStatus;
	TextView lblUsername;
	TextView lblUserStatus;

	ExpandableListView expMessages;

	Handler refreshHandler;
	Runnable runRefresh = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			System.out.println("refreshing message center..");
			refreshMsgCenter();
			refreshHandler.postDelayed(this, 5 * 60000);
		}
	};

	boolean finish = false;
	Thread refreshListener = new Thread() {
		@Override
		public void run() {
			try {
				while (!finish) {
					sleep(1000);
					if (Global.refreshWhat == 1) {
						Global.refreshWhat = -1;
						refreshHandler.removeCallbacks(runRefresh);
						refreshHandler.post(runRefresh);
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	};

	MsgExpandableListAdapter msgAdapter;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.messages);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.start(Global.UA, 20, this);
		tracker.trackPageView("/Messages");

		messagesProgress = new ProgressDialog(getParent());
		messagesProgress.setCancelable(false);

		refreshHandler = new Handler();
		refreshHandler.postDelayed(runRefresh, 5 * 60000);

		refreshListener.setPriority(Thread.MIN_PRIORITY);
		refreshListener.start();

		imgUserImage = (ImageView) findViewById(R.id.img_message_userimage);
		imgUserStatus = (ImageView) findViewById(R.id.img_message_userstatus);
		lblUsername = (TextView) findViewById(R.id.lbl_message_username);
		lblUserStatus = (TextView) findViewById(R.id.lbl_message_userstatus);

		updateUserInfo();

		expMessages = (ExpandableListView) findViewById(R.id.exp_messages);

		DBManager manager = new DBManager(getParent(), Global.TABLE_MESSAGES,
				Global.FIELDS_MESSAGES_WITH_ID);
		manager.open();
		Cursor cache = manager.fetchAllFromDB();
		if (cache.getCount() == 0) {
			Toast toast = Toast.makeText(getParent(),
					"Opening Message Center...", Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();

			MultipartEntity parameters = new MultipartEntity();

			try {
				parameters.addPart("act", new StringBody("getMsgCtr"));
				parameters.addPart("ur", new StringBody(Global.username));
				parameters.addPart("tkn", new StringBody(Global.token));
			} catch (Exception e) {
				e.printStackTrace();
			}

			new MessageCenterTask().execute(parameters);

			cache.close();
		} else {
			LinkedList<Object> messageCenter = new LinkedList<Object>();
			cache.moveToFirst();
			for (int i = 0; i < cache.getCount(); i++) {
				System.out.println("adding data");
				MsgCenterNode msgcenter = new MsgCenterNode(
						cache.getString(cache.getColumnIndex("message_id")),
						cache.getString(cache.getColumnIndex("type")),
						cache.getString(cache.getColumnIndex("subject")),
						cache.getString(cache.getColumnIndex("timestamp")),
						cache.getString(cache.getColumnIndex("msg_status")),
						cache.getString(cache.getColumnIndex("message_to")),
						cache.getString(cache.getColumnIndex("message_from")),
						cache.getString(cache.getColumnIndex("author_name")),
						cache.getString(cache.getColumnIndex("photo")),
						cache.getString(cache.getColumnIndex("replies")),
						cache.getString(cache.getColumnIndex("author")),
						cache.getString(cache.getColumnIndex("message")),
						cache.getString(cache
								.getColumnIndex("last_reply_author")),
						cache.getString(cache.getColumnIndex("last_message")),
						cache.getString(cache.getColumnIndex("time")));

				messageCenter.add(msgcenter);
				cache.moveToNext();
			}
			cache.close();
			categorizeMessages(messageCenter, 0);
			populateOutbox();
			createTable();
			refreshMsgCenter();
		}
		manager.close();
	}

	@SuppressWarnings("unchecked")
	public void categorizeMessages(LinkedList<Object> list, int startingIndex) {
		for (int i = 1; i < list.size(); i++) {
			MsgCenterNode msg = (MsgCenterNode) list.get(i);
			if (msg.type.equalsIgnoreCase("notification")) {
				if (msg.from.equalsIgnoreCase(loginInfo.id) && !msg.to.equalsIgnoreCase(loginInfo.id)) {
					Global.msgs[0][2].add(msg);
				} else if (msg.msgStatus.equalsIgnoreCase("new")) {
					Global.msgs[0][0].add(msg);
				} else {
					Global.msgs[0][1].add(msg);
				}
			} else {
				if (msg.from.equalsIgnoreCase(loginInfo.id) && !msg.to.equalsIgnoreCase(loginInfo.id)) {
					Global.msgs[1][2].add(msg);
				} else if (msg.msgStatus.equalsIgnoreCase("new")) {
					Global.msgs[1][0].add(msg);
				} else {
					Global.msgs[1][1].add(msg);
				}
			}
		}
	}

	private void updateUserInfo() {
		loginInfo = (LoginInfoNode) XMLHandler.loginInfo.get(0);
		Global.name = loginInfo.title + " " + loginInfo.lastname;
		Global.status = loginInfo.status;
		lblUsername.setText(loginInfo.displayName);
		lblUserStatus.setText(loginInfo.status);
		System.out.println("u r " + loginInfo.status);
		if (loginInfo.status.equalsIgnoreCase("ONLINE")) {
			imgUserStatus.setImageResource(R.drawable.bg_photo_online);
			lblUserStatus.setTextColor(Color.rgb(0, 102, 153));
		} else {
			imgUserStatus.setImageResource(R.drawable.bg_photo_oncall);
			lblUserStatus.setTextColor(Color.rgb(101, 176, 73));
		}
		if (Global.imageDrawable != null) {
			imgUserImage.setImageDrawable(Global.imageDrawable);
		} else {
			imgUserImage.setImageResource(R.drawable.default_photo);
		}
	}

	private void createTable() {
		msgAdapter = new MsgExpandableListAdapter(this);
		expMessages.setAdapter(msgAdapter);
		expMessages.setOnChildClickListener(new OnChildClickListener() {

			@SuppressWarnings("unchecked")
			@Override
			public boolean onChildClick(ExpandableListView arg0, View arg1,
					int arg2, int arg3, long arg4) {
				int child = getActualIndex(arg2, arg3);
				if (child != -1) {
					tracker.trackEvent("click", "TableRow",
							"Open Older Messages", 0);
					if (Global.msgs[arg2][child].size() > 0) {
						Intent messagesInGroup = new Intent(getParent(),
								MessagesInGroup.class);
						messagesInGroup.putExtra("group", arg2);
						messagesInGroup.putExtra("child", child);
						ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
						parentActivity.startChildActivity("MessagesInGroup",
								messagesInGroup);
					}
					return true;
				} else {
					tracker.trackEvent("click", "TableRow", "Open New Message",
							0);
					MsgCenterNode item = (MsgCenterNode) Global.msgs[arg2][0]
							.get(arg3);
					if (arg2 == 0) {
						// open new public notifications
						messagesProgress.setMessage("Getting Details...");
						messagesProgress.show();
						MultipartEntity parameters = new MultipartEntity();
						try {
							parameters.addPart("act", new StringBody(
									"getDetails"));
							parameters.addPart("ur", new StringBody(
									Global.username));
							parameters.addPart("tkn", new StringBody(
									Global.token));
							parameters.addPart("id", new StringBody(item.id));
						} catch (Exception e) {
							e.printStackTrace();
						}
						new ShowThreadTask().execute(parameters);
						Global.msgs[0][1].add(0, Global.msgs[0][0].remove(arg3));
						msgAdapter.notifyDataSetChanged();
						arg0.expandGroup(arg2);
						return true;
					} else {
						// open new private message
						if (item.type.equalsIgnoreCase("message")) {
							messagesProgress.setMessage("Loading Message...");
							messagesProgress.show();
							MultipartEntity parameters = new MultipartEntity();
							System.out.println(item.id);
							try {
								parameters.addPart("act", new StringBody(
										"getMsgDetails"));
								parameters.addPart("ur", new StringBody(
										Global.username));
								parameters.addPart("tkn", new StringBody(
										Global.token));
								parameters.addPart("id",
										new StringBody(item.id));
							} catch (Exception e) {
								e.printStackTrace();
							}
							new ShowMessageTask().execute(parameters);
						}
						Global.msgs[1][1].add(0, Global.msgs[1][0].remove(arg3));
						msgAdapter.notifyDataSetChanged();
						arg0.expandGroup(arg2);
						return true;
					}
				}
			}
		});
	}

	private int getActualIndex(int group, int child) {
		int count = msgAdapter.getChildrenCount(group);
		if (group == 0) {
			if (child == count - 1) {
				return 3;
			} else if (child == count - 2) {
				return 2;
			} else if (child == count - 3) {
				return 1;
			} else {
				return -1;
			}
		} else {
			if (child == count - 1) {
				return 2;
			} else if (child == count - 2) {
				return 1;
			} else {
				return -1;
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void populateOutbox() {
		Global.msgs[0][3] = new LinkedList<MsgCenterNode>();
		DBManager manager = new DBManager(getParent(), Global.TABLE_DRAFT,
				Global.FIELDS_DRAFT_WITH_ID);
		manager.open();
		Cursor c = manager.fetchAllFromDB();
		startManagingCursor(c);
		if (c != null) {
			c.moveToLast();
			for (int i = 0; i < c.getCount(); i++) {
				String id = c.getString(c.getColumnIndex("_id"));
				String title = c.getString(c.getColumnIndex("title"));
				String details = c.getString(c.getColumnIndex("details"));
				String attach = c.getString(c.getColumnIndex("attach"));
				String notify = c.getString(c.getColumnIndex("notify"));
				MsgCenterNode msg = new MsgCenterNode(id, "outbox", title, "",
						"unsent", notify, loginInfo.id, loginInfo.displayName,
						loginInfo.photo, "", attach, details, "", "", "");
				Global.msgs[0][3].add(msg);
				c.moveToPrevious();
			}
			c.close();
		}
		manager.close();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if (hasFocus) {
			if (Global.updateProfileInMessages) {
				updateUserInfo();
				Global.updateProfileInMessages = false;
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (Global.updateProfileInMessages) {
			updateUserInfo();
			Global.updateProfileInMessages = false;
		}
		if (Global.updateMessageCenter) {
			msgAdapter.notifyDataSetChanged();
			Global.updateMessageCenter = false;
		}
		if (Global.updateOutbox) {
			populateOutbox();
			msgAdapter.notifyDataSetChanged();
			Global.updateOutbox = false;
		}
		running = true;
	}

	@Override
	public void onStart() {
		super.onStart();

		imgUserImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				tracker.trackEvent("click", "Button", "Change Status", 0);
				Toast toast = Toast.makeText(getParent(), "Changing status...",
						Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 80);
				toast.show();
				MultipartEntity parameters = new MultipartEntity();
				try {
					parameters.addPart("act", new StringBody("statusChange"));
					parameters.addPart("ur", new StringBody(Global.username));
					parameters.addPart("tkn", new StringBody(Global.token));
					parameters.addPart("stat", new StringBody(Global.stat));
				} catch (Exception e) {
					e.printStackTrace();
				}

				new ChangeStatusTask().execute(parameters);
			}
		});
	}

	public void refreshMsgCenter() {
		MultipartEntity parameters = new MultipartEntity();

		try {
			parameters.addPart("act", new StringBody("getMsgCtr"));
			parameters.addPart("ur", new StringBody(Global.username));
			parameters.addPart("tkn", new StringBody(Global.token));
		} catch (Exception e) {
			e.printStackTrace();
		}

		new RefreshMessageCenterTask().execute(parameters);
	}

	private class ChangeStatusTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {

		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.statusList = null;
			con.requestXML("statusChange");
			return XMLHandler.statusList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					int img;
					String statText = "";
					if (Global.stat.equals("2")) {
						img = R.drawable.bg_photo_oncall;
						((LoginInfoNode) XMLHandler.loginInfo.get(0)).status = "ON CALL";
						lblUserStatus.setTextColor(Color.rgb(101, 176, 73));
						statText = "Status: On Call";
						Global.stat = "1";
					} else {
						img = R.drawable.bg_photo_online;
						((LoginInfoNode) XMLHandler.loginInfo.get(0)).status = "ONLINE";
						lblUserStatus.setTextColor(Color.rgb(0, 102, 153));
						statText = "Status: Online";
						Global.stat = "2";
					}
					Global.updateProfile = true;
					Global.updateProfileInTeam = true;
					imgUserStatus.setImageResource(img);
					lblUserStatus.setText(((LoginInfoNode) XMLHandler.loginInfo
							.get(0)).status);
					Toast toast = Toast.makeText(getParent(), statText,
							Toast.LENGTH_LONG);
					toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL,
							0, 80);
					toast.show();
				} else {
					Global.createAlert(getParent(), result.get(2).toString(),
							"Login");
				}
			} catch (Exception e) {
				Toast.makeText(getParent(), "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
		}
	}

	private void openThread() {
		Intent showThread = new Intent(getParent(), ShowThread.class);
		// Replace the view of this ActivityGroup
		// TabMain.group.replaceView(showThread, "ShowThread");
		ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
		parentActivity.startChildActivity("ShowThread", showThread);
	}

	private void openMessage() {
		Intent showMessage = new Intent(getParent(), ShowMessage.class);
		// Replace the view of this ActivityGroup
		// TabMain.group.replaceView(showThread, "ShowThread");
		ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
		parentActivity.startChildActivity("ShowMessage", showMessage);
	}
	
	public void cacheData(LinkedList<Object> data) {
		System.out.println("caching data");
		DBManager manager = new DBManager(getParent(), Global.TABLE_MESSAGES,
				Global.FIELDS_MESSAGES_WITH_ID);
		manager.open();
		manager.clearDB();
		for (int i = 1; i < data.size(); i++) {
			MsgCenterNode msgcenter = (MsgCenterNode) data.get(i);
			manager.insertToDB(Global.FIELDS_MESSAGES, new String[] { msgcenter.id, msgcenter.type, msgcenter.subject, msgcenter.timestamp, msgcenter.msgStatus, msgcenter.to, msgcenter.from, msgcenter.authorName, msgcenter.photo, msgcenter.replies, msgcenter.author, msgcenter.message, msgcenter.lastReplyAuthor, msgcenter.lastMessage, msgcenter.time});
		}
		manager.close();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		tracker.stop();
		messagesProgress.dismiss();
		finish = true;
		refreshHandler.removeCallbacks(runRefresh);
	}

	@Override
	public void onBackPressed() {
		ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
		parentActivity.onBackPressed();
	}

	private class MessageCenterTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {
		protected LinkedList<Object> doInBackground(MultipartEntity... param) {

			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.msgCenterList = null;
			con.requestXML("getMsgCtr");
			return XMLHandler.msgCenterList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					messagesProgress.hide();
					categorizeMessages(result, 1);
					cacheData(result);
					populateOutbox();
					createTable();
				} else {
					messagesProgress.dismiss();
					Global.createAlert(getParent(), result.get(2).toString(),
							"Login");
				}
			} catch (Exception e) {
				messagesProgress.hide();
				Toast.makeText(getParent(), "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
		}
	}

	private class RefreshMessageCenterTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {
		protected LinkedList<Object> doInBackground(MultipartEntity... param) {

			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.msgCenterList = null;
			con.requestXML("getMsgCtr");
			return XMLHandler.msgCenterList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					// clearing messages
					for (int i = 0; i < Global.msgs.length; i++) {
						for (int j = 0; j < Global.msgs[i].length; j++) {
							Global.msgs[i][j] = new LinkedList<MsgCenterNode>();
						}
					}
					categorizeMessages(result, 1);
					cacheData(result);
					populateOutbox();
					System.out.println("this is the adapter: "
							+ expMessages.getAdapter());
					if (expMessages.getAdapter() != null) {
						msgAdapter.notifyDataSetChanged();
					} else {
						createTable();
					}
				} else {
					Global.createAlert(getParent(), result.get(2).toString(),
							"Login");
				}
			} catch (Exception e) {
				Toast.makeText(getParent(), "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
		}
	}

	private class ShowThreadTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {

		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.detailsList = null;
			con.requestXML("getDetails");
			return XMLHandler.detailsList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					messagesProgress.hide();
					openThread();
				} else {
					messagesProgress.dismiss();
					Global.createAlert(getParent(), result.get(2).toString(),
							"Login");
				}
			} catch (Exception e) {
				messagesProgress.hide();
				Toast.makeText(getParent(), "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
			}
		}
	}

	private class ShowMessageTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {

		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.detailMsgList = null;
			con.requestXML("getMsgDetails");
			return XMLHandler.detailMsgList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					messagesProgress.hide();
					openMessage();
				} else {
					messagesProgress.dismiss();
					Global.createAlert(getParent(), result.get(2).toString(),
							"Login");
				}
			} catch (Exception e) {
				messagesProgress.hide();
				Toast.makeText(getParent(), "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
		}
	}

}

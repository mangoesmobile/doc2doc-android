package com.mangoesmobile.doc2doc.tabcontents;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.global.Global;
import com.mangoesmobile.doc2doc.util.LoginInfoNode;
import com.mangoesmobile.doc2doc.util.ServerConnection;
import com.mangoesmobile.doc2doc.util.ThreadNode;
import com.mangoesmobile.doc2doc.util.XMLHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class MainThread extends Activity {

	GoogleAnalyticsTracker tracker;

	Vibrator vibrator;

	String version;

	ProgressDialog updateProgress;

	boolean disableClick = false;

	int readNotification = 0;

	int threadFrom = 0;
	boolean newerButtonVisible = false;
	boolean olderButtonVisible = false;

	private ProgressDialog progressIndicator;

	Animation searchAnimEntrance;
	Animation searchAnimExit;

	ImageView imgUserImage;
	ImageView imgUserStatus;
	TextView lblUsername;
	TextView lblUserStatus;

	ImageView imgTopBar;
	EditText txtSearch;

	CheckBox chkSearch;
	ImageView imgPostNew;

	LinearLayout layoutViewOlder;
	LinearLayout layoutViewNewer;

	TableLayout tblThread;

	EditText txtPost;
	Button btnPost;

	Handler refreshHandler;
	Runnable runRefresh = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			disableClick = true;
			System.out.println("Refreshing thread..");
			updateThread(threadFrom);
			refreshHandler.postDelayed(this, 5 * 60000);
		}
	};

	boolean finish = false;
	Thread refreshListener = new Thread() {
		@Override
		public void run() {
			try {
				while (!finish) {
					sleep(1000);
					if (Global.refreshWhat == 0) {
						Global.refreshWhat = -1;
						refreshHandler.removeCallbacks(runRefresh);
						refreshHandler.post(runRefresh);
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	};

	// EditText txtSearch;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainthread);

		vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.start(Global.UA, 20, this);
		tracker.trackPageView("/MainThread");

		refreshHandler = new Handler();
		refreshHandler.postDelayed(runRefresh, 5 * 60000);

		refreshListener.setPriority(Thread.MIN_PRIORITY);
//		refreshListener.start();

		try {
			version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		instantiateLayouts();

//		updateUserInfo();

//		LinkedList<Object> threads = XMLHandler.threadList;
//		for (int i = 1; i < (threads.size() > 20 ? 20 : threads.size()); i++) {
//			ThreadNode eachThread = (ThreadNode) threads.get(i);
//			tblThread.addView(createThreadRow(eachThread));
//			// loadImage(eachThread, i - 1);
//		}
//
//		if (threadFrom >= 19) {
//			layoutViewNewer.setVisibility(View.VISIBLE);
//			newerButtonVisible = true;
//		}
//
//		if (threads.size() > 20) {
//			layoutViewOlder.setVisibility(View.VISIBLE);
//			olderButtonVisible = true;
//		}
//
//		checkForNewVersion();

	}

	private void updateUserInfo() {
		LoginInfoNode loginInfo = (LoginInfoNode) XMLHandler.loginInfo.get(0);
		Global.name = loginInfo.title + " " + loginInfo.lastname;
		Global.status = loginInfo.status;
		lblUsername.setText(loginInfo.displayName);
		lblUserStatus.setText(loginInfo.status);
		System.out.println("u r " + loginInfo.status);
		if (loginInfo.status.equalsIgnoreCase("ONLINE")) {
			imgUserStatus.setImageResource(R.drawable.bg_photo_online);
			lblUserStatus.setTextColor(Color.rgb(0, 102, 153));
		} else {
			imgUserStatus.setImageResource(R.drawable.bg_photo_oncall);
			lblUserStatus.setTextColor(Color.rgb(101, 176, 73));
		}
		if (!loginInfo.photo.equals("")) {
			String url = Global.baseUrl + "?act=image&ur=" + Global.username
					+ "&tkn=" + Global.token + "&im=" + loginInfo.photo;// +
																		// "&rand=77634537428873";
			Global.loadPhoto(this, url, imgUserImage);
		}
	}

	private void instantiateLayouts() {
		progressIndicator = new ProgressDialog(getParent());
		progressIndicator.setCancelable(false);

		searchAnimEntrance = AnimationUtils.loadAnimation(this,
				R.anim.searchbox_entrance);
		searchAnimExit = AnimationUtils.loadAnimation(this,
				R.anim.searchbox_exit);

		imgUserImage = (ImageView) findViewById(R.id.img_userimage);
		imgUserStatus = (ImageView) findViewById(R.id.img_userstatus);
		lblUsername = (TextView) findViewById(R.id.lbl_username);
		lblUserStatus = (TextView) findViewById(R.id.lbl_userstatus);

		imgTopBar = (ImageView) findViewById(R.id.img_topbar);
		txtSearch = (EditText) findViewById(R.id.txt_search);

		chkSearch = (CheckBox) findViewById(R.id.chk_search);
		imgPostNew = (ImageView) findViewById(R.id.img_post);

		layoutViewOlder = (LinearLayout) findViewById(R.id.layout_viewolder);
		layoutViewNewer = (LinearLayout) findViewById(R.id.layout_viewnewer);

		tblThread = (TableLayout) findViewById(R.id.tbl_thread);
	}

	private TableRow createThreadRow(ThreadNode rowElement) {
		final ThreadNode eachThread = rowElement;

		TableRow row = (TableRow) LayoutInflater.from(this).inflate(
				R.layout.row_mainthread, null, false);
		row.setId(Integer.parseInt(eachThread.id));
		row.setContentDescription(eachThread.details);

		ImageView imgThreadPhoto = (ImageView) row
				.findViewById(R.id.img_mainthread_userimage);
		TextView lblTitle = (TextView) row
				.findViewById(R.id.lbl_mainthread_title);
		TextView lblReplies = (TextView) row
				.findViewById(R.id.lbl_mainthread_replies);
		TextView lblAuthor = (TextView) row
				.findViewById(R.id.lbl_mainthread_author);
		TextView lblResponse = (TextView) row
				.findViewById(R.id.lbl_mainthread_response);
		TextView lblLatestReply = (TextView) row
				.findViewById(R.id.lbl_mainthread_latestreply);
		TextView lblReplyAuthor = (TextView) row
				.findViewById(R.id.lbl_mainthread_replyauthor);
		TextView lblReplyTime = (TextView) row
				.findViewById(R.id.lbl_mainthread_replytime);
		ImageView imgThreadStatus = (ImageView) row
				.findViewById(R.id.img_mainthread_threadstatus);

		if (!eachThread.photo.equals("")) {
			String url = Global.baseUrl + "?act=image&ur=" + Global.username
					+ "&tkn=" + Global.token + "&im=" + eachThread.photo;
			Global.loadPhoto(this, url, imgThreadPhoto);
		}

		lblTitle.setText(eachThread.title);
		lblReplies.setText("(" + eachThread.replies + ")");
		lblAuthor.setText("by " + eachThread.author + " on "
				+ Global.formatDate(eachThread.timestamp));
		if (eachThread.lastReplyAuthor.length() > 0) {
			lblResponse.setText("Re: \"");
			lblLatestReply.setText(eachThread.lastReply);
			lblReplyAuthor.setText("\" by " + eachThread.lastReplyAuthor);
			lblReplyTime.setText(", " + eachThread.lastReplyTime + " ago");
		} else {
			lblResponse.setText("No one responded yet");
			lblReplyAuthor.setText("");
			lblLatestReply.setText("");
			lblReplyTime.setText("");
		}

		if (eachThread.threadStatus.equalsIgnoreCase("CLOSED")) {
			imgThreadStatus.setImageResource(R.drawable.closed);
		}

		// adding event listener
		row.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tracker.trackEvent("click", "TableRow", "Thread", 84);
				progressIndicator.setMessage("Getting Details...");
				progressIndicator.show();
				MultipartEntity parameters = new MultipartEntity();

				try {
					parameters.addPart("act", new StringBody("getDetails"));
					parameters.addPart("ur", new StringBody(Global.username));
					parameters.addPart("tkn", new StringBody(Global.token));
					parameters.addPart("id", new StringBody(eachThread.id));
				} catch (Exception e) {
					e.printStackTrace();
				}

				new ShowThreadTask().execute(parameters);
			}
		});

		return row;
	}

	public void updateImageView(ImageView view, Drawable d) {
		view.setImageDrawable(d);
	}

	public void checkForNewVersion() {
		updateProgress = new ProgressDialog(getParent());
		updateProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		updateProgress.setTitle("Downloading Update...");
		updateProgress.setIcon(R.drawable.update_icon);
		updateProgress.setCancelable(false);
		updateProgress.setProgress(0);
		if (!version.equalsIgnoreCase(XMLHandler.version)) {
			AlertDialog alert = new AlertDialog.Builder(getParent()).create();
			alert.setTitle("Update Found");
			alert.setMessage("New Version is Available. Do you want to download update?");
			alert.setButton("Update", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					updateProgress.show();
					new UpdateTask().execute(Global.baseUrl
							+ "downloads/doc2doc_v" + XMLHandler.version
							+ ".apk");
				}
			});
			alert.setButton2("Not now", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
			alert.show();
		}
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if (hasFocus) {
			System.out.println("mainthread got focus");
			if (Global.updateProfile) {
				updateUserInfo();
				updateThread(threadFrom);
				Global.update = false;
				Global.updateProfile = false;
			}
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		imgUserImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tracker.trackEvent("click", "Button", "Change Status", 0);
				Toast toast = Toast.makeText(getParent(), "Changing status...",
						Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 80);
				toast.show();
				MultipartEntity parameters = new MultipartEntity();
				try {
					parameters.addPart("act", new StringBody("statusChange"));
					parameters.addPart("ur", new StringBody(Global.username));
					parameters.addPart("tkn", new StringBody(Global.token));
					parameters.addPart("stat", new StringBody(Global.stat));
				} catch (Exception e) {
					e.printStackTrace();
				}

				new ChangeStatusTask().execute(parameters);
			}
		});

		chkSearch
				.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							tracker.trackEvent("click", "Button",
									"Search Thread", 0);
							imgTopBar
									.setImageResource(R.drawable.bg_top_search);
							txtSearch.setVisibility(View.VISIBLE);
							txtSearch.startAnimation(searchAnimEntrance);
						} else {
							txtSearch.startAnimation(searchAnimExit);
							txtSearch.setText("");
							searchAnimExit
									.setAnimationListener(new Animation.AnimationListener() {

										@Override
										public void onAnimationStart(
												Animation animation) {
											// TODO Auto-generated method stub

										}

										@Override
										public void onAnimationRepeat(
												Animation animation) {
											// TODO Auto-generated method stub

										}

										@Override
										public void onAnimationEnd(
												Animation animation) {
											// TODO Auto-generated method stub
											imgTopBar
													.setImageResource(R.drawable.bg_top);
											txtSearch.setVisibility(View.GONE);
										}
									});
						}
					}
				});

		imgPostNew.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tracker.trackEvent("click", "Button", "Post New", 0);
				postThread();
			}
		});

		txtSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				// hiding the button for showing view newer and older thread
				if (arg0.length() > 0) {
					layoutViewNewer.setVisibility(View.GONE);
					layoutViewOlder.setVisibility(View.GONE);
				} else {
					if (newerButtonVisible) {
						layoutViewNewer.setVisibility(View.VISIBLE);
					}
					if (olderButtonVisible) {
						layoutViewOlder.setVisibility(View.VISIBLE);
					}
				}

				for (int i = 0; i < tblThread.getChildCount(); i++) {
					TableRow row = (TableRow) tblThread.getChildAt(i);
					TextView title = (TextView) row
							.findViewById(R.id.lbl_mainthread_title);
					TextView lastReply = (TextView) row
							.findViewById(R.id.lbl_mainthread_latestreply);

					if (title.getText().toString().toLowerCase()
							.contains(arg0.toString().toLowerCase())
							|| row.getContentDescription().toString()
									.toLowerCase()
									.contains(arg0.toString().toLowerCase())
							|| lastReply.getText().toString().toLowerCase()
									.contains(arg0.toString().toLowerCase())) {
						row.setVisibility(View.VISIBLE);
					} else {
						row.setVisibility(View.GONE);
					}
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
			}
		});

		// for showing newer threads
		layoutViewNewer.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!disableClick) {
					tracker.trackEvent("click", "TableRow", "View Newer", 65);
					progressIndicator.setMessage("Getting Newer threads..");
					progressIndicator.show();
					System.out.println(threadFrom);
					threadFrom -= 19;
					refreshHandler.removeCallbacks(runRefresh);
					refreshHandler.post(runRefresh);
				}
			}
		});

		// for showing older threads
		layoutViewOlder.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!disableClick) {
					tracker.trackEvent("click", "TableRow", "View Older", 66);
					progressIndicator.setMessage("Getting Older threads..");
					progressIndicator.show();
					threadFrom += 19;
					refreshHandler.removeCallbacks(runRefresh);
					refreshHandler.post(runRefresh);
				}
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		System.out.println("mainthread resumed");
		if (Global.updateProfile) {
			updateUserInfo();
			Global.updateProfile = false;
		}
		if (Global.update) {
			refreshHandler.removeCallbacks(runRefresh);
			refreshHandler.post(runRefresh);
			Global.update = false;
		} else {
			System.out.println("Everything is fine");
		}
	}

	private void postThread() {
		Intent postThread = new Intent(getParent(), PostThread.class);
		ActivityGroupMain parentActivity = (ActivityGroupMain) getParent();
		parentActivity.startChildActivity("PostThread", postThread);
	}

	private void openThread() {
		Intent showThread = new Intent(getParent(), ShowThread.class);
		ActivityGroupMain parentActivity = (ActivityGroupMain) getParent();
		parentActivity.startChildActivity("ShowThread", showThread);
	}

	private void refreshThread(LinkedList<Object> result) {
		tblThread.removeAllViews();
		LinkedList<Object> threads = result;
		for (int i = 1; i < (threads.size() > 20 ? 20 : threads.size()); i++) {
			ThreadNode eachThread = (ThreadNode) threads.get(i);
			tblThread.addView(createThreadRow(eachThread));
		}
	}

	private void updateThread(int from) {
		MultipartEntity parameters = new MultipartEntity();

		try {
			parameters.addPart("act", new StringBody("getThreads"));
			parameters.addPart("ur", new StringBody(Global.username));
			parameters.addPart("tkn", new StringBody(Global.token));
			parameters.addPart("from", new StringBody(from + ""));
			parameters.addPart("to", new StringBody("20"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		new RefreshThreadTask().execute(parameters);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		tracker.stop();
		finish = true;
		refreshHandler.removeCallbacks(runRefresh);
		progressIndicator.dismiss();
//		progressIndicator = null;
		readNotification = 0;
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		System.gc();
	}

	@Override
	public void onBackPressed() {
		ActivityGroupMain parentActivity = (ActivityGroupMain) getParent();
		parentActivity.onBackPressed();
	}

	private class ShowThreadTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {

		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.detailsList = null;
			con.requestXML("getDetails");
			return XMLHandler.detailsList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					progressIndicator.hide();
					openThread();
				} else {
					progressIndicator.dismiss();
					Global.createAlert(getParent(), result.get(2).toString(),
							"Login");
				}
			} catch (Exception e) {
				progressIndicator.hide();
				Toast.makeText(getParent(), "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
		}
	}

	private class RefreshThreadTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {

		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.detailsList = null;
			con.requestXML("getDetails");
			return XMLHandler.detailsList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			if (Global.main != null) {
				try {
					if (result.get(0).equals("success")) {
						progressIndicator.hide();
						layoutViewNewer.setVisibility(View.GONE);
						layoutViewOlder.setVisibility(View.GONE);
						newerButtonVisible = false;
						olderButtonVisible = false;
						refreshThread(result);
						if (disableClick) {
							disableClick = false;
						}
						if (threadFrom >= 19) {
							layoutViewNewer.setVisibility(View.VISIBLE);
							newerButtonVisible = true;
						}
						if (result.size() > 20) {
							layoutViewOlder.setVisibility(View.VISIBLE);
							olderButtonVisible = true;
						}
					} else {
						progressIndicator.dismiss();
						Global.createAlert(getParent(), result.get(2)
								.toString(), "Login");
					}
				} catch (Exception e) {
					progressIndicator.hide();
					Toast.makeText(getParent(),
							"Connection error! Please retry", Toast.LENGTH_LONG)
							.show();
					e.printStackTrace();
					disableClick = false;
				}
			}
		}

		protected void finalize() {
			try {
				super.finalize();
				System.gc();
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	private class ChangeStatusTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {

		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.statusList = null;
			con.requestXML("statusChange");
			return XMLHandler.statusList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					int img;
					String statText = "";
					if (Global.stat.equals("2")) {
						img = R.drawable.bg_photo_oncall;
						((LoginInfoNode) XMLHandler.loginInfo.get(0)).status = "ON CALL";
						lblUserStatus.setTextColor(Color.rgb(101, 176, 73));
						statText = "Status: On Call";
						Global.stat = "1";
					} else {
						img = R.drawable.bg_photo_online;
						((LoginInfoNode) XMLHandler.loginInfo.get(0)).status = "ONLINE";
						lblUserStatus.setTextColor(Color.rgb(0, 102, 153));
						statText = "Status: Online";
						Global.stat = "2";
					}
					Global.updateProfileInTeam = true;
					Global.updateProfileInMessages = true;
					imgUserStatus.setImageResource(img);
					lblUserStatus.setText(((LoginInfoNode) XMLHandler.loginInfo
							.get(0)).status);
					Toast toast = Toast.makeText(getParent(), statText,
							Toast.LENGTH_LONG);
					toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL,
							0, 80);
					toast.show();
				} else {
					Global.createAlert(getParent(), result.get(2).toString(),
							"Login");
				}
			} catch (Exception e) {
				Toast.makeText(getParent(), "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
			}
		}
	}

	private class UpdateTask extends AsyncTask<String, Void, String> {

		protected String doInBackground(String... param) {
			try {

				final int MAX_BUFFER_SIZE = 1024; // 1kb
				double downloaded = 0;

				URL url = new URL(param[0]);
				HttpURLConnection c = (HttpURLConnection) url.openConnection();

				c.setRequestMethod("GET");
				c.setDoOutput(true);
				c.connect();

				int fileSize = c.getContentLength();
				
				String PATH = Environment.getExternalStorageDirectory()
						+ "/download/";
				File file = new File(PATH);
				file.mkdirs();
				File outputFile = new File(file, "doc2doc_v"
						+ XMLHandler.version + ".apk");
				FileOutputStream fos = new FileOutputStream(outputFile);

				InputStream is = c.getInputStream();

				while (true) {
					byte buffer[];
					if (fileSize - downloaded > MAX_BUFFER_SIZE) {
						buffer = new byte[MAX_BUFFER_SIZE];
					} else {
						buffer = new byte[(int) (fileSize - downloaded)];
					}
					int read = is.read(buffer);
					if (read == -1) {
						updateProgress.setProgress(100);
						break;
					}
					fos.write(buffer, 0, read);
					downloaded += read;

					updateProgress
							.setProgress((int) ((downloaded / fileSize) * 100));
					
				}

				fos.close();
				is.close();

				return "success";
			} catch (Exception e) {
				e.printStackTrace();
				return "failure";
			}
		}

		protected void onPostExecute(String result) {
			if (result.equalsIgnoreCase("success")) {
				updateProgress.dismiss();
				vibrator.vibrate(300);
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.fromFile(new File(Environment
						.getExternalStorageDirectory()
						+ "/download/"
						+ "doc2doc_v" + XMLHandler.version + ".apk")),
						"application/vnd.android.package-archive");
				startActivity(intent);
			} else {
				updateProgress.dismiss();
				Toast.makeText(getParent(), "Update error!", Toast.LENGTH_LONG)
						.show();
			}
		}
	}

}
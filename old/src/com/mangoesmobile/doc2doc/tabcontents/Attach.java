package com.mangoesmobile.doc2doc.tabcontents;

import java.io.File;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.global.Global;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

public class Attach extends Activity {

	GoogleAnalyticsTracker tracker;

	protected static final int TAKE_PICTURE = 226;

	TextView lblTitle;
	TextView lblAuthor;
	
	ImageView imgUser;

	ImageView btnPhoto;
	ImageView btnVideo;
	ImageView btnSound;
	ImageView btnFile;

	TextView lblFileAttached;
	ImageView imgAttached;

	ImageView btnDone;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.attach);		

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.start(Global.UA, 20, this);
		tracker.trackPageView("/Attach");
		
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setTitle("Doc2Doc - Attach File");

		lblTitle = (TextView) findViewById(R.id.lbl_attach_title);
		lblAuthor = (TextView) findViewById(R.id.lbl_attach_author);
		
		imgUser = (ImageView) findViewById(R.id.img_attach_user);

		btnPhoto = (ImageView) findViewById(R.id.img_attach_photo);
		btnVideo = (ImageView) findViewById(R.id.img_attach_video);
		btnSound = (ImageView) findViewById(R.id.img_attach_sound);
		btnFile = (ImageView) findViewById(R.id.img_attach_file);

		lblFileAttached = (TextView) findViewById(R.id.lbl_attach_fileattached);
		imgAttached = (ImageView) findViewById(R.id.img_attach_fileattached);

		btnDone = (ImageView) findViewById(R.id.img_attach_done);
		
		if (Global.imageDrawable != null) {
			imgUser.setImageDrawable(Global.imageDrawable);
		}
		lblTitle.setText(getIntent().getStringExtra("title"));
		lblAuthor.setText(getIntent().getStringExtra("author"));
	}

	@Override
	public void onStart() {
		super.onStart();

		btnPhoto.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				tracker.trackEvent("click", "Button", "Attach Photo", 0);
				Global.imageUri = Uri.fromFile(new File(Environment
						.getExternalStorageDirectory().getAbsolutePath()
						+ File.separator
						+ "Doc2Doc"
						+ System.currentTimeMillis() + ".jpg"));
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, Global.imageUri);
				intent.putExtra("return-data", "false");
				startActivityForResult(intent, TAKE_PICTURE);
			}
		});

		btnDone.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
	}

	public void onPause() {
		super.onPause();
		System.out.println("attach paused");
	}

	@Override
	public void onResume() {
		super.onResume();
		if (Global.imageUri != null) {
			// Drawable d = null;
			// try {
			// d = Drawable.createFromStream(
			// getContentResolver().openInputStream(mOutputFileUri), null );
			// } catch (FileNotFoundException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// imgAttach.setImageDrawable(d);
			System.out.println("Uri not null: " + Global.imageUri.toString());
//			imgAttach.setImageURI(mOutputFileUri);
			imgAttached.setImageURI(Global.imageUri);
		}
	}

	// @Override
	// protected void onActivityResult(int requestCode, int resultCode, Intent
	// data) {
	// // TODO Auto-generated method stub
	// // super.onActivityResult(requestCode, resultCode, data);
	// System.out.println("pic result");
	// if (resultCode == RESULT_OK) {
	// if (requestCode == TAKE_PICTURE) {
	// System.out.println("pic taken");
	// Uri selectedImageUri = data.getData();
	// imgAttach.setImageURI(selectedImageUri);
	// //
	// lblFileName.setText(getBaseContext().getResources().getResourceEntryName(R.id.img_attach_name));
	// }
	// }
	//
	// }
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		imgAttached.setImageURI(null);
		System.out.println("attach destroyed");
	}

	@Override
	public void onBackPressed() {
		if (getParent() instanceof ActivityGroupMain) {
			ActivityGroupMain parentActivity = (ActivityGroupMain) getParent();
			parentActivity.onBackPressed();
		} else if (getParent() instanceof ActivityGroupMessages) {
			ActivityGroupMessages parentActivity = (ActivityGroupMessages) getParent();
			parentActivity.onBackPressed();
		} else {
			super.onBackPressed();
		}
	}
}

package com.mangoesmobile.doc2doc;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.LinkedList;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.global.Global;
import com.mangoesmobile.doc2doc.tabcontents.ActivityGroupMain;
import com.mangoesmobile.doc2doc.tabcontents.ActivityGroupMessages;
import com.mangoesmobile.doc2doc.tabcontents.Messages;
import com.mangoesmobile.doc2doc.tabcontents.Team;
import com.mangoesmobile.doc2doc.tabcontents.Reference;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.LoginInfoNode;
import com.mangoesmobile.doc2doc.util.ServerConnection;
import com.mangoesmobile.doc2doc.util.XMLHandler;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

public class TabHostGroup extends TabActivity {

	private static final int CAMERA_PIC_REQUEST = 226;

	private static final int GALLERY_PIC_REQUEST = 425;

	GoogleAnalyticsTracker tracker;

	ProgressDialog progressIndicator;

	private boolean running = true;
	NotificationManager mNotificationManager;
	private boolean newNotification = false;

	ImageView imgMain;
	RelativeLayout relMessages;
	ImageView imgMessages;
	TextView lblNotifications;
	ImageView imgTeam;
	ImageView imgReference;
	boolean notificationNotChecked = true;

	Drawable image = null;
	Uri profilePic = null;

	public static TabHost tabHost;

	Handler refreshHandler;
	Runnable runRefresh = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			System.out.println("refreshing notification.."
					+ XMLHandler.str_notifications);
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					showNewNotification();
				}
			});
			refreshHandler.postDelayed(this, 5 * 60000);
		}
	};

	boolean idleLogout = false;
	boolean idleStatusChange = false;
	Handler checkIdle;
	Runnable runIdleAction = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					setActionIfIdle();
				}
			});
		}
	};

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tabs);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.start(Global.UA, 20, this);
		tracker.trackPageView("/TabHostGroup");

		refreshHandler = new Handler();
		refreshHandler.postDelayed(runRefresh, 5 * 60000);

		checkIdle = new Handler();

		progressIndicator = new ProgressDialog(this);

		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		Global.tabGroup = this;

		tabHost = getTabHost(); // The activity TabHost
		TabHost.TabSpec spec; // Resusable TabSpec for each tab
		Intent intent; // Reusable Intent for each tab

		// // Create an Intent to launch an Activity for the tab (to be reused)
		// intent = new Intent().setClass(this, MainThread.class);
		//
		// // Initialize a TabSpec for each tab and add it to the TabHost
		// spec = tabHost.newTabSpec("home")
		// .setIndicator("Main Thread", res.getDrawable(R.drawable.home))
		// .setContent(intent);
		// tabHost.addTab(spec);

		// Create an Intent to launch the first Activity for the tab (to be
		// reused)
		intent = new Intent().setClass(this, TabMain.class);

		// Initialize a TabSpec for the first tab and add it to the TabHost
		spec = tabHost.newTabSpec("tabmain").setIndicator("TabMainActivity")
				.setContent(new Intent(this, ActivityGroupMain.class));
		tabHost.addTab(spec);

		// Do the same for the other tabs
		spec = tabHost.newTabSpec("tabmessages")
				.setIndicator("TabMessagesActivity")
				.setContent(new Intent(this, ActivityGroupMessages.class));
		tabHost.addTab(spec);

		intent = new Intent().setClass(this, Team.class);
		spec = tabHost.newTabSpec("team").setIndicator("Team")
				.setContent(intent);
		tabHost.addTab(spec);

		intent = new Intent().setClass(this, Reference.class);
		spec = tabHost.newTabSpec("settings").setIndicator("Settings")
				.setContent(intent);
		tabHost.addTab(spec);

		tabHost.setCurrentTab(0);

		setupTabReplica();

//		showNewNotification();
	}

	@Override
	public void onResume() {
		super.onResume();
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		System.out.println("tabhost resumed");
		running = true;
		if (newNotification) {
			tabHost.setCurrentTab(1);
			imgMain.setImageResource(R.drawable.btn_board);
			imgMessages.setImageResource(R.drawable.btn_messages_down);
			imgTeam.setImageResource(R.drawable.btn_team);
			imgReference.setImageResource(R.drawable.btn_reference);
			lblNotifications.setVisibility(View.GONE);
			notificationNotChecked = false;
			newNotification = false;
			if (Messages.running) {
				Global.updateMessageCenter = true;
			}
		}
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if (hasFocus) {
			System.out.println("doc2doc on focus");
			checkIdle.removeCallbacks(runIdleAction);
		} else {
//			System.out.println("doc2doc lost focus");
//			if (((LoginInfoNode) XMLHandler.loginInfo.get(0)).status
//					.equalsIgnoreCase("ONLINE")) {
//				checkIdle.postDelayed(runIdleAction, 4 * 60 * 60000);
//				System.out.println("going idle from online");
//			} else if (((LoginInfoNode) XMLHandler.loginInfo.get(0)).status
//					.equalsIgnoreCase("ON CALL")) {
//				checkIdle.postDelayed(runIdleAction, 2 * 60 * 60000);
//				System.out.println("going idle from oncall");
//			}
		}
	}

	private void setupTabReplica() {
		imgMain = (ImageView) findViewById(R.id.img_main);
		relMessages = (RelativeLayout) findViewById(R.id.rel_message);
		imgMessages = (ImageView) findViewById(R.id.img_message);
		lblNotifications = (TextView) findViewById(R.id.lbl_notifications);
		imgTeam = (ImageView) findViewById(R.id.img_team);
		imgReference = (ImageView) findViewById(R.id.img_reference);

		imgMain.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tabHost.setCurrentTab(0);
				imgMain.setImageResource(R.drawable.btn_board_down);
				imgMessages.setImageResource(R.drawable.btn_messages);
				imgTeam.setImageResource(R.drawable.btn_team);
				imgReference.setImageResource(R.drawable.btn_reference);
				notificationNotChecked = true;
			}
		});
		relMessages.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tabHost.setCurrentTab(1);
				imgMain.setImageResource(R.drawable.btn_board);
				imgMessages.setImageResource(R.drawable.btn_messages_down);
				imgTeam.setImageResource(R.drawable.btn_team);
				imgReference.setImageResource(R.drawable.btn_reference);
				lblNotifications.setVisibility(View.GONE);
				notificationNotChecked = false;
			}
		});
		imgTeam.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tabHost.setCurrentTab(2);
				imgMain.setImageResource(R.drawable.btn_board);
				imgMessages.setImageResource(R.drawable.btn_messages);
				imgTeam.setImageResource(R.drawable.btn_team_down);
				imgReference.setImageResource(R.drawable.btn_reference);
				notificationNotChecked = true;
			}
		});
		imgReference.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tabHost.setCurrentTab(3);
				imgMain.setImageResource(R.drawable.btn_board);
				imgMessages.setImageResource(R.drawable.btn_messages);
				imgTeam.setImageResource(R.drawable.btn_team);
				imgReference.setImageResource(R.drawable.btn_reference_down);
				notificationNotChecked = true;
			}
		});
	}

	public void showNewNotification() {
		if (notificationNotChecked) {
			if (!XMLHandler.str_notifications.equals("0")) {
				if (running) {
					lblNotifications.setText(XMLHandler.str_notifications);
					lblNotifications.setVisibility(View.VISIBLE);
				} else {
					int icon = R.drawable.appicon;
					CharSequence tickerText = "New Notification";
					long when = System.currentTimeMillis();

					Notification notification = new Notification(icon,
							tickerText, when);
					notification.defaults |= Notification.DEFAULT_SOUND;
					notification.defaults |= Notification.DEFAULT_VIBRATE;
					notification.flags |= Notification.FLAG_AUTO_CANCEL;
					Context context = getApplicationContext();
					CharSequence contentTitle = "New Notification(s)";
					CharSequence contentText = "You have "
							+ XMLHandler.str_notifications
							+ " new notification(s)";
					Intent notificationIntent = new Intent(context,
							TabHostGroup.class);
					notificationIntent
							.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					PendingIntent contentIntent = PendingIntent.getActivity(
							this, 0, notificationIntent, 0);

					notification.setLatestEventInfo(context, contentTitle,
							contentText, contentIntent);
					final int NOTIFY = 1;
					mNotificationManager.notify(NOTIFY, notification);
					newNotification = true;
				}
			}
		}
	}

	public void setActionIfIdle() {
		if (((LoginInfoNode) XMLHandler.loginInfo.get(0)).status
				.equalsIgnoreCase("ON CALL")) {
			checkIdle.removeCallbacks(runIdleAction);
			checkIdle.postDelayed(runIdleAction, 2 * 60 * 60000);
			MultipartEntity parameters = new MultipartEntity();
			try {
				parameters.addPart("act", new StringBody("statusChange"));
				parameters.addPart("ur", new StringBody(Global.username));
				parameters.addPart("tkn", new StringBody(Global.token));
				parameters.addPart("stat", new StringBody(Global.stat));
			} catch (Exception e) {
				e.printStackTrace();
			}
			new ChangeStatusTask().execute(parameters);
			idleStatusChange = true;
		} else if (((LoginInfoNode) XMLHandler.loginInfo.get(0)).status
				.equalsIgnoreCase("ONLINE")) {
			logout();
		}
	}
	
	private void showLoading(final String message, final boolean cancelable) {
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				progressIndicator.setMessage(message);
				progressIndicator.setCancelable(cancelable);
				progressIndicator.show();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.app_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.menu_refresh:
			refresh();
			return true;
		case R.id.menu_feedback:
			sendFeedback();
			return true;
		case R.id.menu_changepp:
			changePP();
			return true;
		case R.id.menu_logout:
			showLoading("Logging out...", false);
			logout();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void refresh() {
		int curr = getTabHost().getCurrentTab();
		Toast toast;
		switch (curr) {
		case 0:
			Global.refreshWhat = 0;
			toast = Toast.makeText(this, "Refreshing Thread...",
					Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			break;
		case 1:
			Global.refreshWhat = 1;
			toast = Toast.makeText(this, "Refreshing Messages...",
					Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			break;
		case 2:
			Global.refreshWhat = 2;
			toast = Toast.makeText(this, "Refreshing Team...",
					Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			break;
		default:
			break;
		}
	}

	public void sendFeedback() {
		LoginInfoNode logininfo = (LoginInfoNode) XMLHandler.loginInfo.get(0);
		Intent feedbackIntent = new Intent(Intent.ACTION_SEND);
		feedbackIntent.setType("message/rfc822");
		feedbackIntent.putExtra(Intent.EXTRA_EMAIL,
				new String[] { "doc2doc@mangoesmobile.com" });
		feedbackIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback on Doc2Doc - "
				+ logininfo.displayName);
		startActivity(Intent.createChooser(feedbackIntent, "Send Feedback"));
	}

	public void logout() {
		// progressIndicator.setMessage("Logging out...");
		// progressIndicator.setCancelable(false);
		// progressIndicator.show();
		// MultipartEntity parameters = new MultipartEntity();
		//
		// try {
		// parameters.addPart("act", new StringBody("logout"));
		// parameters.addPart("ur", new StringBody(Global.username));
		// parameters.addPart("tkn", new StringBody(Global.token));
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		// new LogoutTask().execute(parameters);
		Doc2DocRequest dr = new Doc2DocRequest();
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", Global.token);

		asyncTask.runTask("authenticate/logout", params,
				new Doc2DocTaskListener() {

					@Override
					public void onMalformedURLException(MalformedURLException e) {
						// TODO Auto-generated method stub
						hideLoading();
						Log.d("logout-response-m", e.getMessage());
					}

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						hideLoading();
						Toast.makeText(Global.login,
								"Connection error! Please retry",
								Toast.LENGTH_LONG).show();
						Log.d("logout-response-i", e.getMessage());
					}

					@Override
					public void onFileNotFoundException(FileNotFoundException e) {
						// TODO Auto-generated method stub
						hideLoading();
						Log.d("logout-response-f", e.getMessage());
					}

					@Override
					public void onComplete(String response) {
						hideLoading();
						if (response.equals("FALSE")) {
							Global.createAlert(Global.login,
									"Invalid session!", "Login");
						} else {
							Global.logout(Global.tabGroup);
							if (idleLogout) {
								showLoggedoutNotification();
								idleLogout = false;
							}
						}
						Log.d("logout-response-c", response);
					}
				});
	}

	private void hideLoading() {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				progressIndicator.hide();
			}
		});
	}

	public void changePP() {
		showDialog(0);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog = new Dialog(this);
		switch (id) {
		case 0:
			dialog.show();
			break;
		case 1:
			dialog.show();
			break;
		default:
			dialog = null;
		}
		return dialog;
	}

	@Override
	protected void onPrepareDialog(int id, final Dialog dialog) {
		dialog.setContentView(R.layout.profilepic);
		dialog.setTitle("Change Profile Picture");
		switch (id) {
		case 0: {
			ImageView imgPhoto = (ImageView) dialog
					.findViewById(R.id.img_profilepic);
			if (Global.imageDrawable != null) {
				imgPhoto.setImageDrawable(Global.imageDrawable);
			} else {
				imgPhoto.setImageResource(R.drawable.default_photo);
			}
			Button btnChoose = (Button) dialog
					.findViewById(R.id.btn_choosephoto);
			btnChoose.setText("New Photo");
			btnChoose.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					chooseSource(dialog);
				}
			});
		}
			break;
		case 1: {
			ImageView imgPhoto = (ImageView) dialog
					.findViewById(R.id.img_profilepic);
			if (image != null) {
				imgPhoto.setImageDrawable(image);
			}
			Button btnChoose = (Button) dialog
					.findViewById(R.id.btn_choosephoto);
			btnChoose.setText("Update Photo");
			btnChoose.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					updatePhoto();
				}
			});
		}
			break;
		}
		super.onPrepareDialog(id, dialog);
	}

	public void chooseSource(final Dialog photoDialog) {
		final CharSequence[] items = { "Camera", "Gallery" };
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Pick Source");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				photoDialog.dismiss();
				switch (item) {
				case (0):
					takePhoto();
					break;
				case (1):
					galleryPhoto();
					break;
				}
				dialog.dismiss();
			}
		});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	private void takePhoto() {
		Intent cameraIntent = new Intent(
				android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		cameraIntent.putExtra("crop", "true");
		cameraIntent.putExtra("aspectX", 1);
		cameraIntent.putExtra("aspectY", 1);
		cameraIntent.putExtra("outputX", 96);
		cameraIntent.putExtra("outputY", 96);
		cameraIntent.putExtra("return-data", "false");
		startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
	}

	private void galleryPhoto() {
		Intent galleryIntent = new Intent();
		galleryIntent.setType("image/*");
		galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
		galleryIntent.putExtra("crop", "true");
		galleryIntent.putExtra("aspectX", 1);
		galleryIntent.putExtra("aspectY", 1);
		galleryIntent.putExtra("outputX", 96);
		galleryIntent.putExtra("outputY", 96);
		profilePic = Uri.fromFile(new File(Environment
				.getExternalStorageDirectory().getAbsolutePath()
				+ File.separator
				+ "Doc2Doc"
				+ System.currentTimeMillis()
				+ ".jpg"));
		galleryIntent.putExtra(MediaStore.EXTRA_OUTPUT, profilePic);
		galleryIntent.putExtra("return-data", "false");
		startActivityForResult(
				Intent.createChooser(galleryIntent, "Select Picture"),
				GALLERY_PIC_REQUEST);
	}

	private void updatePhoto() {
		showLoading("Updating profile pic...", false);
		MultipartEntity parameters = new MultipartEntity();
		byte[] bitmapdata = null;
		if (image != null) {
			Bitmap bmp = ((BitmapDrawable) image).getBitmap();
			System.out.println("bmp is not null");
			int bitmapWidth = bmp.getWidth(), bitmapHeight = bmp.getHeight();
			int dstWidth = 80, dstHeight = 80;
			while (bitmapWidth / 2 > dstWidth || bitmapHeight / 2 > dstHeight) {
				bitmapWidth /= 2;
				bitmapHeight /= 2;
			}
			bmp = Bitmap.createScaledBitmap(bmp, bitmapWidth, bitmapHeight,
					true);
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bmp.compress(Bitmap.CompressFormat.JPEG, 75, stream);
			bitmapdata = stream.toByteArray();
		}
		try {
			parameters.addPart("act", new StringBody("photoChange"));
			parameters.addPart("ur", new StringBody(Global.username));
			parameters.addPart("tkn", new StringBody(Global.token));
			if (bitmapdata != null) {
				parameters.addPart("img", new InputStreamBody(
						new ByteArrayInputStream(bitmapdata), "image"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		new PhotoChangeTask().execute(parameters);
	}

	@Override
	public void onPause() {
		super.onPause();
		System.out.println("tabhost paused");
		running = false;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		refreshHandler.removeCallbacks(runRefresh);
		checkIdle.removeCallbacks(runIdleAction);
		progressIndicator.dismiss();
		tracker.stop();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (resultCode == RESULT_OK) {
			if (requestCode == CAMERA_PIC_REQUEST) {
				Bitmap bmp = (Bitmap) data.getExtras().get("data");
				image = new BitmapDrawable(bmp);
			} else if (requestCode == GALLERY_PIC_REQUEST) {
				Uri currImageURI = profilePic;
				if (currImageURI != null) {
					try {
						Bitmap bmp = MediaStore.Images.Media.getBitmap(
								getContentResolver(), currImageURI);
						image = new BitmapDrawable(bmp);
						profilePic = null;
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			showDialog(1);
		}

	}

	public void showStatusChangeNotification() {
		int icon = R.drawable.appicon;
		CharSequence tickerText = "Status changed!";
		long when = System.currentTimeMillis();

		Notification idleLogout = new Notification(icon, tickerText, when);
		idleLogout.defaults |= Notification.DEFAULT_SOUND;
		idleLogout.defaults |= Notification.DEFAULT_VIBRATE;
		idleLogout.flags |= Notification.FLAG_AUTO_CANCEL;
		Context context = getApplicationContext();
		CharSequence contentTitle = "Status changed in Doc2Doc";
		CharSequence contentText = "Inactive for 2 hrs, status: ONLINE.";
		Intent statusChangeIntent = new Intent(context, TabHostGroup.class);
		statusChangeIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				statusChangeIntent, 0);

		idleLogout.setLatestEventInfo(context, contentTitle, contentText,
				contentIntent);
		final int NOTIFY = 1;
		mNotificationManager.notify(NOTIFY, idleLogout);
	}

	public void showLoggedoutNotification() {
		int icon = R.drawable.appicon;
		CharSequence tickerText = "Logged out!";
		long when = System.currentTimeMillis();

		Notification idleLogout = new Notification(icon, tickerText, when);
		idleLogout.defaults |= Notification.DEFAULT_SOUND;
		idleLogout.defaults |= Notification.DEFAULT_VIBRATE;
		idleLogout.flags |= Notification.FLAG_AUTO_CANCEL;
		Context context = getApplicationContext();
		CharSequence contentTitle = "Logged out from Doc2Doc";
		CharSequence contentText = "Inactive for 4 hrs, you have been logged out.";
		Intent loggedoutIntent = new Intent(context, Login.class);
		loggedoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				loggedoutIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		idleLogout.setLatestEventInfo(context, contentTitle, contentText,
				contentIntent);
		final int NOTIFY = 1;
		mNotificationManager.notify(NOTIFY, idleLogout);
	}

	private class PhotoChangeTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {

		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.photoChangeList = null;
			con.requestXML("photoChange");
			return XMLHandler.photoChangeList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					progressIndicator.hide();
					Global.imageDrawable = image;
					((LoginInfoNode) XMLHandler.loginInfo.get(0)).photo = (String) result
							.get(1);
					Global.updateProfile = true;
					Global.updateProfileInTeam = true;
					Global.updateProfileInMessages = true;
					Global.update = true;
					Toast.makeText(Global.tabGroup, "Profile picture updated",
							Toast.LENGTH_LONG).show();
				} else {
					progressIndicator.dismiss();
					Global.createAlert(Global.tabGroup, result.get(2)
							.toString(), "Login");
				}
			} catch (Exception e) {
				progressIndicator.hide();
				Toast.makeText(Global.tabGroup,
						"Connection error! Please retry", Toast.LENGTH_LONG)
						.show();
				e.printStackTrace();
			}
		}
	}

	// private class LogoutTask extends
	// AsyncTask<MultipartEntity, Void, LinkedList<Object>> {
	//
	// protected LinkedList<Object> doInBackground(MultipartEntity... param) {
	// ServerConnection con = new ServerConnection(param[0]);
	// XMLHandler.logoutList = null;
	// con.requestXML("logout");
	// return XMLHandler.logoutList;
	// }
	//
	// protected void onPostExecute(LinkedList<Object> result) {
	// try {
	// if (result.get(0).equals("success")) {
	// progressIndicator.dismiss();
	// Global.logout(Global.tabGroup);
	// if(idleLogout) {
	// showLoggedoutNotification();
	// System.out.println("logged out for being inactive");
	// idleLogout = false;
	// }
	// } else {
	// progressIndicator.dismiss();
	// Global.createAlert(Global.tabGroup, result.get(2)
	// .toString(), "Login");
	// }
	// } catch (Exception e) {
	// progressIndicator.hide();
	// Toast.makeText(
	// Global.tabGroup,
	// "Connection error! Please retry", Toast.LENGTH_LONG).show();
	// e.printStackTrace();
	// }
	// }
	// }

	private class ChangeStatusTask extends
			AsyncTask<MultipartEntity, Void, LinkedList<Object>> {

		protected LinkedList<Object> doInBackground(MultipartEntity... param) {
			ServerConnection con = new ServerConnection(param[0]);
			XMLHandler.statusList = null;
			con.requestXML("statusChange");
			return XMLHandler.statusList;
		}

		protected void onPostExecute(LinkedList<Object> result) {
			try {
				if (result.get(0).equals("success")) {
					if (Global.stat.equals("2")) {
						((LoginInfoNode) XMLHandler.loginInfo.get(0)).status = "ON CALL";
						Global.stat = "1";
					} else {
						((LoginInfoNode) XMLHandler.loginInfo.get(0)).status = "ONLINE";
						Global.stat = "2";
					}
					Global.updateProfile = true;
					Global.updateProfileInTeam = true;
					Global.updateProfileInMessages = true;
					if (idleStatusChange) {
						showStatusChangeNotification();
						System.out.println("status changed for being inactive");
						idleStatusChange = false;
					}
				} else {
					Global.createAlert(Global.tabGroup, result.get(2)
							.toString(), "Login");
				}
			} catch (Exception e) {
				Toast.makeText(Global.tabGroup,
						"Connection error! Please retry", Toast.LENGTH_LONG)
						.show();
			}
		}
	}
}

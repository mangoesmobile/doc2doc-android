package com.mangoesmobile.doc2doc.global;

import java.util.ArrayList;
import java.util.LinkedList;

import com.mangoesmobile.doc2doc.Login;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.tabcontents.ActivityGroupMain;
import com.mangoesmobile.doc2doc.tabcontents.ActivityGroupMessages;
import com.mangoesmobile.doc2doc.util.ImageCache;
import com.mangoesmobile.doc2doc.util.MsgCenterNode;
import com.mangoesmobile.doc2doc.util.ImageCache.ImageCallback;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;

public class Global {
	public static final String baseUrl = "/localhost/doc2doc-server/CodeIgniter/application/controllers";
	public static String cookie = "";
	public static String username;
	public static String token;

	public static String UA = "UA-22022360-2";

	public static String name;
	public static String status;
	public static Drawable imageDrawable = null;

	public static int refreshWhat = -1;
	
	public static boolean update = false;
	public static boolean updateProfile = false;
	public static boolean updateProfileInTeam = false;
	public static boolean updateProfileInMessages = false;
	public static boolean updateOutbox = false;
	
	public static boolean updateMessageCenter = false;

	public static Activity login;
	public static Activity tabGroup;
	public static ActivityGroupMain main;
	public static ActivityGroupMessages messages;
	public static Activity team;
	public static Activity reference;

	// for caching purpose
	public static final String TABLE_DRAFT = "draft";
	public static final String TABLE_TEAM = "team";
	public static final String TABLE_MESSAGES = "messages";
	public static final String[] FIELDS_DRAFT_WITH_ID = {"_id", "title", "details", "attach", "notify"};
	public static final String[] FIELDS_TEAM_WITH_ID = {"_id", "user_id", "title", "lastname", "firstname", "display_name", "specialization", "location", "email", "phone", "status", "photo"};
	public static final String[] FIELDS_MESSAGES_WITH_ID = {"_id", "message_id", "type", "subject", "timestamp", "msg_status", "message_to", "message_from", "author_name", "photo", "replies", "author", "message", "last_reply_author", "last_message", "time"};
	public static final String[] FIELDS_DRAFT = {"title", "details", "attach", "notify"};
	public static final String[] FIELDS_TEAM = {"user_id", "title", "lastname", "firstname", "display_name", "specialization", "location", "email", "phone", "status", "photo"};
	public static final String[] FIELDS_MESSAGES = {"message_id", "type", "subject", "timestamp", "msg_status", "message_to", "message_from", "author_name", "photo", "replies", "author", "message", "last_reply_author", "last_message", "time"};
	
	public static String stat = "2";

	public static Uri imageUri = null;
	public static ArrayList<String> taggedId = new ArrayList<String>();

	// for message center
	public static final String[][] type = {
			{ "Public - New Notifications", "Public - Old Notifications",
					"Public - Sent Notifications" , "Public - Drafts"},
			{ "Private - Unread Pings/Messages",
					"Private - Read Pings/Messages",
					"Private - Sent Pings/Messages" } };

	@SuppressWarnings("rawtypes")
	public static LinkedList[][] msgs = {
			{ new LinkedList<MsgCenterNode>(), new LinkedList<MsgCenterNode>(),
					new LinkedList<MsgCenterNode>(), new LinkedList<MsgCenterNode>() },
			{ new LinkedList<MsgCenterNode>(), new LinkedList<MsgCenterNode>(),
					new LinkedList<MsgCenterNode>() } };

	public static void createAlert(final Activity a, String message,
			String button) {
		AlertDialog alert = new AlertDialog.Builder(a).create();
		alert.setTitle("Error");
		alert.setIcon(android.R.drawable.ic_dialog_alert);
		alert.setMessage(message);
		final String btnTitle = button;
		alert.setButton(button, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (btnTitle.equals("OK")) {
					return;
				} else {
					logout(a);
				}
			}
		});
		alert.show();
	}

	public static void logout(Activity a) {
		Intent login = new Intent();
		login.setClass(a, Login.class);
		a.startActivity(login);
		a.finish();

		imageDrawable = null;
		stat = "2";

		// clearing messages
		for (int i = 0; i < msgs.length; i++) {
			for (int j = 0; j < msgs[i].length; j++) {
				msgs[i][j] = new LinkedList<MsgCenterNode>();
			}
		}
		
		if (Global.main != null) {
			Global.main.finish();
			Global.main = null;
		}
		if (Global.messages != null) {
			Global.messages.finish();
			Global.messages = null;
		}
		if (Global.team != null) {
			Global.team.finish();
			Global.team = null;
		}
		if (Global.reference != null) {
			Global.reference.finish();
			Global.reference = null;
		}

		if (Global.tabGroup != null) {
			Global.tabGroup.finish();
			Global.tabGroup = null;
		}
	}

	public static String limitText(String text, int length) {
		if (text.length() > length) {
			return text.substring(0, length - 3) + "...";
		}
		return text;
	}

	public static String formatDate(String date) {
		try {
			int time = Integer.parseInt(date.substring(11, 13));
			String hour = (time < 12 ? "am" : "pm");
			if (time == 0) {
				time = 12;
			} else if (time > 12) {
				time -= 12;
			}
			String formattedDate = date.substring(8, 10) + "/"
					+ date.substring(5, 7) + " at " + time
					+ date.substring(13, 16) + hour;
			return formattedDate;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public static void loadPhoto(final Context context, String url,
			final ImageView view) {
		view.setImageResource(R.drawable.loading_animation);
		final AnimationDrawable animator = (AnimationDrawable) view
				.getDrawable();
		class Starter implements Runnable {
			@Override
			public void run() {
				animator.start();
			}
		}
		view.post(new Starter());

		ImageCache img = ImageCache.getInstance();
		ImageCallback callback = new ImageCallback() {

			@Override
			public void onImageLoaded(final Drawable image, String url) {
				// TODO Auto-generated method stub
				((Activity) context).runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						animator.stop();
						view.setImageDrawable(image);
						if (view.getId() == R.id.img_userimage) {
							imageDrawable = image;
						}
					}
				});
			}
		};
		img.loadAsync(url, callback, context);
	}	
}

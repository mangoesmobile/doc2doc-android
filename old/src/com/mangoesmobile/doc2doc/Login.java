package com.mangoesmobile.doc2doc;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import com.bugsense.trace.BugSenseHandler;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.global.Global;
import com.mangoesmobile.doc2doc.tabcontents.PostThread;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends Activity {

	GoogleAnalyticsTracker tracker;

	private ProgressDialog loginProgress;

	private EditText txtUsername;
	private EditText txtPassword;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		BugSenseHandler.setup(this, "c91e41bc");

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.start(Global.UA, 20, this);
		tracker.trackPageView("/Login");

		Global.login = this;

		loginProgress = new ProgressDialog(this);

		txtUsername = (EditText) findViewById(R.id.txt_username);
		txtPassword = (EditText) findViewById(R.id.txt_password);

		SharedPreferences pref = getPreferences(Context.MODE_PRIVATE);
		txtUsername.setText(pref.getString("username", ""));
		txtPassword.setText(pref.getString("password", ""));
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		loginProgress.dismiss();
	}

	public void clickLogin(View view) {
		tracker.trackEvent("click", "Button", "login", 56);
		if (txtUsername.getText().length() > 0
				&& txtPassword.getText().length() > 0) {
			loginProgress.setMessage("Logging in...");
			loginProgress.setCancelable(false);
			loginProgress.show();

			// MultipartEntity parameters = new MultipartEntity();
			//
			// try {
			// parameters.addPart("act", new StringBody("login"));
			// parameters.addPart("ur", new StringBody(txtUsername.getText()
			// .toString()));
			// parameters.addPart("pwrd", new StringBody(txtPassword.getText()
			// .toString()));
			// parameters.addPart("from", new StringBody("0"));
			// parameters.addPart("to", new StringBody("20"));
			// } catch (Exception e) {
			// e.printStackTrace();
			// }
			//
			// new LoginTask().execute(parameters);
			authenticate("authenticate/login",
					txtUsername.getText().toString(),
					encryptPassword(txtPassword.getText().toString()));

		} else {
			Global.createAlert(this, "Please enter username and(or) password",
					"OK");
		}
	}

	private String encryptPassword(String pass) {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-1");
			md.update(pass.getBytes("UTF-8"));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		byte[] raw = md.digest();
		StringBuilder string = new StringBuilder();
		for (byte b : raw) {
			String hexString = Integer.toHexString(0x00FF & b);
			string.append(hexString.length() == 1 ? "0" + hexString : hexString);
		}
		return string.toString();
	}

	private void authenticate(String action, String user, String pass) {
		Doc2DocRequest dr = new Doc2DocRequest();
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("uname", user);
		params.putString("enc_pword", pass);

		asyncTask.runTask(action, params, new Doc2DocTaskListener() {

			@Override
			public void onMalformedURLException(MalformedURLException e) {
				// TODO Auto-generated method stub
				hideLoading();
				Log.d("login-response-m", e.getMessage());
			}

			@Override
			public void onIOException(IOException e) {
				// TODO Auto-generated method stub
				hideLoading();
				Toast.makeText(Global.login, "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				Log.d("login-response-i", e.getMessage());
			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e) {
				// TODO Auto-generated method stub
				hideLoading();
				Log.d("login-response-f", e.getMessage());
			}

			@Override
			public void onComplete(String response) {
				hideLoading();
				if (response.equals("FALSE")) {
					Global.createAlert(Global.login,
							"Incorrect username/password!", "OK");
				} else {
					login(response);
				}
				Log.d("login-response-c", response);
			}
		});
	}

	public void onClick(View v) {
		if (v.getId() == R.id.btn_offline) {
			tracker.trackEvent("click", "Button", "Offline Mode", 0);
			Intent postThread = new Intent(this, PostThread.class);
			startActivity(postThread);
		}
	}

	private void hideLoading() {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				loginProgress.hide();
			}
		});
	}

	// private class LoginTask extends
	// AsyncTask<MultipartEntity, Void, LinkedList<Object>> {
	// protected LinkedList<Object> doInBackground(MultipartEntity... param) {
	// ServerConnection con = new ServerConnection(param[0]);
	// con.requestXML("login");
	// return XMLHandler.threadList;
	// }
	//
	// protected void onPostExecute(LinkedList<Object> result) {
	// try {
	// System.out.println(XMLHandler.threadList.get(0).toString());
	// if (result.get(0).equals("success")) {
	// loginProgress.dismiss();
	// login();
	// } else {
	// loginProgress.hide();
	// Global.createAlert(Global.login, result.get(2).toString(), "OK");
	// }
	// } catch (Exception e) {
	// loginProgress.hide();
	// Toast.makeText(
	// Global.login,
	// "Connection error! Please retry", Toast.LENGTH_LONG).show();
	// e.printStackTrace();
	// }
	// }
	// }

	private void login(String token) {
		Global.username = txtUsername.getText().toString();
		Global.token = token;
		Editor e = getPreferences(Context.MODE_PRIVATE).edit();
		e.putString("username", txtUsername.getText().toString());
		e.putString("password", txtPassword.getText().toString());
		e.commit();
		tracker.stop();
		finish();
		Intent tabGroup = new Intent();
		tabGroup.setClass(this, TabHostGroup.class);
		startActivity(tabGroup);
	}
}

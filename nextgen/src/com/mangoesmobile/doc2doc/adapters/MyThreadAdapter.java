/**
 * 
 */
package com.mangoesmobile.doc2doc.adapters;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.foound.widget.AmazingAdapter;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.cache.BoardMessageCacheManager;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.ImageLoader;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class MyThreadAdapter extends AmazingAdapter implements Filterable {
	Bundle data;
	ArrayList<String> threadId; // keeps track of id of the threads which are
								// added to data
	ArrayList<String> originalData;
	Filter filter;
	Activity context;
	String table;
	LayoutInflater inflater;
	ImageLoader imageLoader;

	private Object lock = new Object(); // used for performing synchoronized
										// filtering

	public MyThreadAdapter(Activity context, String table) {
		this.data = new Bundle();
		this.threadId = new ArrayList<String>();
		this.originalData = new ArrayList<String>();
		this.filter = new BoardFilter();
		this.context = context;
		this.table = table;
		inflater = context.getLayoutInflater();
		imageLoader = new ImageLoader(context);
	}

	public void setData(Bundle bundle) {
		Log.d("board-adapter", "Bundle size: " + bundle.size());
		for (int i = bundle.size() - 1; i >= 0; i--) {
			Bundle content = bundle.getBundle(String.valueOf(i));
			// Log.d("board-adapter", "content id: " + content.getString("id"));
			Bundle old = data.getBundle(content.getString("id"));
			data.putBundle(content.getString("id"), content);
			// if the bundle was there before, remove the id from that position
			// and add it to the beginning of threadId
			if (old != null) {
				threadId.remove(content.getString("id"));
			}
			threadId.add(0, content.getString("id"));

		}
		updateCache();
		originalData = threadId;
		notifyDataSetChanged();
		notifyMayHaveMorePages();
	}

	private void updateCache() {
		BoardMessageCacheManager manager = new BoardMessageCacheManager(
				context, table);
		manager.open();
		manager.deleteAllContents();
		for (int i = 0; i < threadId.size(); i++) {
			Bundle content = data.getBundle(threadId.get(i));
			manager.insertContent(
					content.getString("id"),
					content.getString("subject"),
					content.getString("contents"),
					content.getString("timestamp"),
					content.getString("status"),
					content.getString("user_id"),
					content.getString("username"),
					content.getString("profile_pic"),
					content.getString("total_num_of_replies"),
					content.getString("reply_contents"),
					content.getString("reply_author"),
					content.getString("reply_time"),
					content.containsKey("attached_files") ? content
							.getString("attached_files") : "",
					content.containsKey("tagged_keywords") ? content
							.getString("tagged_keywords") : "",
					content.containsKey("tagged_users") ? content
							.getString("tagged_users") : "");
		}
		manager.close();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return threadId.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.getBundle(threadId.get(arg0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#onNextPageRequested(int)
	 */
	@Override
	protected void onNextPageRequested(int page) {
		// TODO Auto-generated method stub
		Log.d("board-adapter-pages", "Got onNextPageRequested page = " + page);
		fetchThread(page);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.foound.widget.AmazingAdapter#bindSectionHeader(android.view.View,
	 * int, boolean)
	 */
	@Override
	protected void bindSectionHeader(View view, int position,
			boolean displaySectionHeader) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getAmazingView(int,
	 * android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getAmazingView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		View row = convertView;
		if (row == null) {
			row = inflater.inflate(R.layout.row_board, null);
			holder = new ViewHolder();
			holder.progUserImage = (ProgressBar) row
					.findViewById(R.id.prog_mainthread_userimage);
			holder.imgThreadPhoto = (ImageView) row
					.findViewById(R.id.img_mainthread_userimage);
			holder.lblTitle = (TextView) row
					.findViewById(R.id.lbl_mainthread_title);
			holder.lblReplies = (TextView) row
					.findViewById(R.id.lbl_mainthread_replies);
			holder.lblAuthor = (TextView) row
					.findViewById(R.id.lbl_mainthread_author);
			holder.lblResponse = (TextView) row
					.findViewById(R.id.lbl_mainthread_response);
			holder.lblLatestReply = (TextView) row
					.findViewById(R.id.lbl_mainthread_latestreply);
			holder.lblReplyAuthor = (TextView) row
					.findViewById(R.id.lbl_mainthread_replyauthor);
			holder.lblReplyTime = (TextView) row
					.findViewById(R.id.lbl_mainthread_replytime);
			holder.imgThreadStatus = (ImageView) row
					.findViewById(R.id.img_mainthread_threadstatus);
			row.setTag(holder);
		} else {
			holder = (ViewHolder) row.getTag();
		}

		Bundle content = (Bundle) getItem(position);
		holder.lblTitle.setText(content.getString("subject"));
		holder.lblReplies.setText("("
				+ content.getString("total_num_of_replies") + ")");
		holder.lblAuthor.setText("by " + content.getString("username") + " on "
				+ Global.formatDate(content.getString("timestamp")));
		if (content.containsKey("reply_contents")) {
			holder.lblResponse.setText("RE: ");
			holder.lblLatestReply.setText("\""
					+ content.getString("reply_contents") + "\"");
			holder.lblReplyAuthor.setText(" by "
					+ content.getString("reply_author"));
			holder.lblReplyTime.setText(" on "
					+ Global.formatDate(content.getString("reply_time")));
		} else {
			holder.lblResponse.setText("No one responded yet");
			holder.lblLatestReply.setText("");
			holder.lblReplyAuthor.setText("");
			holder.lblReplyTime.setText("");
		}
		if(content.getString("status").equals("new")) {
			holder.imgThreadStatus.setVisibility(View.VISIBLE);
		} else {
			holder.imgThreadStatus.setVisibility(View.GONE);
		}
//		Global.loadPhoto(context, Global.url + content.getString("profile_pic"), holder.imgThreadPhoto, holder.progUserImage);
		imageLoader.DisplayImage(Global.url + content.getString("profile_pic"), holder.imgThreadPhoto, holder.progUserImage);
		return row;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.foound.widget.AmazingAdapter#configurePinnedHeader(android.view.View,
	 * int, int)
	 */
	@Override
	public void configurePinnedHeader(View header, int position, int alpha) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getPositionForSection(int)
	 */
	@Override
	public int getPositionForSection(int section) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getSectionForPosition(int)
	 */
	@Override
	public int getSectionForPosition(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getSections()
	 */
	@Override
	public Object[] getSections() {
		// TODO Auto-generated method stub
		return null;
	}

	private void fetchThread(int page) {
		Doc2DocRequest dr = new Doc2DocRequest();
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", Global.token);
		params.putString("page_no", String.valueOf(page));

		asyncTask.runTask("thread/fetch_my_threads", params,
				new Doc2DocTaskListener() {

					@Override
					public void onMalformedURLException(MalformedURLException e) {
						// TODO Auto-generated method stub
						mHandler.sendMessage(Message.obtain(mHandler,
								Global.ERROR_RESPONSE));
						Log.d("board-response", e.getMessage());
					}

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						mHandler.sendMessage(Message.obtain(mHandler,
								Global.CONNECTION_ERROR));
						Log.d("board-response", e.getMessage());
					}

					@Override
					public void onFileNotFoundException(FileNotFoundException e) {
						// TODO Auto-generated method stub
						mHandler.sendMessage(Message.obtain(mHandler,
								Global.ERROR_RESPONSE));
						Log.d("board-response", e.getMessage());
					}

					@Override
					public void onComplete(String response) {
						Log.d("board-response-json", response);
						if (!response.equals("FALSE")) {
							parseJson(response);
						} else {
							mHandler.sendMessage(Message.obtain(mHandler,
									Global.INVALID_SESSION));
						}
					}
				});
	}

	private void parseJson(String response) {
		JSONArray arr = null;
		try {
			arr = new JSONArray(response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mHandler.sendMessage(Message
					.obtain(mHandler, Global.ERROR_RESPONSE));
			return;
		}
		if (arr.length() == 0) {
			mHandler.sendMessage(Message
					.obtain(mHandler, Global.EMPTY_RESPONSE));
			return;
		}
		for (int i = 0; i < arr.length(); i++) {
			Bundle content = new Bundle();
			JSONObject json;
			try {
				json = arr.getJSONObject(i);
				content.putString("id", json.getString("id"));
				content.putString("subject", json.getString("subject"));
				content.putString("contents", json.getString("contents"));
				content.putString("timestamp", json.getString("timestamp"));
				content.putString("status", json.getString("read_status"));
				// if (json.has("last_reply_time")) {
				// content.putString("last_reply_time",
				// json.getString("last_reply_time"));
				// }
				JSONObject author = json.getJSONObject("author");
				content.putString("user_id", author.getString("user_id"));
				content.putString("username", author.getString("username"));
				content.putString("profile_pic", author.getString("profile_pic_file_path"));
				content.putString("total_num_of_replies",
						json.getString("total_num_of_replies"));
				if (json.has("replies")) {
					JSONArray rep = json.getJSONArray("replies");
					JSONObject replies = rep.getJSONObject(0);
					content.putString("reply_contents",
							replies.getString("contents"));
					JSONObject reply_author = replies.getJSONObject("author");
					content.putString("reply_author",
							reply_author.getString("username"));
					content.putString("reply_time",
							replies.getString("timestamp"));
				}
				// adds attatchments if exist
				if (json.has("$attached_files")) {
					JSONArray attachments = json
							.getJSONArray("$attached_files");
					String file_urls = "";
					for (int j = 0; j < attachments.length(); j++) {
						file_urls += attachments.getJSONObject(j).getString(
								"file_path");
						if (j != attachments.length() - 1)
							file_urls += ",";
					}
					content.putString("attached_files", file_urls);
				}
				// adds tagged keywords if exists
				if (json.has("tagged_keywords")) {
					JSONArray keywords = json.getJSONArray("tagged_keywords");
					String tagged_keywords = "";
					for (int j = 0; j < keywords.length(); j++) {
						tagged_keywords += keywords.getString(j);
						if (j != keywords.length() - 1)
							tagged_keywords += ",";
					}
					content.putString("tagged_keywords", tagged_keywords);
				}
				// adds tagged users if exists
				if (json.has("tagged_users")) {
					JSONArray tags = json.getJSONArray("tagged_users");
					String tagged_users = "";
					for (int j = 0; j < tags.length(); j++) {
						tagged_users += tags.getJSONObject(j).getString(
								"username");
						if (j != tags.length() - 1)
							tagged_users += ",";
					}
					content.putString("tagged_users", tagged_users);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// Log.d("board-adapter", "content id: " + content.getString("id"));
			Bundle old = data.getBundle(content.getString("id"));
			data.putBundle(content.getString("id"), content);
			// if the bundle was there before, remove the id from that position
			// and add it to the end of threadId
			if (old != null) {
				threadId.remove(content.getString("id"));
			}
			threadId.add(content.getString("id"));
		}
		updateCache();
		mHandler.sendMessage(Message.obtain(mHandler, Global.GOT_RESPONSE));
	}

	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				nextPage();
				notifyDataSetChanged();
				notifyMayHaveMorePages();
				break;
			case Global.EMPTY_RESPONSE:
				notifyNoMorePages();
				break;
			case Global.CONNECTION_ERROR:
				Toast.makeText(context, "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				notifyMayHaveMorePages();
				break;
			case Global.ERROR_RESPONSE:
				notifyMayHaveMorePages();
				break;
			case Global.INVALID_SESSION:
				Toast.makeText(context, "Invalid session! Please relogin", Toast.LENGTH_LONG).show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				context.sendBroadcast(i);
				break;
			}
		}
	};

	static class ViewHolder {
		public ProgressBar progUserImage;
		public ImageView imgThreadPhoto;
		public TextView lblTitle;
		public TextView lblReplies;
		public TextView lblAuthor;
		public TextView lblResponse;
		public TextView lblLatestReply;
		public TextView lblReplyAuthor;
		public TextView lblReplyTime;
		public ImageView imgThreadStatus;
	}

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		if (filter == null) {
			filter = new BoardFilter();
		}
		return filter;
	}

	private class BoardFilter extends Filter {

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			// TODO Auto-generated method stub
			String query = constraint.toString().toLowerCase();
			FilterResults results = new FilterResults();
			ArrayList<String> temp;
			synchronized (lock) {
				temp = new ArrayList<String>(originalData);
			}
			if (query == null || query.length() == 0) {
				synchronized (lock) {
					results.values = originalData;
					results.count = originalData.size();
				}
			} else {
				ArrayList<String> filtered = new ArrayList<String>();
				for (int i = 0; i < temp.size(); i++) {
					String id = temp.get(i);
					Bundle content = data.getBundle(id);
					if (content.getString("subject").toLowerCase()
							.contains(query)
							|| content.getString("contents").toLowerCase()
									.contains(query)
							|| content.getString("username").toLowerCase()
									.contains(query)) {
						filtered.add(id);
					} else if (content.containsKey("tagged_keywords")) {
						if (content.getString("tagged_keywords").toLowerCase()
								.contains(query))
							filtered.add(id);
					} else if (content.containsKey("tagged_users")) {
						if (content.getString("tagged_users").toLowerCase()
								.contains(query))
							filtered.add(id);
					}
				}
				results.values = filtered;
				results.count = filtered.size();
			}
			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			// TODO Auto-generated method stub
			threadId = (ArrayList<String>) results.values;
			notifyDataSetChanged();
			if (constraint.length() > 0) {
				notifyNoMorePages();
			} else {
				notifyMayHaveMorePages();
			}
		}

	}

}

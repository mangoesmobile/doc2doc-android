/**
 * 
 */
package com.mangoesmobile.doc2doc.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.foound.widget.AmazingAdapter;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.cache.TeamCacheManager;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.ImageLoader;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class TagAdapter extends AmazingAdapter {

	Activity context;
	Bundle data;
	ArrayList<String> userId;
	ArrayList<String> originalData;
	LayoutInflater inflater;
	ImageLoader imageLoader;

	public TagAdapter(Activity context) {
		this.context = context;
		this.data = new Bundle();
		this.userId = new ArrayList<String>();
		this.originalData = new ArrayList<String>();
		inflater = context.getLayoutInflater();
		imageLoader = new ImageLoader(context);
	}

	public void setData(Bundle bundle) {
		Log.d("tag-adapter", "Bundle size: " + bundle.size());
//		for (int i = bundle.size() - 1; i >= 0; i--) {
//			Bundle content = bundle.getBundle(String.valueOf(i));
//			Log.d("tag-adapter", "content id: " + content.getString("user_id"));
//			if (!content.getString("user_id").equals("null")) { // this is
//																// temporary.
//				// this condition
//				// should be removed
//				Bundle old = data.getBundle(content.getString("user_id"));
//				data.putBundle(content.getString("user_id"), content);
//				// if the bundle was there before, remove the id from that
//				// position
//				// and add it to the beginning of threadId
//				if (old != null) {
//					userId.remove(content.getString("user_id"));
//				}
//				userId.add(0, content.getString("user_id"));
//			}
//
//		}
		data = new Bundle();
		userId = new ArrayList<String>();
		for (int i = 0; i < bundle.size(); i++) {
			Bundle content = bundle.getBundle(String.valueOf(i));
			data.putBundle(content.getString("user_id"), content);
			userId.add(content.getString("user_id"));
		}
		updateCache();
		originalData = userId;
		notifyDataSetChanged();
		notifyNoMorePages();
	}

	private void updateCache() {
		TeamCacheManager manager = new TeamCacheManager(context);
		manager.open();
		manager.deleteAllContents();
		for (int i = 0; i < userId.size(); i++) {
			Bundle content = data.getBundle(userId.get(i));
			manager.insertContent(content.getString("user_id"),
					content.getString("first_name"),
					content.getString("last_name"), content.getString("title"),
					content.getString("email"), content.getString("phone"),
					content.getString("profile_pic_file_path"),
					content.getString("organization"),
					content.getString("specialization"),
					content.getString("username"),
					content.getString("availability_status"),
					content.getString("latitude"),
					content.getString("longitude"));
		}
		manager.close();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return userId.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.getBundle(userId.get(arg0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#onNextPageRequested(int)
	 */
	@Override
	protected void onNextPageRequested(int page) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.foound.widget.AmazingAdapter#bindSectionHeader(android.view.View,
	 * int, boolean)
	 */
	@Override
	protected void bindSectionHeader(View view, int position,
			boolean displaySectionHeader) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getAmazingView(int,
	 * android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getAmazingView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		View row = convertView;
		if (row == null) {
			row = inflater.inflate(R.layout.row_teamlist,
					null);
			row.setBackgroundResource(R.drawable.row_check);
			holder = new ViewHolder();
			holder.progUserImage = (ProgressBar) row
					.findViewById(R.id.prog_teamlist_userimage);
			holder.imgTeamPhoto = (ImageView) row
					.findViewById(R.id.img_teamlist_user);
			holder.imgTeamStatus = (ImageView) row
					.findViewById(R.id.img_teamlist_userstatus);
			holder.lblName = (TextView) row
					.findViewById(R.id.lbl_teamlist_name);
			holder.lblSpecialization = (TextView) row
					.findViewById(R.id.lbl_teamlist_specialization);
			holder.lblLocation = (TextView) row
					.findViewById(R.id.lbl_teamlist_location);
			holder.lblUserStatus = (TextView) row
					.findViewById(R.id.lbl_teamlist_userstatus);
			holder.layoutMessaging = (LinearLayout) row
					.findViewById(R.id.layout_teamlist_messaging);
			holder.chkUserCheck = (CheckBox) row
					.findViewById(R.id.chk_teamlist_selected);
			holder.lblUserStatus.setVisibility(View.GONE);
			holder.layoutMessaging.setVisibility(View.GONE);
			holder.chkUserCheck.setVisibility(View.VISIBLE);
			row.setTag(holder);
		} else {
			holder = (ViewHolder) row.getTag();
		}

		Bundle content = (Bundle) getItem(position);
		int status;
		String user_status = content.getString("availability_status");
		if(user_status.equals("online")) {
			status = R.drawable.bg_photo_online;
			holder.lblUserStatus.setTextColor(Color.rgb(0, 102, 153));
		} else if(user_status.equals("on call")) {
			status = R.drawable.bg_photo_oncall;
			holder.lblUserStatus.setTextColor(Color.rgb(101, 176, 73));
		} else {
			status = R.drawable.bg_photo_offline;
			holder.lblUserStatus.setTextColor(Color.DKGRAY);
		}
		holder.imgTeamStatus.setImageResource(status);
		holder.lblName.setText(content.getString("title") + " "
				+ content.getString("first_name") + " "
				+ content.getString("last_name"));
		holder.lblSpecialization.setText(content.getString("specialization")
				.equals("null") ? "" : content.getString("specialization"));
		holder.lblLocation.setText(content.getString("organization").equals(
				"null") ? "" : content.getString("organization"));
		holder.chkUserCheck.setChecked(content.getBoolean("checked"));
//		Global.loadPhoto(context, Global.url + content.getString("profile_pic_file_path"), holder.imgTeamPhoto, holder.progUserImage);
		imageLoader.DisplayImage(Global.url + content.getString("profile_pic_file_path"), holder.imgTeamPhoto, holder.progUserImage);
		row.setBackgroundResource(Global.getListItemBackground(content.getBoolean("checked")));
		return row;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.foound.widget.AmazingAdapter#configurePinnedHeader(android.view.View,
	 * int, int)
	 */
	@Override
	public void configurePinnedHeader(View header, int position, int alpha) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getPositionForSection(int)
	 */
	@Override
	public int getPositionForSection(int section) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getSectionForPosition(int)
	 */
	@Override
	public int getSectionForPosition(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getSections()
	 */
	@Override
	public Object[] getSections() {
		// TODO Auto-generated method stub
		return null;
	}

	static class ViewHolder {
		public ProgressBar progUserImage;
		public ImageView imgTeamPhoto;
		public ImageView imgTeamStatus;
		public TextView lblName;
		public TextView lblSpecialization;
		public TextView lblLocation;
		public TextView lblUserStatus;
		public LinearLayout layoutMessaging;
		public CheckBox chkUserCheck;
	}

}

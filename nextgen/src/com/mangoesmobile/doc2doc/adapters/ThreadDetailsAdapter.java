/**
 * 
 */
package com.mangoesmobile.doc2doc.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.foound.widget.AmazingAdapter;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.ActivityGroupBoard;
import com.mangoesmobile.doc2doc.ActivityGroupMessages;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.contents.ViewAttachments;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.ImageLoader;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class ThreadDetailsAdapter extends AmazingAdapter {
	Bundle data;
	ArrayList<String> replyId;
	Activity context;
	LayoutInflater inflater;
	ImageLoader imageLoader;
	GoogleAnalyticsTracker tracker;
	
	public ThreadDetailsAdapter(Activity context) {
		this.data = new Bundle();
		this.replyId = new ArrayList<String>();
		this.context = context;
		inflater = context.getLayoutInflater();
		imageLoader = new ImageLoader(context);
		tracker = GoogleAnalyticsTracker.getInstance();
	}

	public void setData(Bundle bundle) {
		Log.d("board-details-adapter", "Bundle size: " + bundle.size());
		replyId = new ArrayList<String>();
		for (int i = 0; i < bundle.size(); i++) {
			Bundle content = bundle.getBundle(String.valueOf(i));
			data.putBundle(content.getString("id"), content);
			replyId.add(content.getString("id"));
		}
		notifyDataSetChanged();
		// notifyMayHaveMorePages();
		notifyNoMorePages();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.getBundle(replyId.get(arg0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#onNextPageRequested(int)
	 */
	@Override
	protected void onNextPageRequested(int page) {
		// TODO Auto-generated method stub
		// do nothing for now because we are not using pagination
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.foound.widget.AmazingAdapter#bindSectionHeader(android.view.View,
	 * int, boolean)
	 */
	@Override
	protected void bindSectionHeader(View view, int position,
			boolean displaySectionHeader) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getAmazingView(int,
	 * android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getAmazingView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		View row = convertView;
		if (row == null) {
			row = inflater.inflate(R.layout.row_showthread,
					null);
			holder = new ViewHolder();
			holder.imgUserImage = (ImageView) row
					.findViewById(R.id.img_showthread_user);
			holder.progUserImage = (ProgressBar) row
					.findViewById(R.id.prog_showthread_userimage);
			holder.lblReply = (TextView) row
					.findViewById(R.id.lbl_showthread_userreply);
			holder.lblAuthor = (TextView) row
					.findViewById(R.id.lbl_showthread_replyauthor);
			holder.btnAttachments = (Button) row
					.findViewById(R.id.btn_showthread_replyattachment);
			holder.btnLocation = (Button) row
					.findViewById(R.id.btn_showthread_replylocation);
			row.setTag(holder);
		} else {
			holder = (ViewHolder) row.getTag();
		}
		Bundle content = (Bundle) getItem(position);
		holder.lblReply.setText(content.getString("contents"));
		holder.lblAuthor.setText("by " + content.getString("username") + " on "
				+ Global.formatDate(content.getString("timestamp")));
		if(content.containsKey("attached_files")) {
			holder.btnAttachments.setOnClickListener(new OnAttachmentClickListener(content.getString("attached_files")));
			holder.btnAttachments.setVisibility(View.VISIBLE);
		} else {
			holder.btnAttachments.setVisibility(View.GONE);
		}
//		Global.loadPhoto(context,
//				Global.url + content.getString("profile_pic"),
//				holder.imgUserImage, holder.progUserImage);
		imageLoader.DisplayImage(Global.url + content.getString("profile_pic"), holder.imgUserImage, holder.progUserImage);
		return row;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.foound.widget.AmazingAdapter#configurePinnedHeader(android.view.View,
	 * int, int)
	 */
	@Override
	public void configurePinnedHeader(View header, int position, int alpha) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getPositionForSection(int)
	 */
	@Override
	public int getPositionForSection(int section) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getSectionForPosition(int)
	 */
	@Override
	public int getSectionForPosition(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.foound.widget.AmazingAdapter#getSections()
	 */
	@Override
	public Object[] getSections() {
		// TODO Auto-generated method stub
		return null;
	}

	static class ViewHolder {
		public ProgressBar progUserImage;
		public ImageView imgUserImage;
		public TextView lblReply;
		public TextView lblAuthor;
		public Button btnAttachments;
		public Button btnLocation;
	}
	
	class OnAttachmentClickListener implements OnClickListener {
		
		String attachments;
		
		public OnAttachmentClickListener(String attachments) {
			// TODO Auto-generated constructor stub
			this.attachments = attachments;
		}
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			Intent intent = new Intent(context,
					ViewAttachments.class);
			intent.putExtra("attached_files", attachments);
			if (context.getParent() instanceof ActivityGroupBoard) {
				ActivityGroupBoard parent = (ActivityGroupBoard) context.getParent();
				parent.startChildActivity("view_attachment", intent);
			} else {
				ActivityGroupMessages parent = (ActivityGroupMessages) context.getParent();
				parent.startChildActivity("view_attachment", intent);
			}
			tracker.trackEvent("Click", "Button", "Launch View Attachments", 0);
		}
		
	}

}

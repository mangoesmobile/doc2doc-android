package com.mangoesmobile.doc2doc.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;


import android.os.Bundle;

public class AsyncDoc2DocTask {

	Doc2DocRequest dr;
	
	public AsyncDoc2DocTask(Doc2DocRequest dr) {
		this.dr = dr;
	}
	
	public void runTask(final String action, final Bundle params, final Doc2DocTaskListener listener) {
		new Thread() {
            @Override 
            public void run() {
                try {
                	String response = dr.request(action, params);
                    listener.onComplete(response);
                } catch (FileNotFoundException e) {
                    listener.onFileNotFoundException(e);
                } catch (MalformedURLException e) {
                    listener.onMalformedURLException(e);
                } catch (IOException e) {
                    listener.onIOException(e);
                }
            }
        }.start();
	}
	
	public interface Doc2DocTaskListener {
		
		/**
         * Called when a request completes with the given response.
         *
         * Executed by a background thread: do not update the UI in this method.
         */
		public void onComplete(String response);
		
		/**
         * Called when a request has a network or request error.
         *
         * Executed by a background thread: do not update the UI in this method.
         */
		public void onIOException(IOException e);

        /**
         * Called when a request fails because the requested resource is
         * invalid or does not exist.
         *
         * Executed by a background thread: do not update the UI in this method.
         */
        public void onFileNotFoundException(FileNotFoundException e);

        /**
         * Called if an invalid action is provided (which may result in a
         * malformed URL).
         *
         * Executed by a background thread: do not update the UI in this method.
         */
        public void onMalformedURLException(MalformedURLException e);
	}
	
}

package com.mangoesmobile.doc2doc;

import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.services.Doc2DocService;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class Doc2DocLauncher extends Activity {

	protected boolean _active = true;
	protected int _splashTime = 3000; // time to display the splash screen in ms

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);		
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		if (!Doc2DocService.isRunning()) {
			// thread for displaying the SplashScreen
			Thread splashTread = new Thread() {
				@Override
				public void run() {
					try {
						int waited = 0;
						while (_active && (waited < _splashTime)) {
							sleep(100);
							if (_active) {
								waited += 100;
							}
						}
					} catch (InterruptedException e) {
						// do nothing
					} finally {
						finish();
						Intent login = new Intent(Doc2DocLauncher.this,
								Login.class);
						startActivity(login);
					}
				}
			};
			splashTread.start();
		} else {
			finish();
			Intent afterLogin = new Intent(Doc2DocLauncher.this,
					D2DTabHost.class);
			startActivity(afterLogin);
		}
	}
}
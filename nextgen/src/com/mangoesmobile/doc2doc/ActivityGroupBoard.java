/**
 * 
 */
package com.mangoesmobile.doc2doc;

import com.mangoesmobile.doc2doc.contents.Board;

import android.content.Intent;
import android.os.Bundle;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class ActivityGroupBoard extends ActivityGroupWorkaround {

	public static final String GOT_ATTACHMENT = "GOT_ATTACHMENT";
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub
		startChildActivity("board", new Intent(ActivityGroupBoard.this,
				Board.class));
	}

}

/**
 * 
 */
package com.mangoesmobile.doc2doc;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Map;
import java.util.Stack;

import com.mangoesmobile.doc2doc.contents.AttachFile;
import com.mangoesmobile.doc2doc.contents.UserLocation;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

/**
 * The purpose of this Activity is to manage the activities in a tab. Note:
 * Child Activities can handle Key Presses before they are seen here.
 * 
 * @author Eric Harlow
 */
public class ActivityGroupWorkaround extends ActivityGroup {
	
	private Stack<String> mIdList;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (mIdList == null)
			mIdList = new Stack<String>();
	}

	/**
	 * This is called when a child activity of this one calls its finish method.
	 * This implementation calls {@link LocalActivityManager#destroyActivity} on
	 * the child activity and starts the previous activity. If the last child
	 * activity just called finish(),this activity (the parent), calls finish to
	 * finish the entire group.
	 */
	@Override
	public void finishFromChild(Activity child) {
		if (mIdList.size() > 1) {
			LocalActivityManager manager = getLocalActivityManager();
			destroy(mIdList.pop());
			try {
				Intent lastIntent = manager.getActivity(mIdList.peek())
						.getIntent();
				Window newWindow = manager.startActivity(mIdList.peek(),
						lastIntent);
				setContentView(newWindow.getDecorView());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			Intent intent = new Intent();
			intent.setAction(D2DTabHost.CLOSE_TAB);
			sendBroadcast(intent);
		}
	}

	/**
	 * Starts an Activity as a child Activity to this.
	 * 
	 * @param Id
	 *            Unique identifier of the activity to be started.
	 * @param intent
	 *            The Intent describing the activity to be started.
	 * @throws android.content.ActivityNotFoundException.
	 */
	public void startChildActivity(String Id, Intent intent) {
		Window window = getLocalActivityManager().startActivity(Id, intent);
		if (window != null) {
			mIdList.push(Id);
			setContentView(window.getDecorView());
		}
	}

	public boolean destroy(String id) {
		LocalActivityManager manager = getLocalActivityManager();
		if (manager != null) {
			manager.destroyActivity(id, true);
			try {
				final Field mActivitiesField = LocalActivityManager.class
						.getDeclaredField("mActivities");
				if (mActivitiesField != null) {
					mActivitiesField.setAccessible(true);
					@SuppressWarnings("unchecked")
					final Map<String, Object> mActivities = (Map<String, Object>) mActivitiesField
							.get(manager);
					if (mActivities != null) {
						mActivities.remove(id);
					}
					final Field mActivityArrayField = LocalActivityManager.class
							.getDeclaredField("mActivityArray");
					if (mActivityArrayField != null) {
						mActivityArrayField.setAccessible(true);
						@SuppressWarnings("unchecked")
						final ArrayList<Object> mActivityArray = (ArrayList<Object>) mActivityArrayField
								.get(manager);
						if (mActivityArray != null) {
							for (Object record : mActivityArray) {
								final Field idField = record.getClass()
										.getDeclaredField("id");
								if (idField != null) {
									idField.setAccessible(true);
									final String _id = (String) idField
											.get(record);
									if (id.equals(_id)) {
										mActivityArray.remove(record);
										break;
									}
								}
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		Activity activity = getLocalActivityManager().getCurrentActivity();
		if(activity instanceof AttachFile) {
			AttachFile attachFile = AttachFile.getInstance();
			attachFile.handleActivityResult(requestCode, resultCode, intent);
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
		Activity current = getLocalActivityManager().getActivity(
				mIdList.peek());
		if(current instanceof UserLocation) {
			current.finish();
		}
	}

//	/**
//	 * The primary purpose is to prevent systems before
//	 * android.os.Build.VERSION_CODES.ECLAIR from calling their default
//	 * KeyEvent.KEYCODE_BACK during onKeyDown.
//	 */
//	@Override
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//		if (keyCode == KeyEvent.KEYCODE_BACK) {
//			// preventing default implementation previous to
//			// android.os.Build.VERSION_CODES.ECLAIR
//			return true;
//		}
//		return super.onKeyDown(keyCode, event);
//	}
//
//	/**
//	 * Overrides the default implementation for KeyEvent.KEYCODE_BACK so that
//	 * all systems call onBackPressed().
//	 */
//	@Override
//	public boolean onKeyUp(int keyCode, KeyEvent event) {
//		if (keyCode == KeyEvent.KEYCODE_BACK) {
//			onBackPressed();
//			return true;
//		}
//		return super.onKeyUp(keyCode, event);
//	}

	/**
	 * If a Child Activity handles KeyEvent.KEYCODE_BACK. Simply override and
	 * add this method.
	 */
	@Override
	public void onBackPressed() {
		Log.d("stack-size", "stack size: " + mIdList.size());
		if (mIdList.size() > 1) {
			Activity current = getLocalActivityManager().getActivity(
					mIdList.peek());
			current.finish();
		} else {
			Intent intent = new Intent();
			intent.setAction(D2DTabHost.CLOSE_TAB);
			sendBroadcast(intent);
		}
	}	
}

/**
 * 
 */
package com.mangoesmobile.doc2doc;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.mangoesmobile.doc2doc.R;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.contents.NewThread;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;
import com.mangoesmobile.doc2doc.util.Global;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class Login extends Activity {

	GoogleAnalyticsTracker tracker;
	
	private ProgressDialog loading;
	private TextView txtUsername;
	private TextView txtPassword;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.startNewSession(Global.UA, 10, this);
		tracker.trackPageView("Login");
		
		instantiateLayout();

		SharedPreferences pref = getPreferences(Context.MODE_PRIVATE);
		txtUsername.setText(pref.getString("username", ""));
		txtPassword.setText(pref.getString("password", ""));
	}

	private void instantiateLayout() {
		loading = new ProgressDialog(this);
		txtUsername = (EditText) findViewById(R.id.txt_username);
		txtPassword = (EditText) findViewById(R.id.txt_password);
	}

	public void onClick(View v) {
		if (v.getId() == R.id.btn_login) {
			if (textEntered()) {
				Global.showLoading(loading, "Logging in...", false);
				authenticate("authenticate/login", txtUsername.getText()
						.toString(), encryptPassword(txtPassword.getText()
						.toString()));
				tracker.trackEvent("Click", "Button", "Do Login", 0);
			} else {
				Global.createAlert(Login.this,
						"Please enter username/password!", "OK");
			}
		} else if (v.getId() == R.id.btn_offline) {
			Intent intent = new Intent(Login.this, NewThread.class);
			intent.putExtra("offline", true);
			startActivity(intent);
			tracker.trackEvent("Click", "Button", "Start Offline", 0);
		}
	}

	private boolean textEntered() {
		return (txtUsername.getText().length() > 0 && txtPassword.getText()
				.length() > 0);
	}

	private String encryptPassword(String pass) {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-1");
			md.update(pass.getBytes("UTF-8"));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		byte[] raw = md.digest();
		StringBuilder string = new StringBuilder();
		for (byte b : raw) {
			String hexString = Integer.toHexString(0x00FF & b);
			string.append(hexString.length() == 1 ? "0" + hexString : hexString);
		}
		return string.toString();
	}

	private void authenticate(String action, String user, String pass) {
		Doc2DocRequest dr = new Doc2DocRequest();
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("uname", user);
		params.putString("enc_pword", pass);

		asyncTask.runTask(action, params, new Doc2DocTaskListener() {

			@Override
			public void onMalformedURLException(MalformedURLException e) {
				// TODO Auto-generated method stub
				mHandler.sendMessage(Message.obtain(mHandler,
						Global.ERROR_RESPONSE));
				Log.d("login-response", "malformed url: " + e.getMessage());
			}

			@Override
			public void onIOException(IOException e) {
				// TODO Auto-generated method stub
				mHandler.sendMessage(Message.obtain(mHandler,
						Global.CONNECTION_ERROR));
				Log.d("login-response", "connection error: " + e.getMessage());
			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e) {
				// TODO Auto-generated method stub
				mHandler.sendMessage(Message.obtain(mHandler,
						Global.ERROR_RESPONSE));
				Log.d("login-response", "file not found: " + e.getMessage());
			}

			@Override
			public void onComplete(String response) {
				Log.d("login-response", response);
				if (response.equals("FALSE")) {
					mHandler.sendMessage(Message.obtain(mHandler,
							Global.EMPTY_RESPONSE));
				} else {
					Message msg = Message.obtain();
					msg.what = Global.GOT_RESPONSE;
					msg.obj = response;
					mHandler.sendMessage(msg);
				}
			}
		});
	}

	private void login(String token) {
		Global.token = token;
		Global.userStatus = "online";
		Editor e = getPreferences(Context.MODE_PRIVATE).edit();
		e.putString("username", txtUsername.getText().toString());
		e.putString("password", txtPassword.getText().toString());
		e.commit();
		finish();
		Intent doc2docTabs = new Intent(Login.this, D2DTabHost.class);
		startActivity(doc2docTabs);
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		tracker.stopSession();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		loading.dismiss();
	}

	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				Global.hideLoading(loading);
				login((String) msg.obj);
				break;
			case Global.EMPTY_RESPONSE:
				Global.hideLoading(loading);
				Global.createAlert(Login.this, "Incorrect username/password!",
						"OK");
				break;
			case Global.CONNECTION_ERROR:
				Global.hideLoading(loading);
				Toast.makeText(Login.this, "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				break;
			case Global.ERROR_RESPONSE:
				Global.hideLoading(loading);
				break;
			case Global.INVALID_SESSION:
				Global.hideLoading(loading);
				break;
			}
		}
	};
}

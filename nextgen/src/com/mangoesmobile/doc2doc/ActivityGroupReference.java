/**
 * 
 */
package com.mangoesmobile.doc2doc;

import com.mangoesmobile.doc2doc.contents.Reference;
import android.content.Intent;
import android.os.Bundle;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class ActivityGroupReference extends ActivityGroupWorkaround {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);	
	    // TODO Auto-generated method stub
	    startChildActivity("reference", new Intent(ActivityGroupReference.this,
				Reference.class));
	}

}

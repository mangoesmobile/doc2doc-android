/**
 * 
 */
package com.mangoesmobile.doc2doc.notification;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.foound.widget.AmazingListView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.adapters.MyPingAdapter;
import com.mangoesmobile.doc2doc.cache.PingCacheManager;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class PingNotificationManager {

	GoogleAnalyticsTracker tracker;
	
	Activity context;
	AmazingListView list;
	TextView search;
	MyPingAdapter adapter;
	Bundle data;
	
	public PingNotificationManager (Activity context, AmazingListView list, TextView search) {
		this.context = context;
		this.list = list;
		this.search = search;
		adapter = new MyPingAdapter(context);
		list.setAdapter(adapter);
		adapter.notifyMayHaveMorePages();
		data = new Bundle();
		this.search.addTextChangedListener(new PingTextWatcher());
		tracker = GoogleAnalyticsTracker.getInstance();
	}
	
	public void fetchPingFromCache() {
		PingCacheManager manager = new PingCacheManager(context);
		manager.open();
		Cursor cursor = manager.fetchAll();
		context.startManagingCursor(cursor);
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			for (int i = 0; i < cursor.getCount(); i++) {
				Bundle content = new Bundle();
				content.putString("id", cursor.getString(cursor
						.getColumnIndex(PingCacheManager.ID)));
				content.putString("sender_id", cursor.getString(cursor
						.getColumnIndex(PingCacheManager.SENDER_ID)));
				content.putString("sender_name", cursor.getString(cursor
						.getColumnIndex(PingCacheManager.SENDER_NAME)));
				content.putString("sender_profile_pic_file_path", cursor.getString(cursor
						.getColumnIndex(PingCacheManager.SENDER_PRO_PIC)));
				content.putString("recipient_id", cursor.getString(cursor
						.getColumnIndex(PingCacheManager.RECIPIENT_ID)));
				content.putString("recipient_name", cursor.getString(cursor
						.getColumnIndex(PingCacheManager.RECIPIENT_NAME)));
				content.putString("recipient_profile_pic_file_path", cursor.getString(cursor
						.getColumnIndex(PingCacheManager.RECIPIENT_PRO_PIC)));
				content.putString("status", cursor.getString(cursor
						.getColumnIndex(PingCacheManager.STATUS)));
				content.putString("timestamp", cursor.getString(cursor
						.getColumnIndex(PingCacheManager.TIMESTAMP)));				
				data.putBundle(String.valueOf(i), content);
				cursor.moveToNext();
			}
			adapter.setData(data);
			adapter.setInitialPage(data.size() / 10); // it indicates that cache
			// has got this much
			// page of data
			adapter.resetPage();
		}
		cursor.close();
		manager.close();
		fetchPing();
	}
	
	public void fetchPing() {
		Doc2DocRequest dr = new Doc2DocRequest();
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", Global.token);
		params.putString("page_no", "0");

		asyncTask.runTask("ping/read", params,
				new Doc2DocTaskListener() {

					@Override
					public void onMalformedURLException(MalformedURLException e) {
						// TODO Auto-generated method stub
						mHandler.sendMessage(Message.obtain(mHandler,
								Global.ERROR_RESPONSE));
						Log.d("thread_notification-response", e.getMessage());
					}

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						mHandler.sendMessage(Message.obtain(mHandler,
								Global.CONNECTION_ERROR));
						Log.d("thread_notification-response", e.getMessage());
					}

					@Override
					public void onFileNotFoundException(FileNotFoundException e) {
						// TODO Auto-generated method stub
						mHandler.sendMessage(Message.obtain(mHandler,
								Global.ERROR_RESPONSE));
						Log.d("thread_notification-response", e.getMessage());
					}

					@Override
					public void onComplete(String response) {
						Log.d("thread_notification-response-json", response);
						if (!response.equals("FALSE")) {
							parseJson(response);
						} else {
							mHandler.sendMessage(Message.obtain(mHandler,
									Global.INVALID_SESSION));
						}
					}
				});
	}

	private void parseJson(String response) {
		JSONArray arr = null;
		try {
			arr = new JSONArray(response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mHandler.sendMessage(Message.obtain(mHandler,
					Global.ERROR_RESPONSE));
			return;
		}
		if (arr.length() == 0) {
			mHandler.sendMessage(Message.obtain(mHandler,
					Global.EMPTY_RESPONSE));
			return;
		}
		for (int i = 0; i < arr.length(); i++) {
			Bundle content = new Bundle();
			JSONObject json;
			try {
				json = arr.getJSONObject(i);
				content.putString("id", json.getString("id"));
				//sender info
				JSONObject sender = json.getJSONObject("sender");
				content.putString("sender_id", sender.getString("user_id"));
				content.putString("sender_name", sender.getString("username"));
				content.putString("sender_profile_pic_file_path", sender.getString("profile_pic_file_path"));
				//recipient info
				JSONObject recipient = json.getJSONObject("recipient");
				content.putString("recipient_id", recipient.getString("user_id"));
				content.putString("recipient_name", recipient.getString("username"));
				content.putString("recipient_profile_pic_file_path", recipient.getString("profile_pic_file_path"));
				
				content.putString("status", json.getString("status"));
				content.putString("timestamp", json.getString("timestamp"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			data.putBundle(String.valueOf(i), content);
		}
		mHandler.sendMessage(Message.obtain(mHandler,
				Global.GOT_RESPONSE));
	}

	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				adapter.setData(data);
				break;
			case Global.EMPTY_RESPONSE:
				adapter.notifyNoMorePages();
				break;
			case Global.CONNECTION_ERROR:
				Toast.makeText(context, "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				break;
			case Global.ERROR_RESPONSE:
				break;
			case Global.INVALID_SESSION:
				Toast.makeText(context, "Invalid session! Please relogin", Toast.LENGTH_LONG).show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				context.sendBroadcast(i);
				break;
			}
		}
	};	

	class PingTextWatcher implements TextWatcher {

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			adapter.getFilter().filter(s);
			tracker.trackEvent("TextChange", "EditText", "Search Message Center", 0);
		}

	}
}

/**
 * 
 */
package com.mangoesmobile.doc2doc.cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class TeamCacheManager {
	private static final String TABLE = "team";
	// database fields
	public static final String USER_ID = "user_id";
	public static final String FIRST_NAME = "first_name";
	public static final String LAST_NAME = "last_name";
	public static final String TITLE = "title";
	public static final String EMAIL = "email";
	public static final String PHONE = "phone";
	public static final String PROFILE_PIC = "profile_pic";
	public static final String ORGANIZATION = "organization";
	public static final String SPECIALIZATION = "specialization";
	public static final String USERNAME = "username";
	public static final String AVAILABILITY_STATUS = "availability_status";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	
	public static final String[] COLUMNS = new String[] {USER_ID, FIRST_NAME, LAST_NAME, TITLE, EMAIL, PHONE, PROFILE_PIC, ORGANIZATION, SPECIALIZATION, USERNAME, AVAILABILITY_STATUS, LATITUDE, LONGITUDE};
	
	private Context context;
	private SQLiteDatabase db;
	private CacheHelper helper;
	
	public TeamCacheManager(Context context) {
		this.context = context;
	}
	
	public TeamCacheManager open() throws SQLException {
		helper = new CacheHelper(context);
		db = helper.getWritableDatabase();
		return this;
	}
	
	public void close() {
		helper.close();
	}
	
	public long insertContent(String user_id, String first_name, String last_name, String title, String email, String phone, String pro_pic_filename, String organization, String specialization, String username, String availability_status, String latitude, String longitude) {
		ContentValues values = createContentValues(user_id, first_name, last_name, title, email, phone, pro_pic_filename, organization, specialization, username, availability_status, latitude, longitude);
		return db.insert(TABLE, null, values);
	}
	
	public int deleteAllContents() {
		return db.delete(TABLE, null, null);
	}
	
	public boolean deleteContent(String id) {
		return db.delete(TABLE, USER_ID + "=?", new String[] {id}) > 0;
	}
	
	public Cursor fetchAll() {
		return db.query(TABLE, COLUMNS, null, null, null, null, null);
	}
	
	public Cursor fetchRow(String id) throws SQLException {
		return db.query(TABLE, COLUMNS, USER_ID + "=?", new String[] {id}, null, null, null);
	}
	
	private ContentValues createContentValues(String user_id, String first_name, String last_name, String title, String email, String phone, String pro_pic_filename, String organization, String specialization, String username, String availability_status, String latitude, String longitude) {
		ContentValues values = new ContentValues();
		values.put(USER_ID, user_id);
		values.put(FIRST_NAME, first_name);
		values.put(LAST_NAME, last_name);
		values.put(TITLE, title);
		values.put(EMAIL, email);
		values.put(PHONE, phone);
		values.put(PROFILE_PIC, pro_pic_filename);
		values.put(ORGANIZATION, organization);
		values.put(SPECIALIZATION, specialization);
		values.put(USERNAME, username);
		values.put(AVAILABILITY_STATUS, availability_status);
		values.put(LATITUDE, latitude);
		values.put(LONGITUDE, longitude);
		return values;
	}
}

/**
 * 
 */
package com.mangoesmobile.doc2doc.cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class BoardMessageCacheManager {
	
	private String table;
	// database fields
	public static final String ID = "id";
	public static final String SUBJECT = "subject";
	public static final String CONTENTS = "contents";
	public static final String TIMESTAMP = "timestamp";
	public static final String STATUS = "status";
	public static final String USER_ID = "user_id";
	public static final String USERNAME = "username";
	public static final String PROFILE_PIC = "profile_pic";
	public static final String TOTAL_NUM_OF_REPLIES = "total_num_of_replies";
	public static final String REPLY_CONTENTS = "reply_contents";
	public static final String REPLY_AUTHOR = "reply_author";
	public static final String REPLY_TIME = "reply_time";
	public static final String ATTACHED_FILES = "attached_files";
	public static final String TAGGED_KEYWORDS = "tagged_keywords";
	public static final String TAGGED_USERS = "tagged_users";
	
	public static final String[] COLUMNS = new String[] {ID, SUBJECT, CONTENTS, TIMESTAMP, STATUS, USER_ID, USERNAME, PROFILE_PIC, TOTAL_NUM_OF_REPLIES, REPLY_CONTENTS, REPLY_AUTHOR, REPLY_TIME, ATTACHED_FILES, TAGGED_KEYWORDS, TAGGED_USERS};
	
	private Context context;
	private SQLiteDatabase db;
	private CacheHelper helper;
	
	public BoardMessageCacheManager(Context context, String table) {
		this.context = context;
		this.table = table;
	}
	
	public BoardMessageCacheManager open() throws SQLException {
		helper = new CacheHelper(context);
		db = helper.getWritableDatabase();
		return this;
	}
	
	public void close() {
		helper.close();
	}
	
	public long insertContent(String id, String subject, String contents, String timestamp, String status, String user_id, String username, String profile_pic, String total_num_of_replies, String reply_contents, String reply_author, String reply_time, String attached_files, String tagged_keywords, String tagged_users) {
		ContentValues values = createContentValues(id, subject, contents, timestamp, status, user_id, username, profile_pic, total_num_of_replies, reply_contents, reply_author, reply_time, attached_files, tagged_keywords, tagged_users);
		return db.insert(table, null, values);
	}
	
	public boolean updateStatus(String status, String id) {
		ContentValues values = new ContentValues();
		values.put(STATUS, status);
		return db.update(table, values, ID + "=?", new String[] {id}) > 0;
	}
	
	public int deleteAllContents() {
		return db.delete(table, null, null);
	}
	
	public boolean deleteContent(String id) {
		return db.delete(table, ID + "=?", new String[] {id}) > 0;
	}
	
	public Cursor fetchAll() {
		return db.query(table, COLUMNS, null, null, null, null, null);
	}
	
	public Cursor fetchRow(String id) throws SQLException {
		return db.query(table, COLUMNS, ID + "=?", new String[] {id}, null, null, null);
	}
	
	private ContentValues createContentValues(String id, String subject, String contents, String timestamp, String status, String user_id, String username, String profile_pic, String total_num_of_replies, String reply_contents, String reply_author, String reply_time, String attached_files, String tagged_keywords, String tagged_users) {
		ContentValues values = new ContentValues();
		values.put(ID, id);
		values.put(SUBJECT, subject);
		values.put(CONTENTS, contents);
		values.put(TIMESTAMP, timestamp);
		values.put(STATUS, status);
		values.put(USER_ID, user_id);
		values.put(USERNAME, username);
		values.put(PROFILE_PIC, profile_pic);
		values.put(TOTAL_NUM_OF_REPLIES, total_num_of_replies);
		values.put(REPLY_CONTENTS, reply_contents);
		values.put(REPLY_AUTHOR, reply_author);
		values.put(REPLY_TIME, reply_time);
		values.put(ATTACHED_FILES, attached_files);
		values.put(TAGGED_KEYWORDS, tagged_keywords);
		values.put(TAGGED_USERS, tagged_users);
		return values;
	}

}

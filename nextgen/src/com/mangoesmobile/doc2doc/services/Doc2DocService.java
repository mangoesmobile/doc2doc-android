/**
 * 
 */
package com.mangoesmobile.doc2doc.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import org.json.JSONException;
import org.json.JSONObject;

import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.BestLocation;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class Doc2DocService extends Service {

	public static final String UPDATE_LOCATION = "UPDATE_LOCATION";

	private static boolean isRunning = false;
	private static boolean isBound = false;

	private NotificationManager mNotificationManager;
	public static final int NOTIFY = 667;

	Messenger client;

	private Handler refreshHandler = new Handler();
	private Runnable runRefresh = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			fetchNotification();
			refreshHandler.postDelayed(this, 5 * 60000);
		}
	};

	class MessageHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.MSG_REGISTER_CLIENT:
				Log.d("Doc2DocService", "client registered");
				client = msg.replyTo;
				isBound = true;
				mNotificationManager.cancel(NOTIFY);
				break;
			case Global.MSG_UNREGISTER_CLIENT:
				Log.d("Doc2DocService", "client unregistered");
				client = null;
				isBound = false;
				break;
			case Global.REFRESH_MESSAGE:
				refreshHandler.removeCallbacks(runRefresh);
				refreshHandler.post(runRefresh);
				break;
			default:
				super.handleMessage(msg);
			}
		}
	};

	final Messenger mMessenger = new Messenger(new MessageHandler());

	private Bundle bundle = new Bundle();

	private LocationManager lm;
	private BestLocation bl;

	@Override
	public void onCreate() {
		super.onCreate();
		isRunning = true;
		mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		bl = new BestLocation();
		setupAndStartLocationManager();
		refreshHandler.post(runRefresh);
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return mMessenger.getBinder();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("Doc2DocService", "service started");
		return START_STICKY; // run until explicitly stopped.
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d("Doc2DocService", "service stopped");
		isRunning = false;
		refreshHandler.removeCallbacks(runRefresh);
		lm.removeUpdates(gpsListener);
		lm.removeUpdates(netListener);
	}

	public static boolean isRunning() {
		return isRunning;
	}

	private void setupAndStartLocationManager() {
		lm = (LocationManager) getSystemService(LOCATION_SERVICE);
		lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 100,
				gpsListener);
		lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 100,
				netListener);
	}

	private void fetchNotification() {
		Log.d("Doc2DocService", "fetching new notifications");
		Doc2DocRequest dr = new Doc2DocRequest();
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", Global.token);

		asyncTask.runTask("notification/get_all_new_counts", params,
				new Doc2DocTaskListener() {

					@Override
					public void onMalformedURLException(MalformedURLException e) {
						// TODO Auto-generated method stub
						notificationHandler.sendMessage(Message.obtain(
								notificationHandler, Global.ERROR_RESPONSE));
						Log.d("notification-response", e.getMessage());
					}

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						notificationHandler.sendMessage(Message.obtain(
								notificationHandler, Global.CONNECTION_ERROR));
						Log.d("notification-response", e.getMessage());
					}

					@Override
					public void onFileNotFoundException(FileNotFoundException e) {
						// TODO Auto-generated method stub
						notificationHandler.sendMessage(Message.obtain(
								notificationHandler, Global.ERROR_RESPONSE));
						Log.d("notification-response", e.getMessage());
					}

					@Override
					public void onComplete(String response) {
						Log.d("notification-response", response);
						if (!response.equals("FALSE")) {
							parseJson(response);
						} else {
							notificationHandler.sendMessage(Message
									.obtain(notificationHandler,
											Global.INVALID_SESSION));
						}
					}
				});
	}

	private void parseJson(String response) {
		try {
			JSONObject json = new JSONObject(response);
			bundle.putString("total", json.getString("total"));
			bundle.putString("ping", json.getString("ping"));
			bundle.putString("thread", json.getString("thread"));
			bundle.putString("message", json.getString("message"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			notificationHandler.sendMessage(Message.obtain(notificationHandler,
					Global.ERROR_RESPONSE));
			e.printStackTrace();
		}
		notificationHandler.sendMessage(Message.obtain(notificationHandler,
				Global.GOT_RESPONSE));
	}

	private Handler notificationHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				if (isBound) {
					Log.d("Doc2DocService", "sending data from service");
					Message response = Message.obtain();
					response.what = Global.GET_DATA;
					response.setData(bundle);
					try {
						client.send(response);
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (NullPointerException e) {
						e.printStackTrace();
					}
				} else {
					Global.total = bundle.getString("total");
					Global.ping = bundle.getString("ping");
					Global.thread = bundle.getString("thread");
					Global.message = bundle.getString("message");
					if (!Global.total.equals("0")) {
						showNotification();
					}
				}
				break;
			case Global.EMPTY_RESPONSE:
				break;
			case Global.CONNECTION_ERROR:
				break;
			case Global.ERROR_RESPONSE:
				break;
			case Global.INVALID_SESSION:
				Toast.makeText(Doc2DocService.this,
						"Doc2Doc session dismissed. Please relogin",
						Toast.LENGTH_LONG).show();
				stopSelf();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				sendBroadcast(i);
				break;
			}
		}
	};

	private void showNotification() {
		int icon = R.drawable.ic_notification;
		CharSequence tickerText = "New Notification";
		long when = System.currentTimeMillis();

		Notification notification = new Notification(icon, tickerText, when);
		notification.defaults |= Notification.DEFAULT_SOUND;
		notification.defaults |= Notification.DEFAULT_VIBRATE;
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		CharSequence contentTitle = "New Notification(s)";
		CharSequence contentText = "You have " + bundle.getString("total")
				+ " new notification(s)";
		Intent notificationIntent = new Intent(this, D2DTabHost.class);
		notificationIntent.putExtra("from_notification", true);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent contentIntent = PendingIntent.getActivity(
				Doc2DocService.this, 0, notificationIntent, 0);

		notification.setLatestEventInfo(this, contentTitle, contentText,
				contentIntent);
		mNotificationManager.notify(NOTIFY, notification);
	}

	LocationListener gpsListener = new LocationListener() {

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			Log.d(provider + " status", provider + " enabled");
		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			Log.d(provider + " status", provider + " disabled");
		}

		@Override
		public void onLocationChanged(Location location) {
			// TODO Auto-generated method stub
			if (bl.isBetterLocation(location, Global.currentBestLocation)) {
				Global.currentBestLocation = location;
				sendBroadcast(new Intent(UPDATE_LOCATION));
				updateLocation(location.getLatitude(), location.getLongitude());
			}
		}
	};

	LocationListener netListener = new LocationListener() {

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			Log.d(provider + " status", provider + " enabled");
		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			Log.d(provider + " status", provider + " disabled");
		}

		@Override
		public void onLocationChanged(Location location) {
			// TODO Auto-generated method stub
			if (bl.isBetterLocation(location, Global.currentBestLocation)) {
				Global.currentBestLocation = location;
				sendBroadcast(new Intent(UPDATE_LOCATION));
				updateLocation(location.getLatitude(), location.getLongitude());
			}
		}
	};

	private void updateLocation(double lat, double lng) {
		Log.d("Doc2DocService", "updating location");
		Doc2DocRequest dr = new Doc2DocRequest();
		AsyncDoc2DocTask task = new AsyncDoc2DocTask(dr);
		Bundle params = new Bundle();
		params.putString("access_token", Global.token);
		params.putString("longitude", String.valueOf(lng));
		params.putString("latitude", String.valueOf(lat));
		task.runTask("location/set_my_location", params,
				new Doc2DocTaskListener() {

					@Override
					public void onMalformedURLException(MalformedURLException e) {
						// TODO Auto-generated method stub
						e.printStackTrace();
					}

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						e.printStackTrace();
					}

					@Override
					public void onFileNotFoundException(FileNotFoundException e) {
						// TODO Auto-generated method stub
						e.printStackTrace();
					}

					@Override
					public void onComplete(String response) {
						// TODO Auto-generated method stub
						Log.d("location-response", response);
						if (response.equals("FALSE")) {
							locationHandler.sendMessage(Message
									.obtain(notificationHandler,
											Global.INVALID_SESSION));
						}
					}
				});
	}

	private Handler locationHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				break;
			case Global.EMPTY_RESPONSE:
				break;
			case Global.CONNECTION_ERROR:
				break;
			case Global.ERROR_RESPONSE:
				break;
			case Global.INVALID_SESSION:
				Toast.makeText(Doc2DocService.this,
						"Doc2Doc session dismissed. Please relogin",
						Toast.LENGTH_LONG).show();
				stopSelf();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				sendBroadcast(i);
				break;
			}
		}
	};

}

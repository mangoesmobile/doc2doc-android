/**
 * 
 */
package com.mangoesmobile.doc2doc.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class BoardService extends Service {

	Messenger client;

	private Handler refreshHandler = new Handler();
	private Runnable runRefresh = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			fetchNotiThreadfication();
			refreshHandler.postDelayed(this, 5 * 60000);
		}
	};

	class MessageHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.MSG_REGISTER_CLIENT:
				Log.d("BoardService", "client registered");
				client = msg.replyTo;
				break;
			case Global.REFRESH_BOARD:
				refreshHandler.removeCallbacks(runRefresh);
				refreshHandler.post(runRefresh);
				break;
			default:
				super.handleMessage(msg);
			}
		}
	};

	final Messenger mMessenger = new Messenger(new MessageHandler());

	private Bundle bundle = new Bundle();

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return mMessenger.getBinder();
	}

	@Override
	public void onCreate() {
		super.onCreate();

		refreshHandler.post(runRefresh);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("BoardService", "service started");
		return START_STICKY; // run until explicitly stopped.
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d("BoardService", "service stopped");
		client = null;
		refreshHandler.removeCallbacks(runRefresh);
	}

	private void fetchNotiThreadfication() {
		Log.d("BoardService", "fetching thread");
		Doc2DocRequest dr = new Doc2DocRequest();
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", Global.token);
		params.putString("page_no", "0");

		asyncTask.runTask("thread/fetch_all", params,
				new Doc2DocTaskListener() {

					@Override
					public void onMalformedURLException(MalformedURLException e) {
						// TODO Auto-generated method stub
						boardThreadHandler.sendMessage(Message.obtain(
								boardThreadHandler, Global.ERROR_RESPONSE));
						Log.d("board-response", e.getMessage());
					}

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						boardThreadHandler.sendMessage(Message.obtain(
								boardThreadHandler, Global.CONNECTION_ERROR));
						Log.d("board-response", e.getMessage());
					}

					@Override
					public void onFileNotFoundException(FileNotFoundException e) {
						// TODO Auto-generated method stub
						boardThreadHandler.sendMessage(Message.obtain(
								boardThreadHandler, Global.ERROR_RESPONSE));
						Log.d("board-response", e.getMessage());
					}

					@Override
					public void onComplete(String response) {
						Log.d("board-response", response);
						if (!response.equals("FALSE")) {
							parseJson(response);
						} else {
							boardThreadHandler.sendMessage(Message.obtain(
									boardThreadHandler, Global.INVALID_SESSION));
						}
					}
				});
	}

	private void parseJson(String response) {
		JSONArray arr = null;
		try {
			arr = new JSONArray(response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			boardThreadHandler.sendMessage(Message.obtain(boardThreadHandler,
					Global.ERROR_RESPONSE));
			return;
		}
		if (arr.length() == 0) {
			boardThreadHandler.sendMessage(Message.obtain(boardThreadHandler,
					Global.EMPTY_RESPONSE));
			return;
		}
		for (int i = 0; i < arr.length(); i++) {
			Bundle content = new Bundle();
			JSONObject json;
			try {
				json = arr.getJSONObject(i);
				content.putString("id", json.getString("id"));
				content.putString("subject", json.getString("subject"));
				content.putString("contents", json.getString("contents"));
				content.putString("timestamp", json.getString("timestamp"));
				content.putString("status", json.getString("read_status"));
				// if (json.has("last_reply_time")) {
				// content.putString("last_reply_time",
				// json.getString("last_reply_time"));
				// }
				JSONObject author = json.getJSONObject("author");
				content.putString("user_id", author.getString("user_id"));
				content.putString("username", author.getString("username"));
				content.putString("profile_pic",
						author.getString("profile_pic_file_path"));
				content.putString("total_num_of_replies",
						json.getString("total_num_of_replies"));
				if (json.has("replies")) {
					JSONArray rep = json.getJSONArray("replies");
					JSONObject replies = rep.getJSONObject(0);
					content.putString("reply_contents",
							replies.getString("contents"));
					JSONObject reply_author = replies.getJSONObject("author");
					content.putString("reply_author",
							reply_author.getString("username"));
					content.putString("reply_time",
							replies.getString("time_ago"));
				}
				// adds attatchments if exist
				if (json.has("$attached_files")) {
					JSONArray attachments = json
							.getJSONArray("$attached_files");
					String file_urls = "";
					for (int j = 0; j < attachments.length(); j++) {
						file_urls += attachments.getJSONObject(j).getString(
								"file_path");
						if (j != attachments.length() - 1)
							file_urls += ",";
					}
					content.putString("attached_files", file_urls);
				}
				// adds tagged keywords if exists
				if (json.has("tagged_keywords")) {
					JSONArray keywords = json.getJSONArray("tagged_keywords");
					String tagged_keywords = "";
					for (int j = 0; j < keywords.length(); j++) {
						tagged_keywords += keywords.getString(j);
						if (j != keywords.length() - 1)
							tagged_keywords += ",";
					}
					content.putString("tagged_keywords", tagged_keywords);
				}
				if (json.has("tagged_users")) {
					JSONArray tags = json.getJSONArray("tagged_users");
					String tagged_users = "";
					for (int j = 0; j < tags.length(); j++) {
						tagged_users += tags.getJSONObject(j).getString(
								"username");
						if (j != tags.length() - 1)
							tagged_users += ",";
					}
					content.putString("tagged_users", tagged_users);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bundle.putBundle(String.valueOf(i), content);
		}
		boardThreadHandler.sendMessage(Message.obtain(boardThreadHandler,
				Global.GOT_RESPONSE));
	}

	private Handler boardThreadHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				Log.d("BoardService", "sending data from service");
				Message response = Message.obtain();
				response.what = Global.GET_DATA;
				response.setData(bundle);
				try {
					client.send(response);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
				break;
			case Global.EMPTY_RESPONSE:
				Log.d("BoardService", "sending data from service");
				Message empty = Message.obtain();
				empty.what = Global.EMPTY_RESPONSE;
//				empty.setData(bundle);
				try {
					client.send(empty);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
				break;
			case Global.CONNECTION_ERROR:
				break;
			case Global.ERROR_RESPONSE:
				break;
			case Global.INVALID_SESSION:
				Toast.makeText(BoardService.this,
						"Invalid session! Please relogin", Toast.LENGTH_LONG)
						.show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				sendBroadcast(i);
				break;
			}
		}
	};
}

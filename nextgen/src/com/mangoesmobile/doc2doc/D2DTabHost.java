/**
 * 
 */
package com.mangoesmobile.doc2doc;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import com.mangoesmobile.doc2doc.R;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.contents.Messages;
import com.mangoesmobile.doc2doc.services.Doc2DocService;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.UserInfo;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;
import com.mangoesmobile.doc2doc.util.Global;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class D2DTabHost extends TabActivity implements OnCheckedChangeListener {

	GoogleAnalyticsTracker tracker;

	// configure the notification
	Notification notification;
	NotificationManager notificationManager;
	
	public static final String REFRESH_BOARD = "REFRESH_BOARD";
	public static final String REFRESH_MESSAGES = "REFRESH_MESSAGES";
	public static final String REFRESH_TEAM = "REFRESH_TEAM";
	public static final String CLOSE_TAB = "CLOSE_TAB";
	public static final String INVALID_SESSION = "INVALID_SESSION";
	public static final String NEW_NOTIFICATION = "NEW_NOTIFICATION";
	private static final int CAMERA_PIC_REQUEST = 0;
	private static final int GALLERY_PIC_REQUEST = 1;

	private ProgressDialog loading;
	private TabHost tabHost;
	private TextView lblNotifications;
	private RadioGroup tabs;

	private Uri mCapturedImageURI;
	private String path;

	private BroadcastReceiver closeTabReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			// TODO Auto-generated method stub
			finish();
		}
	};

	private BroadcastReceiver invalidSessionReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			// TODO Auto-generated method stub
			interrupt();
		}
	};

	class D2DTabHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GET_DATA:
				Log.d("D2DTab", "got data from service");
				Bundle bundle = msg.getData();
				Global.total = bundle.getString("total");
				Global.ping = bundle.getString("ping");
				Global.thread = bundle.getString("thread");
				Global.message = bundle.getString("message");
				Messages.updateNotification = true;
				Intent intent = new Intent(NEW_NOTIFICATION);
				sendBroadcast(intent);
				showNotification();
				break;
			default:
				Log.d("D2DTab", "in default");
				super.handleMessage(msg);
				break;
			}
		}
	};

	final Messenger mMessenger = new Messenger(new D2DTabHandler());
	Messenger mService;

	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			mService = new Messenger(service);
			Message msg = Message.obtain();
			msg.what = Global.MSG_REGISTER_CLIENT;
			msg.replyTo = mMessenger;
			try {
				Log.d("D2DTab", "sending client to service");
				mService.send(msg);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected - process crashed.
			Log.d("D2DTab", "service disconnected");
			mService = null;
		}
	};

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub
		setContentView(R.layout.tabs);

		tracker = GoogleAnalyticsTracker.getInstance();

		instantiateLayout();
		prepareNotification();
		
		tabHost = getTabHost(); // The activity TabHost
		TabHost.TabSpec spec; // Resusable TabSpec for each tab
		// Initialize a TabSpec for the first tab and add it to the TabHost
		spec = tabHost.newTabSpec("tabboard").setIndicator("TabBoardActivity")
				.setContent(new Intent(this, ActivityGroupBoard.class));
		tabHost.addTab(spec);

		// Do the same for the other tabs
		spec = tabHost.newTabSpec("tabmessages")
				.setIndicator("TabMessagesActivity")
				.setContent(new Intent(this, ActivityGroupMessages.class));
		tabHost.addTab(spec);

		spec = tabHost.newTabSpec("tabteam").setIndicator("TabTeamActivity")
				.setContent(new Intent(this, ActivityGroupTeam.class));
		tabHost.addTab(spec);

		spec = tabHost.newTabSpec("tabreferences")
				.setIndicator("TabRefencesActivity")
				.setContent(new Intent(this, ActivityGroupReference.class));
		tabHost.addTab(spec);
		
		tabHost.setCurrentTab(0);
		
		startService(new Intent(D2DTabHost.this, Doc2DocService.class));

	}

	private void instantiateLayout() {
		loading = new ProgressDialog(this);
		tabs = (RadioGroup) findViewById(R.id.rg_tabs);
		lblNotifications = (TextView) findViewById(R.id.lbl_notifications);
	}

	@Override
	public void onStart() {
		super.onStart();

		tabs.setOnCheckedChangeListener(this);

		if (getIntent().getBooleanExtra("from_notification", false)) {
			tabs.check(R.id.rd_messages);
		}
		
		// listens for close tabs
		IntentFilter filter = new IntentFilter();
		filter.addAction(CLOSE_TAB);
		registerReceiver(closeTabReceiver, filter);

		// listens for close tabs
		IntentFilter invalidSessionFilter = new IntentFilter();
		invalidSessionFilter.addAction(INVALID_SESSION);
		registerReceiver(invalidSessionReceiver, invalidSessionFilter);
	}

	@Override
	public void onResume() {
		super.onResume();
		
		bindService(new Intent(this, Doc2DocService.class), mConnection,
				Context.BIND_AUTO_CREATE);

		showNotification();
	}

	@Override
	public void onPause() {
		super.onPause();

		if (mService != null) {
			try {
				Message msg = Message
						.obtain(null, Global.MSG_UNREGISTER_CLIENT);
				msg.replyTo = mMessenger;
				mService.send(msg);
			} catch (RemoteException e) {
				// There is nothing special we need to do if the service has
				// crashed.
			}
		}
		// Detach our existing connection.
		unbindService(mConnection);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		loading.dismiss();

		unregisterReceiver(closeTabReceiver);
		unregisterReceiver(invalidSessionReceiver);
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		// TODO Auto-generated method stub
		if (checkedId == R.id.rd_board) {
			tabHost.setCurrentTab(0);
			tracker.trackEvent("Select", "Tab", "Launch Board", 0);
		} else if (checkedId == R.id.rd_messages) {
			tabHost.setCurrentTab(1);
			tracker.trackEvent("Select", "Tab", "Launch Messages", 0);
			lblNotifications.setVisibility(View.GONE);
		} else if (checkedId == R.id.rd_team) {
			tabHost.setCurrentTab(2);
			tracker.trackEvent("Select", "Tab", "Launch Team", 0);
		} else if (checkedId == R.id.rd_references) {
			tabHost.setCurrentTab(3);
			tracker.trackEvent("Select", "Tab", "Launch References", 0);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.app_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_refresh) {
			refresh();
			tracker.trackEvent("Click", "OptionsMenu", "Refresh", 0);
			return true;
		} else if (item.getItemId() == R.id.menu_feedback) {
			sendFeedback();
			tracker.trackEvent("Click", "OptionsMenu", "Send Feedback", 0);
			return true;
		} else if (item.getItemId() == R.id.menu_changepp) {
			changePP();
			tracker.trackEvent("Click", "OptionsMenu", "Change Profile Pic", 0);
			return true;
		} else if (item.getItemId() == R.id.menu_logout) {
			Global.showLoading(loading, "Logging out...", false);
			logout();
			tracker.trackEvent("Click", "OptionsMenu", "Logout", 0);
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	private void showNotification() {
		if (!Global.total.equals("0") && tabHost.getCurrentTab() != 1) {
			lblNotifications.setText(Global.total);
			lblNotifications.setVisibility(View.VISIBLE);
		}
		Global.total = "0";
	}

	private void refresh() {
		Intent broadcast = new Intent();
		switch (tabHost.getCurrentTab()) {
		case 0:
			broadcast.setAction(REFRESH_BOARD);
			sendBroadcast(broadcast);
			Toast.makeText(D2DTabHost.this, "Refreshing Board...",
					Toast.LENGTH_LONG).show();
			break;
		case 1:
			broadcast.setAction(REFRESH_MESSAGES);
			sendBroadcast(broadcast);
			Message msg = Message.obtain();
			msg.what = Global.REFRESH_MESSAGE;
			try {
				mService.send(msg);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Toast.makeText(D2DTabHost.this, "Refreshing Messages...",
					Toast.LENGTH_LONG).show();
			break;
		case 2:
			broadcast.setAction(REFRESH_TEAM);
			sendBroadcast(broadcast);
			Toast.makeText(D2DTabHost.this, "Refreshing Team...",
					Toast.LENGTH_LONG).show();
			break;
		default:
			break;
		}
	}

	public void changePP() {
		showDialog(0);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog = new Dialog(this);
		switch (id) {
		case 0:
			dialog.show();
			break;
		case 1:
			dialog.show();
			break;
		default:
			dialog = null;
		}
		return dialog;
	}

	@Override
	protected void onPrepareDialog(int id, final Dialog dialog) {
		dialog.setContentView(R.layout.profilepic);
		dialog.setTitle("Change Profile Picture");
		switch (id) {
		case 0: {
			ImageView imgPhoto = (ImageView) dialog
					.findViewById(R.id.img_profilepic);
			if (Global.profilePic == null) {
				imgPhoto.setImageResource(R.drawable.default_photo);
			} else {
				imgPhoto.setImageDrawable(Global.profilePic);
			}
			Button btnChoose = (Button) dialog
					.findViewById(R.id.btn_choosephoto);
			btnChoose.setText("New Photo");
			btnChoose.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					chooseSource(dialog);
				}
			});
		}
			break;
		case 1: {
			ImageView imgPhoto = (ImageView) dialog
					.findViewById(R.id.img_profilepic);
			Drawable d = Drawable.createFromPath(path);
			imgPhoto.setImageDrawable(d);
			Button btnChoose = (Button) dialog
					.findViewById(R.id.btn_choosephoto);
			btnChoose.setText("Update Photo");
			btnChoose.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					updatePhoto();
				}
			});
		}
			break;
		}
		super.onPrepareDialog(id, dialog);
	}

	public void chooseSource(final Dialog photoDialog) {
		final CharSequence[] items = { "Camera", "Gallery" };
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Pick Source");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				photoDialog.dismiss();
				switch (item) {
				case (0):
					takePhoto();
					break;
				case (1):
					galleryPhoto();
					break;
				}
				dialog.dismiss();
			}
		});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	private void takePhoto() {
		String fileName = System.currentTimeMillis() + ".jpg";
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, fileName);
		values.put(
				MediaStore.Images.Media.ORIENTATION,
				getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ? 0
						: 90);
		mCapturedImageURI = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

		Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		cameraIntent.putExtra("crop", "true");
		cameraIntent.putExtra("aspectX", 1);
		cameraIntent.putExtra("aspectY", 1);
		cameraIntent.putExtra("outputX", 96);
		cameraIntent.putExtra("outputY", 96);
		cameraIntent.putExtra("return-data", "false");
		cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
		startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
	}

	private void galleryPhoto() {
		String fileName = System.currentTimeMillis() + ".jpg";
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, fileName);
		values.put(
				MediaStore.Images.Media.ORIENTATION,
				getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ? 0
						: 90);
		mCapturedImageURI = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
		galleryIntent.setType("image/*");
		galleryIntent.putExtra("crop", "true");
		galleryIntent.putExtra("aspectX", 1);
		galleryIntent.putExtra("aspectY", 1);
		galleryIntent.putExtra("outputX", 96);
		galleryIntent.putExtra("outputY", 96);
		galleryIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
		galleryIntent.putExtra("return-data", "false");
		startActivityForResult(
				Intent.createChooser(galleryIntent, "Select Picture"),
				GALLERY_PIC_REQUEST);
	}

	private void sendFeedback() {
		Intent feedbackIntent = new Intent(Intent.ACTION_SEND);
		feedbackIntent.setType("message/rfc822");
		feedbackIntent.putExtra(Intent.EXTRA_EMAIL,
				new String[] { "doc2doc@mangoesmobile.com" });
		feedbackIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback on Doc2Doc - "
				+ Global.userInfo.getString("username"));
		startActivity(Intent.createChooser(feedbackIntent, "Send Feedback"));
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (resultCode == RESULT_OK) {
			if (requestCode == CAMERA_PIC_REQUEST) {
				path = getPath(mCapturedImageURI);
				mCapturedImageURI = null;
			} else if (requestCode == GALLERY_PIC_REQUEST) {
				path = getPath(mCapturedImageURI);
				mCapturedImageURI = null;
			}
			showDialog(1);
		}

	}

	// returns the real path from Uri (only applicable for gallery media)
	private String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		startManagingCursor(cursor);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	private void updatePhoto() {
		Log.d("TabHost", "changing profile pic");
//		Global.showLoading(loading, "Changing profile pic...", false);
		Toast.makeText(D2DTabHost.this, "Changing profile pic...", Toast.LENGTH_SHORT).show();
		Doc2DocRequest dr = new Doc2DocRequest(uploadHandler);
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", Global.token);
		params.putString("file_1", path);

		asyncTask.runTask("user/set_profile_pic", params,
				new Doc2DocTaskListener() {

					@Override
					public void onMalformedURLException(MalformedURLException e) {
						// TODO Auto-generated method stub
						profilePicHandler.sendMessage(Message.obtain(
								profilePicHandler, Global.ERROR_RESPONSE));
						Log.d("board-response", e.getMessage());
					}

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						profilePicHandler.sendMessage(Message.obtain(
								profilePicHandler, Global.CONNECTION_ERROR));
						Log.d("board-response", e.getMessage());
					}

					@Override
					public void onFileNotFoundException(FileNotFoundException e) {
						// TODO Auto-generated method stub
						profilePicHandler.sendMessage(Message.obtain(
								profilePicHandler, Global.ERROR_RESPONSE));
						Log.d("board-response", e.getMessage());
					}

					@Override
					public void onComplete(String response) {
						Log.d("board-response", response);
						if (response.equals("TRUE")) {
							profilePicHandler.sendMessage(Message.obtain(
									profilePicHandler, Global.GOT_RESPONSE));
						} else {
							profilePicHandler.sendMessage(Message.obtain(
									profilePicHandler, Global.INVALID_SESSION));
						}
					}
				});
	}

	private Handler profilePicHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
//				Global.hideLoading(loading);
				Toast.makeText(D2DTabHost.this,
						"Profile picture updated successfully",
						Toast.LENGTH_LONG).show();
				Global.profilePic = null;
				Intent intent = new Intent(UserInfo.GOT_USERINFO);
				intent.putExtra("response", Global.CHANGE_PIC);
				sendBroadcast(intent);
				break;
			case Global.EMPTY_RESPONSE:
//				Global.hideLoading(loading);
				break;
			case Global.CONNECTION_ERROR:
//				Global.hideLoading(loading);
				Toast.makeText(D2DTabHost.this,
						"Connection error! Please retry", Toast.LENGTH_LONG)
						.show();
				break;
			case Global.ERROR_RESPONSE:
//				Global.hideLoading(loading);
				break;
			case Global.INVALID_SESSION:
//				Global.hideLoading(loading);
				Toast.makeText(D2DTabHost.this,
						"Invalid session! Please relogin", Toast.LENGTH_LONG)
						.show();
				interrupt();
				break;
			}
		}
	};

	public void logout() {
		Doc2DocRequest dr = new Doc2DocRequest();
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", Global.token);

		asyncTask.runTask("authenticate/logout", params,
				new Doc2DocTaskListener() {

					@Override
					public void onMalformedURLException(MalformedURLException e) {
						// TODO Auto-generated method stub
						logoutHandler.sendMessage(Message.obtain(logoutHandler,
								Global.ERROR_RESPONSE));
						e.printStackTrace();
					}

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						logoutHandler.sendMessage(Message.obtain(logoutHandler,
								Global.CONNECTION_ERROR));
						e.printStackTrace();
					}

					@Override
					public void onFileNotFoundException(FileNotFoundException e) {
						// TODO Auto-generated method stub
						logoutHandler.sendMessage(Message.obtain(logoutHandler,
								Global.ERROR_RESPONSE));
						e.printStackTrace();
					}

					@Override
					public void onComplete(String response) {
						if (response.equals("FALSE")) {
							logoutHandler.sendMessage(Message.obtain(logoutHandler,
									Global.INVALID_SESSION));
						} else {
							logoutHandler.sendMessage(Message.obtain(logoutHandler,
									Global.GOT_RESPONSE));
						}
						Log.d("logout-response", response);
					}
				});
	}

	private Handler logoutHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				Global.hideLoading(loading);
				stopService(new Intent(D2DTabHost.this, Doc2DocService.class));
				finish();
				startActivity(new Intent(D2DTabHost.this, Login.class));
				Global.token = "";
				Global.userInfo = new Bundle();
				Global.userStatus = "";
				Global.profilePic = null;
				tracker.stopSession();
				break;
			case Global.EMPTY_RESPONSE:
				Global.hideLoading(loading);
				break;
			case Global.INVALID_SESSION:
				Global.hideLoading(loading);
				Toast.makeText(D2DTabHost.this,
						"Invalid session! Please relogin", Toast.LENGTH_LONG)
						.show();
				interrupt();
				break;
			case Global.CONNECTION_ERROR:
				Global.hideLoading(loading);
				Toast.makeText(D2DTabHost.this,
						"Connection error! Please retry", Toast.LENGTH_LONG)
						.show();
				break;
			case Global.ERROR_RESPONSE:
				Global.hideLoading(loading);
				break;
			}
		}
	};

	private void interrupt() {
		stopService(new Intent(D2DTabHost.this, Doc2DocService.class));
		finish();
		startActivity(new Intent(D2DTabHost.this, Login.class));
		Global.token = "";
		Global.userInfo = new Bundle();
		Global.userStatus = "";
		Global.profilePic = null;
	}
	
	private void prepareNotification() {
		notification = new Notification(android.R.drawable.stat_sys_upload,
				"Uploading file", System.currentTimeMillis());
		notification.flags = notification.flags
				| Notification.FLAG_ONGOING_EVENT;
		notification.contentView = new RemoteViews(getApplicationContext()
				.getPackageName(), R.layout.update_notification);
		PendingIntent contentIntent = PendingIntent.getActivity(
				getApplicationContext(), 0, new Intent(),
				PendingIntent.FLAG_UPDATE_CURRENT);
		notification.contentIntent = contentIntent;
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	}

	private void updateProgress(int file) {
		notificationManager.notify(file, notification);
	}

	private Handler uploadHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Doc2DocRequest.UPLOAD_START:
				notification.contentView.setTextViewText(
						R.id.text, "Uploading "
								+ (String) msg.getData().getString("filename"));
				updateProgress(msg.arg1);
				break;
			case Doc2DocRequest.PUBLISH_PROGRESS:
				updateProgress(msg.arg1);
				break;
			case Doc2DocRequest.UPLOAD_FINISH:
				notificationManager.cancel(msg.arg1);
			default:
				break;
			}
		}
	};

}

/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.adapters.ReferenceGridAdapter;
import android.app.Activity;
import android.os.Bundle;
import android.widget.GridView;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class Reference extends Activity {

	GoogleAnalyticsTracker tracker;
	
	GridView gridItems;

	private Bundle data;
	private ReferenceGridAdapter adapter;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub
		setContentView(R.layout.references);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("Reference");
		
		instantiateLayout();
		generateItems();
	}

	@Override
	public void onStart() {
		super.onStart();

		gridItems.setAdapter(adapter);
		adapter.setData(data);
	}

	private void instantiateLayout() {
		gridItems = (GridView) findViewById(R.id.grid_reference);
		adapter = new ReferenceGridAdapter(Reference.this);
	}

	private void generateItems() {
		final int[] icons = { R.drawable.ic_treatmentguideline,
				R.drawable.ic_medline, R.drawable.ic_pubmed,
				R.drawable.ic_dailymed, R.drawable.ic_aidsinfo,
				R.drawable.ic_other };
		data = new Bundle();
		for (int i = 0; i < icons.length; i++) {
			Bundle content = new Bundle();
			content.putInt("icon", icons[i]);
			data.putBundle(String.valueOf(i), content);
		}
	}

}

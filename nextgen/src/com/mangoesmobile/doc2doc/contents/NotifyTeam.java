/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.foound.widget.AmazingListView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.adapters.TagAdapter;
import com.mangoesmobile.doc2doc.cache.TeamCacheManager;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class NotifyTeam extends ListActivity {

	GoogleAnalyticsTracker tracker;
	
	public static final String PEOPLE_TAGGED = "PEOPLE_TAGGED";

	private ArrayList<CharSequence> tags = new ArrayList<CharSequence>();
	private ArrayList<CharSequence> names = new ArrayList<CharSequence>();
	private ArrayList<CharSequence> id = new ArrayList<CharSequence>();

	private boolean isChecked;
	CheckBox chkSelectAll;
	AmazingListView listTags;
	TagAdapter adapter;

	Bundle bundle = new Bundle();

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub
		setContentView(R.layout.taglist);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("Notify People");
		
		instantiateLayout();
	}

	@Override
	public void onStart() {
		super.onStart();

		setListAdapter(adapter);
		listTags.setLoadingView(getLayoutInflater().inflate(
				R.layout.loading_view, null));
		listTags.setItemsCanFocus(false);

		fetchTeamFromCache();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		getTags();
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		CheckBox chkSelected = (CheckBox) v
				.findViewById(R.id.chk_teamlist_selected);
		Bundle content = (Bundle) adapter.getItem(position);
		content.putBoolean("checked", !chkSelected.isChecked());
		adapter.notifyDataSetChanged();
		tracker.trackEvent("Click", "ListItem", "Select Individual", 0);
	}

	public void onClick(View v) {
		if (v.getId() == R.id.rel_selectall) {
			isChecked = !isChecked;
			chkSelectAll.setChecked(isChecked);
			for (int i = 0; i < adapter.getCount(); i++) {
				Bundle content = (Bundle) adapter.getItem(i);
				content.putBoolean("checked", isChecked);
			}
			adapter.notifyDataSetChanged();
			tracker.trackEvent("Click", "Button", "Select All", 0);
		} else if (v.getId() == R.id.btn_taglist_done) {
			onBackPressed();
		}
	}

	private void instantiateLayout() {
		if (getIntent().getBooleanExtra("has_tags", false)) {
			id = getIntent().getCharSequenceArrayListExtra("tags");
		}
		isChecked = false;
		chkSelectAll = (CheckBox) findViewById(R.id.chk_selectall);
		listTags = (AmazingListView) getListView();
		adapter = new TagAdapter(NotifyTeam.this);
	}

	private void fetchTeamFromCache() {
		adapter.notifyMayHaveMorePages();
		TeamCacheManager manager = new TeamCacheManager(NotifyTeam.this);
		manager.open();
		Cursor cursor = manager.fetchAll();
		startManagingCursor(cursor);
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			for (int i = 0; i < cursor.getCount(); i++) {
				Bundle content = new Bundle();
				String user_id = cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.USER_ID));
				content.putString("user_id", user_id);
				content.putString("first_name", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.FIRST_NAME)));
				content.putString("last_name", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.LAST_NAME)));
				content.putString("title", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.TITLE)));
				content.putString("email", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.EMAIL)));
				content.putString("phone", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.PHONE)));
				content.putString("profile_pic_file_path", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.PROFILE_PIC)));
				content.putString("organization", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.ORGANIZATION)));
				content.putString("specialization", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.SPECIALIZATION)));
				content.putString("username", cursor.getString(cursor
						.getColumnIndex(TeamCacheManager.USERNAME)));
				content.putString(
						"availability_status",
						cursor.getString(cursor
								.getColumnIndex(TeamCacheManager.AVAILABILITY_STATUS)));
				content.putBoolean("checked", id.contains(user_id));
				bundle.putBundle(String.valueOf(i), content);
				cursor.moveToNext();
			}
			adapter.setData(bundle);
		} else {
			fetchTeam();
		}
		cursor.close();
		manager.close();
	}

	private void fetchTeam() {
		Log.d("Team", "fetching team");
		Doc2DocRequest dr = new Doc2DocRequest();
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", Global.token);

		asyncTask.runTask("user/all_vcards", params, new Doc2DocTaskListener() {

			@Override
			public void onMalformedURLException(MalformedURLException e) {
				// TODO Auto-generated method stub
				teamHandler.sendMessage(Message.obtain(teamHandler,
						Global.ERROR_RESPONSE));
				Log.d("team-response", e.getMessage());
			}

			@Override
			public void onIOException(IOException e) {
				// TODO Auto-generated method stub
				teamHandler.sendMessage(Message.obtain(teamHandler,
						Global.CONNECTION_ERROR));
				Log.d("team-response", e.getMessage());
			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e) {
				// TODO Auto-generated method stub
				teamHandler.sendMessage(Message.obtain(teamHandler,
						Global.ERROR_RESPONSE));
				Log.d("team-response", e.getMessage());
			}

			@Override
			public void onComplete(String response) {
				Log.d("team-response", response);
				if (!response.equals("FALSE")) {
					parseJson(response);
				} else {
					teamHandler.sendMessage(Message.obtain(teamHandler,
							Global.INVALID_SESSION));
				}
			}
		});
	}

	private void parseJson(String response) {
		JSONArray arr = null;
		try {
			arr = new JSONArray(response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			teamHandler.sendMessage(Message.obtain(teamHandler,
					Global.ERROR_RESPONSE));
			return;
		}
		if (arr.length() == 0) {
			teamHandler.sendMessage(Message.obtain(teamHandler,
					Global.EMPTY_RESPONSE));
			return;
		}
		for (int i = 0; i < arr.length(); i++) {
			Bundle content = new Bundle();
			JSONObject json;
			try {
				json = arr.getJSONObject(i);
				if (!json.has("user_id")) {
					content.putString("user_id", "null"); // this is temporary
				} else {
					String user_id = json.getString("user_id");
					content.putString("user_id", user_id);
					content.putString("first_name",
							json.getString("first_name"));
					content.putString("last_name", json.getString("last_name"));
					content.putString("title", json.getString("title"));
					content.putString("email", json.getString("email"));
					content.putString("phone", json.getString("phone"));
					content.putString("profile_pic_file_path",
							json.getString("profile_pic_file_path"));
					content.putString("organization",
							json.getString("organization"));
					content.putString("specialization",
							json.getString("specialization"));
					content.putString("username", json.getString("username"));
					content.putString("availability_status",
							json.getString("availability_status"));
					JSONObject geolocation = json.getJSONObject("geolocation");
					content.putString("latitude", geolocation.getString("latitude"));
					content.putString("longitude", geolocation.getString("longitude"));
					content.putBoolean("checked", id.contains(user_id));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bundle.putBundle(String.valueOf(i), content);
		}
		teamHandler.sendMessage(Message
				.obtain(teamHandler, Global.GOT_RESPONSE));
	}

	private Handler teamHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				adapter.setData(bundle);
				break;
			case Global.EMPTY_RESPONSE:
				break;
			case Global.CONNECTION_ERROR:
				break;
			case Global.ERROR_RESPONSE:
				break;
			case Global.INVALID_SESSION:
				Toast.makeText(NotifyTeam.this, "Invalid session! Please relogin", Toast.LENGTH_LONG).show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				sendBroadcast(i);
				break;
			}
		}
	};

	private void getTags() {
		for (int i = 0; i < adapter.getCount(); i++) {
			Bundle content = (Bundle) adapter.getItem(i);
			if (content.getBoolean("checked")) {
				tags.add(content.getString("user_id"));
				names.add(content.getString("title") + " "
						+ content.getString("first_name") + " "
						+ content.getString("last_name"));
			}
		}

		Intent intent = new Intent();
		intent.setAction(PEOPLE_TAGGED);
		intent.putCharSequenceArrayListExtra("tags", tags);
		intent.putCharSequenceArrayListExtra("names", names);
		sendBroadcast(intent);
	}

}

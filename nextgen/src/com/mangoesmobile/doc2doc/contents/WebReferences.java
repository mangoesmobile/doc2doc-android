/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.ActivityGroupReference;
import com.mangoesmobile.doc2doc.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class WebReferences extends Activity {

	GoogleAnalyticsTracker tracker;
	
	private ProgressDialog loading;
	//
	// SeekBar seekRate;
	// Button btnLogout;
	// TextView lblRate;
	WebView webview;
	ImageButton btnHome;
	ImageButton btnSelectText;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reference);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("References");
		
		loading = new ProgressDialog(getParent());
		loading.setMessage("Loading, please wait...");

		webview = (WebView) findViewById(R.id.web_reference);
		btnHome = (ImageButton) findViewById(R.id.btn_reference_home);
		btnSelectText = (ImageButton) findViewById(R.id.btn_reference_selectandcopy);
		
		webview.requestFocus(View.FOCUS_DOWN);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.getSettings().setBuiltInZoomControls(true);
		webview.loadUrl(getIntent().getStringExtra("url"));

		webview.setWebViewClient(new ReferenceWebViewClient());
	}

	private void emulateShiftHeld(WebView view) {
		try {
			KeyEvent shiftPressEvent = new KeyEvent(0, 0, KeyEvent.ACTION_DOWN,
					KeyEvent.KEYCODE_SHIFT_LEFT, 0, 0);
			shiftPressEvent.dispatch(view);
			Toast.makeText(this, "Select text now", Toast.LENGTH_SHORT).show();
		} catch (Exception e) {
			Log.e("dd", "Exception in emulateShiftHeld()", e);
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		
		btnHome.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
//				webview.loadUrl(getIntent().getStringExtra("url"));
//				tracker.trackEvent("Click", "Button", "Return To Homepage", 0);
				ActivityGroupReference parent = (ActivityGroupReference) getParent();
				parent.finishFromChild(WebReferences.this);
			}
		});
		
		btnSelectText.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				emulateShiftHeld(webview);
				tracker.trackEvent("Click", "Button", "Copy Text From Reference Page", 0);
			}
		});
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {
			webview.goBack();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		loading.dismiss();
	}

	private class ReferenceWebViewClient extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			loading.show();
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			loading.hide();
		}
	}
}

/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import java.util.List;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.adapters.MapOverlay;
import com.mangoesmobile.doc2doc.adapters.TeamAdapter;
import com.mangoesmobile.doc2doc.services.Doc2DocService;
import com.mangoesmobile.doc2doc.util.BestLocation;
import com.mangoesmobile.doc2doc.util.Global;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class UserLocation extends MapActivity {

	BroadcastReceiver locationReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			updateOwnLocation(Global.currentBestLocation);
		}
	};
	
	GoogleAnalyticsTracker tracker;

	private boolean updateOwn;
	private boolean updateTeam;
	private boolean updateIndividual;

	LocationManager lm;
	MapView map;
	MapController controller;
	MapOverlay locations;
	List<Overlay> mapOverlays;
	Drawable pin; // used to show team members

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub
		setContentView(R.layout.locationmap);

		
		updateOwn = getIntent().getBooleanExtra("update_own", false);
		updateTeam = getIntent().getBooleanExtra("update_team", false);
		updateIndividual = getIntent().getBooleanExtra("update_individual",
				false);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView(updateTeam ? "Location of Whole Team" : "Location of Individual");
		
		instantiateLayout();

		pin = getResources().getDrawable(R.drawable.pin2);
		pin.setBounds(0 - pin.getIntrinsicWidth() / 2,
				0 - pin.getIntrinsicHeight(), pin.getIntrinsicWidth() / 2, 0);
	}

	@Override
	public void onStart() {
		super.onStart();

		if (updateOwn) {
			if (Global.currentBestLocation != null) {
				updateOwnLocation(Global.currentBestLocation);
			} else {
				updateBestLastKnownLocation();
			}

			IntentFilter fileFilter = new IntentFilter();
			fileFilter.addAction(Doc2DocService.UPDATE_LOCATION);
			registerReceiver(locationReceiver, fileFilter);
		}

		if (updateTeam) {
			TeamAdapter adapter = (TeamAdapter) getIntent()
					.getSerializableExtra("adapter");
			if (adapter.getCount() > 0) {
				for (int i = 0; i < adapter.getCount(); i++) {
					Bundle content = adapter.getItem(i);
					String user_id = content.getString("user_id");
					String name = content.getString("title") + " "
							+ content.getString("first_name") + " "
							+ content.getString("last_name");
					try {
						Double lat = Double.parseDouble(content
								.getString("latitude")) * 1E6;
						Double lng = Double.parseDouble(content
								.getString("longitude")) * 1E6;
						updateUserLocation(lat, lng, user_id, name);
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (NullPointerException e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				}
				map.invalidate();
			} else {
				Toast.makeText(UserLocation.this, "Team location is currently not available", Toast.LENGTH_LONG).show();
			}
		}

		if (updateIndividual) {
			String user_id = getIntent().getStringExtra("user_id");
			String name = getIntent().getStringExtra("name");
			try {
				Double lat = Double.parseDouble(getIntent().getStringExtra(
						"latitude")) * 1E6;
				Double lng = Double.parseDouble(getIntent().getStringExtra(
						"longitude")) * 1E6;
				GeoPoint point = updateUserLocation(lat, lng, user_id, name);
				controller.animateTo(point);
				controller.setZoom(16);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				Toast.makeText(UserLocation.this, "location is currently not available", Toast.LENGTH_LONG).show();
				e.printStackTrace();
			} catch (NullPointerException e) {
				// TODO: handle exception
				Toast.makeText(UserLocation.this, "location is currently not available", Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (updateOwn) {
			unregisterReceiver(locationReceiver);
		}
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	private void instantiateLayout() {
		lm = (LocationManager) getSystemService(LOCATION_SERVICE);
		map = (MapView) findViewById(R.id.mapview);
		map.setBuiltInZoomControls(true);
		controller = map.getController();
		locations = new MapOverlay(getResources().getDrawable(R.drawable.pin),
				getParent());
		mapOverlays = map.getOverlays();
		mapOverlays.add(locations);
	}

	private void updateOwnLocation(Location location) {
		locations.removeOverlay(0);
		Double lat = location.getLatitude() * 1E6;
		Double lng = location.getLongitude() * 1E6;
		GeoPoint point = new GeoPoint(lat.intValue(), lng.intValue());
		String userid = "";
		String username = "";
		if (Global.userInfo.size() > 0) {
			userid = Global.userInfo.getString("user_id");
			username = Global.userInfo.getString("title") + " "
					+ Global.userInfo.getString("first_name") + " "
					+ Global.userInfo.getString("last_name");
		}
		OverlayItem item = new OverlayItem(point, userid, username);
		locations.addOverlay(item);
		controller.animateTo(point);
		if (updateTeam) {
			controller.setZoom(2);
		} else {
			controller.setZoom(16);
		}
	}

	private GeoPoint updateUserLocation(Double lat, Double lng, String user_id,
			String name) {
		if (!updateIndividual) {
			if (Global.userInfo != null
					&& user_id.equals(Global.userInfo.getString("user_id"))) {
				return null;
			}
		}
		GeoPoint point = new GeoPoint(lat.intValue(), lng.intValue());
		OverlayItem item = new OverlayItem(point, user_id, name);
		item.setMarker(pin);
		locations.addOverlay(item);
		return point;
	}

	private void updateBestLastKnownLocation() {
		Location loc1 = lm
				.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		Location loc2 = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		BestLocation bl = new BestLocation();
		if (loc1 != null) {
			if (bl.isBetterLocation(loc1, loc2)) {
				updateOwnLocation(loc1);
			} else {
				updateOwnLocation(loc2);
			}
		} else if (loc2 != null) {
			if (bl.isBetterLocation(loc2, loc1)) {
				updateOwnLocation(loc2);
			} else {
				updateOwnLocation(loc1);
			}
		} else {
			Toast.makeText(UserLocation.this,
					"Wating for location to be updated...", Toast.LENGTH_LONG);
		}
	}

}

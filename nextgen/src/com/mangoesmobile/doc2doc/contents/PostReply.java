/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.ActivityGroupBoard;
import com.mangoesmobile.doc2doc.ActivityGroupMessages;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.ImageLoader;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class PostReply extends Activity {

	GoogleAnalyticsTracker tracker;

	// configure the notification
	Notification notification;
	NotificationManager notificationManager;

	boolean activityOpen = false;

	ProgressDialog loading;
	ImageView imgUser;
	TextView lblSubject;
	TextView lblAuthor;
	EditText txtReply;
	Button btnNotify;
	Button btnReply;
	TextView lblNoOfFiles;
	TextView lblNoOfTags;

	ArrayList<CharSequence> attachedFiles = new ArrayList<CharSequence>();
	ArrayList<CharSequence> taggedPeople = new ArrayList<CharSequence>();

	BroadcastReceiver fileReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			attachedFiles = intent.getCharSequenceArrayListExtra("files");
			if (attachedFiles.size() > 0) {
				lblNoOfFiles.setText("(" + attachedFiles.size() + ")");
			} else {
				lblNoOfFiles.setText("");
			}
		}
	};

	BroadcastReceiver tagReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			taggedPeople = intent.getCharSequenceArrayListExtra("tags");
			if (taggedPeople.size() > 0) {
				lblNoOfTags.setText("(" + taggedPeople.size() + ")");
			} else {
				lblNoOfTags.setText("");
			}
		}
	};

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub
		setContentView(R.layout.replythread);

		activityOpen = true;

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("Reply To "
				+ getIntent().getStringExtra("reply_to"));

		instantiateLayout();
		prepareNotification();
	}

	@Override
	public void onStart() {
		super.onStart();

		if (getIntent().getStringExtra("reply_to").equalsIgnoreCase("Message")) {
			btnNotify.setVisibility(View.INVISIBLE);
		}

		putValues();

		IntentFilter fileFilter = new IntentFilter();
		fileFilter.addAction(AttachFile.FILE_ATTACHED);
		registerReceiver(fileReceiver, fileFilter);

		IntentFilter tagFilter = new IntentFilter();
		tagFilter.addAction(NotifyTeam.PEOPLE_TAGGED);
		registerReceiver(tagReceiver, tagFilter);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		loading.dismiss();

		unregisterReceiver(fileReceiver);
		unregisterReceiver(tagReceiver);

		activityOpen = false;
	}

	public void onClick(View v) {
		if (v.getId() == R.id.btn_replythread_attach) {
			Intent intent = new Intent(PostReply.this, AttachFile.class);
			intent.putExtra("subject", getIntent().getStringExtra("subject"));
			intent.putExtra("username",
					"by " + getIntent().getStringExtra("username") + " on "
							+ getIntent().getStringExtra("timestamp"));
			if (attachedFiles.size() > 0) {
				intent.putExtra("has_attachments", true);
				intent.putCharSequenceArrayListExtra("attachments",
						attachedFiles);
			}
			if (getParent() instanceof ActivityGroupBoard) {
				ActivityGroupBoard parent = (ActivityGroupBoard) getParent();
				parent.startChildActivity("attach_file", intent);
			} else {
				ActivityGroupMessages parent = (ActivityGroupMessages) getParent();
				parent.startChildActivity("attach_file", intent);
			}
			tracker.trackEvent("Click", "Button",
					"Launch Attach File Activity", 0);
		} else if (v.getId() == R.id.btn_replythread_notify) {
			Intent intent = new Intent(PostReply.this, NotifyTeam.class);
			if (taggedPeople.size() > 0) {
				intent.putExtra("has_tags", true);
				intent.putCharSequenceArrayListExtra("tags", taggedPeople);
			}
			if (getParent() instanceof ActivityGroupBoard) {
				ActivityGroupBoard parent = (ActivityGroupBoard) getParent();
				parent.startChildActivity("notify_team", intent);
			} else {
				ActivityGroupMessages parent = (ActivityGroupMessages) getParent();
				parent.startChildActivity("notify_team", intent);
			}
			tracker.trackEvent("Click", "Button",
					"Launch Notify People Activity", 0);
		} else if (v.getId() == R.id.btn_replythread_reply) {
			if (txtReply.getText().length() > 0) {
				postReply(txtReply.getText().toString(), getIntent()
						.getStringExtra("reply_to").toLowerCase());
				tracker.trackEvent("Click", "Button", "Post "
						+ getIntent().getStringExtra("reply_to") + " Reply", 0);
			} else {
				Global.createAlert(getParent(),
						"Please write something to reply.", "OK");
			}
		}
	}

	private void instantiateLayout() {
		loading = new ProgressDialog(getParent());
		imgUser = (ImageView) findViewById(R.id.img_replythread_user);
		lblSubject = (TextView) findViewById(R.id.lbl_replythread_title);
		lblAuthor = (TextView) findViewById(R.id.lbl_replythread_author);
		txtReply = (EditText) findViewById(R.id.txt_replythread_reply);
		btnNotify = (Button) findViewById(R.id.btn_replythread_notify);
		btnReply = (Button) findViewById(R.id.btn_replythread_reply);
		lblNoOfFiles = (TextView) findViewById(R.id.lbl_replythread_noofattachment);
		lblNoOfTags = (TextView) findViewById(R.id.lbl_replythread_noofpeople);
	}

	private void putValues() {
//		Global.loadPhoto(PostReply.this, Global.url
//				+ getIntent().getStringExtra("profile_pic"), imgUser, null);
		ImageLoader il = new ImageLoader(getApplicationContext());
		il.DisplayImage(Global.url
						+ getIntent()
								.getStringExtra("profile_pic"), imgUser, null);
		lblSubject.setText(getIntent().getStringExtra("subject"));
		lblAuthor.setText("by " + getIntent().getStringExtra("username")
				+ " on " + getIntent().getStringExtra("timestamp"));
	}

	private void postReply(String reply, String type) {
		Log.d("ReplyBoardThread", "replying to a thread");
		// Global.showLoading(loading, "Posting reply...", false);
		Toast.makeText(PostReply.this, "Posting reply...", Toast.LENGTH_LONG)
				.show();
		btnReply.setEnabled(false);
		Doc2DocRequest dr = new Doc2DocRequest(uploadHandler);
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", Global.token);
		params.putString(type + "_id", getIntent().getStringExtra("id"));
		params.putString("contents", reply);
		params.putString("tag_user_ids",
				Global.getStringFromList("-", taggedPeople));
		for (int i = 0; i < attachedFiles.size(); i++) {
			params.putString("file_" + (i + 1), attachedFiles.get(i).toString());
		}

		asyncTask.runTask(type + "/post_reply", params,
				new Doc2DocTaskListener() {

					@Override
					public void onMalformedURLException(MalformedURLException e) {
						// TODO Auto-generated method stub
						replyHandler.sendMessage(Message.obtain(replyHandler,
								Global.ERROR_RESPONSE));
						Log.d("board-reply-response", e.getMessage());
					}

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						replyHandler.sendMessage(Message.obtain(replyHandler,
								Global.CONNECTION_ERROR));
						Log.d("board-reply-response", e.getMessage());
					}

					@Override
					public void onFileNotFoundException(FileNotFoundException e) {
						// TODO Auto-generated method stub
						replyHandler.sendMessage(Message.obtain(replyHandler,
								Global.ERROR_RESPONSE));
						Log.d("board-reply-response", e.getMessage());
					}

					@Override
					public void onComplete(String response) {
						Log.d("board-reply-response", response);
						if (response.equals("TRUE")) {
							replyHandler.sendMessage(Message.obtain(
									replyHandler, Global.GOT_RESPONSE));
						} else {
							replyHandler.sendMessage(Message.obtain(
									replyHandler, Global.INVALID_SESSION));
						}
					}
				});
	}

	private Handler replyHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				btnReply.setEnabled(true);
				Toast.makeText(PostReply.this, "Your reply has been submitted",
						Toast.LENGTH_LONG).show();
				if (activityOpen) {
					onBackPressed();
				}
				if (getIntent().getStringExtra("reply_to").equalsIgnoreCase(
						"thread")) {
					sendBroadcast(new Intent(D2DTabHost.REFRESH_BOARD));
				}
				sendBroadcast(new Intent(D2DTabHost.REFRESH_MESSAGES));
				break;
			case Global.EMPTY_RESPONSE:
				btnReply.setEnabled(true);
				// Global.hideLoading(loading);
				break;
			case Global.CONNECTION_ERROR:
				btnReply.setEnabled(true);
				// Global.hideLoading(loading);
				Toast.makeText(PostReply.this,
						"Connection error! Please retry", Toast.LENGTH_LONG)
						.show();
				break;
			case Global.ERROR_RESPONSE:
				btnReply.setEnabled(true);
				// Global.hideLoading(loading);
				break;
			case Global.INVALID_SESSION:
				btnReply.setEnabled(true);
				// Global.hideLoading(loading);
				Toast.makeText(PostReply.this,
						"Invalid session! Please relogin", Toast.LENGTH_LONG)
						.show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				sendBroadcast(i);
				break;
			}
		}
	};

	private void prepareNotification() {
		notification = new Notification(android.R.drawable.stat_sys_upload,
				"Uploading file", System.currentTimeMillis());
		notification.flags = notification.flags
				| Notification.FLAG_ONGOING_EVENT;
		notification.contentView = new RemoteViews(getApplicationContext()
				.getPackageName(), R.layout.update_notification);
		PendingIntent contentIntent = PendingIntent.getActivity(
				getApplicationContext(), 0, new Intent(),
				PendingIntent.FLAG_UPDATE_CURRENT);
		notification.contentIntent = contentIntent;
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	}

	private void updateProgress(int file) {
		notificationManager.notify(file, notification);
	}

	private Handler uploadHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Doc2DocRequest.UPLOAD_START:
				notification.contentView.setTextViewText(
						R.id.text,
						"Uploading "
								+ (String) msg.getData().getString("filename"));
				updateProgress(msg.arg1);
				break;
			case Doc2DocRequest.PUBLISH_PROGRESS:
				updateProgress(msg.arg1);
				break;
			case Doc2DocRequest.UPLOAD_FINISH:
				notificationManager.cancel(msg.arg1);
			default:
				break;
			}
		}
	};

}

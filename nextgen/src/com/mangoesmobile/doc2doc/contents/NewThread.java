/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.ActivityGroupBoard;
import com.mangoesmobile.doc2doc.ActivityGroupMessages;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.Login;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.cache.DraftManager;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.ImageLoader;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class NewThread extends Activity {

	GoogleAnalyticsTracker tracker;

	boolean activityOpen = false;
	boolean isPosting = false;

	// configure the notification
	Notification notification;
	NotificationManager notificationManager;

	ImageView imgUser;
	EditText txtTitle;
	EditText txtDetails;
	EditText txtTaggedKeywords;
	Button btnAttach;
	TextView lblNoOfFiles;
	Button btnNotify;
	TextView lblNoOfTags;
	Button btnPost;
	Button btnSave;

	boolean fromDraft = false;
	boolean offline = false;

	private ArrayList<CharSequence> attachedFiles = new ArrayList<CharSequence>();
	private ArrayList<CharSequence> taggedPeople = new ArrayList<CharSequence>();

	BroadcastReceiver fileReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			attachedFiles = intent.getCharSequenceArrayListExtra("files");
			if (attachedFiles.size() > 0) {
				lblNoOfFiles.setText("(" + attachedFiles.size() + ")");
			} else {
				lblNoOfFiles.setText("");
			}
		}
	};

	BroadcastReceiver tagReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			taggedPeople = intent.getCharSequenceArrayListExtra("tags");
			if (taggedPeople.size() > 0) {
				lblNoOfTags.setText("(" + taggedPeople.size() + ")");
			} else {
				lblNoOfTags.setText("");
			}
		}
	};

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub
		setContentView(R.layout.postthread);

		fromDraft = getIntent().getBooleanExtra("from_draft", false);
		offline = getIntent().getBooleanExtra("offline", false);
		
		if (!offline && Global.token.equals("")) {
			finish();
			startActivity(new Intent(getApplicationContext(), Login.class));
		}

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("Post New Thread");

		activityOpen = true;

		instantiateLayout();
		prepareNotification();
	}

	@Override
	public void onStart() {
		super.onStart();

		if (Global.profilePic != null) {
			imgUser.setImageDrawable(Global.profilePic);
		}
		putValues();

		IntentFilter fileFilter = new IntentFilter();
		fileFilter.addAction(AttachFile.FILE_ATTACHED);
		registerReceiver(fileReceiver, fileFilter);

		IntentFilter tagFilter = new IntentFilter();
		tagFilter.addAction(NotifyTeam.PEOPLE_TAGGED);
		registerReceiver(tagReceiver, tagFilter);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();

		if (!isPosting) {
			if (txtTitle.getText().length() > 0
					|| txtDetails.getText().length() > 0
					|| attachedFiles.size() > 0) {
				saveToDraft();
				Toast.makeText(NewThread.this, "Saved to drafts",
						Toast.LENGTH_SHORT).show();
				tracker.trackEvent("Click", "Button", "Save Thread Draft", 0);
			}
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		try {
			unregisterReceiver(fileReceiver);
			unregisterReceiver(tagReceiver);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		activityOpen = false;
	}

	private void instantiateLayout() {
		imgUser = (ImageView) findViewById(R.id.img_postthread_user);
		txtTitle = (EditText) findViewById(R.id.txt_postthread_title);
		txtDetails = (EditText) findViewById(R.id.txt_postthread_details);
		txtTaggedKeywords = (EditText) findViewById(R.id.txt_postthread_tags);
		btnAttach = (Button) findViewById(R.id.btn_postthread_attach);
		lblNoOfFiles = (TextView) findViewById(R.id.lbl_postthread_noofattachment);
		btnNotify = (Button) findViewById(R.id.btn_postthread_notify);
		lblNoOfTags = (TextView) findViewById(R.id.lbl_postthread_noofpeople);
		btnPost = (Button) findViewById(R.id.btn_postthread_post);
		btnSave = (Button) findViewById(R.id.btn_postthread_save);
		if (offline) {
			btnPost.setVisibility(View.GONE);
			btnNotify.setVisibility(View.GONE);
		}
	}

	public void onClick(View v) {
		if (v.getId() == R.id.btn_postthread_attach) {
			Intent intent = new Intent(NewThread.this, AttachFile.class);
			intent.putExtra("subject", txtTitle.getText().toString());
			intent.putExtra("username", "by you");
			if (attachedFiles.size() > 0) {
				intent.putExtra("has_attachments", true);
				intent.putCharSequenceArrayListExtra("attachments",
						attachedFiles);
			}
			if (getParent() instanceof ActivityGroupBoard) {
				ActivityGroupBoard parent = (ActivityGroupBoard) getParent();
				parent.startChildActivity("attach_file", intent);
			} else if (getParent() instanceof ActivityGroupMessages) {
				ActivityGroupMessages parent = (ActivityGroupMessages) getParent();
				parent.startChildActivity("attach_file", intent);
			} else {
				startActivity(intent);
			}
			tracker.trackEvent("Click", "Button",
					"Launch Attach File Activity", 0);
		} else if (v.getId() == R.id.btn_postthread_notify) {
			Intent intent = new Intent(NewThread.this, NotifyTeam.class);
			if (taggedPeople.size() > 0) {
				intent.putExtra("has_tags", true);
				intent.putCharSequenceArrayListExtra("tags", taggedPeople);
			}
			if (getParent() instanceof ActivityGroupBoard) {
				ActivityGroupBoard parent = (ActivityGroupBoard) getParent();
				parent.startChildActivity("notify_team", intent);
			} else {
				ActivityGroupMessages parent = (ActivityGroupMessages) getParent();
				parent.startChildActivity("notify_team", intent);
			}
			tracker.trackEvent("Click", "Button",
					"Launch Notify People Activity", 0);
		} else if (v.getId() == R.id.btn_postthread_post) {
			if (txtTitle.getText().length() > 0
					&& txtDetails.getText().length() > 0) {
				postThread(txtTitle.getText().toString(), txtDetails.getText()
						.toString(), txtTaggedKeywords.getText().toString());
				isPosting = true;
				tracker.trackEvent("Click", "Button", "Post Thread", 0);
			} else {
				Activity parent;
				if (offline) {
					parent = NewThread.this;
				} else {
					parent = getParent();
				}
				Global.createAlert(parent,
						"Please enter title and(or) details.", "OK");
			}
		} else if (v.getId() == R.id.btn_postthread_save) {
			if (txtTitle.getText().length() > 0
					&& txtDetails.getText().length() > 0) {
				saveToDraft();
				Toast.makeText(NewThread.this, "Saved to drafts",
						Toast.LENGTH_SHORT).show();
				if (activityOpen) {
					finish();
				}
				tracker.trackEvent("Click", "Button", "Save Thread Draft", 0);
			} else {
				Activity parent;
				if (offline) {
					parent = NewThread.this;
				} else {
					parent = getParent();
				}
				Global.createAlert(parent,
						"Please enter title and(or) details.", "OK");
			}
		}
	}

	private void saveToDraft() {
		DraftManager manager = new DraftManager(NewThread.this, "thread_draft");
		manager.open();
		if (fromDraft) {
			manager.deleteContent(getIntent().getStringExtra("id"));
		}
		manager.insertContent(txtTitle.getText().toString(), txtDetails
				.getText().toString(), Global.getStringFromList(",",
				attachedFiles), txtTaggedKeywords.getText().toString(), Global
				.getStringFromList(",", taggedPeople), "null", new Timestamp(
				System.currentTimeMillis()).toLocaleString());
		manager.close();
		sendBroadcast(new Intent(D2DTabHost.REFRESH_MESSAGES));
	}

	private void putValues() {
		ImageLoader il = new ImageLoader(getApplicationContext());
		il.DisplayImage(
				Global.url + Global.userInfo.getString("profile_pic_file_path"),
				imgUser, null);
		if (fromDraft) {
			txtTitle.setText(getIntent().getStringExtra("subject"));
			txtDetails.setText(getIntent().getStringExtra("contents"));
			txtTaggedKeywords.setText(getIntent().getStringExtra(
					"tagged_keywords"));
			String attachments = getIntent().getStringExtra("attached_files");
			if (attachments != null && !attachments.equals("null")) {
				String[] attachs = attachments.split(",");
				for (int i = 0; i < attachs.length; i++) {
					attachedFiles.add(attachs[i]);
				}
				if (attachedFiles.size() > 0) {
					lblNoOfFiles.setText("(" + attachedFiles.size() + ")");
				}
			}
			String tagged = getIntent().getStringExtra("tagged_users");
			if (tagged != null && !tagged.equals("null")) {
				String[] tags = tagged.split(",");
				for (int i = 0; i < tags.length; i++) {
					taggedPeople.add(tags[i]);
				}
				if (taggedPeople.size() > 0) {
					lblNoOfTags.setText("(" + taggedPeople.size() + ")");
				}
			}
		} else if (getIntent().getAction() != null && getIntent().getAction().equals(Intent.ACTION_SEND)) {
			if (getIntent().getExtras().containsKey(Intent.EXTRA_STREAM)) {
				Uri uri = (Uri) getIntent().getExtras().getParcelable(
						Intent.EXTRA_STREAM);
				Log.d("path", uri.toString());
				if (uri.toString().startsWith("file")) {
					attachedFiles.add(uri.toString());
				} else {
					attachedFiles.add(getPath(uri));
				}
				lblNoOfFiles.setText("(" + attachedFiles.size() + ")");
			}
		}
	}

	private String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		startManagingCursor(cursor);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	private void postThread(String subject, String contents, String tags) {
		Log.d("PostNew", "posting a thread");
		// Global.showLoading(loading, "Posting thread...", false);
		Toast.makeText(NewThread.this, "Posting thread...", Toast.LENGTH_LONG)
				.show();
		btnPost.setEnabled(false);
		Doc2DocRequest dr = new Doc2DocRequest(uploadHandler);
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", Global.token);
		params.putString("subject", subject);
		params.putString("contents", contents);
		params.putString("tag_user_ids",
				Global.getStringFromList("-", taggedPeople));
		params.putString("tag_keywords", Global.formatValues(tags, "null"));
		for (int i = 0; i < attachedFiles.size(); i++) {
			Log.d("file_" + (i + 1), attachedFiles.get(i).toString());
			params.putString("file_" + (i + 1), attachedFiles.get(i).toString());
		}

		asyncTask.runTask("thread/post_new", params, new Doc2DocTaskListener() {

			@Override
			public void onMalformedURLException(MalformedURLException e) {
				// TODO Auto-generated method stub
				postHandler.sendMessage(Message.obtain(postHandler,
						Global.ERROR_RESPONSE));
				Log.d("post-new-response-m", e.getMessage());
			}

			@Override
			public void onIOException(IOException e) {
				// TODO Auto-generated method stub
				postHandler.sendMessage(Message.obtain(postHandler,
						Global.CONNECTION_ERROR));
				Log.d("post-new-response-i", e.getMessage());
			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e) {
				// TODO Auto-generated method stub
				postHandler.sendMessage(Message.obtain(postHandler,
						Global.ERROR_RESPONSE));
				Log.d("post-new-response-f", e.getMessage());
			}

			@Override
			public void onComplete(String response) {
				Log.d("post-new-response-c", response);
				if (response.equals("TRUE")) {
					postHandler.sendMessage(Message.obtain(postHandler,
							Global.GOT_RESPONSE));
				} else {
					postHandler.sendMessage(Message.obtain(postHandler,
							Global.INVALID_SESSION));
				}
			}
		});
	}

	private Handler postHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				isPosting = false;
				btnPost.setEnabled(true);
				// Board.UPDATE_BOARD = true;
				if (fromDraft) {
					DraftManager manager = new DraftManager(NewThread.this,
							"thread_draft");
					manager.open();
					manager.deleteContent(getIntent().getStringExtra("id"));
					manager.close();
				}
				Toast.makeText(NewThread.this, "Your thread has been posted",
						Toast.LENGTH_LONG).show();
				sendBroadcast(new Intent(D2DTabHost.REFRESH_BOARD));
				// sendBroadcast(new Intent(D2DTabHost.REFRESH_MESSAGES));
				if (activityOpen) {
					finish();
				}
				break;
			case Global.EMPTY_RESPONSE:
				isPosting = false;
				btnPost.setEnabled(true);
				// Global.hideLoading(loading);
				break;
			case Global.CONNECTION_ERROR:
				isPosting = false;
				btnPost.setEnabled(true);
				// Global.hideLoading(loading);
				Toast.makeText(NewThread.this,
						"Connection error! Please retry", Toast.LENGTH_LONG)
						.show();
				break;
			case Global.ERROR_RESPONSE:
				isPosting = false;
				btnPost.setEnabled(true);
				// Global.hideLoading(loading);
				break;
			case Global.INVALID_SESSION:
				isPosting = false;
				btnPost.setEnabled(true);
				// Global.hideLoading(loading);
				Toast.makeText(NewThread.this,
						"Invalid session! Please relogin", Toast.LENGTH_LONG)
						.show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				sendBroadcast(i);
				break;
			}
		}
	};

	private void prepareNotification() {
		notification = new Notification(android.R.drawable.stat_sys_upload,
				"Uploading file", System.currentTimeMillis());
		notification.flags |= Notification.FLAG_ONGOING_EVENT;
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.contentView = new RemoteViews(getApplicationContext()
				.getPackageName(), R.layout.update_notification);
		PendingIntent contentIntent = PendingIntent.getActivity(
				getApplicationContext(), 0, new Intent(),
				PendingIntent.FLAG_UPDATE_CURRENT);
		notification.contentIntent = contentIntent;
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	}

	private void updateProgress(int file) {
		notificationManager.notify(file, notification);
	}

	private Handler uploadHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Doc2DocRequest.UPLOAD_START:
				notification.contentView.setTextViewText(
						R.id.text,
						"Uploading "
								+ (String) msg.getData().getString("filename"));
				updateProgress(msg.arg1);
				break;
			case Doc2DocRequest.PUBLISH_PROGRESS:
				updateProgress(msg.arg1);
				break;
			case Doc2DocRequest.UPLOAD_FINISH:
				notificationManager.cancel(msg.arg1);
			default:
				break;
			}
		}
	};

}

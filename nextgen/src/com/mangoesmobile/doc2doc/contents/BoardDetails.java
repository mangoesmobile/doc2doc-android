/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.foound.widget.AmazingListView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.ActivityGroupBoard;
import com.mangoesmobile.doc2doc.ActivityGroupMessages;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.adapters.ThreadDetailsAdapter;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.ImageLoader;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Paint.Align;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextUtils.EllipsizeCallback;
import android.text.TextUtils.TruncateAt;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class BoardDetails extends Activity {

	GoogleAnalyticsTracker tracker;

	ImageView imgUser;
	TextView lblSubject;
	TextView lblAuthor;
	TextView lblContents;
	TextView lblSeeMore;
	Button btnAttachments;
	AmazingListView listBoardReplies;

	Bundle boardContent;

	CharSequence ellipsizedText;
	Bundle replies = new Bundle();

	ThreadDetailsAdapter adapter;

	private BroadcastReceiver refreshReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			// TODO Auto-generated method stub
			if (!arg1.getBooleanExtra("dont_update_self", false)) {
				adapter.notifyMayHaveMorePages();
				fetchThreadDetails();
			}
		}
	};

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub
		setContentView(R.layout.showthread);
		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("Board Details");

		instantiateLayout();
	}

	@Override
	public void onStart() {
		super.onStart();

		// listens for refresh from tab menu
		IntentFilter filter = new IntentFilter();
		filter.addAction(D2DTabHost.REFRESH_BOARD);
		registerReceiver(refreshReceiver, filter);

		putValues();
		
		listBoardReplies.setLoadingView(getLayoutInflater().inflate(
				R.layout.loading_view, null));
		listBoardReplies.setAdapter(adapter);
		adapter.notifyMayHaveMorePages();
		fetchThreadDetails();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		unregisterReceiver(refreshReceiver);
	}

	public void onClick(View v) {
		if (v.getId() == R.id.btn_showthread_location) {

		} else if (v.getId() == R.id.btn_showthread_attachment) {
			String attachments = boardContent.getString("attached_files");
			if (attachments != null) {
				Intent intent = new Intent(BoardDetails.this,
						ViewAttachments.class);
				intent.putExtra("attached_files", attachments);
				if (getParent() instanceof ActivityGroupBoard) {
					ActivityGroupBoard parent = (ActivityGroupBoard) getParent();
					parent.startChildActivity("view_attachment", intent);
				} else {
					ActivityGroupMessages parent = (ActivityGroupMessages) getParent();
					parent.startChildActivity("view_attachment", intent);
				}
			} else {
				Toast.makeText(BoardDetails.this, "No attachment",
						Toast.LENGTH_SHORT).show();
			}
			tracker.trackEvent("Click", "Button", "Launch View Attachments", 0);
		} else if (v.getId() == R.id.btn_showthread_reply) {
			Intent intent = new Intent(BoardDetails.this, PostReply.class);
			intent.putExtra("reply_to", "Thread");
			intent.putExtra("id", boardContent.getString("id"));
			intent.putExtra("subject", boardContent.getString("subject"));
			intent.putExtra("username", boardContent.getString("username"));
			intent.putExtra("profile_pic",
					boardContent.getString("profile_pic"));
			intent.putExtra("timestamp",
					Global.formatDate(boardContent.getString("timestamp")));
			if (getParent() instanceof ActivityGroupBoard) {
				ActivityGroupBoard parent = (ActivityGroupBoard) getParent();
				parent.startChildActivity("reply_thread", intent);
			} else {
				ActivityGroupMessages parent = (ActivityGroupMessages) getParent();
				parent.startChildActivity("reply_thread", intent);
			}
			tracker.trackEvent("Click", "Button", "Create Thread Reply", 0);
		} else if(v.getId() == R.id.lbl_showthread_seemore) {
			if(lblSeeMore.getText().equals("Expand")) {
				lblContents.setText(boardContent.getString("contents"));
				lblSeeMore.setText("Collapse");
			} else {
				lblContents.setText(ellipsizedText);
				lblSeeMore.setText("Expand");
			}
		}
	}

	private void instantiateLayout() {
		imgUser = (ImageView) findViewById(R.id.img_showthread_parentuser);
		lblSubject = (TextView) findViewById(R.id.lbl_showthread_title);
		lblAuthor = (TextView) findViewById(R.id.lbl_showthread_author);
		lblContents = (TextView) findViewById(R.id.lbl_showthread_detail);
		lblSeeMore = (TextView) findViewById(R.id.lbl_showthread_seemore);
		btnAttachments = (Button) findViewById(R.id.btn_showthread_attachment);
		listBoardReplies = (AmazingListView) findViewById(R.id.ls_thread_replies);
		boardContent = getIntent().getBundleExtra("content");
		adapter = new ThreadDetailsAdapter(this);
		if (boardContent.getString("attached_files") == null) {
			btnAttachments.setVisibility(View.GONE);
		}
	}

	// puts the values received from parent activity
	private void putValues() {
		// Global.loadPhoto(BoardDetails.this,
		// Global.url + boardContent.getString("profile_pic"), imgUser,
		// null);
		ImageLoader il = new ImageLoader(getApplicationContext());
		il.DisplayImage(Global.url + boardContent.getString("profile_pic"),
				imgUser, null);
		lblSubject.setText(boardContent.getString("subject"));
		lblAuthor.setText("by " + boardContent.getString("username") + " on "
				+ Global.formatDate(boardContent.getString("timestamp")));

		EllipsizeCallback callback = new EllipsizeCallback() {

			@Override
			public void ellipsized(int start, int end) {
				if (start != 0) {
					// TODO Auto-generated method stub
					lblSeeMore.setVisibility(View.VISIBLE);
//					lblSeeMore.setText(start + " " + end);
				}
			}
		};

		TextPaint mTextPaint = new TextPaint();
		mTextPaint.setAntiAlias(true);
		mTextPaint.setTextSize(16);
		mTextPaint.setColor(0xFF000000);
		mTextPaint.setTextAlign(Align.LEFT);

		ellipsizedText = TextUtils.ellipsize(
				boardContent.getString("contents"), mTextPaint, 900.0f,
				TruncateAt.END, true, callback);
		lblContents.setText(boardContent.getString("contents"));
	}

	private void fetchThreadDetails() {
		Log.d("BoardDetails", "fetching thread details");
		Doc2DocRequest dr = new Doc2DocRequest();
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", Global.token);
		params.putString("thread_id", boardContent.getString("id"));

		asyncTask.runTask("thread/thread_details", params,
				new Doc2DocTaskListener() {

					@Override
					public void onMalformedURLException(MalformedURLException e) {
						// TODO Auto-generated method stub
						boardDetailsHandler.sendMessage(Message.obtain(
								boardDetailsHandler, Global.ERROR_RESPONSE));
						Log.d("board-response", e.getMessage());
					}

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						boardDetailsHandler.sendMessage(Message.obtain(
								boardDetailsHandler, Global.CONNECTION_ERROR));
						Log.d("board-response", e.getMessage());
					}

					@Override
					public void onFileNotFoundException(FileNotFoundException e) {
						// TODO Auto-generated method stub
						boardDetailsHandler.sendMessage(Message.obtain(
								boardDetailsHandler, Global.ERROR_RESPONSE));
						Log.d("board-response", e.getMessage());
					}

					@Override
					public void onComplete(String response) {
						Log.d("board-response", response);
						if (!response.equals("FALSE")) {
							parseJson(response);
						} else {
							boardDetailsHandler.sendMessage(Message
									.obtain(boardDetailsHandler,
											Global.INVALID_SESSION));
						}
					}
				});
	}

	private void parseJson(String response) {
		JSONArray arr = null;
		try {
			arr = new JSONObject(response).getJSONArray("replies");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			boardDetailsHandler.sendMessage(Message.obtain(boardDetailsHandler,
					Global.EMPTY_RESPONSE));
			return;
		}
		if (arr.length() == 0) {
			boardDetailsHandler.sendMessage(Message.obtain(boardDetailsHandler,
					Global.EMPTY_RESPONSE));
			return;
		}
		for (int i = 0; i < arr.length(); i++) {
			Bundle content = new Bundle();
			JSONObject json;
			try {
				json = arr.getJSONObject(i);
				content.putString("id", json.getString("id"));
				content.putString("thread_id", json.getString("thread_id"));
				content.putString("contents", json.getString("contents"));
				content.putString("timestamp", json.getString("timestamp"));
				JSONObject author = json.getJSONObject("author");
				// content.putString("author_id", json.getString("author_id"));
				content.putString("username", author.getString("username"));
				content.putString("profile_pic",
						author.getString("profile_pic_file_path"));
				if (json.has("$attached_files")) {
					JSONArray attachments = json
							.getJSONArray("$attached_files");
					String file_urls = "";
					for (int j = 0; j < attachments.length(); j++) {
						file_urls += attachments.getJSONObject(j).getString(
								"file_path");
						if (j != attachments.length() - 1)
							file_urls += ",";
					}
					content.putString("attached_files", file_urls);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			replies.putBundle(i + "", content);
		}
		boardDetailsHandler.sendMessage(Message.obtain(boardDetailsHandler,
				Global.GOT_RESPONSE));
	}

	private Handler boardDetailsHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				adapter.setData(replies);
				Intent updateIntent = new Intent(D2DTabHost.REFRESH_BOARD);
				updateIntent.putExtra("dont_update_self", true);
				sendBroadcast(updateIntent);
				sendBroadcast(new Intent(D2DTabHost.REFRESH_MESSAGES));
				break;
			case Global.EMPTY_RESPONSE:
				adapter.notifyNoMorePages();
				Intent updateIntent2 = new Intent(D2DTabHost.REFRESH_BOARD);
				updateIntent2.putExtra("dont_update_self", true);
				sendBroadcast(updateIntent2);
				sendBroadcast(new Intent(D2DTabHost.REFRESH_MESSAGES));
				break;
			case Global.CONNECTION_ERROR:
				Toast.makeText(BoardDetails.this,
						"Connection error! Please retry", Toast.LENGTH_LONG)
						.show();
				adapter.notifyNoMorePages();
				break;
			case Global.ERROR_RESPONSE:
				break;
			case Global.INVALID_SESSION:
				Toast.makeText(BoardDetails.this,
						"Invalid session! Please relogin", Toast.LENGTH_LONG)
						.show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				sendBroadcast(i);
				break;
			}
		}
	};

}

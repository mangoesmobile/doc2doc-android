/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.ActivityGroupMessages;
import com.mangoesmobile.doc2doc.ActivityGroupTeam;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.cache.DraftManager;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask;
import com.mangoesmobile.doc2doc.util.Doc2DocRequest;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.AsyncDoc2DocTask.Doc2DocTaskListener;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class NewMessage extends Activity {

	GoogleAnalyticsTracker tracker;

	// configure the notification
	Notification notification;
	NotificationManager notificationManager;

	boolean fromDraft;
	boolean activityOpen = false;
	boolean isPosting = false;

	LinearLayout layoutRecipients;
	EditText txtSubject;
	EditText txtMessage;
	EditText txtTaggedKeywords;
	Button btnSend;
	TextView lblNoOfFiles;

	private ArrayList<CharSequence> recipientId = new ArrayList<CharSequence>();
	private ArrayList<CharSequence> recipientName = new ArrayList<CharSequence>();
	protected ArrayList<CharSequence> attachedFiles = new ArrayList<CharSequence>();

	BroadcastReceiver fileReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			attachedFiles = intent.getCharSequenceArrayListExtra("files");
			if (attachedFiles.size() > 0) {
				lblNoOfFiles.setText("(" + attachedFiles.size() + ")");
			} else {
				lblNoOfFiles.setText("");
			}
		}
	};

	BroadcastReceiver tagReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			recipientId = intent.getCharSequenceArrayListExtra("tags");
			recipientName = intent.getCharSequenceArrayListExtra("names");
			addRecipients();
		}
	};

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub
		setContentView(R.layout.sendmessage);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("New Message");

		activityOpen = true;

		instantiateLayout();
		prepareNotification();
		fromDraft = getIntent().getBooleanExtra("from_draft", false);

		if (getIntent().getBooleanExtra("has_recipient", false)) {
			recipientId.add(getIntent().getCharSequenceExtra("id"));
			recipientName.add(getIntent().getCharSequenceExtra("name"));
			addRecipients();
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		putValues();

		IntentFilter fileFilter = new IntentFilter();
		fileFilter.addAction(AttachFile.FILE_ATTACHED);
		registerReceiver(fileReceiver, fileFilter);

		IntentFilter tagFilter = new IntentFilter();
		tagFilter.addAction(NotifyTeam.PEOPLE_TAGGED);
		registerReceiver(tagReceiver, tagFilter);
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();

		if (!isPosting) {
			if (txtSubject.getText().length() > 0
					|| txtMessage.getText().length() > 0) {
				saveToDraft();
				Toast.makeText(NewMessage.this, "Saved to drafts",
						Toast.LENGTH_SHORT).show();
				tracker.trackEvent("Click", "Button", "Save Message Draft", 0);
			}
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		unregisterReceiver(fileReceiver);
		unregisterReceiver(tagReceiver);

		activityOpen = false;
	}

	public void onClick(View v) {
		if (v.getId() == R.id.layout_sendmessage_recipient) {
			Intent intent = new Intent(NewMessage.this, NotifyTeam.class);
			if (recipientId.size() > 0) {
				intent.putExtra("has_tags", true);
				intent.putCharSequenceArrayListExtra("tags", recipientId);
			}
			// determine parent
			if (getParent() instanceof ActivityGroupTeam) {
				ActivityGroupTeam parent = (ActivityGroupTeam) getParent();
				parent.startChildActivity("notify_team", intent);
			} else {
				ActivityGroupMessages parent = (ActivityGroupMessages) getParent();
				parent.startChildActivity("notify_team", intent);
			}
			tracker.trackEvent("Click", "Button",
					"Launch Notify People Activity", 0);
		} else if (v.getId() == R.id.btn_sendmessage_attach) {
			Intent intent = new Intent(NewMessage.this, AttachFile.class);
			intent.putExtra("subject", txtSubject.getText().toString());
			intent.putExtra("username", "by you");
			if (attachedFiles.size() > 0) {
				intent.putExtra("has_attachments", true);
				intent.putCharSequenceArrayListExtra("attachments",
						attachedFiles);
			}
			if (getParent() instanceof ActivityGroupTeam) {
				ActivityGroupTeam parent = (ActivityGroupTeam) getParent();
				parent.startChildActivity("attach_file", intent);
			} else {
				ActivityGroupMessages parent = (ActivityGroupMessages) getParent();
				parent.startChildActivity("attach_file", intent);
			}
			tracker.trackEvent("Click", "Button",
					"Launch Attach File Activity", 0);
		} else if (v.getId() == R.id.btn_sendmessage_send) {
			if (recipientId.size() > 0) {
				if (txtSubject.getText().length() > 0
						&& txtMessage.getText().length() > 0) {
					sendMessage(txtSubject.getText().toString(), txtMessage
							.getText().toString(), txtTaggedKeywords.getText()
							.toString());
					isPosting = true;
					tracker.trackEvent("Click", "Button", "Send Message", 0);
				} else {
					Global.createAlert(getParent(),
							"Please enter subject and(or) message.", "OK");
				}
			} else {
				Global.createAlert(getParent(),
						"Please select at least one recipient.", "OK");
			}
		} else if (v.getId() == R.id.btn_sendmessage_save) {
			if (recipientId.size() > 0) {
				if (txtSubject.getText().length() > 0
						&& txtMessage.getText().length() > 0) {
					saveToDraft();
					Toast.makeText(NewMessage.this, "Saved to drafts",
							Toast.LENGTH_SHORT).show();
					if (activityOpen) {
						finish();
					}
					tracker.trackEvent("Click", "Button", "Save Message Draft",
							0);
				} else {
					Global.createAlert(getParent(),
							"Please enter title and(or) details.", "OK");
				}
			} else {
				Global.createAlert(getParent(),
						"Please select at least one recipient.", "OK");
			}
		}
	}

	private void instantiateLayout() {
		layoutRecipients = (LinearLayout) findViewById(R.id.layout_sendmessage_recipientlist);
		txtSubject = (EditText) findViewById(R.id.txt_sendmessage_subject);
		txtMessage = (EditText) findViewById(R.id.txt_sendmessage_message);
		txtTaggedKeywords = (EditText) findViewById(R.id.txt_sendmessage_tags);
		btnSend = (Button) findViewById(R.id.btn_sendmessage_send);
		lblNoOfFiles = (TextView) findViewById(R.id.lbl_sendmessage_noofattachments);
	}

	private void addRecipients() {
		layoutRecipients.removeAllViews();
		for (int i = 0; i < recipientName.size(); i++) {
			View view = getLayoutInflater().inflate(R.layout.row_text, null);
			TextView lblName = (TextView) view.findViewById(R.id.lbl_text);
			view.findViewById(R.id.btn_text_remove).setVisibility(View.GONE);
			lblName.setText(recipientName.get(i));
			layoutRecipients.addView(view);
		}
	}

	private void putValues() {
		if (fromDraft) {
			txtSubject.setText(getIntent().getStringExtra("subject"));
			txtMessage.setText(getIntent().getStringExtra("contents"));
			txtTaggedKeywords.setText(getIntent().getStringExtra(
					"tagged_keywords"));
			String attachments = getIntent().getStringExtra("attached_files");
			if (attachments != null && !attachments.equals("null")) {
				String[] attachs = attachments.split(",");
				for (int i = 0; i < attachs.length; i++) {
					attachedFiles.add(attachs[i]);
				}
				if (attachedFiles.size() > 0) {
					lblNoOfFiles.setText("(" + attachedFiles.size() + ")");
				}
			}
			String tagged = getIntent().getStringExtra("tagged_users");
			if (tagged != null && !tagged.equals("null")) {
				String[] tags = tagged.split(",");
				for (int i = 0; i < tags.length; i++) {
					recipientId.add(tags[i]);
				}
			}
			String tagged_names = getIntent().getStringExtra(
					"tagged_user_names");
			if (tagged_names != null && !tagged_names.equals("null")) {
				String[] tags = tagged_names.split(",");
				for (int i = 0; i < tags.length; i++) {
					recipientName.add(tags[i]);
				}
				addRecipients();
			}
		}
	}

	private void saveToDraft() {
		DraftManager manager = new DraftManager(NewMessage.this,
				"message_draft");
		manager.open();
		if (fromDraft) {
			manager.deleteContent(getIntent().getStringExtra("id"));
		}
		manager.insertContent(txtSubject.getText().toString(), txtMessage
				.getText().toString(), Global.getStringFromList(",",
				attachedFiles), txtTaggedKeywords.getText().toString(), Global
				.getStringFromList(",", recipientId), Global.getStringFromList(
				",", recipientName), new Timestamp(System.currentTimeMillis()).toLocaleString());
		manager.close();
		sendBroadcast(new Intent(D2DTabHost.REFRESH_MESSAGES));
	}

	private void sendMessage(String subject, String contents, String tags) {
		Log.d("NewMessage", "sending message");
		// Global.showLoading(loading, "Sending Message...", false);
		Toast.makeText(NewMessage.this, "Sending Message...", Toast.LENGTH_LONG)
				.show();
		btnSend.setEnabled(false);
		Doc2DocRequest dr = new Doc2DocRequest(uploadHandler);
		AsyncDoc2DocTask asyncTask = new AsyncDoc2DocTask(dr);

		Bundle params = new Bundle();
		params.putString("access_token", Global.token);
		params.putString("subject", subject);
		params.putString("contents", contents);
		params.putString("tag_user_ids",
				Global.getStringFromList("-", recipientId));
		params.putString("tag_keywords", Global.formatValues(tags, "null"));
		for (int i = 0; i < attachedFiles.size(); i++) {
			params.putString("file_" + (i + 1), attachedFiles.get(i).toString());
		}

		asyncTask.runTask("message/post_new", params,
				new Doc2DocTaskListener() {

					@Override
					public void onMalformedURLException(MalformedURLException e) {
						// TODO Auto-generated method stub
						messageHandler.sendMessage(Message.obtain(
								messageHandler, Global.ERROR_RESPONSE));
						Log.d("send-new-response", e.getMessage());
					}

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						messageHandler.sendMessage(Message.obtain(
								messageHandler, Global.CONNECTION_ERROR));
						Log.d("send-new-response", e.getMessage());
					}

					@Override
					public void onFileNotFoundException(FileNotFoundException e) {
						// TODO Auto-generated method stub
						messageHandler.sendMessage(Message.obtain(
								messageHandler, Global.ERROR_RESPONSE));
						Log.d("send-new-response", e.getMessage());
					}

					@Override
					public void onComplete(String response) {
						Log.d("send-new-response", response);
						if (response.equals("TRUE")) {
							messageHandler.sendMessage(Message.obtain(
									messageHandler, Global.GOT_RESPONSE));
						} else {
							messageHandler.sendMessage(Message.obtain(
									messageHandler, Global.INVALID_SESSION));
						}
					}
				});
	}

	private Handler messageHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Global.GOT_RESPONSE:
				isPosting = false;
				btnSend.setEnabled(true);
				if (fromDraft) {
					DraftManager manager = new DraftManager(NewMessage.this,
							"message_draft");
					manager.open();
					manager.deleteContent(getIntent().getStringExtra("id"));
					manager.close();
				}
				sendBroadcast(new Intent(D2DTabHost.REFRESH_MESSAGES));
				Toast.makeText(NewMessage.this, "Your message has been sent",
						Toast.LENGTH_LONG).show();
				if (activityOpen) {
					finish();
				}
				break;
			case Global.EMPTY_RESPONSE:
				isPosting = false;
				btnSend.setEnabled(true);
				// Global.hideLoading(loading);
				break;
			case Global.CONNECTION_ERROR:
				isPosting = false;
				btnSend.setEnabled(true);
				// Global.hideLoading(loading);
				Toast.makeText(NewMessage.this,
						"Connection error! Please retry", Toast.LENGTH_LONG)
						.show();
				break;
			case Global.ERROR_RESPONSE:
				isPosting = false;
				btnSend.setEnabled(true);
				// Global.hideLoading(loading);
				break;
			case Global.INVALID_SESSION:
				isPosting = false;
				btnSend.setEnabled(true);
				// Global.hideLoading(loading);
				Toast.makeText(NewMessage.this,
						"Invalid session! Please relogin", Toast.LENGTH_LONG)
						.show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				sendBroadcast(i);
				break;
			}
		}
	};

	private void prepareNotification() {
		notification = new Notification(android.R.drawable.stat_sys_upload,
				"Uploading file", System.currentTimeMillis());
		notification.flags |= Notification.FLAG_ONGOING_EVENT;
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.contentView = new RemoteViews(getApplicationContext()
				.getPackageName(), R.layout.update_notification);
		PendingIntent contentIntent = PendingIntent.getActivity(
				getApplicationContext(), 0, new Intent(),
				PendingIntent.FLAG_UPDATE_CURRENT);
		notification.contentIntent = contentIntent;
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	}

	private void updateProgress(int file) {
		notificationManager.notify(file, notification);
	}

	private Handler uploadHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Doc2DocRequest.UPLOAD_START:
				notification.contentView.setTextViewText(
						R.id.text,
						"Uploading "
								+ (String) msg.getData().getString("filename"));
				updateProgress(msg.arg1);
				break;
			case Doc2DocRequest.PUBLISH_PROGRESS:
				updateProgress(msg.arg1);
				break;
			case Doc2DocRequest.UPLOAD_FINISH:
				notificationManager.cancel(msg.arg1);
			default:
				break;
			}
		}
	};

}

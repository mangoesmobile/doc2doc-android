/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.ActivityGroupMessages;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.adapters.MessagesGridAdapter;
import com.mangoesmobile.doc2doc.cache.DraftManager;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.ImageLoader;
import com.mangoesmobile.doc2doc.util.UserInfo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class Messages extends Activity {

	GoogleAnalyticsTracker tracker;

	public static final int MY_THREADS = 0;
	public static final int THREAD_DRAFTS = 1;
	public static final int INBOX = 2;
	public static final int SENT = 3;
	public static final int MESSAGE_DRAFTS = 4;
	public static final int PINGS = 5;

	public static boolean updateNotification = false;

	private ImageView imgUser;
	private ProgressBar progUser;
	private ImageView imgUserStatus;
	private TextView lblUsername;
	private TextView lblUserStatus;
	private GridView gridButtons;

	private Bundle data;
	private MessagesGridAdapter adapter;

	BroadcastReceiver userInfoReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			int response = intent
					.getIntExtra("response", Global.ERROR_RESPONSE);
			switch (response) {
			case Global.GOT_RESPONSE:
				updateUserInfo();
				break;
			case Global.CHANGE_STATUS:
				Global.userStatus = intent.getStringExtra("userStatus");
				Global.userInfo.putString("availability_status",
						Global.userStatus);
				updateUserInfo();
				break;
			case Global.CHANGE_PIC:
				fetchUserInfo();
				break;
			case Global.EMPTY_RESPONSE:
				break;
			case Global.CONNECTION_ERROR:
				Toast.makeText(Messages.this, "Connection error! Please retry",
						Toast.LENGTH_LONG).show();
				break;
			case Global.ERROR_RESPONSE:
				break;
			case Global.INVALID_SESSION:
				Toast.makeText(Messages.this,
						"Invalid session! Please relogin", Toast.LENGTH_LONG)
						.show();
				Intent i = new Intent(D2DTabHost.INVALID_SESSION);
				sendBroadcast(i);
				break;
			}
		}
	};

	BroadcastReceiver notificationreciver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			updateNotification();
		}
	};

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub
		setContentView(R.layout.messages);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView("Message Center");

		instantiateLayout();
		generateButtons();
	}

	@Override
	public void onStart() {
		super.onStart();

		gridButtons.setAdapter(adapter);
		adapter.setData(data);
		gridButtons.setOnItemClickListener(new GridItemClickListener());

		// listens for update of user info
		IntentFilter userFilter = new IntentFilter();
		userFilter.addAction(UserInfo.GOT_USERINFO);
		registerReceiver(userInfoReceiver, userFilter);

		// listens for update of user info
		IntentFilter notificationFilter = new IntentFilter();
		notificationFilter.addAction(D2DTabHost.NEW_NOTIFICATION);
		registerReceiver(notificationreciver, notificationFilter);

		if (Global.userInfo.size() == 0) {
			fetchUserInfo();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		if (Global.userInfo.size() > 0) {
			updateUserInfo();
		}

		if (updateNotification) {
			updateNotification();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		unregisterReceiver(userInfoReceiver);
		unregisterReceiver(notificationreciver);
	}

	public void onClick(View v) {
		if (v.getId() == R.id.img_messages_userimage) {
			Toast.makeText(Messages.this, "Changing status...",
					Toast.LENGTH_LONG).show();
			UserInfo.changeStatus(Messages.this,
					Global.userStatus.equals("online") ? "on call" : "online");
			tracker.trackEvent("Click", "ImageView", "Change Status", 0);
		} else if (v.getId() == R.id.btn_messages_location) {
			Intent intent = new Intent(Messages.this, UserLocation.class);
			intent.putExtra("update_own", true);
			ActivityGroupMessages parent = (ActivityGroupMessages) getParent();
			parent.startChildActivity("my_location", intent);
			tracker.trackEvent("Click", "Button", "Show Own Location", 0);
		}
	}

	private void instantiateLayout() {
		imgUser = (ImageView) findViewById(R.id.img_messages_userimage);
		progUser = (ProgressBar) findViewById(R.id.prog_messages_userimage);
		imgUserStatus = (ImageView) findViewById(R.id.img_messages_userstatus);
		lblUsername = (TextView) findViewById(R.id.lbl_messages_username);
		lblUserStatus = (TextView) findViewById(R.id.lbl_messages_userstatus);
		gridButtons = (GridView) findViewById(R.id.grid_messages);
		adapter = new MessagesGridAdapter(Messages.this);
	}

	private void generateButtons() {
		final int[] icons = { R.drawable.btn_threads_click,
				R.drawable.btn_message_click, R.drawable.btn_ping_click,
				R.drawable.btn_drafts_click, R.drawable.btn_swinfen_click };
		final String[] titles = { "Public", "Private", "Pings", "Drafts",
				"Swinfen" };
		final String[] notifications = { Global.thread, Global.message,
				Global.ping, null, null };
		data = new Bundle();
		for (int i = 0; i < icons.length; i++) {
			Bundle content = new Bundle();
			content.putInt("icon", icons[i]);
			content.putString("title", titles[i]);
			content.putString("notifications", notifications[i]);
			data.putBundle(String.valueOf(i), content);
		}
	}

	private void updateNotification() {
		if (!Global.thread.equals("0")) {
			// TODO Auto-generated method stub
			Bundle content = data.getBundle("0");
			content.putString("notifications", Global.thread);
		}
		if (!Global.message.equals("0")) {
			Bundle content = data.getBundle("1");
			content.putString("notifications", Global.message);
		}
		if (!Global.ping.equals("0")) {
			Bundle content = data.getBundle("2");
			content.putString("notifications", Global.ping);
		}
		adapter.setData(data);
		updateNotification = false;
	}

	private void fetchUserInfo() {
		UserInfo.fetchUserInfo(Messages.this);
	}

	private void updateUserInfo() {
		String userStatus = Global.userInfo.getString("availability_status");
		Log.d("user-status", userStatus);
		if (userStatus.equals("online")) {
			imgUserStatus.setImageResource(R.drawable.bg_photo_online);
			lblUserStatus.setTextColor(Color.rgb(0, 102, 153));
		} else if (userStatus.equals("on call")) {
			imgUserStatus.setImageResource(R.drawable.bg_photo_oncall);
			lblUserStatus.setTextColor(Color.rgb(101, 176, 73));
		}
		lblUserStatus.setText(userStatus.toUpperCase());
		lblUsername.setText(Global.userInfo.getString("title") + " "
				+ Global.userInfo.getString("first_name").substring(0, 1)
				+ ". " + Global.userInfo.getString("last_name"));
		if (Global.profilePic == null) {
			// Global.loadPhoto(Messages.this, Global.url +
			// Global.userInfo.getString("profile_pic_file_path"), imgUser,
			// progUser);
			ImageLoader il = new ImageLoader(getApplicationContext());
			il.DisplayImage(
					Global.url
							+ Global.userInfo
									.getString("profile_pic_file_path"),
					imgUser, progUser);
		} else {
			imgUser.setImageDrawable(Global.profilePic);
			progUser.setVisibility(View.GONE);
		}
	}

	private void chooseMessageOption() {
		final CharSequence[] items = { "New Message", "Inbox", "Sent" };
		AlertDialog.Builder builder = new AlertDialog.Builder(getParent());
		builder.setTitle("Messages");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				Intent intent;
				ActivityGroupMessages parent = (ActivityGroupMessages) getParent();
				switch (item) {
				case 0:
					intent = new Intent(Messages.this, NewMessage.class);
					intent.putExtra("has_recipient", false);					
					parent.startChildActivity("new_message", intent);
					tracker.trackEvent("Click", "Button", "Create New Message",
							0);
					break;
				case 1:
					intent = new Intent(Messages.this,
							Notifications.class);
					intent.putExtra("command", INBOX);
					intent.putExtra("title", "Inbox");
					parent.startChildActivity("inbox", intent);
					Bundle content = data.getBundle("1");
					content.putString("notifications", Global.message);
					adapter.setData(data);
					tracker.trackEvent("Click", "Button", "Show Inbox", 0);
					break;
				case 2:
					intent = new Intent(Messages.this,
							Notifications.class);
					intent.putExtra("command", SENT);
					intent.putExtra("title", "Sent");
					parent.startChildActivity("sent", intent);
					tracker.trackEvent("Click", "Button", "Show Sentbox", 0);
					break;
				}
			}
		});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	private void chooseDraftOption() {
		DraftManager boardManager = new DraftManager(Messages.this,
				"thread_draft");
		boardManager.open();
		long boardSize = boardManager.getCount();
		boardManager.close();

		DraftManager messageManager = new DraftManager(Messages.this,
				"message_draft");
		messageManager.open();
		long messageSize = messageManager.getCount();
		messageManager.close();
		String board = "Public" + (boardSize > 0 ? " (" + boardSize + ")" : "");
		String message = "Private"
				+ (messageSize > 0 ? " (" + messageSize + ")" : "");
		final CharSequence[] items = { board, message };
		AlertDialog.Builder builder = new AlertDialog.Builder(getParent());
		builder.setTitle("Drafts");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				Intent intent;
				ActivityGroupMessages parent = (ActivityGroupMessages) getParent();
				;
				switch (item) {
				case 0:
					intent = new Intent(Messages.this, Notifications.class);
					intent.putExtra("command", THREAD_DRAFTS);
					intent.putExtra("title", "Thread Drafts");
					parent.startChildActivity("thread_drafts", intent);
					tracker.trackEvent("Click", "Button", "Show Thread Drafts",
							0);
					break;
				case 1:
					intent = new Intent(Messages.this, Notifications.class);
					intent.putExtra("command", MESSAGE_DRAFTS);
					intent.putExtra("title", "Message Drafts");
					parent.startChildActivity("message_drafts", intent);
					tracker.trackEvent("Click", "Button",
							"Show Message Drafts", 0);
					break;
				}
			}
		});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	class GridItemClickListener implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			Intent intent;
			ActivityGroupMessages parent = (ActivityGroupMessages) getParent();
			Bundle content;
			switch (arg2) {
			case 0:
				Global.thread = "0";
				intent = new Intent(Messages.this, Notifications.class);
				intent.putExtra("command", MY_THREADS);
				intent.putExtra("title", "My Threads");
				parent.startChildActivity("my_threads", intent);
				content = data.getBundle("0");
				content.putString("notifications", Global.thread);
				adapter.setData(data);
				tracker.trackEvent("Click", "Button", "Show My Threads", 0);
				break;
			case 1:
				if (Global.message.equals("0")) {
					chooseMessageOption();
				} else {
					Global.message = "0";
					intent = new Intent(Messages.this, Notifications.class);
					intent.putExtra("command", INBOX);
					intent.putExtra("title", "Inbox");
					parent.startChildActivity("inbox", intent);
					content = data.getBundle("1");
					content.putString("notifications", Global.message);
					adapter.setData(data);
					tracker.trackEvent("Click", "Button", "Show Inbox", 0);
				}
				break;
			case 2:
				Global.ping = "0";
				intent = new Intent(Messages.this, Notifications.class);
				intent.putExtra("command", PINGS);
				intent.putExtra("title", "Pings");
				parent.startChildActivity("my_threads", intent);
				content = data.getBundle("2");
				content.putString("notifications", Global.ping);
				adapter.setData(data);
				tracker.trackEvent("Click", "Button", "Show My Pings", 0);
				break;
			case 3:
				chooseDraftOption();
				break;
			case 4:
				intent = new Intent();
				intent.setClassName("com.mangoesmobile.swinfen",
						"com.mangoesmobile.swinfen.SwinfenLauncher");
				parent.startActivity(intent);
				break;
			}
		}

	}

}

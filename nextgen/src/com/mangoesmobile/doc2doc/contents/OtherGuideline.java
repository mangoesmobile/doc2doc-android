/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.ActivityGroupReference;
import com.mangoesmobile.doc2doc.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class OtherGuideline extends Activity {

	GoogleAnalyticsTracker tracker;
	
	Button btnMD;
	Button btnEP;
	Button btnMDCalc;
	Button btnMDConsult;
	Button btnCT;
	Button btnTN;
	Button btn5MC;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);	
	    // TODO Auto-generated method stub
	    setContentView(R.layout.other_guidelines);
	    
	    tracker = GoogleAnalyticsTracker.getInstance();
	    tracker.trackPageView("Other Guideline");
	    
	    instantiateLayout();
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		btnMD.setOnClickListener(new ButtonClickListener(0));		
		btnEP.setOnClickListener(new ButtonClickListener(1));		
		btnMDCalc.setOnClickListener(new ButtonClickListener(2));		
		btnMDConsult.setOnClickListener(new ButtonClickListener(3));		
		btnCT.setOnClickListener(new ButtonClickListener(4));		
		btnTN.setOnClickListener(new ButtonClickListener(5));		
		btn5MC.setOnClickListener(new ButtonClickListener(6));		
	}
	
	private void instantiateLayout() {
		btnMD = (Button) findViewById(R.id.btn_medical_dictionary);
		btnEP = (Button) findViewById(R.id.btn_epocrates);
		btnMDCalc = (Button) findViewById(R.id.btn_mdcalc);
		btnMDConsult = (Button) findViewById(R.id.btn_mdconsult);
		btnCT = (Button) findViewById(R.id.btn_clinical_trails);
		btnTN = (Button) findViewById(R.id.btn_toxnet);
		btn5MC = (Button) findViewById(R.id.btn_5min_consult);
	}
	
	class ButtonClickListener implements OnClickListener {

		int index;
		
		public ButtonClickListener(int index) {
			this.index = index;
		}
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			ActivityGroupReference parent = (ActivityGroupReference) getParent();
			Intent intent;
			switch (index) {
			case 0:
				intent = new Intent(OtherGuideline.this, WebReferences.class);
				intent.putExtra("url", "http://www.merriam-webster.com/mobilemedlineplus/");
				parent.startChildActivity("web_reference", intent);
				tracker.trackEvent("Click", "Button", "Medical Dictionary", 0);
				break;
			case 1:
				intent = new Intent(OtherGuideline.this, WebReferences.class);
				intent.putExtra("url", "https://online.epocrates.com/noFrame/");
				parent.startChildActivity("web_reference", intent);
				tracker.trackEvent("Click", "Button", "Epocrates", 0);
				break;
			case 2:
				intent = new Intent(OtherGuideline.this, WebReferences.class);
				intent.putExtra("url", "http://www.mdcalc.com/");
				parent.startChildActivity("web_reference", intent);
				tracker.trackEvent("Click", "Button", "MDCalc", 0);
				break;
			case 3:
				intent = new Intent(OtherGuideline.this, WebReferences.class);
				intent.putExtra("url", "http://m.mdconsult.com/login/login.do?URI=http://mobile.mdconsult.com/home/home.do");
				parent.startChildActivity("web_reference", intent);
				tracker.trackEvent("Click", "Button", "MDConsult", 0);
				break;
			case 4:
				intent = new Intent(OtherGuideline.this, WebReferences.class);
				intent.putExtra("url", "http://pubmedhh.nlm.nih.gov/nlmd/clinicaltrials/ctsearch.html");
				parent.startChildActivity("web_reference", intent);
				tracker.trackEvent("Click", "Button", "Clinical Trials", 0);
				break;
			case 5:
				intent = new Intent(OtherGuideline.this, WebReferences.class);
				intent.putExtra("url", "http://toxnet.nlm.nih.gov/pda/");
				parent.startChildActivity("web_reference", intent);
				tracker.trackEvent("Click", "Button", "ToxNet", 0);
				break;
			case 6:
				intent = new Intent(OtherGuideline.this, WebReferences.class);
				intent.putExtra("url", "http://m.5minuteconsult.com/login");
				parent.startChildActivity("web_reference", intent);
				tracker.trackEvent("Click", "Button", "5 Minute Consult", 0);
				break;

			default:
				break;
			}
		}
		
	}

}

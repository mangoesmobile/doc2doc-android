/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.util.Global;
import com.mangoesmobile.doc2doc.util.ImageLoader;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class Vcard extends Activity {

	ImageView imgUser;
	TextView lblName;
	TextView lblUsername;
	TextView lblEmail;
	TextView lblPhone;
	TextView lblOrganization;
	TextView lblSpecialization;

	ImageLoader imageLoader;
	Bundle content;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);	
	    // TODO Auto-generated method stub
	    setContentView(R.layout.vcard);
	    
	    instantiateLayout();
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		putValues();
	}
	
	private void instantiateLayout() {
		imgUser = (ImageView) findViewById(R.id.img_vcard_user);
		lblName = (TextView) findViewById(R.id.lbl_vcard_name);
		lblUsername = (TextView) findViewById(R.id.lbl_vcard_username);
		lblEmail = (TextView) findViewById(R.id.lbl_vcard_email);
		lblPhone = (TextView) findViewById(R.id.lbl_vcard_phone);
		lblOrganization = (TextView) findViewById(R.id.lbl_vcard_organization);
		lblSpecialization = (TextView) findViewById(R.id.lbl_vcard_specialization);

		imageLoader = new ImageLoader(Vcard.this);
		content = getIntent().getBundleExtra("data");
	}
	
	private void putValues() {
		imageLoader.DisplayImage(Global.url + content.getString("profile_pic_file_path"), imgUser, null);
		lblName.setText(content.getString("first_name") + " " + content.getString("last_name"));
		lblUsername.setText(lblUsername.getText() + content.getString("username"));
		lblEmail.setText(lblEmail.getText() + content.getString("email"));
		lblPhone.setText(lblPhone.getText() + (content.getString("phone").equals("null") ? "" : content.getString("phone")));
		lblOrganization.setText(lblOrganization.getText() + (content.getString("organization").equals("null") ? "" : content.getString("organization")));
		lblSpecialization.setText(lblSpecialization.getText() + (content.getString("specialization").equals("null") ? "" : content.getString("specialization")));
	}

}

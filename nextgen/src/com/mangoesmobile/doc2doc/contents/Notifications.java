/**
 * 
 */
package com.mangoesmobile.doc2doc.contents;

import com.foound.widget.AmazingListView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mangoesmobile.doc2doc.ActivityGroupMessages;
import com.mangoesmobile.doc2doc.D2DTabHost;
import com.mangoesmobile.doc2doc.R;
import com.mangoesmobile.doc2doc.adapters.DraftAdapter;
import com.mangoesmobile.doc2doc.adapters.MyMessageAdapter;
import com.mangoesmobile.doc2doc.adapters.MyThreadAdapter;
import com.mangoesmobile.doc2doc.cache.BoardMessageCacheManager;
import com.mangoesmobile.doc2doc.notification.MessageNotificationManager;
import com.mangoesmobile.doc2doc.notification.PingNotificationManager;
import com.mangoesmobile.doc2doc.notification.DraftNotificationManager;
import com.mangoesmobile.doc2doc.notification.ThreadNotificationManager;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

/**
 * @author Jamael Tanveer, Copyright: ManGoes Mobile Inc.
 **/
public class Notifications extends ListActivity {

	GoogleAnalyticsTracker tracker;
	
	public int command;

	TextView lblTitle;
	EditText txtSearch;

	private BroadcastReceiver refreshReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			// TODO Auto-generated method stub
			fetchData();
		}
	};

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// TODO Auto-generated method stub
		setContentView(R.layout.messages_notification);

		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackPageView(getIntent().getStringExtra("title"));

		command = getIntent().getIntExtra("command", 0);

		instantiateLayout();
	}

	@Override
	public void onStart() {
		super.onStart();

		// listens for refresh from tab menu
		IntentFilter filter = new IntentFilter();
		filter.addAction(D2DTabHost.REFRESH_MESSAGES);
		registerReceiver(refreshReceiver, filter);

		lblTitle.setText(getIntent().getStringExtra("title"));
		AmazingListView list = (AmazingListView) getListView();
		list.setLoadingView(getLayoutInflater().inflate(R.layout.loading_view,
				null));
		fetchData();
	}

	@Override
	public void onDestroy() {
		super.onResume();

		unregisterReceiver(refreshReceiver);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Intent intent;
		Bundle content;
		ActivityGroupMessages parent = (ActivityGroupMessages) getParent();
		switch (command) {
		case Messages.MY_THREADS:
			content = (Bundle) ((MyThreadAdapter) l.getAdapter())
					.getItem(position);
			intent = new Intent(Notifications.this, BoardDetails.class);
			// intent.putExtra("id", content.getString("id"));
			// intent.putExtra("subject", content.getString("subject"));
			// intent.putExtra("contents", content.getString("contents"));
			// intent.putExtra("timestamp",
			// Global.formatDate(content.getString("timestamp")));
			// intent.putExtra("user_id", content.getString("user_id"));
			// intent.putExtra("username", content.getString("username"));
			// intent.putExtra("profile_pic", content.getString("profile_pic"));
			// intent.putExtra("attached_files",
			// content.getString("attached_files"));
			intent.putExtra("content", content);
			parent.startChildActivity("my_thread", intent);
			content.putString("status", "read");
			((MyThreadAdapter) l.getAdapter()).notifyDataSetChanged();
			BoardMessageCacheManager manager = new BoardMessageCacheManager(
					Notifications.this, "my_threads");
			manager.open();
			manager.updateStatus("read", content.getString("id"));
			manager.close();
			tracker.trackEvent("Click", "ListItem", "Show Thread Details", 0);
			break;
		case Messages.THREAD_DRAFTS:
			content = (Bundle) ((DraftAdapter) l.getAdapter())
					.getItem(position);
			intent = new Intent(Notifications.this, NewThread.class);
			intent.putExtra("from_draft", true);
			intent.putExtra("id", content.getString("id"));
			intent.putExtra("subject", content.getString("subject"));
			intent.putExtra("contents", content.getString("contents"));
			intent.putExtra("tagged_keywords",
					content.getString("tagged_keywords"));
			intent.putExtra("attached_files",
					content.getString("attached_files"));
			intent.putExtra("tagged_users", content.getString("tagged_users"));
			parent.startChildActivity("thread_draft", intent);
			tracker.trackEvent("Click", "ListItem", "Open Thread Draft", 0);
			break;
		case Messages.INBOX:
			content = (Bundle) ((MyMessageAdapter) l.getAdapter())
					.getItem(position);
			intent = new Intent(Notifications.this, MessageDetails.class);
			// intent.putExtra("id", content.getString("id"));
			// intent.putExtra("subject", content.getString("subject"));
			// intent.putExtra("contents", content.getString("contents"));
			// intent.putExtra("timestamp",
			// Global.formatDate(content.getString("timestamp")));
			// intent.putExtra("user_id", content.getString("user_id"));
			// intent.putExtra("username", content.getString("username"));
			// intent.putExtra("profile_pic", content.getString("profile_pic"));
			// intent.putExtra("attached_files",
			// content.getString("attached_files"));
			intent.putExtra("content", content);
			parent.startChildActivity("inbox-message", intent);
			content.putString("status", "read");
			((MyMessageAdapter) l.getAdapter()).notifyDataSetChanged();
			BoardMessageCacheManager manager2 = new BoardMessageCacheManager(
					Notifications.this, "inbox");
			manager2.open();
			manager2.updateStatus("read", content.getString("id"));
			manager2.close();
			tracker.trackEvent("Click", "ListItem",
					"Show Inbox Message Details", 0);
			break;
		case Messages.SENT:
			content = (Bundle) ((MyMessageAdapter) l.getAdapter())
					.getItem(position);
			intent = new Intent(Notifications.this, MessageDetails.class);
			// intent.putExtra("id", content.getString("id"));
			// intent.putExtra("subject", content.getString("subject"));
			// intent.putExtra("contents", content.getString("contents"));
			// intent.putExtra("timestamp",
			// Global.formatDate(content.getString("timestamp")));
			// intent.putExtra("user_id", content.getString("user_id"));
			// intent.putExtra("username", content.getString("username"));
			// intent.putExtra("profile_pic", content.getString("profile_pic"));
			// intent.putExtra("attached_files",
			// content.getString("attached_files"));
			intent.putExtra("content", content);
			parent.startChildActivity("sentbox-message", intent);
			tracker.trackEvent("Click", "ListItem",
					"Show Sentbox Message Details", 0);
			break;
		case Messages.MESSAGE_DRAFTS:
			content = (Bundle) ((DraftAdapter) l.getAdapter())
					.getItem(position);
			intent = new Intent(Notifications.this, NewMessage.class);
			intent.putExtra("from_draft", true);
			intent.putExtra("id", content.getString("id"));
			intent.putExtra("subject", content.getString("subject"));
			intent.putExtra("contents", content.getString("contents"));
			intent.putExtra("tagged_keywords",
					content.getString("tagged_keywords"));
			intent.putExtra("attached_files",
					content.getString("attached_files"));
			intent.putExtra("tagged_users", content.getString("tagged_users"));
			intent.putExtra("tagged_user_names",
					content.getString("tagged_user_names"));
			parent.startChildActivity("message_draft", intent);
			tracker.trackEvent("Click", "ListItem", "Open Message Draft", 0);
			break;
		case Messages.PINGS:
			break;
		}
	}

	private void instantiateLayout() {
		lblTitle = (TextView) findViewById(R.id.lbl_messages_notification);
		txtSearch = (EditText) findViewById(R.id.txt_messages_search);
	}

	private void fetchData() {
		switch (command) {
		case Messages.MY_THREADS:
			ThreadNotificationManager threadManager = new ThreadNotificationManager(
					Notifications.this, (AmazingListView) getListView(),
					txtSearch);
			threadManager.fetchThreadFromCache();
			break;
		case Messages.THREAD_DRAFTS:
			DraftNotificationManager threadDraftManager = new DraftNotificationManager(
					Notifications.this, (AmazingListView) getListView(),
					txtSearch, "thread_draft");
			threadDraftManager.fetchData();
			break;
		case Messages.INBOX:
			MessageNotificationManager messageInboxManager = new MessageNotificationManager(
					Notifications.this, (AmazingListView) getListView(),
					txtSearch, "fetch_inbox");
			messageInboxManager.fetchThreadFromCache();
			break;
		case Messages.SENT:
			MessageNotificationManager messageSentManager = new MessageNotificationManager(
					Notifications.this, (AmazingListView) getListView(),
					txtSearch, "fetch_sentbox");
			messageSentManager.fetchThreadFromCache();
			break;
		case Messages.MESSAGE_DRAFTS:
			DraftNotificationManager messageDraftManager = new DraftNotificationManager(
					Notifications.this, (AmazingListView) getListView(),
					txtSearch, "message_draft");
			messageDraftManager.fetchData();
			break;
		case Messages.PINGS:
			PingNotificationManager pingManager = new PingNotificationManager(
					Notifications.this, (AmazingListView) getListView(),
					txtSearch);
			pingManager.fetchPingFromCache();
			break;
		}
	}

}
